package ru.ml.ui.filter;

import groovy.lang.Writable;
import groovy.text.GStringTemplateEngine;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ru.ml.ui.service.TemplateDataServiceImpl;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URL;

@Slf4j
@RequiredArgsConstructor
public class BootPageRequestFilter implements Filter {


    private static final String XX_ML_REQUEST_HEADER = "XX-ML-REQUEST";

    private final TemplateDataServiceImpl templateDataService;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        if (((HttpServletRequest) request).getHeader(XX_ML_REQUEST_HEADER) == null) {
            try {
                Writable out = new GStringTemplateEngine().createTemplate(new URL(templateDataService.getBootTemplate()))
                        .make(templateDataService.getBootTemplateData());
                response.setCharacterEncoding("UTF-8");
                response.setContentType("text/html");
                response.getWriter().print(out.writeTo(new StringWriter()));
                return;
            } catch (Exception e) {
                log.error("Can't generate default template [{}]", templateDataService.getBootTemplate());
                throw new RuntimeException(e);
            }
        }
        chain.doFilter(request, response);
    }
}
