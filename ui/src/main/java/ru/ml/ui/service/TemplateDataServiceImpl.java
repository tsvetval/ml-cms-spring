package ru.ml.ui.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class TemplateDataServiceImpl {
    @Value("${server.servlet.contextPath}")
    private String CONTEXT_PATH;

    @Value("${ml.page.title}:ML Framework Demo")
    private String ML_PAGE_TITLE;

    @Value("${ml.page.fav.icon}")
    private String ML_FAV_ICON;

    @Value("${ml.framework.version}")
    private String ML_FRAMEWORK_VERSOIN;

    @Value("${ml.ui.boot-template:classpath:templates/boot.hml}")
    private String BOOT_TEMPLATE;


    public String getBootTemplate() {
        return BOOT_TEMPLATE;
    }

    public Map<String, Object> getBootTemplateData() {
        Map<String, Object> result = new HashMap<>();
        result.put("CONTEXT_PATH", CONTEXT_PATH);
        result.put("PROJECT_PAGE_TITLE", ML_PAGE_TITLE);
        result.put("PROJECT_PAGE_FAV_ICON", ML_FAV_ICON);
        result.put("PROJECT_FRAMEWORK_VERSION", ML_FRAMEWORK_VERSOIN);
        return result;
    }

}
