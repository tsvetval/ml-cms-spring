package ru.ml.ui.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.ml.platform.core.model.MlFolder;

import java.util.Collections;
import java.util.List;

@Service
@AllArgsConstructor
public class MlUserServiceImpl {
    // todo implement
    public boolean hasAdminRole(){
        return true;
    }

    public List<MlFolder> getUserRootFolders(){
        return Collections.emptyList();
    }

    public List<MlFolder> checkAccessFolder(List<MlFolder> checkFolderList){
        return checkFolderList;
    }

}
