package ru.ml.ui.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.ml.platform.core.model.MlFolder;
import ru.ml.ui.repository.MlFolderRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@AllArgsConstructor
public class FolderServiceImpl {
    private final MlUserServiceImpl accessService;
    private final MlFolderRepository folderRepository;


    public List<MlFolder> getChildFolders(Long parentId) {
        List<MlFolder> result;
        if (parentId == null) {
            if (accessService.hasAdminRole()) {
                result = folderRepository.getChildFoldersForNullParent();
            } else {
                return accessService.getUserRootFolders();
            }
        } else {
            result = folderRepository.getChildFolders(parentId);
        }
        result = accessService.checkAccessFolder(result);
        return result;
    }


    public MlFolder getFolder(Long folderId){
        return folderRepository.findById(folderId);
    }


    public List<Long> findPath(Long folderId) {
        List<Long> result = new ArrayList<>();
        MlFolder mlFolder = folderRepository.findById(folderId);
        result.add(mlFolder.getId());
        while (mlFolder.getParent() != null) {
            mlFolder = mlFolder.getParent();
            result.add(mlFolder.getId());
        }
        Collections.reverse(result);
        return result;
    }
}
