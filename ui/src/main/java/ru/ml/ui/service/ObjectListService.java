package ru.ml.ui.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import ru.ml.core.filter.FilterBuilder;
import ru.ml.core.filter.result.FilterResult;
import ru.ml.platform.context.CommonDao;
import ru.ml.platform.context.holders.impl.UserContext;
import ru.ml.platform.core.model.MlClass;
import ru.ml.platform.core.model.MlDynamicEntityImpl;
import ru.ml.platform.core.model.MlFolder;
import ru.ml.platform.core.model.context.MlContext;
import ru.ml.platform.core.model.exceptions.MlServerException;
import ru.ml.platform.core.model.security.MlRole;
import ru.ml.ui.dto.objlist.GetObjectDataRequest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ObjectListService {
    private final MlContext mlContext = MlContext.getInstance();

    private final CommonDao commonDao;
    private final UserContext userContext;


    public FilterResult getObjectList(MlFolder folder, GetObjectDataRequest options) {
        FilterBuilder filterBuilder = new FilterBuilder(mlContext.getMetaDataHolder(), commonDao);
        //TODO classifierId
        if (folder.getChildClassCondition() != null && !folder.getChildClassCondition().isEmpty()) {
            filterBuilder.addAdditionalCondition(folder.getChildClassCondition());
        }

        // default folder sorting

        MlClass mlClass = folder.getChildClass();

        filterBuilder.setQueryMlClass(mlClass);
        fillFilter(filterBuilder, options);

        // Если не передана сортровка то сортируем сортировкой по умолчвнию
        if (CollectionUtils.isEmpty(filterBuilder.getOrderMap())) {
            if (!StringUtils.isEmpty(folder.getObjectOrder())) {
                // Парсим строку
                String[] orderItems = folder.getObjectOrder().split(",");
                Map<String, String> orderMap = Arrays.stream(orderItems).collect(Collectors.toMap(key -> key.trim().split("\\s+")[0],
                        key -> key.trim().split("\\s+").length == 2 ? key.trim().split("\\s+")[1] : ""));
                for (Map.Entry<String, String> entry : orderMap.entrySet()) {
                    filterBuilder.addOrder(entry.getKey(), entry.getValue());
                }
            } else {
                // Сортируем по убаванию первиччного ключа
                if (!CollectionUtils.isEmpty(mlContext.getMetaDataHolder().getPrimaryKey(mlClass))) {
                    filterBuilder.addOrder(mlContext.getMetaDataHolder().getPrimaryKey(mlClass).get(0).getEntityFieldName(), "desc");
                }
            }
        }

        FilterResult filterResult = filterBuilder.buildFilterResult();
        filterResult.getResultList();
        return filterResult;
    }


    public FilterResult getObjectList(MlClass mlClass, GetObjectDataRequest options) {
        FilterBuilder filterBuilder = new FilterBuilder(mlContext.getMetaDataHolder(), commonDao);
        filterBuilder.setQueryMlClass(mlClass);
        fillFilter(filterBuilder, options);

        FilterResult filterResult = filterBuilder.buildFilterResult();
        filterResult.getResultList();
        return filterResult;
    }


    private void fillFilter(FilterBuilder filterBuilder, GetObjectDataRequest options) {
        if (filterBuilder == null || filterBuilder.getQueryMlClass() == null) {
            throw new MlServerException("Wrong filter setup. Filter don't initialized or mandatory fields mlClass is empty ");
        }
        filterBuilder.addOrder(options.getOrderAttr(), options.getOrderType());


        List<MlRole> userRoles = userContext.getCurrentUser().getRoles();

        List<String> additionalQueries = mlContext.getSecurityHolder().getAdditionalQueryForClass(userRoles, filterBuilder.getQueryMlClass());
        filterBuilder.addAdditionalCondition(additionalQueries);
        filterBuilder.addAdditionalParameters(getCommonParameters());

        filterBuilder.setPageNumber(options.getCurrentPage());
        filterBuilder.setRowPerPage(options.getObjectsPerPage());

        if (!StringUtils.isEmpty(options.getUseFilter())) {
            if ("extended".equals(options.getUseFilter())) {
                filterBuilder.useExtendedSearch();
                throw new RuntimeException("Not implemented ");
//                String jsonSettings = params.getString("extendedFilterSettings", "[]");
//                Gson gson = new Gson();
//                JsonArray jsonArray = gson.fromJson(jsonSettings, JsonArray.class);
//                for (int i = 0; i < jsonArray.size(); i++) {
//                    JsonObject object = (JsonObject) jsonArray.get(i);
//                    String attrName = object.get("entityName").getAsString();
//                    while (object.has("children")) {
//                        object = (JsonObject) object.get("children").getAsJsonArray().get(0);
//                        attrName += "." + object.get("entityName").getAsString();
//                    }
//                    if (object.get("op") != null && !object.get("op").isJsonNull()) {
//                        filterBuilder.addExtendedParameter(attrName, object.get("op").getAsString(), object.get("value").getAsString());
//                    }
//                }
            }

            if (!StringUtils.isEmpty(options.getSimpleFilter())) {
                filterBuilder.useSimpleSearch().setSimpleSearchValue(options.getSimpleFilter());
                if (!StringUtils.isEmpty(options.getSearchField())) {
                    filterBuilder.setSimpleSearchAttr(options.getSearchField());
                }
            }

        }
    }


    public Map<String, Object> getCommonParameters() {
        //TODO
        Map<String, Object> result = new HashMap<>();
//        result.put("userId",request.getRequest().getSession().getAttribute("user-id"));
//        result.put("user",request.getRequest().getSession().getAttribute("user"));
//        result.put("currentUser",request.getRequest().getSession().getAttribute("user-id"));
        return result;
    }

}
