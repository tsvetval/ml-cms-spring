package ru.ml.ui.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.ml.ui.service.TemplateDataServiceImpl;


@RestController
@RequestMapping("/site/console")
@Slf4j
public class ConsoleController extends AbstractMlController {
    public ConsoleController(TemplateDataServiceImpl templateDataService) {
        super(templateDataService);
    }


}
