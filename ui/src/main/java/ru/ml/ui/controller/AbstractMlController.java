package ru.ml.ui.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import ru.ml.ui.service.TemplateDataServiceImpl;

import java.util.Map;

@Slf4j
@RequiredArgsConstructor
public class AbstractMlController {

    final TemplateDataServiceImpl templateDataService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Object> getTemplateData() {
        return templateDataService.getBootTemplateData();
    }
}
