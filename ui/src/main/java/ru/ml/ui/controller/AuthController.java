package ru.ml.ui.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.ml.platform.context.holders.impl.UserContext;
import ru.ml.platform.core.model.security.MlUser;
import ru.ml.ui.dto.login.UserDataResponse;
import ru.ml.ui.service.TemplateDataServiceImpl;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/site/auth")
@Slf4j
public class AuthController extends AbstractMlController {

    private final UserContext userContext;

    @Autowired
    public AuthController(TemplateDataServiceImpl templateDataService, UserContext userContext) {
        super(templateDataService);
        this.userContext = userContext;
    }

    @GetMapping(value = "/user-data", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDataResponse> getUserData() {
        MlUser user = userContext.getCurrentUser();
        return ResponseEntity.status(OK).body(UserDataResponse.builder()
                .login(user.getLogin())
                .loggedIn(!user.isAnonymous())
                .helpUrl(user.get("helpUrl"))
                .build()
        );
    }


    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDataResponse> authorize() {
        //TODO
        MlUser user = userContext.getCurrentUser();
        return ResponseEntity.status(OK).body(UserDataResponse.builder()
                .login(user.getLogin())
                .loggedIn(!user.isAnonymous())
                .helpUrl(user.get("helpUrl"))
                .build()
        );
    }


    @PostMapping(value = "/logout", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDataResponse> logout() {
        //TODO
        MlUser user = userContext.getCurrentUser();
        return ResponseEntity.status(OK).body(UserDataResponse.builder()
                .login(user.getLogin())
                .loggedIn(!user.isAnonymous())
                .helpUrl(user.get("helpUrl"))
                .build()
        );
    }

/*    @GetMapping(value = "/getPageBlocks", produces = "text/html;charset=UTF-8")
    public String getPageBlocks() {
        try {
            URL url = new URL("classpath:templates/layouts/center.hml");//Resources.getResource("templates/layouts/center.hml");
            Writable out = new GStringTemplateEngine().createTemplate(url).make(templateDataService.getBootTemplateData());
            return out.writeTo(new StringWriter()).toString();
        } catch (Exception e) {
            throw new RuntimeException("");
        }
    }*/

}
