package ru.ml.ui.controller;


import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import ru.ml.core.filter.result.FilterResult;
import ru.ml.platform.core.model.MlFolder;
import ru.ml.platform.core.model.exceptions.MlApplicationException;
import ru.ml.platform.core.model.security.MlUser;
import ru.ml.ui.dto.login.UserDataResponse;
import ru.ml.ui.dto.objlist.GetObjectDataRequest;
import ru.ml.ui.dto.objlist.ObjectListResponse;
import ru.ml.ui.service.FolderServiceImpl;
import ru.ml.ui.service.ObjectListService;

import java.util.Objects;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/site/objlist")
@Slf4j
@AllArgsConstructor
public class ObjectListController {

    private final FolderServiceImpl folderService;
    private final ObjectListService objectListService;

    /**
     * Метод для получения списка объектов для заданной папки, корневой папки, списка объектов заданного класса
     *
     * @param folderId  -   параметры запроса
     *                   если параметры запроса содержат folderId (long) - id папки, то возвращается список объектов,
     *                   содержащихся в папке
     *                   иначе, если параметры запроса содержат className (string) - имя класса, то возвращается список
     *                   объектов данного класса
     *                   иначе возвращается список объектов (папок) корневой папки
     * @param objectDataRequest ssds
     */
    @PostMapping(value = "/{folderId}/get-folder-data", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ObjectListResponse> getFolderData(@PathVariable String folderId, @RequestBody GetObjectDataRequest objectDataRequest) {
        if (!NumberUtils.isCreatable(folderId)){
            throw new MlApplicationException("Wrong folderId format");
        }
        MlFolder folder = folderService.getFolder(Long.parseLong(folderId));
        if (folder.getChildClass() != null) {
            FilterResult filterResult = objectListService.getObjectList(folder, objectDataRequest);
        }



//        return ResponseEntity.status(OK).body(UserDataResponse.builder()
//                .login(user.getLogin())
//                .loggedIn(!user.isAnonymous())
//                .helpUrl(user.get("helpUrl"))
//                .build()
//        );
        return null;
    }

    @PostMapping(value = "/{mlclass}/get-class-data2", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ObjectListResponse> getFolderData2(@PathVariable String mlclass, @RequestBody GetObjectDataRequest getObjectDataRequest) {
/*

        if (getObjectDataRequest.getFolderId() != null){
            //просмотр папки
            folderService.

        }


        return ResponseEntity.status(OK).body(UserDataResponse.builder()
                .login(user.getLogin())
                .loggedIn(!user.isAnonymous())
                .helpUrl(user.get("helpUrl"))
                .build()
        );
*/
        return null;
    }
}
