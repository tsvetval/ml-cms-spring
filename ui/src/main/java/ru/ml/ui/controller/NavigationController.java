package ru.ml.ui.controller;

import com.google.common.collect.ImmutableList;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.ml.platform.core.model.MlFolder;
import ru.ml.ui.dto.navigation.MlFolderResponse;
import ru.ml.ui.service.FolderServiceImpl;
import ru.ml.ui.service.MlUserServiceImpl;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/site/navigation")
@Slf4j
@AllArgsConstructor
public class NavigationController {

    private final MlUserServiceImpl accessService;
    private final FolderServiceImpl folderService;

    /**
     * this is controller for console tree
     * it accepts folderId and returns all it children
     *
     * @param folderId parent folderID (for root folder use keyword "root")
     * @return List of child folders
     */
    @GetMapping(value = "/{folderId}/children", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<MlFolderResponse>> getFolderChildes(@PathVariable String folderId) {
        List<MlFolder> folderList = folderService.getChildFolders("root".equals(folderId) ? null : Long.parseLong(folderId));
        List<MlFolderResponse> result = folderList.stream()
                .map(folder -> MlFolderResponse.builder()
                        .id(folder.getId().toString())
                        .text(folder.getTitle())
                        .children(hasChildren(folder))
                        .icon(folder.getIcon())
                        .url(folder.getUrl())
                        .parent(folder.getParent() == null ? null : folder.getParent().getId().toString())
                        .build())
                .collect(Collectors.toList());
        return ResponseEntity.status(OK).body(result);
    }

    @GetMapping(value = "/{folderId}/findFolderPath", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Long>> findFolderPath(@PathVariable Long folderId) {
        return ResponseEntity.status(OK).body(folderService.findPath(folderId));
    }

    private boolean hasChildren(MlFolder folder) {
        return (folder.getChildFolders() != null &&
                !accessService.checkAccessFolder(folder.getChildFolders()).isEmpty()) ||
                (
                        folder.isClassifier() && !accessService.checkAccessFolder(ImmutableList.of(folder)).isEmpty()
                );
    }

}
