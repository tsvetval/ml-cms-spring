package ru.ml.ui.dto.login;

import io.swagger.annotations.ApiModel;
import lombok.Builder;

@Builder
@ApiModel(description = "Response object for getPageBlocks request")
public class UserDataResponse {
    private String login;
    private boolean loggedIn;
    private String helpUrl;

}
