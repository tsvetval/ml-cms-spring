package ru.ml.ui.dto.objlist;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import lombok.*;
import ru.ml.ui.dto.OrderType;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Response object for navigation tree")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetObjectDataRequest {
    private Long  currentPage;
    private Long objectsPerPage;
    private String orderAttr;
    private String orderType;
    private String useFilter;
    private String simpleFilter;
    private String searchField;


    public Long getCurrentPage() {
        return currentPage != null ? currentPage : 1;
    }

    public Long getObjectsPerPage() {
        return objectsPerPage != null ? currentPage : 10;
    }
}
