package ru.ml.ui.dto.objlist;


import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "Response object for Object List")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ObjectListResponse {


}
