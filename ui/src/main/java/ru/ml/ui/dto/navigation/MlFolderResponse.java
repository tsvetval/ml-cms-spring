package ru.ml.ui.dto.navigation;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Response object for navigation tree")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MlFolderResponse {
    private String id;
    private String text;
    private String parent;
    private boolean children;
    private String icon;
    private String url;
}
