package ru.ml.ui;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.*;
import ru.ml.ui.filter.BootPageRequestFilter;
import ru.ml.ui.service.TemplateDataServiceImpl;

@Configuration
@ComponentScan
@PropertySource("classpath:ml-ui.properties")
public class UIConfiguration {

    @Bean
    public FilterRegistrationBean<BootPageRequestFilter> bootPageFilter(TemplateDataServiceImpl templateDataService) {
        FilterRegistrationBean<BootPageRequestFilter> registrationBean
                = new FilterRegistrationBean<>();

        registrationBean.setFilter(new BootPageRequestFilter(templateDataService));
        registrationBean.addUrlPatterns("/site/*");
        registrationBean.setOrder(10);
        return registrationBean;
    }

}
