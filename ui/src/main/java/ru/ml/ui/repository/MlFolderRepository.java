package ru.ml.ui.repository;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.ml.platform.context.CommonDao;
import ru.ml.platform.core.model.MlClass;
import ru.ml.platform.core.model.MlFolder;
import ru.ml.ui.service.MlUserServiceImpl;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
@AllArgsConstructor
@Slf4j
public class MlFolderRepository {
    private final CommonDao<MlFolder> commonRepository;
    private final MlUserServiceImpl accessService;
    private final EntityManager entityManager;


    public MlClass getChildClassByFolderId(long instanceId) {
        MlFolder folder = commonRepository.findById(instanceId, MlFolder.class);
        return folder.getChildClass();
    }


    public List<MlFolder> getChildFolders(Long parent) {
        String que = "select o from MlFolder o where o.parent.id = :parent order by o.order";
        TypedQuery<MlFolder> typedQuery = commonRepository.getTypedQueryWithoutSecurityCheck(que, MlFolder.class);
        typedQuery.setParameter("parent", parent);
        return typedQuery.getResultList();
    }

    public List<MlFolder> getChildFoldersForNullParent() {
        String que = "select o from MlFolder o where o.parent is null order by o.order";
        TypedQuery<MlFolder> typedQuery = commonRepository.getTypedQueryWithoutSecurityCheck(que, MlFolder.class);
        return typedQuery.getResultList();
    }


    public Long getChildFoldersCount(Long parentId) {
        return getChildFoldersCount(findById(parentId));
    }

    public Long getChildFoldersCount(MlFolder parent) {
        String que = "select count(o) from MlFolder o where o.parent = :parent";
        TypedQuery<Long> typedQuery = entityManager.createQuery(que, Long.class);
        typedQuery.setParameter("parent", parent);
        return typedQuery.getResultList().get(0);
    }



    public MlFolder findById(Object id) {
        return commonRepository.findById(id, MlFolder.class);
    }


}
