<!DOCTYPE html>
<script>
    $( document ).ready(function() {
        $('.ml-header__list').click(function(e){
            /* Подавляем переход по ссылке */
            e.preventDefault();
            /* Меняем состояние для кнопки и блока навигации */
            $(this).toggleClass('m-header__list_active')
            $('#LEFT').toggle()
        })

    });
</script>

<div class="l-header" >
    <div class="ml-header">
        <a  class="ml-header__title"><%=PROJECT_PAGE_TITLE%></a>
        <a href="#" class="ml-header__list m-header__list_active" title="Показать/Скрыть дерево папок"></a>
*{
        <a href="#" class="ml-header__favorites"></a>
}*

        <div class="ml-header__user" id="TOP">
        </div>
    </div>
    <!-- .ml-header -->
</div><!-- .l-header -->
<div class="l-content">
    <div id="LEFT" class="ml-list">
    </div>
    <div class="ml-main">
        <div class="ml-main__header m-main__header_breadcrumbs">
            <div class="ml-main__header-bg"></div>
            <div class="ml-main__header-breadcrumbs-wrap breadCrumbHolder module">
                <div id="navigation-container" class="ml-main__header-breadcrumbs breadCrumb module">

                </div>
            </div>
        </div><!-- .ml-main__header -->
        <div class="ml-main__content" >
            <div class="ml-main__content-actions" id="UTILS">
            </div>

            <div class="ml-main__content-search-page" id="SEARCH">
            </div>

            <div class="ml-main__content-folders" id="CENTER">
            </div>
            <div class="ml-main__content-logo">Version <%=PROJECT_FRAMEWORK_VERSION%></div>
        </div><!-- .ml-main__content -->
    </div><!-- .ml-main -->
</div><!-- .l-content -->


</div>
