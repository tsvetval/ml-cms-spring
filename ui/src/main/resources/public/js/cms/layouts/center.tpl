<div class="l-header">
    <div class="ml-header">
        <a  class="ml-header__title">${PROJECT_PAGE_TITLE}</a>

        <div class="ml-header__user" id="TOP">
        </div>
    </div>
    <!-- .ml-header -->
</div><!-- .l-header -->
<div class="l-content">
    <div class="ml-main">
        <div class="ml-main__header m-main__header_breadcrumbs">
            <div class="ml-main__header-bg"></div>
            <div class="ml-main__header-breadcrumbs-wrap breadCrumbHolder module">
                <div id="navigation-container" class="ml-main__header-breadcrumbs breadCrumb module">

                </div>
            </div>
        </div><!-- .ml-main__header -->
        <div class="ml-main__content" >
            <div class="ml-main__content-actions" id="UTILS">
            </div>

            <div class="ml-main__content-search-page" id="SEARCH">
            </div>

            <div class="ml-main__content-object" id="CENTER">

            </div>
            <div class="ml-main__content-logo">Version ${PROJECT_FRAMEWORK_VERSION}</div>
        </div><!-- .ml-main__content -->
    </div><!-- .ml-main -->
</div><!-- .l-content -->


</div>
