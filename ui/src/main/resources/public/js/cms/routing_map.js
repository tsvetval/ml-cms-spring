/**
 *  Мап роутов на страницы
 * */
define(
    [],
    function () {
        return {

            "/site/auth": {
                "page": {
                    "id": "authPage",
                    "title": "MoonLight",
                    "description": "Authorization page",
                    "template": "/public/js/cms/layouts/security-layout.tpl"
                },
                "pageBlocks": [
                    {
                        "id": "authPageBlock",
                        "bootJs": "cms/page_blocks/security_block/securityBlockBoot",
                        "zone": "CENTER",
                        "description": "Блок авторизации",
                        "order": 3
                    }
                ]
            },



            "/site/console": {
                "page": {
                    "id": "consolePage",
                    "description": "Console page with folder tree",
                    "template": "/public/js/cms/layouts/top-left-center-footer.tpl"
                },
                "pageBlocks": [{
                    "id": "navTreePageBlock",
                    "bootJs": "cms/page_blocks/navigation_block/navigationBlockBoot",
                    "zone": "LEFT",
                    "description": "Дерево навигации",
                    "order": 1
                }, {
                    "id": "utilPageBlock",
                    "bootJs": "cms/page_blocks/util_block/utilBlockBoot",
                    "zone": "UTILS",
                    "description": "Блок утилит",
                    "order": 1
                }, {
                    "id": "authPageBlock",
                    "bootJs": "cms/page_blocks/user_block/UserBlockBoot",
                    "zone": "TOP",
                    "description": "Блок авторизации",
                    "order": 1
                }, {
                    "id": "objectListBlock",
                    "bootJs": "cms/page_blocks/list_block/object_list_block/objectListBlockBoot",
                    "zone": "CENTER",
                    "description": "Блок списка объектов",
                    "order": 1
                }]
            }

        };
    });
