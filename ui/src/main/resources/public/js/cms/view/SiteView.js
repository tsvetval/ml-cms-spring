define([

    // Libs
    'backbone',
    'misc',

    // Deps
    'cms/events/events',
    './PageView',
    '../model/SiteModel',
    'log',
    'breadcrumbs',
    'helpers/ViewHelpers'

], function(Backbone, misc, cmsEvents, PageView, SiteModel, log, Breadcrumbs, helpers) {

    var View = Backbone.View.extend({
        el: '#ml-cms-page-container',

        initialize: function(options) {
            //this.breadcrumbs = misc.option(options, "breadcrumbs", "");


            log.debug('Initialize SiteView');
            this.listenTo(this.model, cmsEvents.RENDER_NEW_PAGE, this.renderNewPage);
            this.listenTo(this.model, cmsEvents.SHOW_PAGE, this.showPage);
            this.listenTo(this.model, cmsEvents.SITE_NOTIFY, this.helpers.notify.create);

            // TODO Инициализируем хлебные крошки если нужно
            this.breadcrumbs =   new Breadcrumbs.view({model : this.model});
            //$("#navigation-container").append(this.breadcrumbs.render().$el);

            /* Ресайз заголовка при изменении ширины экрана */
            $(window).on('resize', this.helpers.resizeTitle)
        },

        showPage : function(page){
            log.debug('SiteView showPage');
            this.$el.find("> *").detach();
            this.$el.append(page.get('pageView').$el);

            /* Ресайз заголовка при открытии страницы */
            this.helpers.resizeTitle()

            var nav = $("#navigation-container");
            if (nav){
                nav.append(this.breadcrumbs.render().$el);
                this.breadcrumbs.compressBreadCrumb();
            }

        },

        renderNewPage : function(page){
            log.debug('SiteView start renderNewPage');
            var _this = this;
            var pageView = new PageView({model : page});
            page.set('pageView', pageView);

            pageView.renderPage(function(pageView){
                _this.$el.find("> *").detach();
                _this.$el.append(pageView.$el);
                var nav = $("#navigation-container");
                if (nav && nav.length>0){
                    nav.append(_this.breadcrumbs.render().$el);
                    _this.breadcrumbs.compressBreadCrumb();

                }
            });
            page.set('alreadyRendered', true);
        },


        /**
         * Обработчик событий от страниц сайта (и контент блоков на них)
         * @param eventObject
         */
        onPageEvent : function(eventName, eventObject){
           log.debug("Handle event from page");
        }

    });

    /* Расширяем хелперы при помощи libs/mixin/helpers/ViewHelpers.js */
    _.extend(View.prototype, {helpers: helpers})

    return View;

});