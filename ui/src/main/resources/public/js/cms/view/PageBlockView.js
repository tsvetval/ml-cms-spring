define([
    // Libs
    'backbone',
    'libs/mixin/helpers/ViewHelpers'
    // Deps

], function(Backbone, helpers, ItemView) {
    var PageBlockView = Backbone.View.extend({
        render: function() {
        }

    });

    /* Расширяем хелперы при помощи libs/mixin/helpers/ViewHelpers.js */
    _.extend(PageBlockView.prototype, {helpers: helpers})

    return PageBlockView;

});