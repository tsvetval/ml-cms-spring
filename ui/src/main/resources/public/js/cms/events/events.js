/**
 *  Константы событий
 * */
define(
    [],
    function () {
        return {

            /**
             * События сайта
             */
            SITE_NOTIFY: 'site:notify',

            /**
             * События view
             */
            RENDER_VIEW : "render",
            HIDE_VIEW : "hide_view",

            /**
             * События страниц
             */
            RESTORE_PAGE : "restorePage",
            REFRESH_PAGE : "refreshPage",
            OPEN_PAGE: "OPEN_PAGE",
            CLOSE_PAGE: "CLOSE_PAGE",
            REPLACE_PAGE: "REPLACE_PAGE",
            CHANGE_PAGE_HASH: "CHANGE_PAGE_HASH",
            CHANGE_PAGE_PARAMS: "CHANGE_PAGE_PARAMS",
            ACTIVATE_PAGE: "ACTIVATE_PAGE",
            REMOVE_PAGE: "REMOVE_PAGE",
            SHOW_PAGE: "showPage",
            RENDER_NEW_PAGE: "renderNewPage",

            /**
             * События поиска
             */
            SEARCH : "search",
            RESET_SEARCH: "resetSearch",

            /**
             * События папки
             */
            OPEN_FOLDER : "openFolder",
            OPEN_SUB_FOLDER : "openSubFolder",
            ACTIVATE_FOLDER : "activateFolder",
            FOLDER_DELETED: "folderDeleted",
            FOLDER_CREATED: "folderCreated",

            /**
             * События навигации
             */
            NAVIGATION: "NAVIGATION",
            PUSH_STATE: "PUSH_STATE",
            REPLACE_STATE: "REPLACE_STATE",

            /**
             * События блоков
             */
            RENDER_PAGE_BLOCK: "renderPageBlock",
            NOTIFY_PAGE_BLOCKS: "NOTIFY_PAGE_BLOCKS",
            SHOW_PAGE_BLOCK: "showPageBlock",
            SHOW_ACTIVE_BLOCK: "showActiveBlocks",
            BLOCKS_RENDER_COMPLETE: "blocks:render:complete",
            BLOCK_DEACTIVATED: "block:deactivated",
            BLOCK_DEACTIVATE: "block:deactivate",

            /**
             * todo: событие используется?
             */
            FIND_MODEL: "FIND_MODEL",

            /**
             * События списков
             */
            GET_SELECTED_IDS : "ids:selected:get",
            UNSET_SELECTED_IDS: "ids:selected:unset",
            OBJECTS_SELECTION_DONE: "objectsSelectionDone",
            LINK_OBJECT_CREATED: "linkObjectCreated",

            /**
             * Визуальные события
             */
            FOCUS: "focus",
            HIGHLIGHT_MANDATORY: "HighlightMandatory",
            REMOVE_HIGHLIGHT_MANDATORY: "RemoveHighlightMandatory",

            /**
             * События моделей
             */
            MODEL_SAVE_COMPLETE: "model:save:complete",
            MODEL_DELETE_COMPLETE: "model:delete:complete"

        };
    });
