define(['log', 'misc', 'backbone', 'cms/events/events', 'cms/events/NotifyPageBlocksEventObject', 'cms/events/ChangePageHashEvent', 'cms/page_blocks/DialogPageBlock'],
    function (log, misc, backbone, cmsEvents, NotifyEventObject, ChangePageHashEvent, Message) {

        var PageBlockModel = backbone.Model.extend({
            defaults: {
                id: undefined,
                zone: undefined,
                name: "Empty pageBlock",
                url: undefined,
                template: undefined,
                pageModel: undefined,
                active: undefined,
                blockRendered: false
            },
            initialize: function () {
                console.log("initialize PageBlockModel");
                this.on('change:active', function () {
                    if (this.get('active')) {
                        this.bindEvents()
                    } else {
                        this.unbindEvents();

                        /* Call deactivate method */
                        this.onDeactivate()
                    }
                }, this)
            },

            bindEvents: function () {
                var _this = this;
                this.get('pageModel').listenTo(this, cmsEvents.NOTIFY_PAGE_BLOCKS, this.get('pageModel').onNotifyPageBlocks);
                this.get('pageModel').listenTo(this, cmsEvents.NAVIGATION, this.get('pageModel').onNavigation);
                this.get('pageModel').listenTo(this, cmsEvents.OPEN_PAGE, this.get('pageModel').openPage);
                this.get('pageModel').listenTo(this, cmsEvents.CLOSE_PAGE, this.get('pageModel').closePage);
                this.get('pageModel').listenTo(this, cmsEvents.REPLACE_PAGE, this.get('pageModel').replacePage);
                this.get('pageModel').listenTo(this, cmsEvents.CHANGE_PAGE_HASH,
                    function (event) {
                        _this.get('pageModel').modifyPageHash(event.hashParams, event.modifyHistoryMode, event.extendHashMode)
                    });
                this.get('pageModel').listenTo(this, cmsEvents.CHANGE_PAGE_PARAMS, this.get('pageModel').modifyPageParams);
                this.get('pageModel').listenTo(this, cmsEvents.SHOW_PAGE_BLOCK, this.get('pageModel').showPageBlocks);

                /* Handle deactivate event */
                this.on(cmsEvents.BLOCK_DEACTIVATE, this.deactivate)

            },

            unbindEvents: function () {
                this.get('pageModel').stopListening(this, cmsEvents.NOTIFY_PAGE_BLOCKS);
                this.get('pageModel').stopListening(this, cmsEvents.NAVIGATION);
                this.get('pageModel').stopListening(this, cmsEvents.OPEN_PAGE);
                this.get('pageModel').stopListening(this, cmsEvents.CLOSE_PAGE);
                this.get('pageModel').stopListening(this, cmsEvents.REPLACE_PAGE);
                this.get('pageModel').stopListening(this, cmsEvents.CHANGE_PAGE_HASH);
                this.get('pageModel').stopListening(this, cmsEvents.CHANGE_PAGE_PARAMS);
                this.get('pageModel').stopListening(this, cmsEvents.SHOW_PAGE_BLOCK);
                this.off(cmsEvents.BLOCK_DEACTIVATE)
            },

            /**
             * Wrapper for activate page block
             */
            activate: function () {
                this.set('active', true)
            },

            /**
             * Wrapper for deactivate page block
             */
            deactivate: function () {
                this.set('active', false)
            },

            /**
             * Run when page block is deactivated, after unbind events
             * @see https://bitbucket.org/tsval/cms/wiki/PageBlockModel#markdown-header-ondeactivate
             */
            onDeactivate: function () {
                /* Extend this method in your page block */

                /* This event just a helper for external listeners */
                this.trigger(cmsEvents.BLOCK_DEACTIVATED)
            },

            notifyPageBlocks: function (data) {
                this.trigger(cmsEvents.NOTIFY_PAGE_BLOCKS, data)
            },

            navigationEvent: function (options) {
                this.trigger(cmsEvents.NAVIGATION, options);
            },

            openPage: function (url, title, params) {
                this.trigger(cmsEvents.OPEN_PAGE, {
                    title: title,
                    url: url,
                    params: params
                });
            },

            closePage: function () {
                this.trigger(cmsEvents.CLOSE_PAGE, {});
            },

            replacePage: function (url, title, params) {
                this.trigger(cmsEvents.REPLACE_PAGE, {
                    title: title,
                    url: url,
                    params: params
                });
            },

            /**
             *
             * @param hashParams передаваемый объект параметров для формирования хеша
             * @param modifyHistoryMode (по дефолту null)
             *          null - не трогать window.history,
             *          pushState - делать window.history.pushState
             *          replaceState - делать window.history.replaceState
             * @param extendHashMode расширять ли текущий хеш (по дефолту extendHash)
             *          'extendHash' - то есть если в хеше были параметры то хеш будет расширен новыми
             *          'replaceHash'- хеш будет сформирован на основании новых параметров
             */
            changePageHash: function (hashParams, modifyHistoryMode, extendHashMode) {
                this.trigger(cmsEvents.CHANGE_PAGE_HASH, new ChangePageHashEvent(hashParams, modifyHistoryMode, extendHashMode));
            },
            //TODO может лучше ограничить только сменой pageTitle
            changePageParams: function (params) {
                this.trigger(cmsEvents.CHANGE_PAGE_PARAMS, params);
            },


            updatePageTitle: function (title) {
                this.set("pageTitle", title);
                this.changePageParams({
                    pageTitle: title
                });
            },

            getBlockInfo: function () {
                return this.get("blockInfo");
            },

            getPageModel: function () {
                return this.get("pageModel");
            },

            getSiteResource: function (resource, type) {
                return this.get('pageModel').get('siteModel').getResource(resource, type)
            },

            /**
             * Вызов серверного экшена для данного блока. См. статический метод callPageBlockAction
             * @param method method
             * @param url destination url
             * @param data payload data
             * @param callback колбек для вызова в случае успешного вызова
             */

            callServerAction: function (method, url, data, callback) {
                return PageBlockModel.callServerAction(this, method, url, data, callback);
            },

            /**
             * Get object for modal window
             * @param header
             * @param message
             * @param stacktrace
             * @returns {*}
             */
            getMessageObject: function (header, message, stacktrace) {
                return new Message({
                    title: header,
                    message: message,
                    stacktrace: stacktrace || undefined,
                    type: stacktrace ? 'stacktraceMessage' : 'errorMessage'
                });
            },

            displayErrorMessage: function (header, message) {
                var errorMessage = this.getMessageObject(header, message);
                errorMessage.show();
            },

            displayExtendedErrorMessage: function (header, message, stacktrace) {
                var errorMessage = this.getMessageObject(header, message, stacktrace);
                errorMessage.show();
            }

        });

        /*
         Статические методы-хелперы
         */

        /**
         * Вызов серверного метода страничного блока. После получения ответа сервера вызывается колбек, куда передаются
         * полученые данные.
         *
         * @param {Object} pageblock  current pageBlock
         * @param {Object} url API url
         * @param {Object} data payload
         * @param {Object} options мапа опций, обязательные поля - см. код
         * в опциях можно задавать обработчики ошибок:
         * onFail           -   универскальный обработчик, будет вызван если не заданы специфичные
         * onServerFail     -   обработчик ошибки сервера
         * onApplicationFail-   обработчик ошибки приложения
         * onSecurityFail   -   обработчик ошибки безопасности
         * onConnectionFail -   обработчик ошибки соединения
         * по умолчанию будт отображено окно с ошибкой
         *
         * @param {Function} callback колбэк, вызываемый в случае успешного выполнения экшена,
         *                 должен на вход получать данные возвращаемые сервером в json-формате
         *
         */
        PageBlockModel.callServerAction = function (pageblock, method, url, data, callback, options) {
            var _this = this;
            var def = new $.Deferred();

            $.ajax(misc.getContextPath() + url, {
                type: method,
                beforeSend: function(xhr){xhr.setRequestHeader('XX-ML-REQUEST', 'true');},
                timeout: options ? options.timeout : undefined,
                data: data,
                lockElement: options ? (options.lockElement || false) : undefined,
                /* Functions running after success or fail callbacks. Array for adding another functions from other places */
                complete: [function () {
                    if (this.lockElement) {
                        this.lockElement.trigger('unlock.requestloader')
                    }
                }],
                success: function (responseData) {
                    /* Exec callback function */
                    if (callback) callback.apply(pageblock, [responseData, data]);

                    /* Resolve deferred object */
                    def.resolve(responseData);
                },
                error: function (result, textStatus, errorThrown) {
                    /* Reject deferred object */
                    def.reject(result);

                    if (result.readyState != 0) { /* Connection OK */
                        if (result.status == 400) { /* Application errors */
                            var errorCallback = options.onApplicationFail || options.onFail || false;
                            if (errorCallback && result.responseJSON) {
                                errorCallback.call(this, result.responseJSON, result.status)
                            } else {
                                pageblock.displayErrorMessage("Ошибка приложения", result.responseJSON.message);
                            }
                        } else if (result.status == 401) { /* Authorization errors */
                            if (result.responseJSON.url) {
                                window.location = misc.getContextPath() + result.responseJSON.url;
                            }
                        } else if (result.status == 403) { /* Security errors */
                            if (result.responseJSON.needAuth) {
                                window.location = result.responseJSON.url;
                            } else {
                                 errorCallback = options.onSecurityFail || options.onFail || false;
                                if (errorCallback && result.responseJSON) {
                                    errorCallback.call(this, result.responseJSON, result.status)
                                } else {
                                    pageblock.displayErrorMessage("Ошибка доступа (#" + pageblock.get('blockInfo').get('id') + ")", result.responseJSON.message);
                                }
                            }
                        }  else if (result.status == 555) { /* Server  error*/
                            errorCallback = options.onServerFail || options.onFail || false;
                            if (errorCallback && result.responseJSON) {
                                errorCallback.call(this, result.responseJSON, result.status)
                            } else {
                                if (result.responseJSON.stacktrace) {
                                    console.log("Stack Trace: " + result.responseJSON.stacktrace);
                                } else {
                                    result.responseJSON.stacktrace = "Дополнительные сведения отсутствуют."
                                }
                                pageblock.displayExtendedErrorMessage("Ошибка сервера", result.responseJSON.message, result.responseJSON.stacktrace);
                            }
                        } else {
                            /* All other errors: 500, 502 e.t.c */
                            pageblock.displayErrorMessage('Ошибка запроса в блоке #' + _this.getBlockInfo().get("id"), "Статус ошибки: " + result.status)
                        }
                    } else { /* Connection error */
                        errorCallback = options.onConnectionFail || options.onFail || false;
                        if (errorCallback) {
                            errorCallback.call(this, result, result.status, errorThrown)
                        } else {
                            pageblock.displayErrorMessage('Ошибка подключения в блоке #' + _this.getBlockInfo().get("id"), 'Проверьте интернет соединение.')
                        }
                    }


                    /* Console log */
                    log.error('Error calling Page Block[id=' + _this.getBlockInfo().get("id") +
                        "] action " + action + "." +
                        "\nRequest data:\n" + JSON.stringify(data) +
                        "\nResponse data: " + JSON.stringify(result) +
                        "\nStatus: " + textStatus +
                        "\nError: " + errorThrown);

                }
            });
            return def.promise();
        };

        return PageBlockModel;
    })
;
