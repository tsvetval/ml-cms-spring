define(
    [
        'log',
        'misc',
        'backbone',
        'underscore',
        'cms/events/events',
        'channel',
        '../collections/PageCollection',
        '../model/PageModel',
        'cms/events/NotifyPageBlocksEventObject'
    ],
    function (log, misc, backbone, _, cmsEvents, channel, PageCollection, PageModel, NotifyEventObject) {
        var SiteModel = backbone.Model.extend({
            defaults:   {
                currentUrl:     undefined,
                activePage:     undefined,
                pageCollection: undefined,
                breadcrumbs:    undefined
            },
            initialize: function () {
                console.log("Initialize SiteModel");
                this.set("pageCollection", new PageCollection());
            },

            fetchMeta: function () {
                var _this = this;
                var $def = new $.Deferred();
                //TODO refactor
                var pages = {
                    "controllers": {
                        "jpqlController": "/controller/jpql",
                        "csfvfvvd": "/test: GET",
                        "objectListController": "/controller/list",
                        "searchObjectsController": "/controller/search",
                        "securitySettingsController": "/controller/security",
                        "utilReinitSettingsController": "/util/reinitSettings RS",
                        "editObjectController": "/controller/object/edit",
                        "authController": "/controller/auth",
                        "utilRestartSecurityController": "/util/restartSecurity RS",
                        "utilLaunchJobController": "/util/launchJob RS",
                        "utilTypicalAttrController": "/util/createTypicalAttribute RS",
                        "reportExecController": "/controller/report",
                        "documentationController": "/controller/documentation",
                        "staticController": "/controller/content",
                        "csdcfds": "/test: POST",
                        "addReplicationController": "/controller/replication/add",
                        "metaController": "/controller/meta",
                        "utilClearCacheController": "/util/clearCache RS",
                        "changePasswordController": "/controller/changePassword",
                        "selectReportParamController": "/controller/report/params",
                        "utilController": "/controller/util",
                        "createObjectController": "/controller/object/create",
                        "logViewController": "/controller/log",
                        "loadKladrController": "/controller/kladr",
                        "treeController": "/controller/tree",
                        "utilReinitController": "/util/restartPersistence RS",
                        "heteroListController": "/controller/list/hetero",
                        "importReplicationController": "/controller/replication/import",
                        "utilStartJobController": "/util/startScheduleJob RS",
                        "historyController": "/controller/history",
                        "showObjectsController": "/controller/list/view",
                        "viewObjectController": "/controller/object/view",
                        "utilExportCSVController": "/util/export/csv RS",
                        "cs": "/test/{name}: GET",
                        "utilStopJobController": "/util/stopScheduleJob RS",
                        "resourceViewController": "/controller/resource",
                        "JWTController": "/jwt",
                        "selectObjectListController": "/controller/list/select",
                        "journalController": "/controller/journal",
                        "utilCropController": "/util/crop RS",
                        "linkedListController": "/controller/list/linked",
                        "utilCreateReplicationController": "/util/createReplication RS",
                        "navigationController": "/controller/navigation"
                    },
                    "pages": {
                        "logViewPage": "/console/log",
                        "addReplicationPage": "/console/replication/add",
                        "JPQLPage": "/pages/jpql",
                        "changePasswordPage": "/console/changePassword",
                        "importReplicationPage": "/console/replication/import",
                        "403Page": "/403",
                        "404Page": "/404",
                        "reportExecPage": "/console/report",
                        "loadKladrPage": "/console/kladr",
                        "consolePage": "/console",
                        "resourcesViewPage": "/pages/resourceView",
                        "authPage": "/login",
                        "editObjectPage": "/console/object/edit",
                        "createObjectPage": "/console/object/create",
                        "viewLinkedListPage": "/console/list/linked",
                        "selectHeteroObjectPage": "/console/list/hetero",
                        "ShowObjectsPage": "/console/list",
                        "viewObjectPage": "/console/object/view",
                        "historyPage": "/console/object/history",
                        "selectReportParamPage": "/console/report/params",
                        "selectObjectPage": "/console/list/select",
                        "FakePage": "/console/rest/wef/wef/wef"
                    }
                };
                _this.set('resources', pages);
                $def.resolve();
                return $def
            },

            getResource: function (resource, type) {
                var type = type || 'controllers';
                return this.get('resources')[type][resource]
            },

            addPage: function (path, params, page_opener) {
                log.debug('SiteModel - Create new SitePage');
                var thisSiteModel = this;
                var newPage = new PageModel(_.extend({
                    path:      path,
                    siteModel: this
                }, params));
                var hashParams = _.omit(_.clone(params), 'event');
                if (page_opener) {
                    _.extend(hashParams, {opener: page_opener.get('guid')});
                }
                newPage.modifyPageHash(hashParams);
                newPage.set('page_opener', page_opener);

                this.get('pageCollection').add(newPage);

                //todo: move events to PageModel
                this.listenTo(newPage, cmsEvents.OPEN_PAGE, this.openPage);

                this.listenTo(newPage, cmsEvents.REMOVE_PAGE, function (page) {
                    thisSiteModel.removePage(page, thisSiteModel.get('pageCollection'));
                });
                this.listenTo(newPage, cmsEvents.ACTIVATE_PAGE, this.setActivePage);

                this.listenTo(newPage, cmsEvents.NOTIFY_PAGE_BLOCKS, this.notifyPageBlocks);

                // Событие добавления нового состояиния в истории
                this.listenTo(newPage, cmsEvents.PUSH_STATE, function (page) {
                    var newUrl = misc.getContextPath() +
                        (page.get('path') ? page.get('path') : '')
                        + page.get('stringHash');
                    log.debug('Push new State to History ' + newUrl);
                    window.history.pushState({pageGUID: page.get('guid')}, null, newUrl);
                });

                // Событие замены текущего состояиния в истории
                this.listenTo(newPage, cmsEvents.REPLACE_STATE, function (page) {
                    var newUrl = misc.getContextPath() +
                        (page.get('path') ? page.get('path') : '')
                        + page.get('stringHash');
                    log.debug('Replace current state in History ' + newUrl);
                    window.history.replaceState({pageGUID: page.get('guid')}, null, newUrl);
                });

                this.listenTo(newPage, cmsEvents.NAVIGATION, function (event) {
                    this.trigger(cmsEvents.NAVIGATION, event)
                });


                this.listenTo(channel, cmsEvents.FIND_MODEL, function (callback) {
                    alert('TODO SiteModel Alert');
                    callback(this);
                });


                return newPage.initializePageData().then(function () {
                    thisSiteModel.setActivePage(newPage, params);
                    newPage.notifyPageBlocks(new NotifyEventObject(cmsEvents.RESTORE_PAGE, params));
                    //TODO кажеьтся нужно убрать
                });
            },

            openPage: function (event) {
                var _this = this;
                //чистим страницы
                var activeIndex = this.get('pageCollection').indexOf(this.get('activePage'));
                var pagesForRemove = this.get('pageCollection').rest(activeIndex + 1);
                _.forEach(pagesForRemove, function (page) {
                    _this.removePage(page, _this.get('pageCollection'));
                });

                //добавляем хеш в историю
//                var newUrl = window.location.pathname + '';
//                window.history.pushState(null, null, newUrl);
                this.addPage(event.url, event.params, event.page_opener);
            },

            removePage: function (page, collection) {
                //TODO удаляем страницу
                collection.remove(page);

                //todo: move to PageModel
                page.destroyObject();
            },

            setActivePage: function (page, params) {
                //todo: use params (need ot pass to page to make initialization)
                log.debug('Site Model Show page');
                var oldPage = this.get('activePage');
                if (oldPage) {
                    // Если уже активная страница
                    if (oldPage == page) {
                        var currentHashParams = JSON.stringify(params.parsedHash);
                        /* var pageHashParams = JSON.stringify(page.get('hashParams')); */
                        if(currentHashParams /* &&  pageHashParams!= currentHashParams */ ){
                            var options = JSON.parse(currentHashParams);
                            if (params.history) options.history = true
                            page.notifyPageBlocks(new NotifyEventObject(cmsEvents.REFRESH_PAGE, options));
                        }
                        return;
                    }
                    oldPage.set('activePage', false);
                }
                var newUrl = misc.getContextPath() +
                    (page.get('path') ? page.get('path') : '')
                    + page.get('stringHash');

                var skipPushState = misc.option(params, "skipPushState", "Не делать push state", false);
                if (!skipPushState) {
                    log.debug('Push new State to History ' + newUrl);
                    window.history.pushState({pageGUID: page.get('guid')}, null, newUrl);
                }

                page.set('activePage', true);
                this.set('activePage', page);

                if (page.get('alreadyRendered')) {
                    this.trigger(cmsEvents.SHOW_PAGE, page);
                } else {
                    this.trigger(cmsEvents.RENDER_NEW_PAGE, page);
                }
            },

            getPageByPath: function (path, params) {
                // Проверяем нет ли на сайте такой же страницы
                var existsPage = this.get('pageCollection').getPageByPath(path, params);
                return  existsPage;
            },

            getPageByGUID: function (UID) {
                var existsPage = this.get('pageCollection').getPageByGUID(UID);
                return  existsPage;
            },

            notifyPageBlocks: function (notifyEventObject) {
                this.get('pageCollection').notifyPageBlocks(notifyEventObject);
            }


        });

        return SiteModel;
    });
