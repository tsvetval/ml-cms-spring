define(
    ['log', 'misc', 'backbone', '../collections/PageBlockInfoCollection', '../model/PageBlockInfoModel',
        '../collections/PageBlockCollection', 'underscore', 'cms/events/events', 'cms/events/NotifyPageBlocksEventObject', 'cms/routing_map'],
    function (log, misc, backbone, PageBlockInfoCollection, PageBlockInfo, PageBlockCollection, _, cmsEvents, NotifyEventObject, routing_map) {
        var PageModel = backbone.Model.extend({
            defaults: {
                url: undefined,
                /*Уникальный в рамках сессии(браузерного окна) идентифкатор страницы*/
                guid: undefined,
                /*Хеш страницы*/
                hashParams: undefined,
                /*Строковое представление хеша*/
                stringHash: undefined,
                name: "Empty page",
                /*Структурный шаблон страницы*/
                structureTemplate: undefined,
                initialized: false,
                pageBlockInfoCollection: undefined,
                pageBlockCollection: undefined,
                pageTitle: undefined,
                alreadyRendered: false,
                /* Состояние инициализации блоков */
                blocksReady: false,
                id: undefined
            },
            initialize: function () {
                console.log("initialize PageModel");
                this.set("pageBlockInfoCollection", new PageBlockInfoCollection());
                this.set("pageBlockCollection", new PageBlockCollection());
                this.set('guid', _.uniqueId('page'));
                this.set('pageTitle', '');
                this.set('hashParams', {});
                var _this = this
                /* Создаем событие после рендера всех блоков */
                this.listenTo(this.get('pageBlockCollection'), 'change:blockRendered', function () {
                    var rendered = _this.get('pageBlockCollection').every(function(model){
                        return model.get('blockRendered')
                    })

                    if(rendered) {
                        _this.trigger(cmsEvents.BLOCKS_RENDER_COMPLETE)
                    }
                })

            },


            /**
             * Производит загрузку структурного шаблона и всех контент блоков на страниук
             * @return {jQuery.Deferred}
             */
            initializePageData: function () {
                var thisPage = this;


                return $.when(this._getStructureTemplate(), this._loadPageInfo())
                    .then(function (structureTemplateAjax, resultPageInfoAjax) {
                        log.debug('Finish loading Structure Template and data from server');
                        thisPage.set({structureTemplate: _.template(structureTemplateAjax[0], resultPageInfoAjax[0])});
                        var _page =  routing_map[thisPage.get('path')].page;
                        var _pageBlocks = routing_map[thisPage.get('path')].pageBlocks;

                        log.debug('Finish loading PageBlockInfo from server');
                        if (_pageBlocks) {
                            _pageBlocks.forEach(function (block) {
                                var pageBlockInfo = new PageBlockInfo({
                                    id: block.id,
                                    zone: block.zone,
                                    orderNum: block.orderNum,
                                    bootJs: block.bootJs
                                });
                                thisPage.get('pageBlockInfoCollection').add(pageBlockInfo);
                            });
                        }
                        if (_page && _page.id) {
                            thisPage.set('id', _page.id);
                            thisPage.set('initialized', true);
                            // Проводим инициализацию реальных блоков возращаем
                            return thisPage._initializePageBlocks()
                        }
                    })
                    .fail(function (xhr) {

                        if (xhr.status == 401) { /* Check if authentificate needed */
                            /* Save current url in cookies */
                            $.cookie('urlBeforeAuth', window.location.href, {path: '/'});
                            window.location = misc.getContextPath() + (xhr.responseJSON.url || this.get('siteModel').getResource('authPage', 'pages'));

                        } else if (xhr.status == 403) { /* Check if access not granted */
                            /* todo: refactor route to page*/
                            thisPage.openPage({
                                url: thisPage.get('siteModel').getResource('403Page', 'pages'),
                                page_opener: thisPage,
                                params: {}
                            })
                        }

                        return (new $.Deferred()).reject()
                    });
            },


            _getStructureTemplate: function () {
                var thisPage = this;
                // Грузим структурный шаблон
                log.debug('Start loading Structure Template from server');
                if (!routing_map[this.get('path')]){
                    log.error('Can\'t find [' + this.get('path') + '] in routing_map');
                }

                return $.ajax(misc.getContextPath() + (this.get('path').charAt(0) != '/' ? "/" : "")
                    + routing_map[this.get('path')].page.template, {
                    type: 'GET',
                    beforeSend: function(xhr){xhr.setRequestHeader('XX-ML-REQUEST', 'true');}
                });

            },
            // получение данных о странице (будут применены к структурному шаблону после загрузки)
            _loadPageInfo: function () {
                var thisPage = this;
                log.debug('Start loading PageBlockInfo from server');
                return $.ajax(misc.getContextPath() + (this.get('path').charAt(0) != '/' ? "/" : "")
                    + this.get('path'), {
                    type: 'GET',
                    beforeSend: function(xhr){xhr.setRequestHeader('XX-ML-REQUEST', 'true');}
                });
            },
            /**
             * По заданному списку pageBlockInfoCollection производит их загрузку
             * @return {*} возвращает deferred объект
             */
            _initializePageBlocks: function () {
                var thisPage = this;
                log.debug('Start to initializePageBlocks');
                var blockInfoCollection = this.get('pageBlockInfoCollection');
                var deferredArray = blockInfoCollection.map(function (blockInfo, i) {
                    return blockInfo.loadRealBlock(thisPage)
                        .then(function (pageBlockModel) {
                            /* Добавление блока к коллекцию */
                            thisPage.get('pageBlockCollection').add(pageBlockModel);
                            pageBlockModel.set("active",true);
                        });
                });
                return $.when.apply(null, deferredArray);
            },

            /**
             * Показать на спранице педжблоки. Метод найдет не загуженные, загрузит и оттобразит на странице. Загруженные
             * повторно загружаться не будут
             * data.folderId = id папки
             * data.pageBlocks = массив id пэйджблоков
             * @event cmsEvents.SHOW_PAGE_BLOCKS
             * */
            showPageBlocks: function (data) {
                /* todo: refactor this */
                var _this = this;
                var pageBlockModels = new PageBlockCollection(); //модели которые должны быть показаны
                var pageBlockIdsForLoad  = [];

                var allInfo = this.get("pageBlockCollection");
                //находим не загруженные блоки
                if(data.pageBlocks && !_.isEmpty(data.pageBlocks)) {
                    data.pageBlocks.forEach(function(block){
                        var existBlock = allInfo.filter(function(blk){
                            return blk.get("blockInfo").get('id') == block;
                        });
                        if(existBlock.length>0){
                            var loadedBlock = existBlock[0];
                            pageBlockModels.add(loadedBlock);
                        }else{
                            pageBlockIdsForLoad.push(block);
                        }
                    });
                }

                /* Загружаем не загруженные блоки */
                $.when(_this.loadPageBlocks(pageBlockIdsForLoad,data.folderId)).then(function(loadedPageBlocks){
                    pageBlockModels.add(loadedPageBlocks.toArray());

                    /* проверка наличия блока в зоне */
                    pageBlockModels.forEach(function(block){
                        var pageBlockInZone = allInfo.filter( function(blk){
                            return blk.get("blockInfo").get('zone') == block.get("blockInfo").get("zone");
                        });
                        pageBlockInZone.forEach(function(blk){
                            blk.set("active",false);
                        });
                    });

                    /* Делаем активными блоки которые пришли по событию */
                    pageBlockModels.forEach(function(block){
                        block.set("active",true);

                    });

                    loadedPageBlocks.forEach(function(block){
                        block.trigger(cmsEvents.RESTORE_PAGE, data);
                    });

                    _this.set('blocksReady', true)
                    _this.trigger(cmsEvents.SHOW_ACTIVE_BLOCK);
                });
            },

            /* todo: возможно стоит полностью переаботать систему компонентов и их связей */
            showPageBlocksHistory: function (data) {
                var pageBlockModels = new PageBlockCollection(); //модели которые должны быть показаны

                var allInfo = this.get("pageBlockCollection");
                //находим блоки в коллекции
                if(data.pageBlocks && !_.isEmpty(data.pageBlocks)) {
                    data.pageBlocks.forEach(function(block){
                        var existBlock = allInfo.filter(function(blk){
                            return blk.get("blockInfo").get('id') == block;
                        });
                        if(existBlock.length>0){
                            var loadedBlock = existBlock[0];
                            pageBlockModels.add(loadedBlock);
                        }
                    });
                }

                /* проверка наличия блока в зоне */
                pageBlockModels.forEach(function(block){
                    var pageBlockInZone = allInfo.filter( function(blk){
                        return blk.get("blockInfo").get('zone') == block.get("blockInfo").get("zone");
                    });
                    pageBlockInZone.forEach(function(blk){
                        blk.set("active",false);
                    });
                });

                /*Folder history activation hack*/
                allInfo.each( function(block){
                    if (_.contains(["CENTER", "SEARCH"], block.get("blockInfo").get('zone'))) {
                        block.deactivate()
                    }

                    if (block.get("blockInfo").get('zone') == 'UTILS') {
                        block.trigger(cmsEvents.OPEN_FOLDER, {folderId: data.id})
                    }
                });

                /* Делаем активными блоки которые пришли по событию */
                pageBlockModels.forEach(function(block){
                    block.set("active",true);
                    block.trigger(cmsEvents.OPEN_FOLDER, {folderId: data.id});

                });

                this.trigger(cmsEvents.SHOW_ACTIVE_BLOCK);

            },

            /**
             * Загружает сначала PageBlockInfo по всем id, потом загужает  PageBlockModel для каждого
             *  pageBlockIdsForLoad = массив id пэйджблоков
             * */
            loadPageBlocks: function(pageBlockIdsForLoad,folderId){
                /* Возвращаем пустую коллекцию если нечего загружать */
                if(_.isEmpty(pageBlockIdsForLoad)) {
                    return new PageBlockCollection();
                }
                var _this = this;
                var defered = $.Deferred();
                $.when(_this.loadPageBlockInfos(pageBlockIdsForLoad, folderId)).then(function(blockInfos){
                    var pageBlockInfos = [];
                    /* when response contains error field redirect to home page */
                    if (blockInfos.error) {
                        /* todo: refactor route to page*/
                        _this.openPage({
                            url: '/' + $.cookie('homePage'),
                            page_opener: _this,
                            params: {}
                        });
                        return false;
                    }
                    blockInfos.forEach(function (block) {
                        var pageBlockInfo = new PageBlockInfo({
                            id: block.id,
                            zone: block.zone,
                            orderNum: block.orderNum,
                            bootJs: block.bootJs
                        });
                        pageBlockInfos.push(pageBlockInfo);
                        _this.get('pageBlockInfoCollection').add(pageBlockInfo);
                    });
                    var pageBlockModels = [];
                    var deferredArray = pageBlockInfos.map(function (blockInfo, i) {
                        return blockInfo.loadRealBlock(_this)
                            .then(function (pageBlockModel) {
                                _this.get('pageBlockCollection').add(pageBlockModel);
                                pageBlockModels.push(pageBlockModel)
                            });
                    });

                    $.when.apply(window, deferredArray).then(function(){
                        var loadedPageBlockModels = new PageBlockCollection();
                        loadedPageBlockModels.add(pageBlockModels);
                        defered.resolve(loadedPageBlockModels);
                    });
                });
                return defered.promise();
            },

            /**
             *  Получение информации о блоках
             *  */
            loadPageBlockInfos: function (pageBlockIds,folderId) {
                return $.ajax(misc.getContextPath() + (this.get('path').charAt(0) != '/' ? "/" : "")
                    + this.get('path'), {
                    type: 'GET',
                    data: {
                        action: 'getPageBlockByIDs',
                        pageBlockIds: JSON.stringify(pageBlockIds),
                        folderId:folderId,
                        ml_request: true
                    }
                });
            },

            /**
             * Обработчик события NOTIFY_PAGE_BLOCK, прижедшего с одного из пейдж блоков
             * @param notifyEventObject
             */
            onNotifyPageBlocks: function (notifyEventObject) {
                //TODO check page
                if (!notifyEventObject.notifyPage) {
                    // передаем событие пейдж блокам
                    this.notifyPageBlocks(notifyEventObject);
                } else {
                    // Передаем событие выше в сайт модель
                    this.trigger(cmsEvents.NOTIFY_PAGE_BLOCKS, notifyEventObject);
                }

                if (notifyEventObject.completeCallback) {
                    notifyEventObject.completeCallback();
                }
            },

            onNavigation: function (options) {
                // Если меняется тайтл страницы то меняем его
                var _do = misc.option(options, "do", "Действие с навигацией");
                var title = misc.option(options, "title", "Заголовок страницы");
                if (_do == 'modify' && title) {
                    this.set('pageTitle', title);
                }
                // шлем навигационное соообщение дальше
                this.trigger(cmsEvents.NAVIGATION, options);
            },

            openPage: function (event) {
                // Если меняется тайтл страницы то меняем его
//            var navigationParams = _.clone(event.params);
//            navigationParams['opener'] =  this.get('guid');
//            this.trigger('NAVIGATION',{
//                do:'push',
//                title: event.title,
//                url: event.url,
//                params:navigationParams
//            });
                // Передаем объект модели странцу открывыющую новую
                event['page_opener'] = this;
                this.trigger(cmsEvents.OPEN_PAGE, event);


            },

            closePage: function (event) {
                var pageOpener = this.get("page_opener");
                if (pageOpener) {
                    this.trigger(cmsEvents.ACTIVATE_PAGE, pageOpener);
                    this.trigger(cmsEvents.REMOVE_PAGE, this);
                }else{
                    this.trigger(cmsEvents.OPEN_PAGE, {url: this.get('siteModel').get('resources')['pages']['consolePage'], params:{}});
                    this.trigger(cmsEvents.REMOVE_PAGE, this);
                }
            },

            replacePage: function (event) {
                event['page_opener'] = this.get('page_opener');
                this.trigger(cmsEvents.OPEN_PAGE, event);
                this.trigger(cmsEvents.REMOVE_PAGE, this);
            },


            notifyPageBlocks: function (notifyEventObject) {
                this.get('pageBlockCollection').notifyPageBlocks(notifyEventObject);
            },

            destroyObject: function () {
                this.trigger('destroy', this);
            },

            modifyPageHash: function (hashParams, modifyHistoryMode, extendHashMode) {
                if (!extendHashMode || extendHashMode == 'extendHash') {
                    _.extend(this.get('hashParams'), hashParams)
                } else {
                    this.set('hashParams', hashParams ? hashParams : {});
                }
                var hash = "#" + JSON.stringify(this.get('hashParams'));
                this.set('stringHash', hash);
                if (modifyHistoryMode === 'pushState') {
                    this.trigger(cmsEvents.PUSH_STATE, this);
                    //TODO
                } else if (modifyHistoryMode === 'replaceState') {
                    this.trigger(cmsEvents.REPLACE_STATE, this);
                }
                this.modifyPageParams(hashParams);
            },

            modifyPageParams: function (params) {
                var _this = this;
                _.map(params, function (v, k) {
                    _this.set(k, v);
                });
            }
        });

        return PageModel;
    });
