/**
 * Модель блока резервного копирования БД
 */
define(
    ['log', 'misc', 'backbone', 'cms/events/events', 'cms/model/PageBlockModel', 'cms/page_blocks/DialogPageBlock'],
    function (log, misc, backbone, cmsEvents ,PageBlockModel, Message) {
        var model = PageBlockModel.extend({
            defaults: {
                renderTemplate: undefined
            },

            /**
             * инициализация модели
             */
            initialize: function () {
                /* PageBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this)

                console.log("initialize BlockModel");
            },

            /**
             * Добавление событий блока
             * @Override PageBlockModel.bindEvents
             */
            bindEvents: function () {
                /* PageBlockModel.bindEvents() добавляет основные события блока */
                model.__super__.bindEvents.call(this)

                this.listenTo(this, cmsEvents.RESTORE_PAGE, function (params) {
                    this.prompt();
                });
            },

            /**
             * Остановка обработчиков событий блока
             * @Override PageBlockModel.unbindEvents
             */
            unbindEvents: function () {
                /*  События блока */
                this.stopListening(this, cmsEvents.RESTORE_PAGE);

                /* PageBlockModel.unbindEvents() удаляет все основные события блока */
                model.__super__.unbindEvents.call(this)
            },

            /**
             * запрос шаблона для отображения блока
             */
            prompt: function () {
                var _this = this;
                var options = {
                    action: "show"
                };

                var callback = function (result) {
                    _this.set('renderTemplate', result.html);
                    _this.trigger(cmsEvents.RENDER_VIEW);
                };

                return this.callServerAction(options, callback);
            },

            /**
             * запрос на создание резервной копии БД
             */
            backup: function (data) {
                var _this = this;
                var options = {
                    action: "backup",
                    data: {
                        dateFrom: data.dateFrom,
                        dateTo: data.dateTo,
                        includeInstances: data.includeInstances
                    }
                };

                var callback = function (result) {
                    _this.showBackupComplete(result);
                };

                return this.callServerAction(options, callback);
            },

            /**
             * отображение результатов создания резервной копии БД
             */
            showBackupComplete: function (result) {
                var modelResultMessage = undefined;
                var instanceResultMessage = undefined;

                if (result.replicationModelCount && result.replicationModelName) {
                    modelResultMessage = "Создан блок репликации структуры БД " +
                    "с именем:\n" + result.replicationModelName + "\n" +
                    "Объектов в блоке: " + result.replicationModelCount;
                }

                if (result.replicationInstanceCount && result.replicationInstanceName) {
                    instanceResultMessage = "Создан блок репликации данных" +
                    "с именем:\n" + result.replicationInstanceName + "\n" +
                    "Объектов в блоке: " + result.replicationInstanceCount;
                }

                var message = undefined;
                if (modelResultMessage) {
                    message = modelResultMessage;
                }
                if (instanceResultMessage) {
                    if (message) {
                        message += "\n\n"
                    }
                    message += instanceResultMessage;
                }

                var title = undefined;

                if (!message) {
                    title = "Подготовка резервного копирования БД за период с " +
                    result.dateFrom + " по " + result.dateTo + " " + "завершена с ошибкой";
                    message = "В БД отсутствуют данные, измененные за указанный период"
                } else {
                    title = "Подготовка резервного копирования БД за период с " +
                    result.dateFrom + " по " + result.dateTo + " " + "успешно завершена";
                }

                var dialog = new Message({
                    title: title,
                    message: message,
                    type: 'infoMessage'
                });
                dialog.show();
            }
        });
        return model;
    });
