define(
    ['log', 'misc', 'backbone', 'cms/events/events','cms/model/PageBlockModel', 'cms/page_blocks/DialogPageBlock'],
    function (log, misc, backbone, cmsEvents, PageBlockModel, Message) {
        var model = PageBlockModel.extend({
            defaults: {
                renderTemplate: undefined,
                controller: 'authController'
            },
            initialize: function () {
                /* PageBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this)

                console.log("initialize SecurityBlockModel");
            },

            /**
             * Добавление событий блока
             * @Override PageBlockModel.bindEvents
             */
            bindEvents: function () {
                /* PageBlockModel.bindEvents() добавляет основные события блока */
                model.__super__.bindEvents.call(this)

                this.listenTo(this, cmsEvents.RESTORE_PAGE, function (params) {
                    this.prompt();
                });
            },

            /**
             * Остановка обработчиков событий блока
             * @Override PageBlockModel.unbindEvents
             */
            unbindEvents: function () {
                /*  События блока */
                this.stopListening(this, cmsEvents.RESTORE_PAGE);

                /* PageBlockModel.unbindEvents() удаляет все основные события блока */
                model.__super__.unbindEvents.call(this)
            },


            prompt: function () {
                var _this = this;

                var callback = function (result) {
                };

                return this.callServerAction('GET', "/site/auth/user-data").then(function(result){
                    if (result.login){
                        _this.set('login', result.login);
                    } else {
                        _this.unset('login')
                    }

                    if (result.helpUrl){
                        _this.set('helpUrl', result.helpUrl);
                    } else {
                        _this.unset('helpUrl')
                    }
                    _this.trigger(cmsEvents.RENDER_VIEW);
                });
            },

            login: function (login, password) {
                var _this = this;

                return this.callServerAction("POST", "/site/auth",  {
                    login: login,
                    password: password
                }).done(function(result){
                    if (result.authorized == true) {
                        var url;
                        if (result.useHomePageUrl || !$.cookie('urlBeforeAuth') || $.cookie('urlBeforeAuth').indexOf(window.location.pathname) >= 0) {
                            url = result.url ? result.url : misc.getContextPath();
                        } else {
                            url = $.cookie('urlBeforeAuth');
                        }
                        $.removeCookie('urlBeforeAuth',{path    : '/'});
                        window.location.href = url;
                    } else {
                        var errorMessage = new Message({
                            title: "Ошибка аутентификации",
                            message: "Неверное имя пользователя или пароль",
                            type: 'dialogError'
                        });
                        errorMessage.show();
                    }
                });
            },

            /**
             * @Deprecated
             * @returns {*}
             */
            logout: function () {
                var _this = this;
                return this.callServerAction("POST", "/site/auth/logout").then(function(result){
                    window.location.href = result.logoutUrl;
                });
            }

        });
        return model;
    });
