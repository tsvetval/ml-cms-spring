/**
 * Базовое представление для атрибута в режиме редактирования
 */
define(
    ['log', 'misc', 'backbone',
        'cms/events/events',
        'cms/page_blocks/form_block/view/AttrView',
        'text!cms/page_blocks/form_block/object_edit_block/templates/attrs/StringAttrView.tpl'],
    function (log, misc, backbone, cmsEvents, AttrView, DefaultTemplate) {
        var view = AttrView.extend({

            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                _.extend(this.events, AttrView.prototype.events);
                this.listenTo(this.model, cmsEvents.FOCUS, this.setFocus);
            },

            /**
             * Добавление обработчиков к обязательным для заполнения полям
             */
            addMandatoryEvents : function(){
                if (!this.$attrLabel) {
                    this.$attrLabel = this.$el.find('.attr-label');
                }

                if (this.$attrLabel && this.model.get('mandatory')){
                    this.$attrLabel.addClass('mandatory-label');
                    this.listenTo(this.model, cmsEvents.HIGHLIGHT_MANDATORY, this.highlightMandatory);
                    this.listenTo(this.model, cmsEvents.REMOVE_HIGHLIGHT_MANDATORY, this.removeHighlightMandatory);
                } else {
                    if (!this.$attrLabel) {
                        log.warn('Can not add mandatory for attr');
                    }
                }

                if(!this.model.get('mandatory')) {
                    var _this = this
                    /* установка и снятие обработчиков для атрибутов которым был изменен mandatory */
                    this.listenTo(this.model, 'change:mandatory', function () {
                        if(_this.model.get('mandatory')) {
                            _this.$el.find('.attr-label').addClass('mandatory-label');
                            _this.listenTo(_this.model, cmsEvents.HIGHLIGHT_MANDATORY, _this.highlightMandatory);
                            _this.listenTo(_this.model, cmsEvents.REMOVE_HIGHLIGHT_MANDATORY, _this.removeHighlightMandatory);
                        } else {
                            _this.$el.find('.attr-label').removeClass('mandatory-label');
                            _this.stopListening(_this.model, cmsEvents.HIGHLIGHT_MANDATORY);
                            _this.stopListening(_this.model, cmsEvents.REMOVE_HIGHLIGHT_MANDATORY);
                        }

                    })
                }
            },

            /**
             * Отображение readonly-атрибутов
             */
            addReadOnly: function () {
                if (!this.$inputField) {
                    this.$inputField = this.$el.find('.attrField');
                }

                if (this.$inputField && this.model.get('readOnly')) {
                    this.$inputField.attr('disabled', 'disabled');
                } else {
                    if (!this.$inputField) {
                        log.warn('Can not add readOnly for attr');
                    }
                }
            },

            /**
             * Подсветить обязательный атрибут
             */
            highlightMandatory : function(){
                var groupId = this.model.get("groupId");
                if (groupId) {
                    var group = this.model.get("pageBlockModel").getGroupById(groupId);
                    if (group) {
                        group.highlightMandatory();
                    }
                }
                this.$el.find('.attr-label').addClass('highlight-mandatory-label');
            },

            /**
             * Удалить подсветку обязательного атрибута
             */
            removeHighlightMandatory : function(){
                this.$el.find('.attr-label').removeClass('highlight-mandatory-label');
                var groupId = this.model.get("groupId");
                if (groupId) {
                    var group = this.model.get("pageBlockModel").getGroupById(groupId);
                    if (group) {
                        group.removeHighlightMandatory();
                    }
                }
            },

            /**
             * Установить фокус формы на атрибут
             */
            setFocus: function () {
                if (!this.$inputField) {
                    this.$inputField = this.$el.find('.attrField');
                }

                if (this.$inputField) {
                    this.$inputField.focus();
                }
            },
            /**
             * Convert moment date format to jquery.inputmask format
             * @param pattern
             * @returns {*}
             */
            toInputMaskFormat: function(pattern){
                return pattern.replace(/M|Y|D|H|m|s/g, '9')
            }

        });

        return view;
    });
