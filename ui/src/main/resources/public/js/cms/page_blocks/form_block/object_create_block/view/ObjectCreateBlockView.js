/**
 * Представление блока создания объекта
 */
define(
    ['log', 'misc', 'backbone', 'underscore',
        'cms/page_blocks/form_block/FormBlockView',
        'markup',
        'cms/events/events',
        'cms/page_blocks/DialogPageBlock',
        'cms/page_blocks/form_block/collections/AttrGroupCollection',
        'cms/page_blocks/form_block/factories/GroupViewFactory',
        'cms/page_blocks/form_block/factories/AttrViewFactory',
        'text!cms/page_blocks/form_block/object_create_block/templates/ObjectCreateTemplate.tpl'],
    function (log, misc, backbone, _, FormBlockView, markup, cmsEvents, Message, AttrGroupCollection,
              GroupViewFactory, AttrViewFactory, ObjectEditTemplate) {
        var view = FormBlockView.extend({

            events : _.extend({
                "click .btnSave" : "saveObjectClick",
                "click .save-close-object-button" : "saveAndCloseObjectClick",
                "click .btnClose" : "cancelEditObjectClick",
                "click .remove-object-button" : "editObjectClick"
            }, FormBlockView.events),

            /**
             * Инициализация представления
             */
            initialize: function () {
                log.debug("initialize ObjectViewBlockView");
                this.viewMode = 'CREATE';
                this.listenTo(this.model, 'render', this.render);
                this.listenTo(this.model, 'attribute:render', this.renderAttribute);
                _.extend(this.events, FormBlockView.prototype.events);
                this.listenTo(this.model.get('pageModel'), cmsEvents.BLOCKS_RENDER_COMPLETE, this.helpers.resizeTitle)
            },

            /**
             * Обработка клика по кнопке "Сохранить"
             * @returns {boolean}
             */
            saveObjectClick : function(){
                this.model.saveObject();
                return false;
            },

            /**
             * Обработка клика по кнопке "Сохранить и закрыть"
             * @returns {boolean}
             */
            saveAndCloseObjectClick : function(){
                return false;
            },

            /**
             * Обработка клика по кнопке "Отмена"
             * @returns {boolean}
             */
            cancelEditObjectClick : function(){
                this.model.cancelEditObject();
                return false;
            },


            /**
             * Отрисовка представления
             * @returns {boolean}
             */
            render: function () {
                var _this = this;
                this.$el.html(_.template(ObjectEditTemplate, {formModel: this.model}));
                // create container
                var $content_container = _this.$el.find('#view_content_container');
                _this._renderNonGroupAttrs($content_container);
                // Выводим группы
                _this._renderRootGroups($content_container);
                this.model.set('blockRendered', true)
                return true;
            }
        });

        return view;
    });
