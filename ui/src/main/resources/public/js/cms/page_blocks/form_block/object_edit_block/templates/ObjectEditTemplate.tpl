<!--<div class="container control-panel col-xs-24" id="view_buttons_container">
    <h4 class="form-title col-xs-10 callback">
        <%=formModel.get('title')%><small>&nbsp(Редактирование объектa класса <%=formModel.get('description')%>)</small>
    </h4>
    <div class="pull-right control-buttons">
        <div>
            <div>
                <button class="btn btn-primary save-object-button" click-action="saveClick">
                    <span class="glyphicon glyphicon-ok"></span> Сохранить
                </button>
                <button class="btn btn-primary save-close-object-button" click-action="saveAndCloseClick">
                    <span class="glyphicon glyphicon-ok"></span> Сохранить и закрыть
                </button>
                <button class="btn btn-primary cancel-object-button" click-action="cancelClick">
                    <span class="glyphicon glyphicon-repeat"></span> Отмена
                </button>
                <% if (canDelete) { %>
                <button class="btn btn-primary remove-object-button" click-action="deleteClick">
                    <span class="glyphicon glyphicon-remove"></span> Удалить
                </button>
                <%}%>
            </div>
        </div>
    </div>
</div>-->

<div class="ml-main__content-title">
    <div class="ml-main__content-title-text" title="<%=formModel.get('title')%>"><%=formModel.escape('title')%></div>
</div>


<div class="btnDelete glyphicon glyphicon-trash" title="Удалить">
</div>

<div class="btnClose" title="Закрыть">
    Закрыть
</div>


<div class="ml-main__content-class-form-controls-submit">
    <div class="btnSave" title="Сохранить">Сохранить</div>
    <div class="btnSaveAndClose glyphicon glyphicon-remove" title="Сохранить и закрыть"></div>
</div>


<div class="ml-main__content-delimiter"></div>
<div class="ml-main__content-object-columns" id="view_content_container"></div>

<div style="clear: both"></div>

</div>
