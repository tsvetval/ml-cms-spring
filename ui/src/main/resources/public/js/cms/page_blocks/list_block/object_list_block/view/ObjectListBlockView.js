/**
 * Представление блока списка объектов
 *
 */
define(
    ['log', 'misc', 'backbone', 'underscore', 'moment',
        'boot_table_ru',
        'cms/page_blocks/list_block/ListBlockView',
        'markup',
        'cms/page_blocks/DialogPageBlock',
        'text!cms/page_blocks/list_block/object_list_block/templates/ObjectListTemplate.tpl',
        'text!cms/page_blocks/list_block/object_list_block/templates/FolderContainerTemplate.tpl',
        'text!cms/page_blocks/list_block/object_list_block/templates/ObjectTableTemplate.tpl',
        'text!cms/page_blocks/list_block/object_list_block/templates/ObjectToolbarTemplate.tpl',
        'text!cms/page_blocks/list_block/object_list_block/templates/FolderItemTemplate.tpl',
        'text!cms/page_blocks/list_block/object_list_block/templates/IFrameTemplate.tpl',
        'table_formatter_mixin',
        'cms/events/events',
    ],
    function (log, misc, backbone, _, moment,
              boot_table,
              ListBlockView,
              markup,
              Message,
              ObjectListTemplate,
              FolderContainerTemplate,
              ObjectTableTemplate,
              ObjectToolbarTemplate,
              FolderItemTemplate,
              IFrameTemplate,
              table_formatter_mixin,
              cmsEvents) {

        var view = ListBlockView.extend({

            events: {
                "click .upper-folder": "parentFolderButtonClick",
                "click .open-object-button": "openObjectClick",
                "click .btnCreate": "createObjectClick",
                "click .btnDelete": "deleteObjectClick",
                "click .show-full-text": "showFullTextClick",
                "click .hide-full-text": "hideFullTextClick",
                "click .file-download-link": "downloadFile",
                "click .select-row": "selectRow",
                "click .create-other-object": "createOtherObjectClick"
            },

            /**
             * открытие объекта из списка
             *
             */
            openObjectClick: function (event) {
                var mlClass = this.model.get('className');
                var objectId = $(event.target).attr('objectId');
                this.model.get('objectList').objectList.forEach(function (object) {
                        if (objectId == object.objectId) {
                            mlClass = object.mlClass;
                        }
                    }
                );
                this.model.openObject(objectId, mlClass);
            },
            /**
             * Удаление выбранного списка объектов
             * */
            deleteObjectClick: function () {
                var _this = this;
                if (this.model.get('isAllSelected')) {
                    var dialogConfirm = new Message({
                        title: "Подтверждение удаления",
                        message: "Вы действительно хотите удалить все объекты?",
                        type: 'dialogMessage'
                    });
                    dialogConfirm.show({
                        okAction: function () {
                            _this.model.deleteObjects();
                        }
                    })
                } else {
                    if (_this.model.get("selectedIds").length > 0) {
                        log.debug('Click delete event catched');
                        var dialogConfirm = new Message({
                            title: "Подтверждение удаления",
                            message: "Вы действительно хотите удалить " + _this.model.get("selectedIds").length + " объектов?",
                            type: 'dialogMessage'
                        });
                        dialogConfirm.show({
                            okAction: function () {
                                _this.model.deleteObjects();
                            }
                        });
                    }
                }

            },
            /**
             * создание объекта
             *
             */
            createObjectClick: function () {
                this.model.createObject(this.model.get('className'));
            },

            /**
             * создание объекта класса потомка
             *
             */
            //TODO
            createOtherObjectClick: function (event) {
                this.model.createObject(event.target.getAttribute('className'));
            },

            /**
             * развернуть список связанных объектов
             *
             */
            showFullTextClick: function (e) {
                $(e.currentTarget).hide();
                $(e.currentTarget).next().show();
                $(e.currentTarget).prevAll(".ellipsis").hide();
                $(e.currentTarget).prevAll(".one-many-text-container").removeClass("collapsed");
            },

            /**
             * свернуть список связанных объектов
             *
             */
            hideFullTextClick: function (e) {
                $(e.currentTarget).hide();
                $(e.currentTarget).prev().show();
                $(e.currentTarget).prevAll(".ellipsis").show();
                $(e.currentTarget).prevAll(".one-many-text-container").addClass("collapsed");
            },

            selectRow: function () {
                if (event.toElement.checked) {
                    this.model.set("selectedIds", _.union(this.model.get("selectedIds"), $(event.srcElement).attr('objectId')));
                } else {
                    this.model.set("selectedIds", _.difference(this.model.get("selectedIds"), $(event.srcElement).attr('objectId')));
                }

            },

            /**
             * инициализация представления
             *
             */
            initialize: function () {
                log.debug("initialize ObjectListBlockViewDesign");
                this.listenTo(this.model, 'render', this.render);
                this.listenTo(this.model.get('pageModel'), cmsEvents.BLOCKS_RENDER_COMPLETE, this.helpers.resizeTitle)
            },

            /**
             * отрисовка представления
             *
             */
            render: function () {
                var _this = this;
                // TODO дял чего?
                if (this.model.get('url')) {
                    this.$el.html(_.template(IFrameTemplate, {url: this.model.get('url')}));
                    return true;
                }
                // Добавляем шаблон для заголовка и разметки блока
                this.$el.html(_.template(ObjectListTemplate, {listModel: this.model}));
                var $folderListContainer = this.$el.find('.folder-list-container');
                var $objectListContainer = this.$el.find('.object-list-container');
                //$folderListContainer.html(_.template(FolderContainerTemplate, {listModel: this.model}));

                var currentFolder = this.model.get('currentFolder');
                if (!currentFolder.parent) {
                    // Если у папки нет родителя то скрываем кнопку вверх
                    this.$el.find(".upper-folder").hide();
                } else {
                    this.$el.find(".upper-folder").show();
                }
                /*                if (currentFolder) {
                 var parentFolderId = currentFolder.parent;
                 if (parentFolderId) {
                 var folder = {
                 id: parentFolderId,
                 title: "Вверх"
                 };
                 this.createParentFolderButton($folderListContainer, folder);
                 } else {
                 var root = {
                 id: undefined,
                 title: "Вверх"
                 };
                 this.createParentFolderButton($folderListContainer, root);
                 }
                 }*/

                var folderList = this.model.get('folders');
                if (folderList) {
                    this.createFolderListView($folderListContainer, folderList);
                }

                var className = this.model.get('className');
                if (className) {
                    var objectList = this.model.get('objectList');
                    this.createObjectListTable($objectListContainer, objectList);

                    /* Additional functions for bootstrap-table */
                    this.updateObjectListTable();
                }

                this.model.set('blockRendered', true)

                return true;
            },

            /**
             * Нажитие кнопки вверх
             *
             */
            parentFolderButtonClick: function () {
                var _this = this;
                var currentFolder = this.model.get('currentFolder');
                if (currentFolder && currentFolder.parent) {
                    _this.model.openFolder({folderId: currentFolder.parent, pageBlocks: undefined});
                }
            },

            /**
             * отображение вложенных папок
             *
             */
            createFolderListView: function ($container, folderList) {
                var _this = this;
                //var $items = $container.find('.folder-list-container');
                folderList.forEach(function (folder) {
                    var $folder = $(_.template(FolderItemTemplate, {folder: folder}));
                    //var $icon = $("<div></div>");

                    if (folder.icon) {
                        //TODO
                        $folder.addClass("ml-main__common-folder");
                        //$folder.addClass(folder.icon);
                        //$icon.addClass("iconURL")   //todo сделать отображение иконок по url
                    } else {
                        $folder.addClass("ml-main__common-folder");
                    }
                    //$folder.find(".icon").append($icon);
                    $folder.on('click', function () {
                        _this.model.openFolder({folderId: folder.id, pageBlocks: folder.pageBlocks});
                    });
                    $container.append($folder);
                });
            },

            /**
             * запрос URL иконки для папки и её отображене
             *
             */
            setIcon: function ($elem, filename) {
                var options = {
                    action: "getIconURL",
                    data: {
                        filename: filename
                    }
                };

                var callback = function (result) {
                    $elem.css("background-image", "url(" + result.iconURL + ")");
                };

                return this.model.callServerAction(options, callback);
            },


            /**
             * Создание таблицы связанных объектов
             * @param $container    -   элемент-контейнер (jQuery-объект)
             * @param data          -   данные для отображения
             */
            createObjectListTable: function ($container, data) {
                var _this = this;
                //this.$toolbar = $(_.template(ObjectToolbarTemplate, {createdClasses:_this.model.get('createdClasses'),showCreate:_this.model.get("showCreate"),showDelete:_this.model.get("showDelete")}));
                //$container.append(this.$toolbar);

                this.$tableContainer = $(_.template(ObjectTableTemplate, {}));
                this.$table = this.$tableContainer.find('table');
                $container.append(this.$tableContainer);

                //Формируем колонки таблицы
                var columns = [];
                columns.push(
                    _this.getSelectMode()
                );
                columns.push({
                    field: 'objectId',
                    visible: true,
                    formatter: _this.formatterSelect
                });


                data.attrList.forEach(function (columnAttr) {
                    var formatter = _this.formatterString;
                    var sortable = true;
                    if (columnAttr.fieldType === "ONE_TO_MANY" || columnAttr.fieldType === "MANY_TO_MANY") {
                        formatter = _this.formatterOneToMany;
                        sortable = false;
                    } else if (columnAttr.fieldType === "MANY_TO_ONE") {
                        formatter = _this.formatterManyToOne;
                        sortable = true;
                    } else if (columnAttr.fieldType === "BOOLEAN") {
                        formatter = _this.formatterBoolean;
                        sortable = true;
                    } else if (columnAttr.fieldType === "ENUM") {
                        formatter = _this.formatterEnum;
                        sortable = true;
                    } else if (columnAttr.fieldType === "DATE") {
                        formatter = _this.formatterDateWithOptions({dateFormat: columnAttr.fieldFormat});
                        sortable = true;
                    } else if (columnAttr.fieldType === "FILE") {
                        formatter = _this.formatterFile;
                        sortable = true;
                    } else if (columnAttr.fieldType === "LONG_LINK") {
                        sortable = false;
                    } else if (columnAttr.fieldType === "HETERO_LINK") {
                        formatter = _this.formatterHeteroLink;
                        sortable = false;
                    }


                    var column = {
                        field: columnAttr.entityFieldName,
                        title: columnAttr.description,
                        align: 'left',
                        valign: 'top',
                        sortable: sortable,
                        formatter: formatter
                    };

                    if (!columnAttr.useInSimpleSearch) {
                        column.class = 'disabled-simple'
                    }
                    columns.push(column);
                });


                //Формируем значения колонок таблицы
                var tableData = this.createTableDataFromValue();
                //Получаем количество объектов
                var totalRows = this.model.get('recordsCount');
                //Текущая страница
                var pageNumber = this.model.get('currentPage');
                //Количество объектов на странице
                var pageSize = this.model.get('objectsPerPage');
                //Наименование столбца для сортировки
                var sortName = this.model.get('orderAttr');
                //Тип сортировки
                var sortOrder = this.model.get('orderType');

                this.$table.bootstrapTable({
                    //toolbar: _this.$toolbar,
                    idField: 'objectId',
                    data: tableData,
                    cache: false,
                    striped: true,
                    pagination: true,
                    pageNumber: pageNumber,
                    pageSize: pageSize,
                    pageList: [5, 10, 20, 50],
                    totalRows: totalRows,
                    sortName: sortName,
                    sortOrder: sortOrder,
                    showColumns: false,
                    /*                    minimumCountColumns: 1,*/ //True to show the columns drop down list.
                    search: false,
                    clickToSelect: false,
                    sidePagination: "server",
                    columns: columns
                }).on('page-change.bs.table', function (e, number, size) {
                    console.log('Event: page-change.bs.table, data: ' + number + ', ' + size);
                    _this.model.changePage(number, size);
                }).on('sort.bs.table', function (e, name, order) {
                    console.log('Event: sort.bs.table, data: ' + name + ', ' + order);
                    _this.model.changeSort(name, order);
                }).on('check.bs.table', function (e, row) {
                    var ids = [row.objectId];
                    _this.model.addSelectedIds(ids);
                    if(ids) $('.btnDelete').removeClass('disabled')
                }).on('uncheck.bs.table', function (e, row) {
                    var ids = [row.objectId];
                    _this.model.removeSelectedIds(ids);
                    if(_.isEmpty(_this.model.get('selectedIds'))) $('.btnDelete').addClass('disabled')
                }).on('check-all.bs.table', function (e) {
                    var selects = _this.$table.bootstrapTable('getData');
                    var ids = $.map(selects, function (row) {
                        return row.objectId;
                    });
                    _this.model.addSelectedIds(ids);
                    if(ids) $('.btnDelete').removeClass('disabled')
                }).on('uncheck-all.bs.table', function (e) {
                    var selects = _this.$table.bootstrapTable('getData');
                    var ids = $.map(selects, function (row) {
                        return row.objectId;
                    });
                    _this.model.removeSelectedIds(ids);
                    if(_.isEmpty(_this.model.get('selectedIds'))) $('.btnDelete').addClass('disabled')

                }).on('dbl-click-cell.bs.table', function (e, field, value, row, $element) {
                    var attr = _.findWhere(data.attrList, {entityFieldName: field});
                    var object = _.findWhere(data.objectList, {objectId: row.objectId});

                    /* If object edit not locked by another user */
                    if (!object.lock) {
                        /* First implementation works only with strings */
                        if (attr && _.contains(["STRING", "DATE"], attr.fieldType) && attr.canEdit) {
                            if (field !== 'objectId' && !$element.data('edited')) {
                                /* Create editable field */
                                _this.formatterEdit(attr, field, value, row, $element, function (value, previousValue) {
                                    /* if value changed */
                                    if(value != previousValue) {
                                        /* Check mandatory fields */
                                        if(attr.mandatory && value == "") {
                                            var message = new Message({
                                                title: "Ошибка данных",
                                                message: "Значение не должно быть пустым"
                                            })
                                            message.show()
                                            $element.text(previousValue ? previousValue : '-').data('edited', false)
                                        } else {
                                            var data = {}
                                            data[field] = value

                                            _this.model.callServerAction({
                                                action: 'saveObject',
                                                data: {
                                                    objectId: row.objectId,
                                                    className: row.className,
                                                    data: JSON.stringify(data)
                                                }
                                            }, function (res) {
                                                $element.text(value ? value : '-').data('edited', false)
                                            })
                                        }
                                    } else {
                                        $element.text(value ? value : '-').data('edited', false)
                                    }
                                })
                            }
                        }
                    } else {
                        var message = new Message({
                            title: "Ошибка редактирования",
                            message: "Объект заблокирован для редактирования <i>" + object.lock.time + "</i> пользователем <b>" + object.lock.user + "</b>"
                        })
                        message.show()
                    }
                });



                /* Add title for simple-search-disabled icon */
                this.$('th.disabled-simple').attr('title', 'Не доступно в простом поиске.')

            },

            /**
             * Hack & update generated bootstrap table
             */
            updateObjectListTable: function () {
                var _this = this;

                /* If selected list count equals count of all records mark model as isAllSelected */
                if (this.model.get('selectedList') && this.model.get('recordsCount')) {
                    if (this.model.get('selectedList').length == this.model.get('recordsCount')) {
                        this.model.set('isAllSelected', true)
                    }
                }

                /* Add new event listener */
                this.$table.on('check-all-list.bs.table', function (e, el) {
                    /* Get checkbox state and save in model */
                    var state = $(el).is(':checked');
                    _this.model.set('isAllSelected', state);

                    /* Update checkboxes & button state */
                    _this.$table.find('input[name="btSelectItem"]').prop('checked', state).prop('disabled', state);
                    _this.$table.find('input[name="btSelectAll"]').prop('checked', state).prop('disabled', state);

                    if (state) {
                        /* Add reset for selected id's */
                        _this.model.set('selectedIds', []);
                        $('.btnDelete').removeClass('disabled')
                    } else {
                        /* If all object was already selected and isAllSelected becomes to false - add all objects to remove list  */
                        if (_this.model.get('selectedList') && _this.model.get('recordsCount')) {
                            if (_this.model.get('selectedList').length == _this.model.get('recordsCount')) {
                                _this.model.removeSelectedIds(_this.model.get('selectedList'));
                            }
                        }
                        $('.btnDelete').addClass('disabled')
                    }
                });

                /* Hack bootstrap-table, add second selectAll checkbox */
                this.$('input[name="btSelectAll"]').parent().html('<nobr><input name="btSelectAll" type="checkbox" /> Стр.</nobr> <br><nobr><input name="btRealSelectAll" type="checkbox" /> Всё</nobr>');

                /* If checked isRealSelectAll checkbox change state for item checkboxes and button */
                if (this.model.get('isAllSelected')) {
                    this.$table.find('input[name="btRealSelectAll"]').prop('checked', true);
                    this.$table.trigger('check-all-list.bs.table', $('input[name="btRealSelectAll"]'))
                }

                /* On change isRealSelectAll checkbox state fire table event */
                this.$el.off('change').on('change', 'input[name="btRealSelectAll"]', function () {
                    _this.$table.trigger('check-all-list.bs.table', this)
                })

                // Подключаем внешний скролл
                this.helpers.tableExtendedScrollInit.apply(this);
            },

            getSelectMode: function(){
                var _this = this;
                return {
                    field: 'state',
                    checkbox: true,
                    formatter: function (value, row, index) {
                        return _this.stateFormatter(value, row, index)
                    }
                }
            },

            /**
             * заполнение данных об объектах списка
             *
             */
            createTableDataFromValue: function () {
                var data = [];
                this.model.get('objectList').objectList.forEach(function (objectData) {
                    data.push($.extend(objectData.attrValues, {objectId: objectData.objectId, className: objectData.mlClass}));
                });
                return data;
            },

            /**
             *  форматтер для отображения выбранных элементов (чекбокс)
             */
            stateFormatter: function (value, row, index) {
                var _objectId = row.objectId;
                return {
                    checked: (_.contains(this.model.get('selectedIds'), _objectId))
                }
            },

            /**
             * скачивание файла
             *
             */
            downloadFile: function (e) {
                e.preventDefault()
                var _this = this;
                var objectId = $(e.currentTarget).attr('objectId');
                var attrName = $(e.currentTarget).attr('attrName');
                this.model.callServerAction(
                    {
                        action: 'downloadFile',
                        data: {
                            objectId: objectId,
                            className: _this.model.get('className'),
                            attrName: attrName
                        }
                    },
                    function (result) {
                        window.location = result.url;
                    }
                );
            }
        });
        _.extend(ListBlockView.prototype, table_formatter_mixin);
        return view;
    });
