<div class="attr-label-container col-xs-offset-<%=attrModel.get('offset')%> col-xs-<%=attrModel.get('titleLength')%>">
    <b class="attr-label"><%=attrModel.get('description')%>:</b>
</div>

<div class="select2holder col-xs-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%>">
    <select class="attrField"></select>
</div>

<!--
<div class="col-md-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%>">
    <%if (attrModel.get('value')){%>
        <%=attrModel.get('value').title%>
    <%}%>
</div>
-->
