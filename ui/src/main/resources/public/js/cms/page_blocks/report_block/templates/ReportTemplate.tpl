<div class="container control-panel col-xs-24" id="view_buttons_container">
    <h4 class="form-title col-xs-10 callback">
        Отчет "<%=reportModel.get('title')%>"
    </h4>
</div>
<div id="report_container">

</div>
<div id="controllPanel">
    <button class="btn btn-primary generate-report-button" click-action="generateClick">
                        <span class="glyphicon glyphicon-ok"></span> Сформировать отчет
                    </button>
</div>
