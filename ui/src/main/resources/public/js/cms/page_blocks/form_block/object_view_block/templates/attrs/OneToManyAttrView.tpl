<div class="attribute-view-onetomany">
    <div class="col-xs-offset-<%=attrModel.get('offset')%> col-xs-<%=attrModel.get('titleLength')%> attribute-label-field">
        <b class="attr-label"><%=attrModel.escape('description')%>:</b>
    </div>
    <div class="col-xs-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%>">
        <div class="list-controls">
            <input type="text" class="attr-list-search form-control input-sm" placeholder="Поиск...">
        </div>
        <span class="attr-list-preview"></span>
        <div class="text-right col-xs-24 table-controls">
            <span class="table-filter-toggle btn btn-sm btn-info" title="Фильтр в таблице"><i class="glyphicon glyphicon-filter"></i></span>
            <span class="table-expand-full btn btn-sm btn-info" title="Развернуть таблицу на всю высоту"><i class="glyphicon glyphicon-resize-full"></i></span>
        </div>
    </div>
    <div class="newLine attr-list-details col-xs-<%=(attrModel.get('totalLength'))%>"
         style=" display: none; padding: 0px 10px;">

        <div class="ml-main__content-wrap scrollbar-external_wrapper" style="overflow: auto; max-height: <%=attrModel.get('tableHeight')%>px;">
            <div class="ml-main__content-inner scrollbar-external">
                <table class="ml-main__content-table table-javascript">

                </table>
            </div>
            <div class="external-scroll_x">
                <div class="scroll-element_outer">
                    <div class="scroll-element_size"></div>
                    <div class="scroll-element_track"></div>
                    <div class="scroll-bar"></div>
                </div>
            </div>
        </div>

        <!--
            <table class="table-javascript">
            </table>-->
    </div>
</div>


