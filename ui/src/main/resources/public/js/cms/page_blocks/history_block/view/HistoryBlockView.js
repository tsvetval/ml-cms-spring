define([
        'boot_table_ru',
        'moment',
        'underscore',
        'cms/view/PageBlockView',
        'table_formatter_mixin',
        'text!cms/page_blocks/list_block/object_list_block/templates/ObjectTableTemplate.tpl'
    ],
    function (boot_table, moment, _, PageBlockView, formatter, ObjectTableTemplate) {
        var view = PageBlockView.extend({
            initialize:function () {
                console.log("Initialize HistoryBlockView");
                this.listenTo(this.model, 'render', this.render)
            },
            events:{
                "click .file-download-link": "downloadFile"
            },

            render:function () {
                var _this = this
                this.$el.html($(_.template(ObjectTableTemplate, {})));
                this.$table = this.$el.find('table');

                var columns = []
                this.model.get('historyData').headers.forEach(function (item) {
                    var column = {
                        field: item.entityFieldName,
                        title: item.description,
                        align: 'left',
                        valign: 'top'
                    };

                    /**/
                    if (item.fieldType == 'DATE') {
                        column.formatter = _this.dateFormatter
                    }
                    if (item.fieldType == 'FILE') {
                        column.formatter = _this.formatterFile
                    }
                    columns.push(column);
                })

                this.$table.bootstrapTable({
                    //toolbar: _this.$toolbar,
                    idField: 'objectId',
                    data: this.model.get('historyData').dataList,
                    cache: false,
                    striped: true,
                    //pagination: true,
                    //pageNumber: this.model.get('historyData').pageCurrent,
                    //pageSize: pageSize,
                    //pageList: [5, 10, 20, 50],
                    //totalRows: totalRows,
                    //sortName: sortName,
                    //sortOrder: sortOrder,
                    //showColumns: false,
                    /*                    minimumCountColumns: 1,*/ //True to show the columns drop down list.
                    //search: false,
                    //clickToSelect: false,
                    sidePagination: "server",
                    columns: columns
                })

                this.$el.find('.scrollbar-external').scrollbar({
                    "autoScrollSize": true,
                    "scrollx": this.$el.find('.external-scroll_x')
                });
                this.$el.find('.external-scroll_x').css('top', this.$el.find('thead').first().height() - 10);

                this.model.set('blockRendered', true)
            },

            dateFormatter: function (value, row) {
                moment.locale('ru')
                return moment(new Date(value)).calendar()
                //new Date для совместимости, скоро не iso строки в качестве аргументов будут deprecated
                //Issue: https://github.com/moment/moment/issues/1407
            },
            formatterFile: function (value, row, index) {
                if (!value || value.length == 0) {
                    return "";
                }
                var file = JSON.parse(value)
                var link = $('<a>', {
                    text: file.fileName,
                    objectId: file.objectId,
                    attrName: file.entityFieldName,
                    startDate: file.startDate,
                    endDate: file.endDate,
                    href: '#',
                    "class": 'file-download-link'
                });
                return link[0].outerHTML;
            },

            downloadFile: function (e) {
                var _this = this;
                var objectId = $(e.currentTarget).attr('objectId');
                var attrName = $(e.currentTarget).attr('attrName');
                var startDate = $(e.currentTarget).attr('startDate');
                var endDate = $(e.currentTarget).attr('endDate');
                this.model.callServerAction(
                    {
                        action: 'downloadFile',
                        data: {
                            objectId: objectId,
                            startDate:startDate,
                            endDate:endDate,
                            className: _this.model.get('className'),
                            attrName: attrName
                        }
                    },
                    function (result) {
                        window.location = result.url;
                    }
                );
            }

        });

        return view;
    });
