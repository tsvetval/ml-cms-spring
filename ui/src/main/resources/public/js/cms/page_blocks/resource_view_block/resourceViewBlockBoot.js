/*
* Создает шаблон лоя pageBlock(JS для Backbone и Java контроллер)
* Контроллер: ru.ml.web.block.controller.impl.PageWizardBlockController
* */


define(
    ['log', 'misc', 'backbone', 'cms/page_blocks/resource_view_block/model/ResourceViewBlockModel', 'cms/page_blocks/resource_view_block/view/ResourceViewBlockView'],
    function (log, misc, backbone, model, view) {
        return {
            model: model,
            view: view
        };
    });
