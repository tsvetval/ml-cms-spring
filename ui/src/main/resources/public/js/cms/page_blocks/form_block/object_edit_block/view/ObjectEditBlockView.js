/**
 * Представление блока создания объекта
 */
define(
    ['log', 'misc', 'backbone', 'underscore',
        'cms/page_blocks/form_block/FormBlockView',
        'markup',
        'cms/events/events',
        'cms/page_blocks/DialogPageBlock',
        'cms/page_blocks/form_block/collections/AttrGroupCollection',
        'cms/page_blocks/form_block/factories/GroupViewFactory',
        'cms/page_blocks/form_block/factories/AttrViewFactory',
        'text!cms/page_blocks/form_block/object_edit_block/templates/ObjectEditTemplate.tpl',
        'text!cms/page_blocks/form_block/object_edit_block/templates/lockModalTemplate.tpl'],
    function (log, misc, backbone, _, FormBlockView, markup, cmsEvents, Message, AttrGroupCollection,
              GroupViewFactory, AttrViewFactory, ObjectEditTemplate, LockModalTemplate) {
        var view = FormBlockView.extend({

            events: _.extend({
                "click .btnSave": "saveObjectClick",
                "click .btnSaveAndClose": "saveAndCloseObjectClick",
                "click .btnClose": "cancelEditObjectClick",
                "click .btnDelete": "deleteObjectClick"
            }, FormBlockView.prototype.events),

            /**
             * Инициализация представления
             */
            initialize: function () {
                log.debug("initialize ObjectViewBlockView");
                _.extend(this.events, FormBlockView.prototype.events);
                this.viewMode = 'EDIT';
                this.listenTo(this.model, 'render', this.render);
                this.listenTo(this.model, 'renderAttribute', this.renderAttribute);

                this.listenTo(this.model.get('pageModel'), cmsEvents.BLOCKS_RENDER_COMPLETE, this.helpers.resizeTitle);
                this.listenTo(this.model, 'object:locked', this.lockEditObject)
            },

            /**
             * Обработка клика по кнопке "Удалить"
             * @returns {boolean}
             */
            deleteObjectClick: function () {
                var _this = this;
                log.debug('Click delete event catched');
                var dialogConfirm = new Message({
                    title: "Подтверждение удаления",
                    message: "Вы действительно хотите удалить объект?",
                    type: 'dialogMessage'
                });
                dialogConfirm.show({
                    okAction: function () {
                        _this.model.deleteObject();
                    }
                });
            },

            /**
             * Обработка клика по кнопке "Сохранить"
             * @returns {boolean}
             */
            saveObjectClick: function () {
                this.model.saveObject();
            },

            /**
             * Обработка клика по кнопке "Сохранить и закрыть"
             * @returns {boolean}
             */
            saveAndCloseObjectClick: function () {
                this.model.saveAndCloseObject();
            },

            /**
             * Обработка клика по кнопке "Отмена"
             * @returns {boolean}
             */
            cancelEditObjectClick: function () {
                this.model.cancelEditObject();
            },

            /**
             * Отрисовка представления
             */
            render: function () {
                var _this = this;
                this.$el.html(_.template(ObjectEditTemplate, {formModel: this.model,canDelete:_this.model.get('canDelete')}));
                // create container
                var $content_container = _this.$el.find('#view_content_container');
                _this._renderNonGroupAttrs($content_container);
                // Выводим группы
                _this._renderRootGroups($content_container);

                this.lockEditObject();

                this.model.set('blockRendered', true);
                return true;
            },

            lockEditObject: function () {
                var _this = this;

                if (this.model.get('objectLocked')) {

                    this.$('.ml-main__content-class-form-controls-submit').hide();
                    var $modal = $(_.template(LockModalTemplate, {lock: this.model.get('objectLocked')}));
                    this.$el.append($modal);

                    $modal.modal('show');
                    $modal.find('#force-relock').off('click').on('click', function (e) {
                        _this.model.relockObject().done(function (response) {
                            $modal.on('hidden.bs.modal', function () {
                                $modal.remove()
                            });
                            $modal.modal('hide');
                            _this.$('.ml-main__content-class-form-controls-submit').show();
                            _this.helpers.resizeTitle();
                        }).fail(function (response) {
                            $modal.find('#force-relock-error').show()
                        })
                    })
                }
            }
        });

        return view;
    });
