/*
* Создает шаблон лоя pageBlock(JS для Backbone и Java контроллер)
* Контроллер: ru.ml.web.block.controller.impl.PageWizardBlockController
* */


define(
    ['log', 'misc', 'backbone', 'cms/page_blocks/wizard_block/model/WizardBlockModel', 'cms/page_blocks/wizard_block/view/WizardBlockView'],
    function (log, misc, backbone, model, view) {
        return {
            model: model,
            view: view
        };
    });
