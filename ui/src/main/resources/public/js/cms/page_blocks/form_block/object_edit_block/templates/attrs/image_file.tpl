<div class="attribute-edit-image_file">
    <div class="attr-label-container col-xs-offset-<%=attrModel.get('offset')%> col-xs-<%=attrModel.get('titleLength')%>">
        <b class="attr-label"><%=attrModel.escape('description')%>:</b>
    </div>
    <div class="col-xs-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%>">


        <%if ((attrModel.get('value') && attrModel.get('value').objectId) || (attrModel.get('value') && attrModel.get('value').url)){%>
        <div class="row preview-handler">
            <div class="panel panel-default">
                <div class="panel-heading pull-right text-right">
                    <div class="col-xs-24 ">
                        <div class="btn-group">
                            <% if (attrModel.get('value').objectId && !attrModel.get('value').deleted) { %>
                            <span class="btn btn-success file-download-link"
                                  title="Скачать файл"
                                  objectId="<%=attrModel.get('value').objectId%>">
                                <i class="glyphicon glyphicon-download"></i> Скачать изображение
                            </span>
                            <% } %>
                            <span class="btn btn-danger buttonDelete" style="overflow: hidden;position: relative;">
                                <i class="glyphicon glyphicon-trash"></i> Удалить изображение
                            </span>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="col-xs-6">
                        <img src="" class="image-preview img-responsive">
                    </div>
                </div>
            </div>


        </div>
        <%} else {%>
        <div class="row">
            <div class="col-xs-24">
                <span class="image-drop-zone" style="overflow: hidden;position: relative;">
                    <input type="file" class="attrField"/>
                    <div class="drop-zone-text">
                        Перетяните изображение сюда
                    </div>
                </span>
            </div>
        </div>
        <% } %>


    </div>
</div>
