define(
    ['log', 'misc', 'backbone', 'cms/events/events',
        'cms/model/PageBlockModel', 'cms/page_blocks/DialogPageBlock', 'jstree'
    ],
    function (log, misc, backbone, cmsEvents, PageBlockModel, Message, jstree ) {
        var model = PageBlockModel.extend({
            defaults: {

                controller: 'securitySettingsController'
            },
            initialize: function () {
                /* PageBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this)

                console.log("initialize BlockModel");
            },

            /**
             * Добавление событий блока
             * @Override PageBlockModel.bindEvents
             */
            bindEvents: function () {
                /* PageBlockModel.bindEvents() добавляет основные события блока */
                model.__super__.bindEvents.call(this)

                var _this = this;
                this.listenTo(this, cmsEvents.RESTORE_PAGE, function (params) {
                    _this.prompt();
                });
            },

            /**
             * Остановка обработчиков событий блока
             * @Override PageBlockModel.unbindEvents
             */
            unbindEvents: function () {
                /*  События блока */
                this.stopListening(this, cmsEvents.RESTORE_PAGE);
                /* PageBlockModel.unbindEvents() удаляет все основные события блока */
                model.__super__.unbindEvents.call(this)
            },

            prompt: function () {
                var _this = this;
                var options = {
                    action: 'getRoles',
                    data: {
                        pageBlockId: this.get('blockInfo').get('id'),
                        ml_request: true
                    }
                };
                _this.callServerAction(options).then(function(result){
                    _this.set("roles",result.roles);
                    _this.trigger(cmsEvents.RENDER_VIEW);
                })
            },
            getRoleInfo: function(roleId){
                var _this = this;
                var options = {
                    action: 'getRoleInfo',
                    data: {
                        roleId: roleId,
                        pageBlockId: this.get('blockInfo').get('id'),
                        ml_request: true
                    }
                };
                _this.callServerAction(options).then(function(result){
                    _this.set("role",result.role);
                    _this.trigger("renderRoleInfo");
                })
            },
            getClassesForShow: function(){
                var _this = this;
                var options = {
                    action: 'getClasses',
                    data: {
                        roleId: _this.get("role").id,
                        pageBlockId: this.get('blockInfo').get('id'),
                        ml_request: true
                    }
                };
                _this.callServerAction(options).then(function(result){
                    _this.set("classes",result.classes);
                    _this.trigger("renderClassesPanel");
                })
            },
            saveClsAccess: function(data){
                var _this = this;
                var options = {
                    action: 'saveClassAccess',
                    data: {
                        roleId: _this.get("role").id,
                        value: JSON.stringify(data),
                        pageBlockId: this.get('blockInfo').get('id'),
                        ml_request: true
                    }
                };
                _this.callServerAction(options).then(function(result){
                    if(result.result){
                        _this.getRoleInfo(_this.get("role").id);
                    }
                })
            },
            getUtilsForShow:function(){
                var _this = this;
                var options = {
                    action: 'getUtils',
                    data: {
                        roleId: _this.get("role").id,
                        pageBlockId: this.get('blockInfo').get('id'),
                        ml_request: true
                    }
                };
                _this.callServerAction(options).then(function(result){
                    _this.set("utils",result.utils);
                    _this.trigger("renderUtilsPanel");
                })
            },
            saveUtilAccess: function(data){
                var _this = this;
                var options = {
                    action: 'saveUtilAccess',
                    data: {
                        roleId: _this.get("role").id,
                        value: JSON.stringify(data),
                        pageBlockId: this.get('blockInfo').get('id'),
                        ml_request: true
                    }
                };
                _this.callServerAction(options).then(function(result){
                    if(result.result){
                        _this.getRoleInfo(_this.get("role").id);
                    }
                })
            },

            /**
             * Get all resources for role
             */
            getResources: function(){
                var _this = this;
                var options = {
                    action: 'getResources',
                    data: {
                        roleId: _this.get("role").id,
                        pageBlockId: this.get('blockInfo').get('id'),
                        ml_request: true
                    }
                };
                _this.callServerAction(options).then(function(result){
                    _this.set("resources", result.resources);
                    _this.trigger("renderResourcePanel");
                })
            },

            /**
             * Save resource access for role
             * @param data
             */
            saveResourcesAccess: function(data){
                var _this = this;
                var options = {
                    action: 'saveResourcesAccess',
                    data: {
                        roleId: _this.get("role").id,
                        value: JSON.stringify(data),
                        pageBlockId: this.get('blockInfo').get('id'),
                        ml_request: true
                    }
                };
                _this.callServerAction(options).then(function(result){
                    if(result.result){
                        _this.getRoleInfo(_this.get("role").id);
                    }
                })
            },

            saveFolderAccess: function (access) {
                var _this = this;
                var options = {
                    action: 'saveFolderAccess',
                    data: {
                        roleId: _this.get("role").id,
                        value: JSON.stringify(access)
                    }
                }

                this.callServerAction(options).then(function(result){
                    if(result.result){
                        _this.getRoleInfo(_this.get("role").id);
                    }
                })
            },

            getFoldersForShow: function(){
                var _this = this;
                var options = {
                    action: 'getFolders',
                    data: {
                        roleId: _this.get("role").id,
                        pageBlockId: this.get('blockInfo').get('id'),
                        ml_request: true
                    }
                };
                _this.callServerAction(options).then(function(result){
                    _this.set("folders",result.folders);
                    _this.trigger("renderFoldersPanel");
                })
            },
            createRole: function(roleName){
                var _this = this;
                var options = {
                    action: 'createRole',
                    data: {
                        roleName: roleName,
                        pageBlockId: this.get('blockInfo').get('id'),
                        ml_request: true
                    }
                };
                _this.callServerAction(options).then(function(result){
                    _this.prompt();
                })
            }
        });
        return model;
    });
