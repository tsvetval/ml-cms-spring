<div class="modal fade">
    <div class="modal-dialog" id="lock-edit-object-modal">
        <div class="modal-content">
            <div class="modal-header">
                Объект заблокирован для редактирования
                <div class="close" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i></div>
            </div>
            <div class="modal-body">
                Объект заблокирован для редактирования <i><%= lock.time%></i> пользователем <b><%= _.escape(lock.user) %></b>
                <br><br>
                Редактирование станет доступным после окончания блокировки пользователем. Так же вы можете
                принудительно забрать блокировку.
            </div>
            <div class="modal-footer">
                <div class="pull-left">
                    <div class="text-danger" id="force-relock-error" style="display: none;">Не получилось снять блокировку</div>
                </div>
                <div class="pull-right">
                    <button class="btn btn-danger" id="force-relock">Забрать блокировку</button>
                    <button class="btn btn-default" data-dismiss="modal">Отмена</button>
                </div>
            </div>
        </div>
    </div>
</div>