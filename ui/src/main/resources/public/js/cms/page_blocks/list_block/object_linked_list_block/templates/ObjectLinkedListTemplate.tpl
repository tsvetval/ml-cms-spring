<div class="ml-main__content-title-text">Просмотр объектов <%=listModel.escape('description')%></div>


    <div class="pull-right control-buttons">
        <div>
            <div>
                <button class="btn btn-primary save-button">
                    <span class="glyphicon glyphicon-ok"></span> Сохранить
                </button>
                <button class="btn btn-primary edit-button">
                    <span class="glyphicon glyphicon-edit"></span> Редактировать
                </button>
                <button class="btn btn-primary cancel-button">
                    <span class="glyphicon glyphicon-repeat"></span> Отмена
                </button>
            </div>
        </div>
    </div>
</div>
<div class="ml-main__content-delimiter"></div>
<div class="object-list-container"></div>
<div class="ml-main__content-delimiter"></div>
