/**
 * @name: UserBlockModel
 * @package:
 * @project: ml-cms-all
 * Created by i_tovstyonok on 19.01.2016.
 */

define(
    ['log', 'misc', 'cms/model/PageBlockModel', 'cms/events/events'],
    function (log, misc, PageBlockModel, cmsEvents) {
        var model = PageBlockModel.extend({
            defaults: {
                controller: 'authController'
            },

            initialize: function () {
                /* PageBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this);

                log.debug('Initialize UserBlockModel')
            },

            /**
             * Добавление событий блока
             * @Override PageBlockModel.bindEvents
             */

            bindEvents: function () {
                /* PageBlockModel.bindEvents() добавляет основные события блока */
                model.__super__.bindEvents.call(this);

                this.on(cmsEvents.RESTORE_PAGE, this.fetch)
            },

            /**
             * Остановка обработчиков событий блока
             * @Override PageBlockModel.unbindEvents
             */
            unbindEvents: function () {
                this.off(cmsEvents.RESTORE_PAGE);

                /* PageBlockModel.unbindEvents() удаляет все основные события блока */
                model.__super__.unbindEvents.call(this)
            },

            fetch: function () {
                var _this = this;
                this.callServerAction("GET", "/site/auth/user-data").then(function (result) {
                    if (result.helpUrl){
                        _this.set('helpUrl', result.helpUrl);
                    } else {
                        _this.unset('helpUrl')
                    }

                    if (result.login){
                        _this.set('login', result.login);
                        _this.trigger(cmsEvents.RENDER_VIEW);
                    } else {
                        _this.unset('login')
                        /* Unset user preferences */
                        $.removeCookie('homePage', { path: misc.getContextPath() + '/'});
                        $.removeCookie('roles', { path: misc.getContextPath() + '/'});
                    }
                })
            },

            logout: function () {
                return this.callServerAction("POST", "/site/auth/logout").then(function(result){
                    /* Unset user preferences */
                    $.removeCookie('homePage', { path: misc.getContextPath() + '/'});
                    $.removeCookie('roles', { path: misc.getContextPath() + '/'});

                    /* Route to logout */
                    window.location.href = result.logoutUrl;
                });
            }

        });

        return model
    }
);