/**
 *  Модель блока выбора связанных объектов
 *
 */
define(
    ['log', 'misc', 'backbone',
        'cms/events/events',
        'cms/page_blocks/list_block/object_list_block/model/ObjectListBlockModel',
        'cms/events/NotifyPageBlocksEventObject'
    ],
    function (log, misc, backbone, cmsEvents ,ObjectListBlockModel, NotifyEventObject) {
        var model = ObjectListBlockModel.extend({
            defaults: {
                objectId: undefined,
                mode: 'viewMode',// режим просмотра/редактирования - значения = viewMode/editMode
                controller: 'linkedListController'
            },

            /**
             * инициализация модели
             */
            initialize: function () {
                /* ObjectListBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this);

                log.debug("initialize ListBlockModel");
            },

            /**
             * Добавление событий блока
             * @Override ObjectListBlockModel.bindEvents
             */
            bindEvents: function () {
                /* ObjectListBlockModel.bindEvents() добавляет основные события блока */
                model.__super__.bindEvents.call(this)

                /*
                 * Слушаем событие выбора объектов для ссылочных атрибутов
                 * */
                this.listenTo(this, cmsEvents.OBJECTS_SELECTION_DONE, this.objectsSelectionDone);
                this.listenTo(this, cmsEvents.LINK_OBJECT_CREATED, this.linkObjectCreated);
            },

            /**
             * Остановка обработчиков событий блока
             * @Override ObjectListBlockModel.unbindEvents
             */
            unbindEvents: function () {
                /* События блока */
                this.stopListening(this, cmsEvents.OBJECTS_SELECTION_DONE);
                this.stopListening(this, cmsEvents.LINK_OBJECT_CREATED);

                /* ObjectListBlockModel.unbindEvents() удаляет все основные события блока */
                model.__super__.unbindEvents.call(this)
            },

            restorePageListener: function(params){
                var _this = this;

                log.debug("ObjectListBlockModel start restorePage");
                _this.set('refAttrId', params.refAttrId);
                _this.set('className', params.className);
                _this.set('description', params.description);
                _this.set('selectMode', params.selectMode);
                _this.set('objectId', params.objectId);
                _this._reset();
                _this._update();
            },
            /**
             * Добавление списка идентификаторов к списку связанных объектов
             * @param ids
             */
            addSelectedIds: function (ids) {
                var notInSelectedList = _.difference(ids, this.get('selectedList'));
                this.set('addIdList', _.union(this.get('addIdList'), notInSelectedList));
                this.set('removeIdList', _.difference(this.get('removeIdList'), ids));

            },

            /**
             * Удаление списка идентификаторов из списка связанных объектов
             * @param ids
             */
            removeSelectedIds: function (ids) {
                var inSelectedListForRemove = _.intersection(ids, this.get('selectedList'));
                this.set('removeIdList', _.union(this.get('removeIdList'), inSelectedListForRemove));
                this.set('addIdList', _.difference(this.get('addIdList'), ids));

            },

            /*
             * Получаем данные с сервера
             * */
            _loadListData: function () {
                var options = {
                    action: 'getObjectData',
                    data: {
                        objectId: this.get('objectId'),
                        className: this.get('className'),
                        refAttrId: this.get('refAttrId'),
                        currentPage: this.get('currentPage'),
                        objectsPerPage: this.get('objectsPerPage'),
                        orderAttr: this.get('orderAttr'),
                        orderType: this.get('orderType'),
                        pageBlockId: this.get('blockInfo').get('id'),
                        ml_request: true

                    }
                };
                if (this.get("search")) {
                    $.extend(options.data, this.get("search"));
                }
                return this.callServerAction(options);
            },

            /**
             * Обновление заголовка страницы
             * @private
             */
            _updatePageTitle: function () {
                var title = "Просмотр объектов " + this.get('description');
                this.updatePageTitle(title);
            },


            /*
             * После создания линкованого объекта
             * */
            linkObjectCreated: function (params) {
                var _this = this;
                var refAttrId = misc.option(params, "refAttrId", "Атрибут для обновления");
                var objectId = misc.option(params, "objectId", "ID созданного объекта", []);
                var title = misc.option(params, "title", "ID созданного объекта", []);

                var currentValue = this.get('objectList');
                var ids = currentValue.objectList.map(function(obj){return obj.objectId});
                ids = _.union(ids,objectId);
                this.set('objectList', {idList: ids, title: undefined});
            },

            /**
             * После выбора ссылочных объектов
             * @param params
             */
            objectsSelectionDone: function (params) {
                var addIdList = misc.option(params, "addIdList", "Список ID для добавления", []);
                var removeIdList = misc.option(params, "removeIdList", "Список ID для удаления", []);
                // ищем модель атрибута и проставляем ему новое значение
                var currentValue = this.get('objectList');
                var newValueIdList = [];
                // Удаляем удаленные
                if (currentValue && currentValue.objectList) {
                    currentValue.objectList.forEach(function (object) {
                        if (!_.contains(removeIdList, object.objectId)) {
                            newValueIdList.push(object.objectId)
                        }
                    })
                }
                // Добавляем новые
                newValueIdList = _.union(newValueIdList, addIdList);
                this.set('objectList', {idList: newValueIdList, title: undefined});
            },


            /**
             * Сохранение объекта
             * @returns {boolean}
             */
            saveObject: function () {
                var _this = this;
                var idList = [];
                var objectList = this.get('objectList').objectList;
                $.each(objectList, function (index, elem) {
                    idList.push(elem.objectId)
                });

                // Отправляем запрос на сохранение
                var options = {
                    action: 'saveObject',
                    data: {
                        objectId: _this.get('objectId'),
                        pageBlockId: this.get('blockInfo').get('id'),
                        refAttrId: _this.get('refAttrId'),
                        idList: JSON.stringify(idList),
                        ml_request: true
                    }
                };
                _this.callServerAction(options).then(function () {
                    _this.notifyPageBlocks(new NotifyEventObject(
                        'changeObject',
                        {value: _this.get('objectList')},
                        'all'

                    ));
                    _this.closePage();
                });
            }
        });
        return model;
    });
