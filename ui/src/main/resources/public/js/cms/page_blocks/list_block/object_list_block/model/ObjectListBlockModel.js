/**
 * Модель блока списка объектов
 *
 */
define(
    ['log', 'misc', 'backbone',
        'cms/events/events',
        'cms/page_blocks/list_block/ListBlockModel',
        'cms/events/NotifyPageBlocksEventObject'
    ],
    function (log, misc, backbone, cmsEvents,
              ListBlockModel, NotifyEventObject) {
        var model = ListBlockModel.extend({
            defaults: {
                controller: 'objectListController'
            },
            /**
            * инициализация модели
            *
            */
            initialize: function () {
                /* ListBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this)

                log.debug('initialize ObjectListBlockModel');
                this.set("pageTitle", 'Просмотр папки <%=folderTitle%>');
            },

            /**
             * Добавление событий блока
             * @Override ListBlockModel.bindEvents
             */

            bindEvents: function () {
                /* ListBlockModel.bindEvents() добавляет основные события блока */
                model.__super__.bindEvents.call(this)

                this.listenTo(this,"changeObject", this.onObjectChange)
            },

            /**
             * Остановка обработчиков событий блока
             * @Override ListBlockModel.unbindEvents
             */
            unbindEvents: function () {
                this.stopListening(this,"changeObject")

                /* ListBlockModel.unbindEvents() удаляет все основные события блока */
                model.__super__.unbindEvents.call(this)
            },

            onObjectChange: function(objectData){
                var _this = this;
                var className = _this.get("className");
                if(className == objectData.className){
                    var object = _this.get("objectList").objectList.filter(function(data){ return data.objectId == objectData.objectId});
                    if(object.length>0){
                        _this.trigger(cmsEvents.REFRESH_PAGE);
                    }
                }
            }
        });
        return model;
    });
