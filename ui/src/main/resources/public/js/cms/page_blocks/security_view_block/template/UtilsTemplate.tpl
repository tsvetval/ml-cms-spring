<div id="utilAccessModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Измениить доступ на утилиты</h4>
                <button type="button" class="btn btn-small btn-sm btn-primary save-util-access">Сохранить</button>
                <button type="button" class="btn btn-small btn-sm btn-danger" data-dismiss="modal">Отмена</button>
            </div>
            <div class="modal-body">
                <span>Доступ к утилитам</span>
                <ul class="nav nav-tabs" role="tablist">
                    <% _.each(utils, function(utilAccess) { %>
                    <li role="presentation"><a href="#util<%=utilAccess.id%>" aria-controls="util<%=utilAccess.id%>" role="tab" data-toggle="tab"><%=utilAccess.name%></a>
                    </li>
                    <% }); %>
                </ul>

                <div class="tab-content">
                    <% _.each(utils, function(utilAccess) { %>
                    <div role="tabpanel" class="tab-pane fade in" id="util<%=utilAccess.id%>">
                        <ul class="list-group">
                            <div style="height: 500px; overflow: auto; margin: 20px;">
                                <% _.each(utilAccess.utils, function(util) { %>
                                <li class="list-group-item">
                                    <input class="util-id" name="<%=util.id%>" type="checkbox"
                                    <%if(util.checked){%>checked="checked"<%}%>>
                                    <%= util.name %>
                                </li>
                                <% }); %>
                            </div>
                        </ul>
                    </div>
                    <% }); %>
                </div>
            </div>
        </div>
    </div>
</div>
