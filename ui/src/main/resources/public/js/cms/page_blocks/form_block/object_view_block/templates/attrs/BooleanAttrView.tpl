<div class="attribute-view-boolean">
    <div class="col-xs-offset-<%=attrModel.get('offset')%> col-xs-<%=attrModel.get('titleLength')%>">
        <b class="attr-label"><%=attrModel.escape('description')%>:</b>
    </div>
    <div class="col-xs-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%>">
        <%if (attrModel.get('value')){%>
        <i class="glyphicon glyphicon-ok" style="color: green;"></i>
        <% }else{ %>
        <i class="glyphicon glyphicon-remove" style="color: red;"></i>
        <%}%>

    </div>
</div>
