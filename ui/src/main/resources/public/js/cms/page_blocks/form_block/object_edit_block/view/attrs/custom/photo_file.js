/**
 * Представление для атрибута типа FILE
 */
define(
    ['log', 'misc', 'backbone', 'moment', 'jquery','jcrop','jqueryUi', 'webcamjs',
        'cms/page_blocks/form_block/object_edit_block/view/attrs/custom/image_file',
        'text!cms/page_blocks/form_block/object_edit_block/templates/attrs/photo_file.tpl',
        'text!cms/page_blocks/form_block/object_edit_block/templates/attrs/photo_crop_window.tpl'
        ],
    function (log, misc, backbone, moment, $, jcrop,jqueryUi, Webcam, ImageFileView, DefaultTemplate, photoCrop) {
        var view = ImageFileView.extend({
            $inputField: undefined,
            $inputFieldFake: undefined,
            $buttonDelete: undefined,
            sizeFactor: undefined,
            width: undefined,
            height: undefined,
            fileName : undefined,
            jcrop_api : undefined,

            events: {
                "click .buttonPhoto": "doPhoto"
            },

            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                _.extend(this.events, ImageFileView.prototype.events);

                /* Webcam.js initial parameters */
                Webcam.setSWFLocation(misc.getContextPath() + '/public/js/libs/webcamjs/webcam.swf');
                Webcam.set('constraints', {
                    optional: [
                        { minWidth: 600 }
                    ]
                });
            },

            /**
             * Отрисовка представления
             */
            render: function () {
                var _this = this;
                if (this.isHidden()) {
                    return;
                }

                this.$el.html(_.template(this.viewTemplate, {
                    attrModel: this.model
                }));

                var title = '';
                this.$buttonDelete = this.$el.find('.buttonDelete');
                if (this.model.get('value')) {
                    title = this.model.get('value').title;
                    this.$buttonDelete.find('label').addClass('highlight-button');
                }

                this.$buttonDelete.on('click', function () {
                    _this.deleteFile();
                });

                this.addMandatoryEvents();
                this.addReadOnly();
                this.addPopoverHelper();
                this.$inputField = this.$el.find('.attrField');
                this.$inputFieldFake = this.$el.find('.attrField_fake');
                this.$inputFieldFake.val(title);
                this.sizeFactor = 0.8;
                this.height = 600;
                this.width = 480;

                /* Add drop zone */
                this.$inputDropZone = this.$el.find('.image-drop-zone');
                this.$inputDropZone.on('dragover', function (e) {
                    $(this).addClass('over')
                })
                this.$inputDropZone.on('dragleave drop', function (e) {
                    $(this).removeClass('over')
                })

                /* Добавляем превью изображения */
                this.$inputFieldPreview = this.$el.find('.image-preview');
                if(this.model.get('value') && this.model.get('value').objectId) {
                    this.model.callServerAction(
                        {
                            action: 'downloadFile',
                            data: {
                                objectId: _this.model.get('value').objectId,
                                className: _this.model.get('className'),
                                attrName: _this.model.get('entityFieldName')
                            }
                        },
                        function (result) {
                            _this.model.get('value').url = result.url
                            _this.$inputFieldPreview.attr('src', result.url)
                        }
                    );
                } else {
                    if (this.model.get('value') && this.model.get('value').url) {
                        _this.$inputFieldPreview.attr('src', this.model.get('value').url)
                    }
                }



            },

            doPhoto: function(){
                var _this = this;
                _this.$el.append(_.template(photoCrop,{sizeFactor:_this.sizeFactor,width: _this.width,height:_this.height}));
                _this.$el.find("#photo").dialog( {
                    title:"Фотография",
                    dialogClass:"no-close",
                    autoOpen:true,
                    width:1100,
                    height:625,
                    modal: true,
                    buttons: {
                        "Закрыть": function() {
                            $( this ).dialog( "close" );
                        }
                    },
                    close: function () {
                        //уничтожение dom-объекта модального окна
                        if ($('#camWindow').hasClass('ui-dialog-content')) {
                            $('#camWindow').dialog('destroy');
                            //Webcam.reset()
                        }
                        $(this).dialog('destroy').remove();
                        $('#camWindow').remove()
                    }
                });

                _this.$el.find("#photo").dialog('open');

                /* Показ\скрытие маски изображения  */
                $("#showMask").off('click').on('click', function(){_this.showOrHideMask()});

                /* Загрузка файла для кропа */
                $("#file-crop").off('change').on('change', function(){
                    _this.uploadFile(_this.getPhotoInfo);
                });



                /* Старт апплета камеры */
                $("#onCamera").off('click').on("click",function(){

                    $('#camWindow').dialog({
                        title:"Сделать снимок",
                        dialogClass:"no-close",
                        autoOpen:true,
                        width:940,
                        height:650,
                        modal: true,
                        open: function () {
                            var v = Date.now();

                            Webcam.set({
                                dest_width: 1000,
                                dest_height: 750,
                                jpeg_quality: 100,
                                flip_horiz: true,
                                upload_name: 'webcam_photo_' + v
                            });
                            Webcam.on('live', function () {
                                $('#take-cam-image').show().off('click').on('click', function () {
                                    Webcam.snap(function (data) {
                                        var host = misc.getContextPath()+ "/upload?userLogin="+$.cookie('userLogin');
                                        Webcam.upload(data, host, function (code, raw) {
                                            $('#camWindow').dialog('close');
                                            _this.getPhotoInfo('webcam_photo_' + v)
                                        })
                                    })
                                });
                            });
                            Webcam.attach('#webCamDialog')

                        },
                        close: function () {
                            Webcam.reset();
                        }
                    });
                });
            },

            showOrHideMask: function(){
                var _this = this;
                var mask = $('#photomask');
                if($("#showMask").is(':checked')){
                    mask.css("height", _this.height*_this.sizeFactor);
                    mask.css("width", _this.width*_this.sizeFactor);
                    mask.show();
                }else{
                    mask.hide();
                }
            },
            uploadFile: function(callback){
                var _this = this;
                var importFile = $('#file-crop')[0].files[0];
                console.log('Start uploading file: ' + importFile.name);
                var xhr = new XMLHttpRequest();
                var url = misc.getContextPath() + "/upload";
                xhr.onreadystatechange = function (e) {
                    if (4 == this.readyState) {
                        console.log('File uploaded: ' + e.target.responseText);
                        callback.apply(_this, [importFile.name]);
                    }
                };
                xhr.open('post', url, true);
                var fd = new FormData;
                fd.append(importFile.name,importFile);
                xhr.send(fd);

            },

            getPhotoInfo: function(name){
                var _this = this;
                if (_this.jcrop_api) {
                    _this.jcrop_api.destroy();
                    $('#photo_full').removeAttr("style");
                }
                _this.fileName = name;
                $.ajax({
                    type:"GET",
                    url: misc.getContextPath() +'/util/crop',
                    data:{'fileName':name,action:'getInfo'}
                }).then(
                        function(resp){
                            _this.showPhoto(resp);
                        }
                    )
            } ,
            showPhoto: function(info){
                var _this = this;
                var photoFull = $('#photo_full');
                photoFull.attr("src",  misc.getContextPath()+"/getTempFile?fileName="+encodeURI(info.fileName));

                photoFull.css("width", info.width);
                photoFull.css("height", info.height);
                _this.model.set('boundx', info.width);
                _this.model.set('boundy', info.height);
                $('#photo_cut').attr("src",  misc.getContextPath()+"/getTempFile?fileName="+encodeURI(info.fileName));
                photoFull.css("display", 'none !important;');
                //_this.sizeFactor = info.height/info.width;
                photoFull.Jcrop({
                    setSelect:[0, 0, info.width, info.height],
                    onChange: function(c){_this.updatePreview(c)},
                    onSelect: function(c){_this.saveDate(c)},
                    aspectRatio:_this.sizeFactor//info.height/info.width//
                }, function () {
                    var bounds = this.getBounds();
                    _this.model.set('boundx', bounds[0]);
                    _this.model.set('boundy', bounds[1]);
                    _this.jcrop_api = this;
                });
            },
            updatePreview: function(c){
                var _this = this;
                if (parseInt(c.w) > 0) {
                    var rx = _this.width / c.w;
                    var ry =  _this.height  / c.h;
                    var factor = _this.sizeFactor;

                    $('#photo_cut').css({
                        width:Math.round(rx * factor * _this.model.get('boundx')) + 'px',
                        height:Math.round(ry * factor * _this.model.get('boundy')) + 'px',
                        marginLeft:'-' + Math.round(rx * c.x * factor) + 'px',
                        marginTop:'-' + Math.round(ry * c.y * factor) + 'px'
                    });
                }
            },
            saveDate: function(c) {
                var _this = this;
                var rrr = c.x + ',' + c.y + ',' + c.h + ',' + c.w;
                var options = {
                    action: "savePhoto",
                    x: c.x,
                    y: c.y,
                    height: c.h,
                    width: c.w,
                    fileName:_this.fileName
                }
                $.ajax({
                    type:"POST",
                    url:misc.getContextPath() +'/util/crop',
                    async:false,
                    data:options
                }).then(function (data) {
                    _this.$el.find('.drop-zone-text').text(data.fileName);
                    _this.model.attributes['value'] = {fileName : data.fileName, url: misc.getContextPath()+"/getTempFile?fileName="+encodeURI(data.fileName) + '&t=' + new Date().getMilliseconds()};
                    _this.render()
                });
                _this.updatePreview(c);
        }

        });

        return view;
    });
