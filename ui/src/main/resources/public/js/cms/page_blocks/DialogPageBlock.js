/**
 * отображает диалог. Не используется
 *
 */
define(['jquery'],function($){
    var DialogPageBlock = function(data){

        this.$dimmer = $('<div>', {
            id: 'modal_' + new Date().getTime(),
            class: 'modal dimmer'
        });

        var $dialog = $('<div class="modal-dialog">');
        $dialog.appendTo(this.$dimmer);

        var $content = $('<div class="modal-content">');
        $content.appendTo($dialog);

        var $header = $('<div class="modal-header">');
        $header.appendTo($content);

        var $body = $('<div class="modal-body">');
        $body.appendTo($content);

        var $footer = $('<div class="modal-footer">');
        $footer.appendTo($content);

        var $dialogHeader = $('<button type="button" class="close">×</button><h2 class="modal-title">' + data.title +'</h2>');
        $dialogHeader.appendTo($header);

        var $dialogBody = $('<div>' + data.message.split('\n').join('<br/>') + '</div>');
        $dialogBody.appendTo($body);

        if(data.type == 'stacktraceMessage'){
            var $stackTraceCaption = $('<div class="stacktrace-header">Дополнительные сведения:</div>');
            $stackTraceCaption.appendTo($body);
            var $stackTraceBody = $('<div class="stacktrace-container"><code>' + data.stacktrace.split('\n').join('<br/>') + '</code></div>');
            $stackTraceBody.appendTo($body);
            $stackTraceBody.hide();
            $stackTraceCaption.on('click', function(){
                var $this = $(this);
                if($this.hasClass('expanded')){
                    $this.removeClass('expanded');
                    $stackTraceBody.slideUp();
                } else {
                    $this.addClass('expanded');
                    $stackTraceBody.slideDown();
                }
            });
        }

        this.$okButton = $('<button type="button" class="btn dialog-button btn-primary ok-btn">OK</button>');
        this.$okButton.appendTo($footer);

        if(data.type == 'dialogMessage'){
            this.$cancelButton = $('<button type="button" class="btn dialog-button btn-default ok-cancel">Отмена</button>');
            this.$cancelButton.appendTo($footer);
        }

        this.$dimmer.appendTo($('body'))
    };

    DialogPageBlock.prototype.hide = function(){
        var _this = this;
        this.$dimmer.fadeOut(500, function () {
            _this.$dimmer.remove()
        });
    };

    DialogPageBlock.prototype.show = function(options){
        var _this = this;

        /* Run callback, hide & remove on click OK button*/
        this.$dimmer.find('.ok-btn').on('click', function(){
            if(options && typeof options.okAction == 'function'){
                options.okAction.call();
            }
            _this.hide();
        });

        /* Hide & remove on click close or cancel button*/
        this.$dimmer.find('.close, .ok-cancel').on('click', function () {
            _this.hide()
        })
        this.$dimmer.fadeIn();
        this.$dimmer.find('.ok-btn').focus();
    };

    return DialogPageBlock;
});
