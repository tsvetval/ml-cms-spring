define(
    ['log', 'misc', 'backbone', 'cms/events/events',
    'cms/model/PageBlockModel','cms/page_blocks/report_block/factories/ParamCollection',
    'cms/page_blocks/report_block/model/ParamModel'],
    function (log, misc, backbone, cmsEvents, PageBlockModel, ParamCollection,ParamModel) {
        var model = PageBlockModel.extend({

            defaults: {
                objectId: undefined,
                description: undefined,
                title: undefined,
                paramsList: undefined,
                controller: 'reportExecController'
            },

            initialize: function () {
                /* PageBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this)

                console.log("initialize BlockModel");
                this.set('paramsList', new ParamCollection());

            },

            /**
             * Добавление событий блока
             * @Override PageBlockModel.bindEvents
             */
            bindEvents: function () {
                /* PageBlockModel.bindEvents() добавляет основные события блока */
                model.__super__.bindEvents.call(this)

                var _this = this;
                this.listenTo(this, cmsEvents.RESTORE_PAGE, function (params) {
                    var objectId = params.objectId;
                    _this.set("objectId", objectId);
                    _this.prompt();
                });
                this.listenTo(this, cmsEvents.OBJECTS_SELECTION_DONE, this.objectsSelectionDone);
            },

            /**
             * Остановка обработчиков событий блока
             * @Override PageBlockModel.unbindEvents
             */
            unbindEvents: function () {
                /*  События блока */
                this.stopListening(this, cmsEvents.RESTORE_PAGE);
                this.stopListening(this, cmsEvents.OBJECTS_SELECTION_DONE);

                /* PageBlockModel.unbindEvents() удаляет все основные события блока */
                model.__super__.unbindEvents.call(this)
            },

            prompt: function () {
                var _this = this;
                var options = {
                    action: "show",
                    data: {
                        objectId: _this.get('objectId')
                    }
                };

                var callback = function (metaData) {
                    var result = metaData.MetaData;
                    _this.set('title', result.name);
                    if (result.params) {
                        result.params.forEach(function (paramInfo) {
                                var param = new ParamModel(paramInfo);
                                param.set('pageBlockModel', _this);
                                _this.get('paramsList').add(param);
                                _this.listenTo(param, cmsEvents.OPEN_PAGE, function (event) {
                                    _this.openPage(event.url, event.title, event.params)
                                });
                            }
                        );
                    }
                    _this.trigger(cmsEvents.RENDER_VIEW);
                };

                return this.callServerAction(options, callback);
            },

            generateReport: function(){
                var _this = this;
                //TODO
                // Пробегаем по всем атрибутам и собираем значения
                var notFilledMandatoryAttrs = [];
                var dataForSave = {};
                this.get('paramsList').forEach(function (param) {
                    //Для каждого атрибута проверяется обязательность
                    if (param.isMandatory() && !param.hasValue()) {
                        notFilledMandatoryAttrs.push(param);
                        param.triggerNotFilledMandatory();
                    } else {
                        param.triggerRemoveHighlightMandatory();
                    }
                    // Устанавливаем значение атрибута
                    dataForSave[param.getParamCode()] = param.serializeValue();
                });
                if (notFilledMandatoryAttrs.length > 0) {
                    return false;
                }
                // Отправляем запрос на сохранение
                var options = {
                    action: 'generateReport',
                    data: {
                        objectId: _this.get('objectId'),
                        className: _this.get('entityName'),
                        pageBlockId: _this.get('blockInfo').get('id'),
                        refAttrId: _this.get('pageModel').get('refAttrId'),
                        data: JSON.stringify(dataForSave),
                        ml_request: true
                    }
                };



                this.callServerAction(options).then(function (result) {
                    if (result.downloadLink) {
                        window.location = result.url;
                    }else{
                        alert(result);
                    }
                })

            },
            /*
             * После выбора ссылочных объектов
             * */
            objectsSelectionDone: function (params) {
                var paramId = misc.option(params, "paramId", "Атрибут для обновления");
                var addIdList = misc.option(params, "addIdList", "Список ID для добавления", []);
                var removeIdList = misc.option(params, "removeIdList", "Список ID для удаления", []);
                // ищем модель атрибута и проставляем ему новое значение
                var paramModel = this.get('paramsList').get(paramId);
                var currentValue = paramModel.get('value');
                var newValueIdList = [];
                //TODO refactor this вынести логику в модель атрибутов
                // Удаляем удаленные
                if (currentValue && currentValue.objectList) {
                    currentValue.objectList.forEach(function (object) {
                        if (!_.contains(removeIdList, object.objectId)) {
                            newValueIdList.push(object.objectId)
                        }
                    })
                }
                // Добавляем новые id
                newValueIdList = _.union(newValueIdList, addIdList);
                paramModel.set('value', {idList: newValueIdList, title: undefined});
            },

            action: function (param) {
                var _this = this;
                var options = {
                    action: "someAction",
                    data: {
                        param: param
                    }
                };

                var callback = function (result) {
                    _this.set("serverData", result.serverData);
                };

                return this.callServerAction(options, callback);
            }
        });
        return model;
    });
