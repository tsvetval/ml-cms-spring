<div class="ml-main__content-title">
    <div class="ml-main__content-title-text" title="<%=formModel.get('title')%>"><%=formModel.escape('title')%></div>
</div>

<div class="btnClose">
    Отмена
</div>


<div class="ml-main__content-class-form-controls-submit">
    <div class="btnSave">Сохранить</div>
</div>


<div class="ml-main__content-delimiter"></div>
<div class="ml-main__content-object-columns" id="view_content_container"></div>

<div style="clear: both"></div>

</div>
