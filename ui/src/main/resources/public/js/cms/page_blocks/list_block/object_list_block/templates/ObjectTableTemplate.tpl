<div class="ml-main__content-wrap scrollbar-external_wrapper">
    <div class="ml-main__content-inner scrollbar-external">
        <table class="ml-main__content-table">

        </table>
    </div>
    <div class="external-scroll_x">
        <div class="scroll-element_outer">
            <div class="scroll-element_size"></div>
            <div class="scroll-element_track"></div>
            <div class="scroll-bar"></div>
        </div>
    </div>
</div>