/**
 * @name: loadKladrModel
 * @package:
 * @project: ml-cms-all
 * Created by i_tovstyonok on 12.04.2016.
 */

define(
    ['log', 'misc', 'cms/model/PageBlockModel',
        'cms/events/events'],
    function (log, misc, PageBlockModel, Events) {
        var model = PageBlockModel.extend({
            defaults: {
                controller: 'loadKladrController',
                pageTitle: "Импорт КЛАДР"
            },

            initialize: function () {
                /* PageBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this);

                log.debug('Initialize loadKladrModel')
            },

            /**
             * Добавление событий блока
             * @Override PageBlockModel.bindEvents
             */

            bindEvents: function () {
                /* PageBlockModel.bindEvents() добавляет основные события блока */
                model.__super__.bindEvents.call(this);

                this.on(Events.RESTORE_PAGE, this.process)
            },

            /**
             * Остановка обработчиков событий блока
             * @Override PageBlockModel.unbindEvents
             */
            unbindEvents: function () {
                this.off(Events.RESTORE_PAGE);

                /* PageBlockModel.unbindEvents() удаляет все основные события блока */
                model.__super__.unbindEvents.call(this)
            },

            process: function () {
                var _this = this;
                this.updatePageTitle(this.get('pageTitle'));
                this.callServerAction({
                    action: 'loadStatus'
                }).then(function (response) {
                    _this.set('import', response);
                    if (response.loadStatus) {
                        _this.startUpdateStatus()
                    }
                })

            },

            uploadImportFile: function (file) {
                var _this = this;
                var data = new FormData();
                data.append(file.name, file);

                $.ajax({
                    url: misc.getContextPath() + '/upload',
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();

                        /* Magic progress bar builder */
                        xhr.upload.addEventListener("progress", function(e){
                            if (e.lengthComputable) {
                                var percentComplete = Math.floor((e.loaded / e.total) * 100);
                                _this.trigger('upload:progress', percentComplete)
                            }
                        }, false);

                        return xhr
                    },
                    method: 'post',
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (response) {
                        _this.startImport(file.name)
                    },
                    error: function (xhr) {
                        console.info('Upload fail:', xhr)
                    }
                })
            },

            startImport: function (filename) {
                var _this = this;
                this.callServerAction({
                    action: 'start',
                    data: {
                        filename: filename
                    }
                }).then(function () {
                    _this.startUpdateStatus()
                })
            },

            startUpdateStatus: function () {
                /* Clear interval for status update*/
                if (this.get('statusInterval')) clearInterval(this.get('statusInterval'));

                var _this = this;
                var statusInterval = setInterval(function () {
                    _this.updateStatus();
                }, 10 * 1000); //10 sec
                _this.set('statusInterval', statusInterval)
            },

            updateStatus: function () {
                var _this = this;
                if (this.get('statusInterval')) {
                    this.callServerAction({
                        action: 'loadStatus',
                        onFail: function (response) {
                            console.info("Getting import status failed: ", response)
                        }
                    }).then(function (response) {
                        if (!response.loadStatus) {
                            clearInterval(_this.get('statusInterval'));

                            /* Set only log, not loadStatus, suppress render form with file input */
                            _this.get('import').log = response.log;
                            _this.trigger('change:import');
                            _this.trigger('import:complete');
                        } else {
                            _this.set('import', response);
                        }
                    })
                }
            },

            resetImport: function () {
                this.unset('import')
            }

        });

        return model
    }
);