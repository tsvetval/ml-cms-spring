define(
    ['log', 'misc', 'backbone', 'cms/events/events', 'cms/model/PageBlockModel', 'cms/events/NotifyPageBlocksEventObject'],
    function (log, misc, backbone, cmsEvents, PageBlockModel, NotifyEventObject) {

        var model = PageBlockModel.extend({
            defaults: {
                /*Идентификатор текущего каталога дерева если есть*/
                folderId: undefined,
                className: undefined,
                refAttrId: undefined,
                refClassName: undefined,
                search: undefined,
                searchMetaData: undefined,
                controller: 'searchObjectsController'
            },

            initialize: function () {
                /* PageBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this)

                console.log("initialize SimpleSearchBlockModel");
            },

            /**
             * Добавление событий блока
             * @Override PageBlockModel.bindEvents
             */
            bindEvents: function () {
                /* PageBlockModel.bindEvents() добавляет основные события блока */
                model.__super__.bindEvents.call(this)

                var _this = this;
                this.listenTo(this, cmsEvents.RESTORE_PAGE, this.prompt);
                this.listenTo(this, cmsEvents.OPEN_FOLDER, this.prompt);
                this.listenTo(this, 'change:search', function (params) {
                    if (this.get('search')) {
                        var s = params.get('search');
                        s.refClassName = params.get('refClassName');
                        params.set('search', s);
                        _this.notifyPageBlocks(new NotifyEventObject(cmsEvents.SEARCH, params.get("search")));
                    }
                });
            },

            /**
             * Остановка обработчиков событий блока
             * @Override PageBlockModel.unbindEvents
             */
            unbindEvents: function () {
                /*  События блока */
                this.stopListening(this, cmsEvents.RESTORE_PAGE);
                this.stopListening(this, cmsEvents.OPEN_FOLDER);
                this.stopListening(this, 'change:search');

                /* PageBlockModel.unbindEvents() удаляет все основные события блока */
                model.__super__.unbindEvents.call(this)
            },

            prompt: function (params) {
                var _this = this;
                _this.set("refClassName", params.refClassName);
                _this.set("refAttrId", params.refAttrId);
                if (params.folderId) {
                    _this.set("folderId", params.folderId);
                }

                var folderId = _this.get('folderId'),
                    refClassName = _this.get('refClassName'),
                    refAttrId = _this.get('refAttrId'),
                    className = _this.get('pageModel').get('className');
                if (!folderId && !className && !refAttrId) {
                    console.log("[folderId], [className] and [refAttrId] are missing in [getSearchMetaData] action of [SearchBlock]. Request rejected.")
                    return false;
                }

                _this.callServerAction({
                        action: "getSearchMetaData",
                        data: {
                            folderId: _this.get('folderId'),
                            refClassName: _this.get('refClassName'),
                            refAttrId: _this.get('refAttrId'),
                            className: _this.get('pageModel').get('className'),
                            objectId:  params.objectId,
                            type: "simple"
                        }
                    },
                    function (result) {
                        if (result.ok) {
                            delete result.ok;
                            _this.set("searchMetaData", result);
                            _this.trigger(cmsEvents.RENDER_VIEW);
                        } else {
                            log.debug("No result for search, generate hide event")
                            _this.trigger(cmsEvents.HIDE_VIEW);
                        }
                    }
                );
            }
        });
        return model;
    });
