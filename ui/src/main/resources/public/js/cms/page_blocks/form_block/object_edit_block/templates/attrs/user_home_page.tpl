<div class="attribute-edit-homePage">
    <div style="margin-bottom: 2px;" class="col-xs-offset-<%=attrModel.get('offset')%> col-xs-<%=attrModel.get('titleLength')%>">
        <b class="attr-label" style="height: 24px; line-height: 24px;  float: left;margin-right: 50px;"><%=attrModel.escape('description')%>:</b>
    </div>
    <div class="select2holder col-xs-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%>">
        <select class="attrField select2-hidden-accessible"></select>
    </div>
</div>