/**
 * Загрузочный модуль блока редактирования объекта
 */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/form_block/object_constructor_block/model/ObjectConstructorBlockModel',
        'cms/page_blocks/form_block/object_constructor_block/view/ObjectConstructorBlockView'],
    function (log, misc, backbone, model, view) {
        return {
            model : model,
            view : view
        };
    });
