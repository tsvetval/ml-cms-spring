define(
    ['log', 'misc', 'backbone', 'cms/events/events', 'cms/model/PageBlockModel', 'markup', 'cms/page_blocks/DialogPageBlock', 'cms/events/NotifyPageBlocksEventObject'],
    function (log, misc, backbone, cmsEvents, PageBlockModel, markup, Message, NotifyEventObject) {
        var model = PageBlockModel.extend({
            defaults: {
                parent: undefined
            },
            initialize: function () {
                /* PageBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this)

                console.log("initialize UtilModel");
            },

            renderUtil: function ($utilContainer) {
                //this.renderPageBlock($utilContainer);
                var utilViewClass = this.get('blockInfo').get('backBoneClasses').view;
                var utilView = new utilViewClass({model: this});
                utilView.setElement($utilContainer);
                this.trigger(cmsEvents.RENDER_VIEW);

            },
            openURL: function (data) {
                var utilInfo = this.get('blockInfo');
                var params = _.clone(utilInfo.get('params'));
                var _this = this;

                function exec() {
                    if (utilInfo.method == 'GET') {
                        if (utilInfo.openType == 'CRAMB') {
                            _this.fillDataForOpenUrl(function (data) {
                                _this.parent.openPage(utilInfo.url, 'Просмотр объекта ...', _.extend(params, data));
                            })
                        } else if (utilInfo.openType == 'TAB') {
                            window.open(misc.getContextPath() + utilInfo.url, '_blank');
                        } else if (utilInfo.openType == 'WINDOW') {
                            window.open(misc.getContextPath() + utilInfo.url, '_blank', 'toolbar=0,location=0,menubar=0');
                        }
                    } else {
                        _this.fillDataForExec(data)
                    }
                }

                if (utilInfo.confirmExec) {
                    var dialog = new Message({
                        title: 'Подтверждение',
                        message: utilInfo.confirmExecMsg,
                        type: 'dialogMessage'
                    });
                    dialog.show({
                        okAction: exec
                    });
                }
                else {
                    exec();
                }
            },
            execUtil: function (data) {
                var _this = this;
                var params = _.clone(_this.get("blockInfo").get('params')); // clone for disable update params in model
                params.utilId = this.get('blockInfo').get('id');
                params.pageBlockId = _this.get('blockModel').get('blockInfo').get('id');
                params.ml_request = true;

                $.extend(params, data);

                var callback = function (result) {
                    _this.showResult(result);
                };

                var url = misc.getContextPath() + this.get("blockInfo").url;
                var requestData = params;
                var def = new $.Deferred();
                $.ajax(url, {
                    type: 'POST',
                    data: requestData,
                    success: function (responseData) {
                        if (callback) callback.apply(_this, [responseData, requestData]);
                        def.resolve(responseData);
                    },
                    error: function (result, textStatus, errorThrown) {
                        if (result.readyState != 0) {
                            if (result.status == 400) {
                                console.log("Application Error: " + result.responseJSON.message);
                                _this.displayErrorMessage("Ошибка приложения", result.responseJSON.message);
                            } else if (result.status == 403) {
                                if (result.responseJSON && result.responseJSON.needAuth) {
                                    indow.location = result.responseJSON .url;
                                } else {
                                    console.log("Security Error: " + result.responseJSON.message);
                                    _this.displayErrorMessage("Ошибка доступа", result.responseJSON.message);
                                }
                            } else if (result.status == 555) {
                                if (result.responseJSON.stacktrace) {
                                    console.log("Stack Trace: " + result.responseJSON.stacktrace);
                                } else {
                                    result.responseJSON.stacktrace = "Дополнительные сведения отсутствуют."
                                }
                                _this.displayExtendedErrorMessage("Ошибка сервера", result.responseJSON.message, result.responseJSON.stacktrace);
                            } else {
                                /* All other errors: 500, 502 e.t.c */
                                _this.displayErrorMessage('Ошибка запроса в блоке #' + data.pageBlockId, "Статус ошибки: " + result.status)
                            }
                        }


                        log.error('Error while exec util, params:' + JSON.stringify(params),
                            "\nStatus: " + textStatus +
                            "\nError: " + errorThrown);
                    }
                });
            },

            fillDataForExec: function (data) {
                var _this = this;
                //TODO rewrite this
                var pbData = [];
                /* todo: refactor */
                this.parent.notifyPageBlocks(new NotifyEventObject(cmsEvents.GET_SELECTED_IDS, {
                        callback: function (param, filter) {
                            if (filter) {
                                data = _.extend(data, filter);
                            }

                            /* Массив id в json @see /peak/ml/web/utils/controller/utils/AbstractUtilController.java:43 */
                            if (param.selectedIds) {
                                param.selectedIds = JSON.stringify(param.selectedIds)
                            }

                            data = _.extend(data, param);
                        }
                    }, null, null, function onFinish() {
                        _this.execUtil(data);
                    }
                ));
            },


            /** todo: refactor
             */
            fillDataForOpenUrl: function (callback) {
                var _this = this;
                var data = {};
                this.parent.notifyPageBlocks(new NotifyEventObject(cmsEvents.GET_SELECTED_IDS, {
                        callback: function (param, filter) {
                            data = _.extend(data, param, filter);
                        }
                    }, null, null, function onFinish() {
                        callback(data);
                    }
                ));
            },


            execUtilWizard: function (data) {
                var _this = this;
                var params = _this.get("blockInfo").get('params');
                $.extend(params, data);

                var options = {
                    action: data.action,
                    data: params
                };
                var callback = function (result) {
                    _this.showResult(result);
                };

                return this.callServerAction(options, callback);
            },

            execComboBoxUtil: function (data) {
                var _this = this;
                var param = _this.get("blockInfo").get('params');
                var options = {
                    action: $('#' + data.comboId).val(),
                    data: param
                };
                var callback = function (result) {
                    _this.showResult(result);
                };

                return this.callServerAction(options, callback);
            },
            showResult: function (result) {
                if (result.showType) {
                    if (result.showType == 'modal') {
                        this.showModal(result);
                    } else if (result.showType == 'execJs') {
                        this[result.functionName](result);
                    }
                }
                else if (result.downloadLink) {
                    window.location = result.url;
                }
            },
            showModal: function (result) {
                this.parent.showModal(result);
            },

            openHistory: function (result) {
                this.navigationEvent({
                    "do": 'push',
                    "title": 'Просмотр истории объекта ...',
                    "url": '/history',
                    "params": result.params
                });
            }

        });

        return model;
    });