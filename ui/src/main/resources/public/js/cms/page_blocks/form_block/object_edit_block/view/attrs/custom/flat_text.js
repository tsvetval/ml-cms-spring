/**
 * Представление flat_text для атрибута типа TEXT
 */
define([
        'cms/page_blocks/form_block/object_edit_block/view/EditAttrView',
        'text!cms/page_blocks/form_block/object_edit_block/templates/attrs/flat_text.tpl'
    ],
    function (EditAttrView, DefaultTemplate) {
        var view = EditAttrView.extend({
            $inputField: undefined,

            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                _.extend(this.events, EditAttrView.prototype.events);
            },


            /**
             * Отрисовка представления
             */
            render: function () {
                var _this = this;
                if (this.isHidden()) {
                    return;
                }
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));
                this.$inputField = this.$el.find('textarea');

                this.$inputField.on('input', function () {
                    _this.keyPressed(_this.$inputField.val());
                });
                this.$inputField.val(this.model.get('value'));
                this.addMandatoryEvents();
                this.addReadOnly();
                this.addPopoverHelper()
            },

            /**
             * Обработчик изменения значения HTML-элемента
             */
            keyPressed: function (value) {
                this.model.set('value', value);
            }
        });

        return view;
    });
