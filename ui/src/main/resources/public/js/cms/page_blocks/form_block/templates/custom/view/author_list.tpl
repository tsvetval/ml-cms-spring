<div class="col-xs-offset-<%=attrModel.get('offset')%> col-xs-<%=attrModel.get('titleLength')%>">
    <b><%=attrModel.escape('description')%>:</b>
</div>
<div class="col-xs-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%>">
    <select multiple style="width: 100%">
        <% _.each(attrModel.get('value').objectList, function(author) {%>
        <option value="<%= author.objectId %>"><%= _.escape(author.title) %></option>
        <%});%>
    </select>
</div>
