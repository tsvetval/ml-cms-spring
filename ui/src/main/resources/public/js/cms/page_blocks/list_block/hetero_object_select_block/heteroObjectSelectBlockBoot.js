/**
 * Загрузочный блок для блока выбора гетерогенных ссылок.
 * Контроллер: ru.ml.web.block.controller.impl.list.HeteroObjectListSelectBlockController
 */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/list_block/hetero_object_select_block/model/HeteroObjectSelectBlockModel',
        'cms/page_blocks/list_block/hetero_object_select_block/view/HeteroObjectSelectBlockView'],
    function (log, misc, backbone, model, view) {
        return {
            model: model,
            view: view
        };
    });
