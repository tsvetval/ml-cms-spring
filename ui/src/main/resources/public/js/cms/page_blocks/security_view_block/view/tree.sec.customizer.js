(function ($, undefined) {
    "use strict";
    //var span = document.createElement('span');
    //span.className = 'glyphicons glyphicons-comments flip jstree-comment'
    var template = '<i class="jstree-icon jstree-themeicon" role="presentation"></i><span class="ml-list__bg"></span><span class="ml-list__item-title"><%=title%></span>';
    var closeTemplate = '<span class="ml-list__close"></span>';

    $.jstree.defaults.tree_sec_customizer = $.noop;
    $.jstree.plugins.tree_sec_customizer = function (options, parent) {
        this.bind = function () {
            parent.bind.call(this);
        };
        /**
         * При отрисовке нод правит html
         * @param obj
         * @param deep
         * @param callback
         * @param force_draw
         * @returns {*}
         */
        this.redraw_node = function (obj, deep, callback, force_draw) {
            var _this = this;
            var node = this.get_node(obj);
            var li = parent.redraw_node.call(this, obj, deep, callback, force_draw);
            var collapseDiv = '<div class="collapse" id="fcollapseExample'+obj+'"><div class="well">';
            var collapseEmpty = true;
            if(node.original.hasClass){
                if(node.original.hasClass =='noRigth') {
                    li.classList.add("test")
                    collapseDiv += '<span class="label label-warning">Нет прав на класс ' +node.original.className +'</span>';
                    collapseEmpty = false;
                }
            }
            li.classList.add("tree-color")
            var li2 = $(li);
            if(node.original.accesses){
                li2.find('a').append("<span class='badge' style='margin-left: 15px;'>"+node.original.accesses.length+"</span>");
                node.original.accesses.forEach(function(access){
                    collapseDiv += '<span class="label label-warning">Доступ из ' +access +'</span>';
                    collapseEmpty = false;
                });
            }
            if(!collapseEmpty){
                collapseDiv+='</div></div>';
                li2.append(collapseDiv);
                li2.find('a').attr("data-toggle","collapse");
                li2.find('a').attr("data-target","#fcollapseExample"+obj);
                li2.find('a').attr("aria-expanded","false");
                li2.find('a').attr("aria-controls","fcollapseExample"+obj);
            }

            return li;
        };
    };
})(jQuery);