<div class="attribute-edit-onetomany">
    <div    style="margin-bottom: 2px;"
            class="attr-label-container col-xs-offset-<%=attrModel.get('offset')%> col-xs-24">
        <b class="attr-label" style="height: 24px; line-height: 24px;  float: left;margin-right: 50px;"><%=attrModel.escape('description')%>:</b>

        <% if (!attrModel.get('readOnly')) { %>

        <div class="ml-main__content-class-form-item-menu custom-toolbar">
            <div class="moveUp ml-main__content-class-form-item-menu-item m-main__content-class-form-item-menu-item_fold"
                 title="Вверх"></div>
            <div class="moveDown ml-main__content-class-form-item-menu-item m-main__content-class-form-item-menu-item_move-down"
                 title="Вниз"></div>
            <% if (!((attrModel.get('notShowCreate') && attrModel.get('viewMode') == 'CREATE') || (attrModel.get('notShowCreateInEdit') && attrModel.get('viewMode') == 'EDIT'))) { %>
                <% if (attrModel.get('createdClasses') && !_.isEmpty(attrModel.get('createdClasses'))) { %>
                <div class="btn-group create-dropdown">
                    <div class="ml-main__content-class-form-item-menu-item m-main__content-class-form-item-menu-item_add dropdown-toggle" data-toggle="dropdown" title="Создать"></div>
                    <ul class="dropdown-menu" role="menu">
                        <% _.each(attrModel.get('createdClasses'), function(cls) { %> <li> <a class="create-other-object createClick" className="<%= cls.className %>"><%= _.escape(cls.title) %></a></li> <% });  %>
                    </ul>
                </div>
                <% } else { %>
                <div class="ml-main__content-class-form-item-menu-item m-main__content-class-form-item-menu-item_add createClick" title="Создать"></div>
                <% } %>
            <% } %>
            <% if (!((attrModel.get('notShowChoose') && attrModel.get('viewMode') == 'CREATE') || (attrModel.get('notShowChooseInEdit') && attrModel.get('viewMode') == 'EDIT'))) { %>
            <div class="ml-main__content-class-form-item-menu-item m-main__content-class-form-item-menu-item_search selectLinkedObject" title="Выбрать"></div>
            <% } %>
            <% if (!((attrModel.get('notShowDelete') && attrModel.get('viewMode') == 'CREATE') || (attrModel.get('notShowDeleteInEdit') && attrModel.get('viewMode') == 'EDIT'))) { %>
            <div class="ml-main__content-class-form-item-menu-item m-main__content-class-form-item-menu-item_remove deleteLinkedObject" title="Удалить"></div>
            <% } %>
            <% if (!((attrModel.get('notShowEdit') && attrModel.get('viewMode') == 'CREATE') || (attrModel.get('notShowEditInEdit') && attrModel.get('viewMode') == 'EDIT'))) { %>
            <div class="ml-main__content-class-form-item-menu-item m-main__content-class-form-item-menu-item_right editManyToOne" title="Открыть"></div>
            <% } %>
            <span style="line-height: 34px;">Выбрано объектов: <span id="selected-list-counter">0</span></span>
        </div>
        <% } %>
    </div>
    <div>
        <!--

            <div id="custom-toolbar" class="btn-group custom-toolbar">
                   <span class="btn btn-primary moveUp inactive-button">
                      <span class="glyphicon glyphicon-chevron-up"></span>
                   </span>
                    <span class="btn btn-primary moveDown inactive-button">
                        <span class="glyphicon glyphicon-chevron-down"></span>
                    </span>
                   <span class="btn btn-primary editManyToOne inactive-button">
                      <span class="glyphicon glyphicon-pencil"></span>
                   </span>
                    <span class="btn btn-primary deleteLinkedObject inactive-button">
                        <span class="glyphicon glyphicon-trash"></span>
                    </span>
                    <span class="btn btn-primary createClick active-button">
                        <span class="glyphicon glyphicon-plus-sign"></span>
                    </span>
                    <span class="btn btn-primary selectLinkedObject active-button">
                        <span class="glyphicon glyphicon-search highlight-button"></span>
                    </span>
            </div>-->


        <div class="ml-main__content-wrap scrollbar-external_wrapper">
            <div class="ml-main__content-inner scrollbar-external">
                <table class="ml-main__content-table table-javascript">

                </table>
            </div>
            <div class="external-scroll_x">
                <div class="scroll-element_outer">
                    <div class="scroll-element_size"></div>
                    <div class="scroll-element_track"></div>
                    <div class="scroll-bar"></div>
                </div>
            </div>
        </div>
        <!--    <table class="table-javascript">
            </table>-->
    </div>
</div>