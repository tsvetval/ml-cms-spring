<div id="resourcesAccessModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Измениить доступ к ресурсам
                    <div class="pull-right">
                        <button type="button" class="btn btn-small btn-sm btn-primary save-page-access">Сохранить</button>
                        <button type="button" class="btn btn-small btn-sm btn-danger" data-dismiss="modal">Отмена</button>
                    </div>
                </h2>

            </div>
            <div class="modal-body">
                <div class="pull-right">
                    <span class="label label-success">Контроллеры</span>
                    <span class="label label-warning">Страниы</span>
                    <span class="label label-info">Группы</span>
                </div>
                <ul class="nav nav-tabs" role="tablist">
                    <% _.each(resources, function(resourceAccess) { %>
                    <li role="presentation" id="resource-access-tab-<%=resourceAccess.id%>"><a href="#resource-access-<%=resourceAccess.id%>" aria-controls="resource-access-<%=resourceAccess.id%>" role="tab" data-toggle="tab"><%=resourceAccess.name%></a>
                    </li>
                    <% }); %>
                </ul>

                <div class="tab-content">
                    <% _.each(resources, function(resourceAccess) { %>
                    <div role="tabpanel" class="tab-pane fade in resource-tree" id="resource-access-<%=resourceAccess.id%>"  style="height: 500px; overflow: auto;">
                        <ul class="list-group">
                            <!--<div style="height: 500px; overflow: auto; margin: 20px;">
                                <% _.each(resourceAccess.resources, function(resource) { %>
                                <li class="list-group-item">
                                    <input class="resource-id" name="<%=resource.id%>" type="checkbox"
                                    <%if(resource.checked){%>checked="checked"<%}%>>
                                    <%= resource.name %>
                                </li>
                                <% }); %>
                            </div>-->
                        </ul>
                    </div>
                    <% }); %>
                </div>
            </div>
        </div>
    </div>
</div>
