define(
    ['log', 'misc', 'backbone', 'jquery', 'bootstrap', 'underscore',
        'cms/page_blocks/form_block/factories/GroupViewFactory',
        'cms/page_blocks/form_block/view/groups/DefaultGroupView',
        'text!cms/page_blocks/form_block/templates/groups/AccordionViewTemplate.tpl'
    ],
    function (log, misc, backbone, $, bootstrap, _,
              GroupViewFactory, GroupView, viewTemplate) {

        /**
         * view.model is instance of AttrGroupCollection
         */
        var view = GroupView.extend({
            initialize: function (options) {
                view.__super__.initialize.apply(this, [options])
                log.debug("initialize AccordionGroupView");
            },

            render : function() {
                var _this = this;
                /* id containera*/
                this.$el.append( _.template(viewTemplate, {containerId : 'accordion-container-' + this.model.containerId}));

                var $container = this.$el.find('.panel.list-group')

                this.model.each(function(groupModel, i) {

                    $container.append($('<a>', {
                        class: 'list-group-item',
                        'data-toggle': 'collapse',
                        'data-target': '#panel-' + groupModel.getGroupId(),
                        'data-parent': '#accordion-container-' + _this.model.containerId
                    }).text(groupModel.get('title')))

                    var $item = $('<div>', {
                        class: 'list-group-item row'
                    })

                    if(groupModel.get('lazyLoad') && _this.viewMode != 'CREATE' && !groupModel.hasMandatory()) {

                        /* Event on list item expand load object data & render */
                        $(document).one('show.bs.collapse', '#panel-' + groupModel.getGroupId(), function () {
                            _this.formBlockModel._loadLazyGroupData(groupModel.getGroupId(), function () {
                                _this.renderLazyGroup(groupModel.getGroupId(), $item)
                            })
                        })
                    } else {
                        _this._renderGroupContent(groupModel, $item)
                    }

                    $container.append($('<div>', {
                        class: 'collapse',
                        id: 'panel-' + groupModel.getGroupId()
                    }).append($item))

                })
            }
        });

        return view;
    });
