<div class="col-xs-offset-<%=attrModel.get('offset')%> col-xs-<%=attrModel.get('titleLength')%>">
    <b><%=attrModel.escape('description')%>:</b>
</div>
<div class="col-xs-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%> field-icon">
    <% if (typeof(attrModel.get('iconURL')) != "undefined") { %>
    <div class="iconselect" style="background-image: url('<%=attrModel.get('iconURL')%>');"></div>
    <% } %>
</div>


