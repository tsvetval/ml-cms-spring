/**
 * Представление для отображение атрибута типа MANY_TO_MANY
 * Наследует от ONE_TO_MANY представления.
 * @see OneToManyAttrView.js
 */
define(
    ['log', 'misc', 'backbone', 'underscore', 'jquery', 'boot_table_ru', 'moment',
        'cms/page_blocks/form_block/object_view_block/view/attrs/OneToManyAttrView',
        'text!cms/page_blocks/form_block/object_view_block/templates/attrs/OneToManyAttrView.tpl',
        'table_formatter_mixin',
        'helpers/ViewHelpers'
    ],
    function (log, misc, backbone, _, $, boot_table, moment,
              OneToManyAttrView, DefaultTemplate, table_formatter_mixin, helpers) {
        var view = OneToManyAttrView.extend({

        });

        return view;
    });
