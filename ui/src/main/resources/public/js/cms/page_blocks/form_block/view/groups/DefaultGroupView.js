define(
    ['log', 'misc', 'backbone', 'bootstrap',
        'cms/page_blocks/form_block/factories/GroupViewFactory',
        'cms/page_blocks/form_block/factories/AttrViewFactory',
        'cms/page_blocks/form_block/collections/AttrGroupCollection',
        'cms/events/events'
    ],
    function (log, misc, backbone, bootstrap,
              GroupViewFactory, AttrViewFactory, AttrGroupCollection, cmsEvents) {

        var view = backbone.View.extend({
            initialize: function (options) {
                console.log("initialize DefaultGroupView");
                this.viewMode = options.viewMode;
                this.formBlockModel = options.formBlockModel;
                this.listenTo(this.model, cmsEvents.HIGHLIGHT_MANDATORY, this.highlightMandatory);
                this.listenTo(this.model, cmsEvents.REMOVE_HIGHLIGHT_MANDATORY, this.removeHighlightMandatory);
            } ,

            render : function() {
                var _this = this;
                var $content_container = $('<div/>', {
                    class : 'default-group-container',
                    id: 'default-group-container-' + this.model.getGroupId()
                });
                this.$el.append($content_container);

                /* рендер атрибутов в группе */
                var outAttrContainer = undefined;
                this.model.get('attrCollection').each(function (attrModel) {
                    if (attrModel.get('newLine') || !outAttrContainer){
                        // создаем новую строку
                        outAttrContainer = new $('<div/>', {"class": "col-xs-24 newLine", id: 'attr-out-container-' + attrModel.get('id')});
                        $content_container.append(outAttrContainer);
                    }
                    // создаем контейнер для атрибута
                    var $attrContainer = $('<div/>', {"class": "attrContainer", id: 'attr-container-' + attrModel.get('id')});
                    outAttrContainer.append($attrContainer);

                    _this._renderAttr(attrModel, $attrContainer);
                })
            },
            _renderAttr : function(attrModel, $attrContainer) {
                var _this = this
                AttrViewFactory.getAttrViewClass(attrModel, this.viewMode, $attrContainer)
                    .then(function (attrViewClass, model, $container) {
                        // Получаем представление для конкретного атрибуты
                        var attrView = new attrViewClass({model: model});
                        attrView.setElement($container);
                        attrView.model.set('viewMode', _this.viewMode)
                        attrView.render();
                    });

            },

            /**
             * Render nested groups and attributes
             * @param groupModel
             * @param $groupContainer
             * @private
             */
            _renderGroupContent: function (groupModel, $groupContainer) {
                var _this = this

                /* рендер вложенных групп */
                var tabAttrGroupCollection = new AttrGroupCollection(),
                    accordionAttrGroupCollection = new AttrGroupCollection();

                groupModel.get('subGroupCollection').each(function (subGroup) {
                    if (subGroup.getGroupType() == 'TAB_LIST') {
                        if (tabAttrGroupCollection.length == 0 && subGroup.getParentGroupId() == groupModel.getGroupId()) {
                            // Пробегаем по всем группам уровня и создаем список табов
                            groupModel.get('subGroupCollection').each(function (tmpGroup) {
                                if (tmpGroup.getGroupType() == 'TAB_LIST' && subGroup.getParentGroupId() == groupModel.getGroupId()) {
                                    tabAttrGroupCollection.add(tmpGroup)
                                }
                            });

                            var $subGroupContainer = $('<div/>', {"class": "tab-group-container", style: "clear:both;"});
                            $groupContainer.append($subGroupContainer)

                            GroupViewFactory.getGroupViewClass(subGroup).then(function(GroupViewClass){
                                var groupView = new  GroupViewClass({model: tabAttrGroupCollection, viewMode : _this.viewMode, formBlockModel: _this.formBlockModel});
                                groupView.setElement($subGroupContainer);
                                groupView.render();
                            });
                        }

                    } else if (subGroup.getGroupType() == 'ACCORDION') {
                        if (accordionAttrGroupCollection.length == 0 && !groupModel.getParentGroupId()) {

                            groupModel.get('subGroupCollection').each(function (tmpGroup) {
                                if (tmpGroup.getGroupType() == 'ACCORDION' && subGroup.getParentGroupId() == groupModel.getGroupId()) {
                                    accordionAttrGroupCollection.add(tmpGroup)
                                }
                            });

                            var $subGroupContainer = $('<div/>', {"class": "accordion-group-container", style: "clear:both;"});
                            $groupContainer.append($subGroupContainer);

                            GroupViewFactory.getGroupViewClass(subGroup).then(function (GroupViewClass) {
                                    var accordionGroupListView = new GroupViewClass({ model: _.extend(accordionAttrGroupCollection, {containerId: subGroup.getGroupId()}), viewMode : _this.viewMode, formBlockModel: _this.formBlockModel});
                                    accordionGroupListView.setElement($subGroupContainer);
                                    accordionGroupListView.render();
                                });
                        }

                    } else {
                        if (subGroup.getGroupType()) {
                            GroupViewFactory.getGroupViewClass(subGroup).then(function(GroupViewClass){
                                var groupView = new  GroupViewClass({model: subGroup, viewMode : _this.viewMode, formBlockModel: _this.formBlockModel});
                                groupView.setElement($groupContainer);
                                groupView.render();
                            });
                        }
                    }
                })

                /* рендер атрибутов в группе */
                var outAttrContainer = undefined;
                groupModel.get('attrCollection').each(function (attrModel) {
                    if (attrModel.get('newLine') || !outAttrContainer){
                        // создаем новую строку
                        outAttrContainer = new $('<div/>', {"class": "col-xs-24 newLine", id: 'attr-out-container-' + attrModel.get('id')});
                        $groupContainer.append(outAttrContainer);

                    }
                    // создаем контейнер для атрибута
                    var $attrContainer = $('<div/>', {"class": "attrContainer", id: 'attr-container-' + attrModel.get('id')});
                    outAttrContainer.append($attrContainer);

                    _this._renderAttr(attrModel, $attrContainer);
                })
            },


            /**
             * допустимо для вьюх где модель это коллекция
             * @param groupId
             * @param $lazyContainer
             */
            renderLazyGroup: function (groupId, $lazyContainer) {
                var _this = this
                var groupModel = this.model.findWhere({id: groupId})
                if(groupModel) {
                    _this._renderGroupContent(groupModel, $lazyContainer)
                }
            },

            /**
             * Highlight mandatory group and parents
             * @param groupId
             */
            highlightMandatory: function (groupId) {
                var group = this.formBlockModel.get('groupList').get(groupId)

                var $tabHeader = $('li[data-groupId=' + groupId + ']');
                var $listHeader = $('.list-group-item[data-target="#panel-' + groupId + '"]');

                if (!$tabHeader.hasClass('ml-mandatory-group')) {
                    $tabHeader.addClass('ml-mandatory-group').addClass('bg-danger');
                }

                if (!$listHeader.hasClass('ml-mandatory-group')) {
                    $listHeader.addClass('ml-mandatory-group').addClass('list-group-item-danger');
                }

                if(group && group.get('parent')) {
                    this.highlightMandatory(group.get('parent'))
                }
            },

            /**
             * Remove mandatory highlighting from group and parents
             * @param groupId
             */
            removeHighlightMandatory: function (groupId) {
                console.log("=========removeHighlightMandatory================");
                //TODO check other attr on the same tab
                var group = this.formBlockModel.get('groupList').get(groupId)
                var $tabHeader = $('li[data-groupId=' + groupId + ']');
                var $listHeader = $('.list-group-item[data-target="#panel-' + groupId + '"]');
                var groupIsMandatory = false;

                if(group) {
                    var attrList = group.get('attrCollection').models;
                    $.each(attrList, function (index, attr) {
                        if (attr.get('mandatory') && !attr.hasValue()) {
                            groupIsMandatory = true;
                        }
                    });
                }

                if (!groupIsMandatory) {
                    if ($tabHeader.hasClass('ml-mandatory-group')) {
                        $tabHeader.removeClass('ml-mandatory-group').removeClass('bg-danger');
                    }

                    if ($listHeader.hasClass('ml-mandatory-group')) {
                        $listHeader.removeClass('ml-mandatory-group').removeClass('list-group-item-danger');
                    }

                    if (group && group.get('parent')) {
                        this.removeHighlightMandatory(group.get('parent'))
                    }
                }
            }
        });

        return view;
    });
