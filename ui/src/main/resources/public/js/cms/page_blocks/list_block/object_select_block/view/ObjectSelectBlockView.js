/**
 * Представление для блока выбора связанных объектов
 *
 */
define(
    ['log', 'misc', 'backbone', 'underscore',
        'boot_table_ru',
        'cms/page_blocks/list_block/object_list_block/view/ObjectListBlockView',
        'cms/page_blocks/DialogPageBlock',
        'text!cms/page_blocks/list_block/object_select_block/templates/ObjectSelectTemplate.tpl',
        'text!cms/page_blocks/list_block/object_list_block/templates/ObjectTableTemplate.tpl',
/*
        'text!cms/page_blocks/list_block/object_select_block/templates/ObjectSelectToolbarTemplate.tpl',
*/
        'text!cms/page_blocks/list_block/object_list_block/templates/FolderItemTemplate.tpl'
    ],
    function (log, misc, backbone, _,
              boot_table,
              ObjectListBlockView,
              Message,
              ObjectSelectTemplate,
              ObjectTableTemplate,
/*
              ObjectToolbarTemplate,
*/
              FolderItemTemplate) {
        var view = ObjectListBlockView.extend({

            events: {
                "click .object-selection-complete": "objectSelectionComplete",
                "click .object-selection-cancel": "objectSelectionCancel",
                "click .open-object-button": "openObjectClick"
            },
            /**
             * отрисовка представления
             * @returns {boolean}
             */
            render: function () {
                var _this = this;
                this.$el.html(_.template(ObjectSelectTemplate, {listModel: this.model}));
                var $folderListContainer = this.$el.find('.folder-list-container');
                var $objectListContainer = this.$el.find('.object-list-container');

                var folders = this.model.get('folders')
                if (folders) {
                    this.createFolderListView($folderListContainer, folders)
                }

                var objectList = this.model.get('objectList');
                if (objectList) {
                    this.createObjectListTable($objectListContainer, objectList);

                    /* Additional functions for bootstrap-table */
                    this.updateObjectListTable();

                    /* Update selected objects counter */
                    this.updateSelectedCounter();
                    this.$table.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table check-all-list.bs.table', function (e, row) {
                        _this.updateSelectedCounter()
                    })
                }

                this.model.set('blockRendered', true)
                return true;
            },

            /**
             * Update selected objects counter
             */
            updateSelectedCounter: function () {
                if ($('input[name="btRealSelectAll"]').is(':checked')) {
                    this.$('#selected-list-counter').text('все');
                    //this.model.set('selectedList', [])
                } else {
                    this.$('#selected-list-counter').text(this.model.get('selectedList').length)
                }
            },

            getSelectMode: function(){
                var _this = this;
                return {
                    field: 'state',
                    checkbox: this.model.get('selectMode') == 'multiselect',
                    radio: this.model.get('selectMode') != 'multiselect',
                    formatter: function (value, row, index) {
                        return _this.stateFormatter(value, row, index)
                    }
                }
            },

            /**
             *  форматтер для отображения выбранных элементов (чекбокс)
             */
            stateFormatter: function (value, row, index) {
                if (this.model.get('selectMode') == 'multiselect') {
                    return {
                        /*disabled: true,*/
                        checked: (_.contains(this.model.get('selectedList'), row.objectId) && !_.contains(this.model.get('removeIdList'), row.objectId)) || _.contains(this.model.get('selectedList'), row.addIdList)
                    }
                } else {
                    return _.contains(this.model.get('selectedList'), row.objectId) && !_.contains(this.model.get('removeIdList'), row.objectId);
                }
            },

            /**
             * завершение выбора связанных объектов
             */
            objectSelectionComplete: function () {
                if (this.model.get('selectMode') == 'select' && this.model.get('addIdList').length == 0) {
                    this.model.closePage();
                } else {
                    this.model.selectionComplete();
                }
            },

            /**
             * отмена выбора связанных объектов
             */
            objectSelectionCancel: function () {
                this.model.closePage();
            }

        });

        return view;
    });
