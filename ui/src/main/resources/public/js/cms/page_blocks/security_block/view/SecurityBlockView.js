define(
    ['log', 'misc', 'backbone',
        'cms/view/PageBlockView', 'cms/page_blocks/DialogPageBlock',
        'text!cms/page_blocks/security_block/templates/login.tpl',
        'text!cms/page_blocks/security_block/templates/logout.tpl'
],
    function (log, misc, backbone, PageBlockView, Message,
              loginTemplate, logOutTemplate) {
        var view = PageBlockView.extend({

            events : {
                "click #loginButton" : "loginClick",
                "click #logoutButton" : "logoutClick", //Deprecated
                "keyup #login": "keyPress",
                "keyup #password": "keyPress",
                "click #helpPage" : "showHelp", //Deprecated
                "click .ml-auth__login-password-show" : "showPassword"
            },

            initialize:function () {
                console.log("initialize SecurityBlockView");
                this.listenTo(this.model, 'render', this.render);
            },
            render:function () {
                /*todo: refactor this */
                if (this.model.get('login')){
                    this.$el.html(_.template(logOutTemplate, {
                        login: this.model.get('login'),
                        showHelp : this.model.get('helpUrl') && (this.model.get('helpUrl') != '')
                    }));
                } else {
                    this.$el.html(_.template(loginTemplate));
                    if ($.cookie('userLogin')) {
                        this.$('#login').val($.cookie('userLogin'))
                    }
                }
                this.model.set('blockRendered', true)
            },

            keyPress : function(event) {
                if(event.keyCode == 13){
                    this.loginClick();
                }
            },

            loginClick:function () {
                var login = this.$el.find('input#login').val();
                var password = this.$el.find('input#password').val();
                this.model.login(login, password);
            },

            /**
             * @Deprecated
             */
            logoutClick:function () {
                this.model.logout();
            },

            /**
             * @Deprecated
             */
            showHelp : function(){
                window.open(misc.getContextPath() + "/" + this.model.get('helpUrl'));
            },

            // Кнопка "Показать пароль"
            showPassword : function() {
                var $buttonElem = this.$el.find('.ml-auth__login-password-show');
                var $inputElem = this.$el.find('.ml-auth__login-password-input');
                if ($inputElem.attr('type') == 'password') {
                    $inputElem.attr('type', 'text');
                    $buttonElem.removeClass('glyphicon-eye-open');
                    $buttonElem.addClass('glyphicon-eye-close').attr('title', 'Скрыть пароль');
                } else {
                    $inputElem.attr('type', 'password');
                    $buttonElem.removeClass('glyphicon-eye-close');
                    $buttonElem.addClass('glyphicon-eye-open').attr('title', 'Показать пароль');
                }
            }

        });


        return view;
    });
