(function ($, undefined) {
    "use strict";

    $.jstree.defaults.tree_checkbox_customizer = $.noop;
    $.jstree.plugins.tree_checkbox_customizer = function (options, parent) {
        this.bind = function () {
            parent.bind.call(this);
        };
        /**
         * При отрисовке нод правит html
         * @param obj
         * @param deep
         * @param callback
         * @param force_draw
         * @returns {*}
         */
        this.redraw_node = function (obj, deep, callback, force_draw) {
            var node = this.get_node(obj);
            var li = parent.redraw_node.call(this, obj, deep, callback, force_draw);

            li.classList.add("tree-color");

            $(li).prepend($('<input>', {
                type: 'checkbox',
                style: 'float: left; height: 25px;',
                id: "access-folder-" + node.id,
                class: "folder-access-state",
                value: node.id.toString(),
                checked: node.state.checked
            }).on('change', function () {
                node.state.checked = $(this).prop('checked')
            }));

            return li;
        };
    };
})(jQuery);