/**
 * Представление для атрибута типа ONE_TO_MANY
 */
define(
    ['log', 'misc', 'backbone', 'underscore', 'jquery', 'boot_table_ru', 'moment', 'helpers/ViewHelpers',
        'cms/page_blocks/form_block/object_edit_block/view/EditAttrView',
        'text!cms/page_blocks/form_block/object_edit_block/templates/attrs/OneToManyAttrView.tpl',
        'table_formatter_mixin', 'libs/requestloader/jquery.requestloader'],
    function (log, misc, backbone, _, $, boot_table, moment, helpers,
              EditAttrView, DefaultTemplate, table_formatter_mixin) {
        var view = EditAttrView.extend({
            events: {
                "click .deleteLinkedObject": "deleteLinkedObject",
                "click .editManyToOne": "editManyToOne",
                "click .createClick": "createClick",
                "click .selectLinkedObject": "selectLinkedObject",
                "click .file-download-link": "downloadFile",
                "click .moveUp": "moveUpClick",
                "click .moveDown": "moveDownClick"
            },

            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                this.$toolbar = undefined;
                this.$table = undefined;
                this.listenTo(this.model, 'attribute:render', this.renderAttrValue);
                _.extend(this.events, EditAttrView.prototype.events);
                this.model.set('view', this)
            },

            /**
             * Отрисовка представления
             */
            render: function () {
                if (this.isHidden()) {
                    return;
                }
                var _this = this;
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));
                this.$table = this.$el.find('.table-javascript');
                this.$toolbar = this.$el.find('.custom-toolbar');
                if (!this.model.get("ordered")) {
                    this.$toolbar.find(".moveUp, .moveDown").hide();
                }

                /* Clear selections in table */
                this.model.set('selectedIds', [])

                this.$attrLabelContainer = this.$el.find('.attr-label-container');

                this.addMandatoryEvents();
                this.addReadOnly();
                this.addPopoverHelper();

                //Формируем колонки таблицы
                var columns = [];
                columns.push({
                    field: 'state',
                    checkbox: true,
                    formatter: function (value, row, index) {
                        var _objectId = row.objectId;
                        return {
                            checked: (_.contains(_this.model.get('selectedIds'), _objectId))
                        }
                    }
                });
                columns.push({
                    field: 'objectId',
                    visible: false
                });
                this.model.get('value').attrList.forEach(function (columnAttr) {
                    var formatter = _this.formatterString;
                    var sortable = true;
                    if (columnAttr.fieldType === "ONE_TO_MANY" || columnAttr.fieldType === "MANY_TO_MANY") {
                        formatter = _this.formatterOneToMany;
                        sortable = false;
                    } else if (columnAttr.fieldType === "MANY_TO_ONE") {
                        formatter = _this.formatterManyToOne;
                        sortable = false;
                    } else if (columnAttr.fieldType === "BOOLEAN") {
                        formatter = _this.formatterBoolean;
                        sortable = true;
                    } else if (columnAttr.fieldType === "ENUM") {
                        formatter = _this.formatterEnum;
                        sortable = true;
                    } else if (columnAttr.fieldType === "DATE") {
                        formatter = _this.formatterDateWithOptions({dateFormat : columnAttr.fieldFormat});;
                        sortable = true;
                    } else if (columnAttr.fieldType === "FILE") {
                        formatter = _this.formatterFile;
                        sortable = true;
                    } else if (columnAttr.fieldType === "LONG_LINK") {
                        sortable = false;
                    }

                    var column = {
                        field: columnAttr.entityFieldName,
                        title: columnAttr.description,
                        align: 'left',
                        valign: 'top',
                        sortable: sortable,
                        formatter: formatter
                    };
                    columns.push(column);
                });

                //Формируем значения колонок таблицы
                var data = this.createTableDataFromValue();


                this.$table.bootstrapTable(/*'append', */{
                    /*toolbar: _this.$toolbar,
                    label : _this.$attrLabelContainer,*/
                    idField: 'objectId',
                    data: data,
                    cache: false,

                    /* height: 400,*/
                    striped: true,
                    pagination: true,
                    pageSize: 5,
                    pageList: [5, 10, 20, 50],
                    showColumns: false,
                    minimumCountColumns: 1,
                    search: false,
                    clickToSelect: true,
                    columns: columns
                    /*toolbarAlign :  'right'*/
                }).on('all.bs.table', function (name, args) {
                    _this.updateButtonState();
                }).on('check.bs.table', function (e, row) {
                    _this.model.set('selectedIds', _.union(_this.model.get('selectedIds'), [row.objectId]));
                }).on('uncheck.bs.table', function (e, row) {
                    _this.model.set('selectedIds', _.difference(_this.model.get('selectedIds'), [row.objectId]));
                }).on('check-all.bs.table', function (e, rows) {
                    var ids = $.map(rows, function (row) {
                        return row.objectId;
                    });
                    _this.model.set('selectedIds', _.union(_this.model.get('selectedIds'), ids));
                }).on('uncheck-all.bs.table', function (e, rows) {
                    var ids = $.map(rows, function (row) {
                        return row.objectId;
                    });
                    _this.model.set('selectedIds', _.difference(_this.model.get('selectedIds'), ids));
                });

                /* Скрол для таблицы */
                this.helpers.tableExtendedScrollInit.apply(this)

                _this.updateButtonState();
            },

            /**
             * Обновление состояния кнопок для работы со связанными объектами
             */
            updateButtonState: function () {
                var selects = this.$table.bootstrapTable('getSelections');
                if (selects.length > 0) {
                    this.$toolbar.find(".deleteLinkedObject")
                        .removeClass("inactive-button")
                        .addClass("active-button");
                } else {
                    this.$toolbar.find(".deleteLinkedObject")
                        .removeClass("active-button")
                        .addClass("inactive-button");
                }

                if (selects.length == 1) {
                    this.$toolbar.find(".moveUp, .moveDown, .editManyToOne")
                        .removeClass("inactive-button")
                        .addClass("active-button");
                } else {
                    this.$toolbar.find(".moveUp, .moveDown, .editManyToOne")
                        .removeClass("active-button")
                        .addClass("inactive-button");
                }


                /* Обновление счетчика выбранных элементов todo: refactor this*/
                if (this.model.get('selectedIds')) {
                    this.$('#selected-list-counter').text(this.model.get('selectedIds').length)
                }

            },

            /**
             * Заполнение таблицы данных о связанных объектах
             */
            createTableDataFromValue: function () {
                var data = [];
                if (this.model.get('value') && this.model.get('value').objectList) {
                    this.model.get('value').objectList.forEach(function (objectData) {
                        data.push($.extend(objectData.attrValues, {objectId: objectData.objectId}));
                    });
                }
                return data;
            },

            /**
             * Просмотр связанного объекта
             */
            linkObjectPreviewClick: function () {
                this.model.openObject($(event.srcElement).attr('objectId'), $(event.srcElement).attr('mlClass'));
                return false;
            },

            /**
             * Удалить связанный объект (удаление связи)
             */
            deleteLinkedObject: function () {
                var selects = this.$table.bootstrapTable('getSelections');
                var ids = $.map(selects, function (row) {
                    return row.objectId;
                });
                // Удаляем из коллекции модели все выбранные объекты
                if (ids && ids.length > 0) {
                    var newObjectList = _.reject(this.model.get('value').objectList, function (obj) {
                        return _.contains(ids, obj.objectId);
                    });
                    
                    /* Remove ids from selectedIds list, update selected counter */
                    this.model.set('selectedIds', _.difference(this.model.get('selectedIds'), ids));

                    this.model.get('value').objectList = newObjectList;
                    this.model.trigger('attribute:render');
                }
            },

            /**
             * Редактировать связанный объект
             */
            editManyToOne: function () {
                var selects = this.$table.bootstrapTable('getSelections');
                var ids = $.map(selects, function (row) {
                    return row.objectId;
                });
                if (ids && ids.length == 1) {
                    var params = {
                        refAttrId: this.model.get('id'),
                        objectId: ids[0],
                        className: this.model.get('linkClass')
                    };
                    this.model.openPage(
                        this.model.get('pageBlockModel').getSiteResource('editObjectPage', 'pages'),
                        'Редактирование объекта ...',
                        params
                    );
                }
            },

            /**
             * Подвинуть выделенный связанный объект вверх в упорядоченном списке
             */
            moveUpClick: function () {
                var selects = this.$table.bootstrapTable('getSelections');
                var ids = $.map(selects, function (row) {
                    return row.objectId;
                });
                if (ids && ids.length == 1) {
                    var pos = this.getItemPosInList(ids[0]);
                    if (pos > 0) {
                        this.model.set('selectedIds', ids);
                        var prevItem = this.model.get('value').objectList[pos - 1];
                        this.model.get('value').objectList[pos - 1] = this.model.get('value').objectList[pos];
                        this.model.get('value').objectList[pos] = prevItem;

                        /* Reload table data with save states */
                        this.renderAttrValue();

                        /* Update pagination when selected row go to previous page */
                        if (!_.findWhere(this.$table.bootstrapTable('getData', true), {objectId: this.model.get('value').objectList[pos-1].objectId})) {
                            this.$table.bootstrapTable('prevPage');
                        }
                    }
                }
            },

            /**
             * Подвинуть выделенный связанный объект вниз в упорядоченном списке
             */
            moveDownClick: function () {
                var selects = this.$table.bootstrapTable('getSelections');
                var ids = $.map(selects, function (row) {
                    return row.objectId;
                });
                if (ids && ids.length == 1) {
                    var pos = this.getItemPosInList(ids[0]);
                    if (pos > -1 && pos < this.model.get('value').objectList.length - 1) {
                        var nextItem = this.model.get('value').objectList[pos + 1];
                        this.model.get('value').objectList[pos + 1] = this.model.get('value').objectList[pos];
                        this.model.get('value').objectList[pos] = nextItem;

                        /* Reload table data with save states */
                        this.renderAttrValue();

                        /* Update pagination when selected row go to next page */
                        if (!_.findWhere(this.$table.bootstrapTable('getData', true), {objectId: this.model.get('value').objectList[pos + 1].objectId})) {
                            this.$table.bootstrapTable('nextPage');
                        }
                    }
                }
            },

            /**
             * Получить положение объекта в упорядоченном списке
             */
            getItemPosInList: function (id) {
                var list = this.model.get('value').objectList;
                var pos = -1;
                $.each(list, function (index, item) {
                    if (item.objectId == id) {
                        pos = index;
                    }
                });
                return pos;
            },

            /**
             * Создать связанный объект
             */
            createClick : function(e){
                var params = {
                    refAttrId: this.model.get('id'),
                    //refClassName: this.model.get('id'),
                    refObjectId : this.model.get('pageBlockModel').get('objectId')
                };

                if ($(e.currentTarget).prop('className')) {
                    params.className = $(e.currentTarget).attr('className');
                }
                if (this.model.get('objectId')) {
                    params['refObjectMlId'] = this.model.get('objectId') + '@' + this.model.get('className');
                }

                this.model.openPage(
                    this.model.get('pageBlockModel').getSiteResource('createObjectPage', 'pages'),
                    'Создание объекта ...',
                    params
                );
            },

            /**
             * Выбрать связанные объекты
             */
            selectLinkedObject: function () {
                var selects = this.$table.bootstrapTable('getData');
                var ids = $.map(selects, function (row) {
                    return row.objectId;
                });
                var params = {
                    objectId: this.model.get('pageBlockModel').get('objectId'),
                    refAttrId: this.model.get('id'),
                    className: this.model.get('linkClass'),
                    selectMode: "multiselect",
                    selectedList: ids
                };
                this.model.openPage(
                    this.model.get('pageBlockModel').getSiteResource('selectObjectPage', 'pages'),
                    'Выбор списка объектов ...',
                    params
                );
            },

            /**
             * Отображение значение связанного объекта
             */
            renderAttrValue: function () {
                this.$('#selected-list-counter').text(this.model.get('selectedIds').length);
                var data = this.createTableDataFromValue();
                this.$table.bootstrapTable('load', data);
            }
        });

        _.extend(EditAttrView.prototype, table_formatter_mixin);
        _.extend(EditAttrView.prototype, {helpers: helpers});

        return view;
    });
