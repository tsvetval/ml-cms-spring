<div class="attribute-edit-external_list">
    <div class="col-xs-offset-<%=attrModel.get('offset')%> col-xs-<%=attrModel.get('titleLength')%>">
        <b class="attr-label"><%=attrModel.escape('description')%>:</b>
    </div>
    <div class="col-xs-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%>">
        <div class="openLink">Открыть в новом окне</div>
    </div>
</div>