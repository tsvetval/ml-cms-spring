define(
    ['log', 'misc', 'backbone'],
    function (log, misc, backbone) {
        var view = backbone.View.extend({
            $attrLabel : undefined,

            events: {
                "dblclick .attr-label": "openAttrView"
            },


            initialize: function () {
                console.log("initialize AttrView for model class = " + this.model.getAttrEntityFiledName());
                this.listenTo(this.model, 'render', this.render);
                this.listenTo(this.model, 'change:hidden', this.render);

            },

            render: function () {
                if (this.isHidden()) {
                    return;
                }
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));
            },

            isHidden: function () {
                var hidden = this.model.get('hidden');
                if (hidden) {
                    this.$el.empty();
                    return true;
                } else {
                    return false;
                }
            },

            openAttrView: function () {
                /* Dirty hack for handle single and double click on once dom element */
                this.doubleclicked = true;

                /* Только для роли главного администратора */
                if(_.contains(JSON.parse($.cookie().roles), 'MAIN_ADMIN_ROLE')) {
                    this.model.openAttrView()
                }
            },


            /**
             * Used in One-to-Many, Many-to-One and Many-to-Many attributes
             * @param e
             */
            expandAttributeTable: function (e) {
                var target = $(e.target);
                var _this = this;
                if (!_this.attrTable) {
                    _this.attrTable = _this.createAttrTable();
                }

                var $container = target.parent().parent();
                if (target.hasClass('expanded-list')) {
                    target.removeClass('expanded-list');
                    $container.find('.attr-list-details').first().slideUp('fast',
                        function () {
                            _this.$('.table-controls').hide();
                            _this.$('.list-controls').show();
                            $container.find('.attr-list-preview').first().fadeIn('fast');
                        }
                    );
                } else {
                    target.addClass('expanded-list');
                    $container.find('.attr-list-preview').first().fadeOut('fast',
                        function () {
                            _this.$('.table-controls').show();
                            _this.$('.list-controls').hide();
                            $container.find('.attr-list-details').first().slideDown('fast');
                            _this.helpers.tableExtendedScrollInit.apply(_this)
                        }
                    );
                }
            },

            addPopoverHelper: function () {
                if (!this.$attrLabel) {
                    this.$attrLabel = this.$el.find('.attr-label');
                }

                if (this.model.get('definition')) {
                    var $popover = $('<span>', {
                        class: 'attr-view-popover-icon',
                        'data-trigger': 'click',
                        tabindex: 0,
                    }).text('?');

                    $popover.popover({
                        content: this.model.get('definition'),
                        placement: 'right',
                        html: true,
                        container: 'body'
                    });
                    this.$attrLabel.append($popover);

                    /* Overhead, todo: need refactor */
                    $('html').on('click', function (e) {
                        if(!$(e.target).closest('.popover').length && !$(e.target).hasClass('attr-view-popover-icon')) {
                            $popover.popover('hide')
                        }
                    });

                    $popover.on('click', function () {
                        $('.attr-view-popover-icon').not(this).popover('hide')
                    })
                }
            },
        });

        return view;
    });
