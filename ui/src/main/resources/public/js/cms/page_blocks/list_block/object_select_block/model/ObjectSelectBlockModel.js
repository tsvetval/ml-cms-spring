/**
 *  Модель блока выбора связанных объектов
 *
 */
define(
    ['log', 'misc', 'backbone',
        'cms/events/events',
        'cms/page_blocks/list_block/object_list_block/model/ObjectListBlockModel',
        'cms/events/NotifyPageBlocksEventObject'
    ],
    function (log, misc, backbone, cmsEvents, ObjectListBlockModel, NotifyEventObject) {
        var model = ObjectListBlockModel.extend({
            defaults: {
                /*
                 * id объекта владельца атрибута
                 * */
                objectId: undefined,
                className: undefined,
                pageTitle: undefined,
                /*
                 * Изначальных список выбранных идентификаторв
                 * */
                selectedList : undefined,
                /*
                 * Список добавленных идентификаторв
                 * */
                addIdList : undefined,
                /*
                * Список удалененых идентификаторв
                * */
                removeIdList : undefined,
                controller: 'selectObjectListController'
            },

            /**
             * инициализация модели
             */
            initialize: function () {
                /* ObjectListBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this)

                log.debug("initialize ListBlockModel");
                this.pageTitle = 'Выбор объектов <%=folderTitle%>';
            },

            /*
             * Переопределяем обработчик события restorePage
             * */
            restorePageListener: function(params){
                var _this = this;
                log.debug("ObjectListBlockModel start restorePage");
                _this.set('refAttrId', params.refAttrId);
                _this.set('className', params.className);
                _this.get('pageModel').set('className', params.className);
                _this.set('selectMode', params.selectMode);
                _this.set('objectId', params.objectId);
                if (params.selectedList) {
                    _this.set('selectedList', params.selectedList);
                } else {
                    _this.set('selectedList', []);
                }
                _this.set('addIdList', []);
                _this.set('removeIdList', []);
                _this._reset();
                _this._update();
            },
            /**
             * Добавление списка идентификаторов к списку связанных объектов
             * @param ids
             */
            addSelectedIds : function(ids){
                if (this.get('selectMode') == 'select') {
                    /* For radio button list select */
                    this.set('addIdList', ids)

                    this.set('selectedList', ids);
                } else {
                    /* For checkbox list select */
                    var notInSelectedList = _.difference(ids, this.get('selectedList'));
                    this.set('addIdList', _.union(this.get('addIdList'), notInSelectedList));
                    this.set('removeIdList', _.difference(this.get('removeIdList'), ids));

                    this.set('selectedList', _.union(this.get('selectedList'), ids));
                }
            },

            /**
             * Удаление списка идентификаторов из списка связанных объектов
             * @param ids
             */
            removeSelectedIds : function(ids){
                var inSelectedListForRemove = _.intersection(ids, this.get('selectedList'));
                this.set('removeIdList', _.union(this.get('removeIdList'), inSelectedListForRemove));
                this.set('addIdList', _.difference(this.get('addIdList'), ids));

                this.set('selectedList', _.difference(this.get('selectedList'), ids));

            },

            /*
             * Получаем данные с сервера
             * */
            _loadListData: function () {
                var options = {
                    action: 'getObjectData',
                    data: {
                        objectId: this.get('objectId'),
                        className: this.get('className'),
                        refAttrId: this.get('refAttrId'),
                        currentPage: this.get('currentPage'),
                        objectsPerPage: this.get('objectsPerPage'),
                        orderAttr: this.get('orderAttr'),
                        orderType: this.get('orderType'),
                        pageBlockId: this.get('blockInfo').get('id'),
                        ml_request: true

                    }
                };
                if (this.get("search")) {
                    $.extend(options.data, this.get("search"));
                }
                return this.callServerAction(options);
            },

            /**
             * Обновление заголовка страницы
             * @private
             */
            _updatePageTitle: function () {
                var title = "Выбор объектов " + this.get('ClassDescription');
                this.updatePageTitle(title);
            },

            /**
             * Завершение выбора связанных объектов
             */
            selectionComplete: function () {
                var data = {
                    addIdList: this.get('addIdList'),
                    removeIdList: this.get('removeIdList'),
                    refAttrId: this.get('refAttrId'),
                    isAllSelected: this.get('isAllSelected')
                };

                if (this.get('search')) {
                    data.search = this.get('search')
                }


                var opener = this.get("pageModel").get("page_opener");

                this.notifyPageBlocks(new NotifyEventObject(
                    cmsEvents.OBJECTS_SELECTION_DONE,
                    data,
                    opener
                ));
                this.closePage();
            }

        });
        return model;
    });
