/**
 * @name: jpqlBlockBoot
 * @package:
 * @project: ml-cms-all
 * @javaController:
 * Created by i_tovstyonok on 29.12.2015.
 */

define(
    ['log', 'misc', 'backbone', 'cms/page_blocks/jpql_block/model/JPQLBlockModel', 'cms/page_blocks/jpql_block/view/jpqlBlockView'],
    function (log, misc, backbone, model, view) {
        return {
            model: model,
            view: view
        };
    });
