define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView',
        'text!cms/page_blocks/util_block/util/template/utilTemplate.tpl'
    ],
    function (log, misc, backbone, PageBlockView, utilTemplate) {
        var view = PageBlockView.extend({
                events: {
                    "click a": "clickUtil"
                },

                initialize: function () {
                    console.log("initialize UtilView");
                    this.listenTo(this.model, 'render', this.render)
                },
                render:     function () {
                    this.$el.append(_.template(utilTemplate, {model: this.model}));

                },

                clickUtil : function(){
                    this.model.openURL({});
                }
            }
        );

        return view;
    });
