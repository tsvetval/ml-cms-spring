/**
 * Представление блока списка объектов
 *
 */
define(
    ['log', 'misc', 'backbone', 'underscore', 'moment',
        'boot_table_ru',
        'cms/page_blocks/list_block/object_list_block/view/ObjectListBlockView',
        'markup',
        'cms/page_blocks/DialogPageBlock',
        'text!cms/page_blocks/list_block/object_list_block/templates/ObjectListTemplate.tpl'
    ],
    function (log, misc, backbone, _, moment,
              boot_table,
              ObjectListBlockView,
              markup,
              Message,
              ObjectListTemplate) {

        var view = ObjectListBlockView.extend({

            render: function () {
                var _this = this;
                if (this.model.get('url')) {
                    this.$el.html(_.template(IFrameTemplate, {url: this.model.get('url')}));
                    return true;
                }
                this.$el.html(_.template(ObjectListTemplate, {listModel: this.model}));
                var $objectListContainer = this.$el.find('.object-list-container');
                var className = this.model.get('className');
                if (className) {
                    var objectList = this.model.get('objectList');
                    this.createObjectListTable($objectListContainer, objectList);
                }
                return true;
            }
        });
        return view;
    });
