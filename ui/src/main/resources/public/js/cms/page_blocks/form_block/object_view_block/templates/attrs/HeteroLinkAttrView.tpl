<div class="attribute-view-hetero">
    <div class="col-xs-offset-<%=attrModel.get('offset')%> col-xs-<%=attrModel.get('titleLength')%>">
        <b class="attr-label"><%=attrModel.escape('description')%>:</b>
    </div>
    <div class="col-xs-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%>">
    <span class="attr-list-preview">
    </span>
    </div>
    <div class="newLine attr-list-details col-xs-<%=(attrModel.get('totalLength'))%>"
         style="overflow: auto; max-height: <%=attrModel.get('tableHeight')%>px; display: none;">
        <table class="table-javascript">
        </table>
    </div>
</div>