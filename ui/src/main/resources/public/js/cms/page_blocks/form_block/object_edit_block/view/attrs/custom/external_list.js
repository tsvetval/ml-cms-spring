/**
 * Представление для отображение атрибута типа DATE
 */
define(
    ['log', 'misc', 'backbone', 'moment',
        'cms/page_blocks/form_block/view/AttrView',
        'text!cms/page_blocks/form_block/object_view_block/templates/attrs/ExternalList.tpl'],
    function (log, misc, backbone, moment, AttrView, DefaultTemplate) {
        var view = AttrView.extend({
            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                _.extend(this.events, AttrView.prototype.events);
                var _this = this
                this.listenTo(this.model.get('pageBlockModel'), 'changeObject', function (params) {
                    _this.model.set('value', params.value)
                    _this.render()
                })
            },

            /**
             * Отрисовка представления
             */
            render: function () {
                var _this = this;
                if (this.isHidden()) {
                    return;
                }
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));
                this.$el.find(".openLink").on('click', function () {
                    _this.selectLinkedObject(_this.model);
                });
                this.addPopoverHelper()
            },

            selectLinkedObject: function (model) {
                var params = {
                    objectId: model.get('pageBlockModel').get('objectId'),
                    refAttrId: model.get('id'),
                    className: model.get('linkClass'),
                    description: model.get('pageBlockModel').get('title') + " → " + model.get('description'),
                    selectMode: "multiselect"
                };
                this.model.openPage(
                    this.model.get('pageBlockModel').getSiteResource('viewLinkedListPage', 'pages'),
                    'Просмотр списка объектов ...',
                    params
                );
            }
        });
        return view;
    });
