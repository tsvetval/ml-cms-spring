/**
 * Представление flat_text для атрибута типа TEXT
 */
define([
        'cms/page_blocks/form_block/object_edit_block/view/EditAttrView',
        'text!cms/page_blocks/form_block/object_edit_block/templates/attrs/yandex_map.tpl',
        'ymap'
    ],
    function (EditAttrView, DefaultTemplate) {
        var view = EditAttrView.extend({
            $inputField: undefined,

            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                _.extend(this.events, EditAttrView.prototype.events);
            },


            /**
             * Отрисовка представления
             */
            render: function () {
                var _this = this;
                if (this.isHidden()) {
                    return;
                }
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));
                _this.initMap(_this.model.get("value"));

            },

            initMap: function (value) {
                var _this = this;
                ymaps.ready(function () {
                    var myMap = new ymaps.Map("map", {
                        center: [59.9190, 30.3529], // питер
                        zoom: 10
                    });
                    if (value) {
                        var myPlacemark = new ymaps.Placemark(JSON.parse(value));
                        myMap.geoObjects.add(myPlacemark);
                    }
                    myMap.events.add('click', function (e) {
                        var coords = e.get('coords');
                        var myPlacemark = new ymaps.Placemark(coords);
                        myMap.geoObjects.removeAll();
                        myMap.geoObjects.add(myPlacemark);
                        _this.model.set('value', JSON.stringify(coords));
                    });

                });
            }
        });

        return view;
    });
