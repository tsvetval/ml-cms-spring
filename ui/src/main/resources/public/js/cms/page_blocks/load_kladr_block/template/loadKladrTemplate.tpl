<section id="load-kladr-block" role="template">
    <% if (!model.get('import').loadStatus) { %>
    <div class="row" id="import-holders-form">
        <div class="col-xs-24 well">
            <h2 class="block-header">Импорт КЛАДР</h2>
            <div class="form-group col-xs-24" id="input-block">
                <div class="col-xs-12">
                    <input type="file" class="form-control" id="import-kladr-file" accept=".7z">
                </div>
            </div>
            <div class="form-group col-xs-24" id="progress-block">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped active"></div>
                </div>
            </div>
            <div class="form-group col-xs-24">
                <div class="col-xs-12 btn-group">
                    <button class="btn btn-success" id="upload-import-kladr-file" disabled="true">Импортировать</button>
                    <button class="btn btn-primary" id="cancel-import-kladr">Отмена</button>
                </div>
            </div>
        </div>
    </div>
    <% } else { %>
    <div class="row" id="import-result">
        <div class="col-xs-24 well">
            <div class="row">
                <h2 class="block-header">Результаты импорта:</h2>
                <ul>
                    <% _.each(model.get('import').log, function (step) { %>
                    <li><%= step %></li>
                    <% }) %>
                </ul>
                <i class="glyphicon glyphicon-refresh animation-rotate"></i>
            </div>
        </div>
        <div class="col-xs-24" id="result-controls">
            <button class="btn btn-primary" id="reset-import">Новый импорт</button>
            <button class="btn btn-default" id="close-page">Закрыть</button>
        </div>
    </div>
    <% } %>

</section>