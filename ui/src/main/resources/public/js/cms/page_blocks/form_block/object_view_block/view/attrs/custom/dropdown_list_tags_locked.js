/**
 * Наследуем view и шаблон от dropdown_list_tags
 */

define(['cms/page_blocks/form_block/object_view_block/view/attrs/custom/dropdown_list_tags'],
    function (AttrView) {
        var view = AttrView.extend({
            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
            }
        })

        return view
    }
)