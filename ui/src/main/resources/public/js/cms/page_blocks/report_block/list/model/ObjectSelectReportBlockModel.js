/**
 *  Модель блока выбора связанных объектов
 *
 */
define(
    ['log', 'misc', 'backbone',
        'cms/events/events',
        'cms/page_blocks/list_block/object_select_block/model/ObjectSelectBlockModel',
        'cms/events/NotifyPageBlocksEventObject'
    ],
    function (log, misc, backbone, cmsEvents, ObjectSelectBlockModel, NotifyEventObject) {
        var model = ObjectSelectBlockModel.extend({
            defaults: {
                /*
                 * id объекта владельца атрибута
                 * */
                objectId: undefined,
                className: undefined,
                pageTitle: undefined,

                /*
                 * Изначальных список выбранных идентификаторв
                 * */
                selectedList : undefined,
                /*
                 * Список добавленных идентификаторв
                 * */
                addIdList : undefined,
                /*
                * Список удалененых идентификаторв
                * */
                removeIdList : undefined,

                paramId: undefined,
                controller: 'selectReportParamController'

            },

            /**
             * инициализация модели
             */
            initialize: function () {
                /* ObjectSelectBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this)

                log.debug("initialize ListBlockModel");

                this.pageTitle = 'Выбор объектов <%=folderTitle%>';

            },

            /**
             * Добавление событий блока
             * @Override ObjectSelectBlockModel.bindEvents
             */
            bindEvents: function () {
                /* ObjectSelectBlockModel.bindEvents() добавляет основные события блока */
                model.__super__.bindEvents.call(this)

                var _this = this;
                this.listenTo(this, cmsEvents.RESTORE_PAGE, function (params) {
                    log.debug("ObjectListBlockModel start cmsEvents.RESTORE_PAGE event");
                    _this.set('refAttrId', params.refAttrId);
                    _this.set("paramId",params.paramId);
                    _this.set('className', params.className);
                    _this.get('pageModel').set('className', params.className);
                    _this.set('selectMode', params.selectMode);
                    _this.set('objectId', params.objectId);
                    if (params.selectedList) {
                        _this.set('selectedList', params.selectedList);
                    } else {
                        _this.set('selectedList', []);
                    }
                    _this.set('addIdList', []);
                    _this.set('removeIdList', []);
                    _this._reset();
                    _this._update();
                });

                this.listenTo(this, cmsEvents.REFRESH_PAGE, function () {
                    log.debug("ObjectListBlockModel start cmsEvents.RESTORE_PAGE event");
                    _this._update();
                });

                this.listenTo(this, cmsEvents.SEARCH, function (params) {
                    var type = misc.option(params, "type", "Тип фильтрации (простой или расширенный)");
                    var criteria = misc.option(params, "criteria", "Критерий поиска");
                    var data = {
                        useFilter: type
                    };
                    if (type == 'simple') {
                        data.simpleFilter = criteria;
                    } else {
                        data.extendedFilterSettings = criteria;
                    }
                    _this.set('search', data);
                    _this._update();
                });
            },

            /**
             * Остановка обработчиков событий блока
             * @Override ObjectSelectBlockModel.unbindEvents
             */
            unbindEvents: function () {
                /*  События блока */
                this.stopListening(this, cmsEvents.RESTORE_PAGE);
                this.stopListening(this, cmsEvents.REFRESH_PAGE);
                this.stopListening(this, cmsEvents.SEARCH);

                /* ObjectSelectBlockModel.unbindEvents() удаляет все основные события блока */
                model.__super__.unbindEvents.call(this)
            },

            /**
             * Добавление списка идентификаторов к списку связанных объектов
             * @param ids
             */
            addSelectedIds : function(ids){
                var notInSelectedList = _.difference(ids, this.get('selectedList'));
                this.set('addIdList', _.union(this.get('addIdList'), notInSelectedList));
                this.set('removeIdList', _.difference(this.get('removeIdList'), ids));

            },

            /**
             * Удаление списка идентификаторов из списка связанных объектов
             * @param ids
             */
            removeSelectedIds : function(ids){
                var inSelectedListForRemove = _.intersection(ids, this.get('selectedList'));
                this.set('removeIdList', _.union(this.get('removeIdList'), inSelectedListForRemove));
                this.set('addIdList', _.difference(this.get('addIdList'), ids));

            },

            /*
             * Получаем данные с сервера
             * */
            _loadListData: function () {
                var options = {
                    action: 'getObjectData',
                    data: {
                        objectId: this.get('objectId'),
                        className: this.get('className'),
                        paramId: this.get('paramId'),
                        currentPage: this.get('currentPage'),
                        objectsPerPage: this.get('objectsPerPage'),
                        orderAttr: this.get('orderAttr'),
                        orderType: this.get('orderType'),
                        pageBlockId: this.get('blockInfo').get('id'),
                        ml_request: true

                    }
                };
                if (this.get("search")) {
                    $.extend(options.data, this.get("search"));
                }
                return this.callServerAction(options);
            },

            /**
             * Обновление заголовка страницы
             * @private
             */
            _updatePageTitle: function () {
                var title = "Выбор объектов " + this.get('ClassDescription');
                this.updatePageTitle(title);
            },

            /**
             * Завершение выбора связанных объектов
             */
            selectionComplete: function () {
                var data = {
                    addIdList:    this.get('addIdList'),
                    removeIdList:    this.get('removeIdList'),
                    paramId: this.get('paramId')
                };
                var opener = this.get("pageModel").get("page_opener");

                this.notifyPageBlocks(new NotifyEventObject(
                    cmsEvents.OBJECTS_SELECTION_DONE,
                    data,
                    opener
                ));
                this.closePage();
            }

        });
        return model;
    });
