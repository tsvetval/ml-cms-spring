<div class="attribute-edit-long">
    <div class="attr-label-container col-xs-offset-<%=attrModel.get('offset')%> col-xs-<%=attrModel.get('titleLength')%>">
        <b class="attr-label"><%=attrModel.escape('description')%>:</b>
    </div>
    <input type="text"
           class="attrField col-xs-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%>"
           value=""/>
</div>