<div id="folderAccessModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Измениить доступ на папкам</h4>
                <button type="button" class="btn btn-small btn-sm btn-primary save-folder-access">Сохранить</button>
                <button type="button" class="btn btn-small btn-sm btn-danger" data-dismiss="modal">Отмена</button>
            </div>
            <div class="modal-body">
                <span>Доступ к папкам</span>
                <ul class="nav nav-tabs" role="tablist">
                    <% _.each(folders, function(folderAccess) { %>
                    <li role="presentation" id="folderTab<%=folderAccess.id%>"><a href="#folder<%=folderAccess.id%>" aria-controls="folder<%=folderAccess.id%>" role="tab" data-toggle="tab"><%=folderAccess.name%></a>
                    </li>
                    <% }); %>
                </ul>

                <div class="tab-content">
                    <% _.each(folders, function(folderAccess) { %>
                    <div role="tabpanel" class="tab-pane fade in" id="folder<%=folderAccess.id%>">
                        <ul class="list-group">
                            <div style="height: 500px; overflow: auto;">
                                <div id="foldersTree<%=folderAccess.id%>"/>
                            </div>
                        </ul>
                    </div>
                    <% }); %>
                </div>
            </div>
        </div>
    </div>
</div>
