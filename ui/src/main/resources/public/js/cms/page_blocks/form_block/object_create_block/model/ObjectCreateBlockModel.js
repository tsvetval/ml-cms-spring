/**
 * Модель блока создания объекта
 */
define(
    ['log', 'misc', 'backbone', 'jquery',
        'cms/events/events',
        'cms/page_blocks/form_block/FormBlockModel',
        'cms/events/NotifyPageBlocksEventObject',
        'cms/page_blocks/form_block/model/AttrModel',
        'cms/page_blocks/form_block/model/AttrGroupModel',
        'cms/page_blocks/form_block/collections/AttrCollection',
        'cms/page_blocks/form_block/collections/AttrGroupCollection',
        'libs/requestloader/jquery.requestloader'
    ],
    function (log, misc, backbone, $, cmsEvents, FormBlockModel, NotifyEventObject, AttrModel, AttrGroupModel, AttrCollection, AttrGroupCollection) {
        var model = FormBlockModel.extend({
            defaults: {
                objectId: undefined,
                entityName: undefined,
                description: undefined,
                title: undefined,
                attrList: undefined,
                nonGroupAttrList: undefined,
                groupList: undefined,
                rootGroupList: undefined,
                pageTitle: '',
                viewMode: 'CREATE',
                controller: 'createObjectController'
            },

            /**
             * Инициализация модели
             */
            initialize: function () {
                /* FormBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this);

                log.debug('initialize ObjectCreateBlockModel');
                this.pageTitle = 'Создание объекта класса <%=data.description%>';
            },

            /**
             * Добавление событий блока
             * @Override FormBlockModel.bindEvents
             */
            bindEvents: function () {
                /* FormBlockModel.bindEvents() добавляет основные события блока */
                model.__super__.bindEvents.call(this);

                /*
                 * Слушаем событие выбора объектов для ссылочных атрибутов
                 * */
                this.listenTo(this, cmsEvents.OBJECTS_SELECTION_DONE, this.objectsSelectionDone);
                this.listenTo(this, cmsEvents.LINK_OBJECT_CREATED, this.linkObjectCreated);
            },

            /**
             * Остановка обработчиков событий блока
             * @Override FormBlockModel.unbindEvents
             */
            unbindEvents: function () {
                /* События блока */
                this.stopListening(this, cmsEvents.OBJECTS_SELECTION_DONE);
                this.stopListening(this, cmsEvents.LINK_OBJECT_CREATED);

                /* PageBlockModel.unbindEvents() удаляет все основные события блока */
                model.__super__.unbindEvents.call(this)
            },

            /*
             * После создания линкованого объекта
             * */
            linkObjectCreated: function (params) {
                var _this = this;
                var refAttrId = misc.option(params, "refAttrId", "Атрибут для обновления");
                var objectId = misc.option(params, "objectId", "ID созданного объекта", []);
                var title = misc.option(params, "title", "ID созданного объекта", []);

                // ищем модель атрибута и проставляем ему новое значение
                var attrModel = this.get('attrList').get(refAttrId);
                attrModel.addLinkObjectId({objectId: objectId, title: title});

                /* Sync */
                this.syncLinkedAttribute(attrModel)
            },

            /*
             * После выбора ссылочных объектов
             * */
            objectsSelectionDone: function (params) {
                var _this = this;
                var refAttrId = misc.option(params, "refAttrId", "Атрибут для обновления");
                var addIdList = misc.option(params, "addIdList", "Список ID для добавления", []);
                var removeIdList = misc.option(params, "removeIdList", "Список ID для удаления", []);
                // ищем модель атрибута и проставляем ему новое значение
                var attrModel = this.get('attrList').get(refAttrId);

                if (params.isAllSelected) {
                    var data = {
                        attrId: attrModel.get('id'),
                        className: attrModel.get('className')
                    };
                    if (params.search) {
                        data = _.extend(data, params.search)
                    }
                    attrModel.callServerAction({
                        action: "getAllRelationValues",
                        data: data
                    }).then(function (response) {
                        attrModel.set('value',{idList: _.map(response.objectList, function (obj) {
                            return obj.id
                        })});
                        _this.syncLinkedAttribute(attrModel)
                    }).fail(function (response) {
                        _this.get('pageModel').get('siteModel').trigger(cmsEvents.SITE_NOTIFY, {type:'warning', text: "При выборе объектов произошла ошибка"})
                    });
                } else {
                    var currentValue = attrModel.get('value');
                    var newValueIdList = [];
                    //TODO refactor this вынести логику в модель атрибутов
                    // Удаляем удаленные
                    if (currentValue && currentValue.objectList) {
                        currentValue.objectList.forEach(function (object) {
                            if (!_.contains(removeIdList, object.objectId)) {
                                newValueIdList.push(object.objectId)
                            }
                        })
                    }
                    // Добавляем новые id
                    newValueIdList = _.union(newValueIdList, addIdList);
                    attrModel.set('value', {idList: newValueIdList, title: undefined});
                    this.syncLinkedAttribute(attrModel)
                }
            },

            /**
             *  @Duplicated
             *  todo: refactor with ObjectEditBlockModel
             *  */
            syncLinkedAttribute: function (model) {
                /* Fetch new data for objects */
                if (model.get('value') && !model.get('value').title) {
                    if (model.get('view').$titleHolderElement) {
                        model.get('view').$titleHolderElement.text('Идет получение заголовка...');
                    }
                    model.callServerAction({
                        action: "getRefAttrValues",
                        data: {
                            objectId: model.get('pageBlockModel').get('objectId'),
                            className: model.get('className'),
                            attrId: model.get('id'),
                            idList: JSON.stringify(model.get('value').idList)
                        },
                        lockElement: model.get('view').$table
                    }).then(function (response) {
                        if (response.result) {
                            if (response.result.id) {
                                model.set('value', {objectId: response.result.id, title: response.result.title}, {silent: true});
                            } else {
                                model.set('value', response.result, {silent: true});
                            }
                        }

                        model.set('selectedIds', []);
                        model.trigger('attribute:render')
                    });
                } else {
                    model.set('selectedIds', []);
                    model.trigger('attribute:render')
                }
            },

            /**
             * Отмена создания объекта
             */
            cancelEditObject: function () {
                this.closePage();
            },

            /**
             * Сохранение объекта
             * @returns {boolean}
             */
            saveObject: function () {
                var _this = this;
                //TODO
                // Пробегаем по всем атрибутам и собираем значения
                var notFilledMandatoryAttrs = [];
                var fileAttrList = [];
                var dataForSave = {};
                this.get('attrList').forEach(function (attr) {
                    //Для каждого атрибута проверяется обязательность
                    if (attr.isMandatory() && !attr.hasValue()) {
                        notFilledMandatoryAttrs.push(attr);
                        attr.triggerNotFilledMandatory();
                    } else {
                        attr.triggerRemoveHighlightMandatory();
                    }

                    if (attr.get('fieldType') == 'FILE' && attr.hasValue()) {
                        fileAttrList.push(attr);
                    }
                    // Устанавливаем значение атрибута
                    dataForSave[attr.getAttrEntityFiledName()] = attr.serializeValue();
                });

                if (notFilledMandatoryAttrs.length > 0) {
                    return false;
                }
                // Отправляем запрос на сохранение
                var options = {
                    action: 'saveObject',
                    data: {
                        objectId: _this.get('objectId'),
                        className: _this.get('entityName'),
                        pageBlockId: _this.get('blockInfo').get('id'),
                        refAttrId: _this.get('pageModel').get('refAttrId'),
                        data: JSON.stringify(dataForSave),
                        ml_request: true
                    },
                    lockElement: $('#CENTER')
                };

                this.uploadFiles(fileAttrList, function () {
                    log.debug('uploadFiles done.');
                    _this.callServerAction(options).then(function (result) {
                        var opener = _this.get("pageModel").get("page_opener");
                        if (_this.get("pageModel").get('refAttrId')) {
                            $.extend( result, {refAttrId : _this.get("pageModel").get('refAttrId')});
                            _this.notifyPageBlocks(new NotifyEventObject(cmsEvents.LINK_OBJECT_CREATED, result, opener));
                            _this.closePage();
                            _this.trigger(cmsEvents.MODEL_SAVE_COMPLETE)
                        } else {
                            /* If user have access for update created object route to edit page */
                            if (result.access.update) {
                                _this.notifyPageBlocks(new NotifyEventObject(cmsEvents.REFRESH_PAGE, {}, opener));
                                _this.replacePage(
                                    _this.getSiteResource('editObjectPage', 'pages'),
                                    'Просмотр объекта ...',
                                    {
                                        objectId: result.objectId,
                                        className: _this.get('className')
                                    }
                                );
                            } else {
                                /* Close page and return to opener */
                                _this.notifyPageBlocks(new NotifyEventObject(cmsEvents.REFRESH_PAGE, {}, opener));
                                _this.closePage();
                            }

                            _this.trigger(cmsEvents.MODEL_SAVE_COMPLETE)
                        }
                    })
                });
            }

        });
        return model;
    });
