define(
    ['log', 'misc', 'backbone', 'cms/events/events',
        'cms/model/PageBlockModel', 'cms/page_blocks/DialogPageBlock'
    ],
    function (log, misc, backbone, Events, PageBlockModel, Message ) {
        var model = PageBlockModel.extend({

            defaults: {
                pageTitle: 'Просмотр папки Документация',
                controller: 'documentationController'
            },

            initialize: function () {
                /* PageBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this)

                console.log("initialize BlockModel");
            },

            /**
             * Добавление событий блока
             * @Override PageBlockModel.bindEvents
             */
            bindEvents: function () {
                /* PageBlockModel.bindEvents() добавляет основные события блока */
                model.__super__.bindEvents.call(this)

                var _this = this;
                this.listenTo(this, Events.RESTORE_PAGE, function (params) {
                    _this.prompt();
                });
                this.listenTo(this, Events.OPEN_FOLDER, function (params) {
                    _this.updatePageTitle(this.get('pageTitle'))
                });
                this.listenTo(this, "getClassInfo", _this.getClassInfo);
            },

            /**
             * Остановка обработчиков событий блока
             * @Override PageBlockModel.unbindEvents
             */
            unbindEvents: function () {
                /*  События блока */
                this.stopListening(this, Events.RESTORE_PAGE);
                this.stopListening(this, Events.OPEN_FOLDER);
                this.stopListening(this, "getClassInfo");

                /* PageBlockModel.unbindEvents() удаляет все основные события блока */
                model.__super__.unbindEvents.call(this)
            },

            getClassInfo: function(cls){
                var _this = this;
                var options = {
                    action: 'getClassInfo',
                    data: {
                        className: cls.clsName,
                        pageBlockId: this.get('blockInfo').get('id'),
                        ml_request: true
                    }
                };
                _this.callServerAction(options).then(function(result){
                    _this.set("classInfo",result.classInfo);
                    _this.trigger("renderClassInfo");

                })
            },

            prompt: function () {
                var _this = this;
                var options = {
                    action: 'getClasses'
                };
                _this.callServerAction(options).then(function(result){
                    _this.set("classList",result.classList);
                    _this.trigger(Events.RENDER_VIEW);
                })
            }

        });
        return model;
    });
