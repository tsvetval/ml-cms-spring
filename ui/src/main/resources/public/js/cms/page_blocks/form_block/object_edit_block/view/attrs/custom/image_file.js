/**
 * Представление для атрибута типа image_file
 * @extend FileAttrView
 */
define(
    ['log', 'misc', 'backbone', 'moment', 'jquery', 'jqueryUi',
        'cms/page_blocks/form_block/object_edit_block/view/attrs/FileAttrView',
        'text!cms/page_blocks/form_block/object_edit_block/templates/attrs/image_file.tpl'
    ],
    function (log, misc, backbone, moment, $, jqueryUi,
              EditAttrView, DefaultTemplate) {
        var view = EditAttrView.extend({
            $inputField: undefined,
            $inputFieldFake: undefined,
            $buttonDelete: undefined,
            width: undefined,
            height: undefined,


            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
            },

            /**
             * Отрисовка представления
             */
            render: function () {
                var _this = this;
                if (this.isHidden()) {
                    return;
                }

                this.$el.html(_.template(this.viewTemplate, {
                    attrModel: this.model
                }));

                var title = '';
                this.$buttonDelete = this.$el.find('.buttonDelete');
                if (this.model.get('value')) {
                    title = this.model.get('value').title;
                    this.$buttonDelete.find('label').addClass('highlight-button');
                }

                this.$buttonDelete.on('click', function () {
                    _this.deleteFile();
                });

                this.addMandatoryEvents();
                this.addReadOnly();
                this.addPopoverHelper();
                this.$inputField = this.$el.find('.attrField');
                this.$inputFieldFake = this.$el.find('.attrField_fake');
                this.$inputFieldFake.val(title);

                this.$inputFieldPreview = this.$el.find('.image-preview');

                /* Add drop zone */
                this.$inputDropZone = this.$el.find('.image-drop-zone');
                this.$inputDropZone.on('dragover', function (e) {
                    $(this).addClass('over')
                })
                this.$inputDropZone.on('dragleave drop', function (e) {
                    $(this).removeClass('over')
                })

                /* Добавляем превью изображения */
                if(this.model.get('value') && this.model.get('value').objectId) {
                    this.model.callServerAction(
                        {
                            action: 'downloadFile',
                            data: {
                                objectId: _this.model.get('value').objectId,
                                className: _this.model.get('className'),
                                attrName: _this.model.get('entityFieldName'),
                                thumbnail: true
                            }
                        },
                        function (result) {
                            _this.model.get('value').url = result.url
                            _this.$inputFieldPreview.attr('src', result.url)
                        }
                    );
                } else {
                    if (this.model.get('value') && this.model.get('value').url) {
                        _this.$inputFieldPreview.attr('src', this.model.get('value').url)
                    }
                }

            },

            /**
             * Обработчик изменения значения HTML-элемента
             */
            selectFile: function () {
                var _this = this
                this.$el.find('.drop-zone-text').text(this.$inputField.val());
                this.preUploadFile(function (filename) {
                    this.$inputField.val(null)
                    _this.model.attributes['value'] = {fileName : filename, url: encodeURI(misc.getContextPath()+"/getTempFile?fileName="+ filename + '&t=' + new Date().getMilliseconds())};
                    _this.render()
                });
            },

            preUploadFile: function (callback) {
                var _this = this;
                var importFile = this.$inputField[0].files[0];
                console.log('Start uploading file: ' + importFile.name);
                var xhr = new XMLHttpRequest();
                var url = misc.getContextPath() + "/upload";
                xhr.onreadystatechange = function (e) {
                    if (4 == this.readyState) {
                        console.log('File uploaded: ' + e.target.responseText);
                        callback.apply(_this, [importFile.name]);
                    }
                };
                xhr.open('post', url, true);
                var fd = new FormData;
                fd.append(importFile.name,importFile);
                xhr.send(fd);
            }
        });

        return view;
    });
