/**
 * Наследуем view и шаблон от OneToManyAttrView
 */

define(['cms/page_blocks/form_block/object_view_block/view/attrs/OneToManyAttrView'],
    function (AttrView) {
        var view = AttrView.extend({
            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
            }
        })

        return view
    }
)