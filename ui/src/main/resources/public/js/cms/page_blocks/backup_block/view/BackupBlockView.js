/**
 *  Представление блока создания резервной копии БД
 */
define(
    ['log', 'misc', 'backbone', 'cms/events/events', 'cms/view/PageBlockView', 'markup', 'cms/page_blocks/DialogPageBlock', "datetimepicker", "moment"],
    function (log, misc, backbone, cmsEvents, PageBlockView, markup, Message) {
        var view = PageBlockView.extend({

            /**
             * инициализация представления
             */
            initialize: function () {
                console.log("initialize BlockView");
                this.listenTo(this.model, cmsEvents.RENDER_VIEW, this.render);
            },

            /**
             * отображение представления
             */
            render: function () {
                var _this = this;
                this.$el.html(this.model.get('renderTemplate'));

                this.$el.find('#dateFrom, #dateTo').datetimepicker({
                    format: 'DD.MM.YYYY',
                    useCurrent: true,
                    locale: 'ru'
                });

                markup.attachActions(this.$el, {
                    backupClick: function () {
                        var data = {
                            dateFrom: _this.$el.find('#dateFrom').val(),
                            dateTo: _this.$el.find('#dateTo').val(),
                            includeInstances: _this.$el.find('#includeInstances').is(':checked')
                        };
                        _this.model.backup(data);
                    },
                    cancelClick: function () {
                        _this.closePage();
                    }
                });
                this.model.set('blockRendered', true)
            }

        });
        return view;
    });
