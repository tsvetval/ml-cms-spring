/**
 * Модель блока списка объектов
 *
 */
define(
    ['log', 'misc', 'backbone',
        'cms/events/events',
        'cms/page_blocks/list_block/ListBlockModel',
        'cms/events/NotifyPageBlocksEventObject'
    ],
    function (log, misc, backbone, cmsEvents, ListBlockModel, NotifyEventObject) {
        var model = ListBlockModel.extend({
            defaults: {
                className: undefined,
                folderId: undefined,
                objectId: undefined,
                pageTitle: 'undefined',
                controller: 'objectListController'
            },

            /**
             * инициализация модели
             *
             */
            initialize: function () {
                /* ListBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this)

                var _this = this;
                log.debug('initialize ClassObjectListBlockModel');
                var title = 'Просмотр папки <%=folderTitle%>';
                _this.set("pageTitle", title);
            },

            restorePageListener: function(params){
                var _this = this;
                _this.getClassName(params.objectId,
                    function(){
                        log.debug("ClassObjectListBlockModel start restorePage");
                        _this._reset();
                        _this._update();
                    });
            },

            _updatePageTitle: function(){
                var _this = this;
                var className = this.get("className");
                var title = 'Просмотр объектов класса '+className;
                _this.set("pageTitle", title);
                _this.changePageParams({
                    pageTitle: title
                });
            },

            getClassName: function(objectId, callback){
                var _this = this;
                var options = {
                    action: 'getClass',
                    url: misc.getContextPath() + this.getSiteResource('showObjectsController'),
                    data: {
                        objectId: objectId,
                        pageBlockId: this.get('blockInfo').get('id'),
                        ml_request: true
                    }
                };
                _this.callServerAction(options, function(result){
                    _this.set("className",result.class);
                    callback.apply();
                })
            }
        });
        return model;
    });
