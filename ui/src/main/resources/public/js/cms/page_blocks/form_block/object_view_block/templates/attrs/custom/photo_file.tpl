<div class="attribute-view-photo_file">
    <div class="col-xs-offset-<%=attrModel.get('offset')%> col-xs-<%=attrModel.get('titleLength')%>">
        <b class="attr-label"><%=attrModel.escape('description')%>:</b>
    </div>
    <div class="col-xs-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%>">
        <%if (attrModel.get('value')){%>
        <div class="col-xs-3 thumbnail">
            <a href="#" data-toggle="modal" data-target="#photo-modal-<%=attrModel.get('id')%>">
                <img src="<%= thumbnail %>" class="img-responsive">
            </a>
        </div>

        <div id="photo-modal-<%=attrModel.get('id')%>" class="modal fade">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <img src="<%= url %>" class="img-responsive">
                    </div>
                </div>
            </div>
        </div>
        <%}%>
    </div>
</div>