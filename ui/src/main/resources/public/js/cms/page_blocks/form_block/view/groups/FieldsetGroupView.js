define(
    ['backbone', 'underscore', 'log', 'cms/page_blocks/form_block/view/groups/DefaultGroupView',
        'cms/page_blocks/form_block/factories/GroupViewFactory',
        'cms/page_blocks/form_block/collections/AttrGroupCollection',
        'text!cms/page_blocks/form_block/templates/groups/FieldsetGroupViewTemplate.tpl',
    ],
    function (backbone, _, log, GroupView, GroupViewFactory, AttrGroupCollection, viewTemplate) {
        var view = GroupView.extend({

            initialize: function (options) {
                view.__super__.initialize.apply(this, [options])
                log.debug("initialize FieldsetGroupView");
            },

            render: function () {
                var _this = this
                this.$el.append(_.template(viewTemplate, {title: this.model.get('title')}))
                var $contentDiv = this.$el.find('.fieldset-content');
                var $groupContainer = $('<div class="group-container" id="group-container-'+ this.model.getGroupId() +'"></div>');
                $contentDiv.append($groupContainer)
                _this._renderGroupContent(this.model, $groupContainer)
            }
        })

        return view
    })