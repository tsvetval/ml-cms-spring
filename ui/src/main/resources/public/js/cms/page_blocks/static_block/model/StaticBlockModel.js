define(
    ['log', 'misc', 'backbone', 'cms/events/events', 'cms/model/PageBlockModel', 'cms/page_blocks/DialogPageBlock'],
    function (log, misc, backbone, cmsEvents, PageBlockModel, Message) {
        var model = PageBlockModel.extend({
            defaults: {
                renderTemplate: undefined,
                controller: 'staticController'
            },

            initialize: function () {
                console.log("initialize StaticBlockModel");

                /* PageBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this)
            },

            /**
             * Добавление событий блока
             * @Override PageBlockModel.bindEvents
             */
            bindEvents: function () {
                /* PageBlockModel.bindEvents() добавляет основные события блока */
                model.__super__.bindEvents.call(this)

                this.listenTo(this, cmsEvents.RESTORE_PAGE, function () {
                    this.prompt();
                });
            },

            /**
             * Остановка обработчиков событий блока
             * @Override PageBlockModel.unbindEvents
             */
            unbindEvents: function () {
                /*  События блока */
                this.stopListening(this, cmsEvents.RESTORE_PAGE);

                /* PageBlockModel.unbindEvents() удаляет все основные события блока */
                model.__super__.unbindEvents.call(this)
            },

            prompt: function () {
                var _this = this;
                var options = {
                    action: "show"
                };

                var callback = function (result) {
                    _this.set('renderTemplate', result.html);
                    _this.trigger(cmsEvents.RENDER_VIEW);
                };

                return this.callServerAction(options, callback);
            }
        });
        return model;
    });
