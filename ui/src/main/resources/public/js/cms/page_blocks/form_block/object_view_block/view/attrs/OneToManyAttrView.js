/**
 * Представление для отображение атрибута типа ONE_TO_MANY
 */
define(
    ['log', 'misc', 'backbone', 'underscore', 'jquery', 'boot_table_ru', 'moment',
        'helpers/ViewHelpers',
        'cms/page_blocks/form_block/view/AttrView',
        'text!cms/page_blocks/form_block/object_view_block/templates/attrs/OneToManyAttrView.tpl',
        'table_formatter_mixin'
    ],
    function (log, misc, backbone, _, $, boot_table, moment, helpers,
              AttrView, DefaultTemplate, table_formatter_mixin) {
        var view = AttrView.extend({
            events : {
                "click .attr-list-preview-item": "linkObjectPreviewClick",
                "click .file-download-link": "downloadFile",
                "mouseup .attr-list-title": _.debounce(function (e) {
                    /* Dirty hack for handle single and double click on once dom element. @see AttrView#openAttrView*/
                    if(this.doubleclicked) {
                        this.doubleclicked = false
                    } else {
                        this.expandAttributeTable.call(this, e)
                    }
                }, 300),
                "click .attr-list-preview-all": function () {
                    this.showAllAttrs = !this.showAllAttrs;
                    this.render();
                },
                "click .table-expand-full": function (e) {
                    var $tableWrapper = this.$('.attr-list-details .scrollbar-external_wrapper');
                    if ($tableWrapper.hasClass('expanded')) {
                        $tableWrapper.css('max-height', this.model.get('tableHeight') + 'px').removeClass('expanded');
                        $(e.target).find('i').removeClass().addClass('glyphicon glyphicon-resize-full');
                        $(e.target).removeClass('active')
                    } else {
                        $tableWrapper.css('max-height', '').addClass('expanded');
                        $(e.target).find('i').removeClass().addClass('glyphicon glyphicon-resize-small');
                        $(e.target).addClass('active')
                    }

                },
                "click .table-filter-toggle": function (e) {
                    this.$('#table-filter-row').toggle();
                    $(e.target).toggleClass('active');
                    /* Init extended scroll in table */
                    this.helpers.tableExtendedScrollInit.apply(this);
                },
                "keyup .attr-list-search": "listViewFilter"
            },

            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                _.extend(this.events, AttrView.prototype.events);
                this.model.set('view', this)
            },

            /**
             * Отрисовка представления
             */
            render : function() {
                if (this.isHidden()) {
                    return;
                }
                var _this = this;
                this.$el.html( _.template(this.viewTemplate, {attrModel : this.model}));
                var $previewContainer = this.$el.find('.attr-list-preview');

                /* Add class for expand list as table */
                if (this.model.get('value') && this.model.get('value').objectList.length > 0){
                    this.$el.find('.attr-label').addClass('attr-list-title');
                    this.attrTable = undefined;
                }

                if (this.model.get('value').objectList.length > 5 && !this.showAllAttrs) {
                    this.$('.attr-list-search').hide();
                    /* Render first 5 elements */
                    var objects = _.first(this.model.get('value').objectList, 5);
                    objects.forEach(function(objectValue){
                        var $objectContainer = $('<span/>',
                            {"class" : "attr-list-preview-item",
                                "objectId" : objectValue.objectId,
                                "mlClass" : objectValue.mlClass}
                        );
                        $objectContainer.html(_.escape(objectValue.title) + ' ');
                        $previewContainer.append($objectContainer);
                        $previewContainer.append('\n');
                    });

                    /* Add "show all" button */
                    $previewContainer.append($('<span>', {
                        class: "attr-list-preview-all",
                        title: "Показать еще " + (this.model.get('value').objectList.length - 5) + " элементов."
                    }).append($('<i>', {
                        class: "glyphicon glyphicon-option-horizontal"
                    })));
                } else {
                    if (this.model.get('value').objectList.length > 5) this.$('.attr-list-search').show();
                    /* Render all elements */
                    this.model.get('value').objectList.forEach(function(objectValue){
                        var $objectContainer = $('<span/>',
                            {"class" : "attr-list-preview-item",
                                "objectId" : objectValue.objectId,
                                "mlClass" : objectValue.mlClass}
                        );
                        $objectContainer.html(_.escape(objectValue.title) + ' ');
                        $previewContainer.append($objectContainer);
                        $previewContainer.append('\n');
                    });

                    if (!_.isEmpty(this.model.get('value').objectList) && this.showAllAttrs) {
                        /* Add "close all" button */
                        $previewContainer.append($('<span>', {
                            class: "attr-list-preview-all",
                            title: "Скрыть остальные"
                        }).append($('<i>', {
                            class: "glyphicon glyphicon-chevron-left"
                        })));
                    }
                }
            },

            createAttrTable: function () {
                var _this = this;
                this.$table = this.$el.find('.table-javascript');

                var columns = [];
                columns.push({
                    field: 'objectId',
                    visible: false
                });
                this.model.get('value').attrList.forEach(function (columnAttr) {
                    var formatter = _this.formatterString;
                    var sortable = true;
                    if (columnAttr.fieldType === "ONE_TO_MANY" || columnAttr.fieldType === "MANY_TO_MANY") {
                        formatter = _this.formatterOneToMany;
                        sortable = false;
                    } else if (columnAttr.fieldType === "MANY_TO_ONE") {
                        formatter = _this.formatterManyToOne;
                        sortable = false;
                    } else if (columnAttr.fieldType === "BOOLEAN") {
                        formatter = _this.formatterBoolean;
                        sortable = true;
                    } else if (columnAttr.fieldType === "ENUM") {
                        formatter = _this.formatterEnum;
                        sortable = true;
                    } else if (columnAttr.fieldType === "DATE") {
                        formatter = _this.formatterDateWithOptions({dateFormat : columnAttr.fieldFormat});;
                        sortable = true;
                    } else if (columnAttr.fieldType === "FILE") {
                        formatter = _this.formatterFile;
                        sortable = true;
                    } else if (columnAttr.fieldType === "LONG_LINK") {
                        sortable = false;
                    }

                    var column = {
                        field: columnAttr.entityFieldName,
                        title: columnAttr.description,
                        align: 'left',
                        valign: 'top',
                        sortable: sortable,
                        formatter: formatter
                    };
                    columns.push(column);
                });

                var data = this.createTableDataFromValue();

                this.$table.bootstrapTable({
/*
                    toolbar: _this.$toolbar,
                    label: _this.$attrLabelContainer,
*/
                    idField: 'objectId',
                    data: data,
                    cache: false,
                    striped: true,
                    pagination: true,
                    pageSize: 5,
                    pageList: [5, 10, 20, 50],
                    showColumns: false,
                    minimumCountColumns: 1,
                    search: false,
                    clickToSelect: true,
                    columns: columns
/*
                    toolbarAlign: 'right'
*/
                }).on('page-change.bs.table sort.bs.table', function () {
                    _this.helpers.tableExtendedScrollInit.apply(_this);
                });

                /* Add new row in header with cols filters */
                var $filterRow = $('<tr>', {
                    id: 'table-filter-row'
                });

                /* Add columns filters */
                _.each(this.model.get('value').attrList, function (item) {
                    if (item.entityFieldName != 'objectId') {
                        if (item.fieldType == 'BOOLEAN') {
                            var $element = $('<select>', {
                                class: 'filter-input form-control',
                                'data-filter-field': item.entityFieldName
                            }).append($('<option>', {value: null}))
                                .append($('<option>', {value: true}).text('Да'))
                                .append($('<option>', {value: false}).text('Нет'));
                        } else {
                            var $element = $('<input>', {
                                class: 'filter-input form-control',
                                'data-filter-field': item.entityFieldName,
                                placeholder: 'Фильтр...'
                            });
                        }
                        $filterRow.append($('<th>').append($element))
                    }
                });
                this.$table.find('thead').append($filterRow);

                /* Bind filter events */
                $filterRow.find('.filter-input').off('change keyup').on('change keyup', function (e) {
                    var filterObject = {};
                    $filterRow.find('.filter-input').each(function (i, item) {
                        if ($(item).val() != '') {
                            var filter = {};
                            filter[$(item).data('filter-field')] = $(item).val();
                            _.extend(filterObject, filter)
                        }
                    });

                    var result = _this.helpers.dataFilter.apply(_this, [filterObject]);
                    _this.$table.bootstrapTable('load', _this.createTableDataFromValue(result));

                    /* Init extended scroll in table */
                    _this.helpers.tableExtendedScrollInit.apply(_this);

                });


                /* Init extended scroll in table */
                this.helpers.tableExtendedScrollInit.apply(this);


                /*this.$table.find('thead th').off('dblclick').on('dblclick', function (e) {
                    var $headerCell = $(this);
                    var $filterInput = $('<input>', {
                        type: 'text',
                        class: 'header-cell-filter'
                    });

                    /!* Reset input and table *!/
                   // _this.$table.find('.header-cell-filter').remove();
                    _this.$table.bootstrapTable('load', _this.createTableDataFromValue())

                    var field = _.findWhere(_this.model.get('value').attrList, {description: $(this).find('.th-inner').text()});

                    if (field && _.contains(["STRING", "TEXT", "DATE"], field.fieldType)) {

                        if (!$headerCell.find('.header-cell-filter').is('input')) {
                            $headerCell.find('.fht-cell').append($filterInput);
                        }

                        $filterInput.focus();
                        $filterInput.off('keyup').on('keyup', function (e) {
                            var result = _this.helpers.dataFilter.apply(_this, [$(this).val(), field.entityFieldName]);
                            _this.$table.bootstrapTable('load', _this.createTableDataFromValue(result))
                        })
                    }
                });*/
            },


            /**
             * Заполнение данных об атрибутах связанных объектов
             */
            createTableDataFromValue: function (value) {
                var data = [];
                if (!value) {
                    if (this.model.get('value') && this.model.get('value').objectList) {
                        this.model.get('value').objectList.forEach(function (objectData) {
                            data.push($.extend(objectData.attrValues, {objectId: objectData.objectId}));
                        });
                    }
                } else {
                    value.forEach(function (objectData) {
                        data.push($.extend(objectData.attrValues, {objectId: objectData.objectId}));
                    });
                }

                return data;
            },

            /**
             * Обработка клика по связанному объекту (переход к просмотру объекта)
             */
            linkObjectPreviewClick : function(event){
                this.model.openObject($(event.currentTarget).attr('objectId'), $(event.currentTarget).attr('mlClass'));
                return false;
            },

            /**
             * Фильтр значений по событию keyup в строке поиска.
             * @param e
             */
            listViewFilter: function(e) {
                var $input = $(e.target);
                var data = this.helpers.dataFilter.apply(this, [$input.val(), 'title']);
                var $container = this.$('.attr-list-preview');
                $container.find('.highlighted').removeClass('highlighted');

                _.each(data, function (item) {
                    $container.find('span[objectid="' + item.objectId +'"]').addClass('highlighted')
                });
            }

        });

        _.extend(AttrView.prototype, table_formatter_mixin);
        _.extend(AttrView.prototype, {helpers: helpers});

        return view;
    });
