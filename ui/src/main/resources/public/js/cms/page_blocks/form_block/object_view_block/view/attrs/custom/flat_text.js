/**
 * Представление flat_text для отображение атрибута типа TEXT
 */
define([
        'cms/page_blocks/form_block/view/AttrView',
        'text!cms/page_blocks/form_block/object_view_block/templates/attrs/custom/flat_text.tpl'
    ],
    function (AttrView, DefaultTemplate) {
        var view = AttrView.extend({
            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                _.extend(this.events, AttrView.prototype.events);
            }
        });

        return view;
    });
