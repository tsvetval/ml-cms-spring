/**
 * Наследуем view и шаблон от dropdown_list_tags
 */

define([
        'select2',
        'cms/page_blocks/form_block/object_edit_block/view/attrs/custom/dropdown_list_tags'
    ],
    function (select2, AttrView) {
        var view = AttrView.extend({
            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
            },

            render: function () {
                if (this.isHidden()) {
                    return;
                }
                var _this = this;
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));

                this.$attrLabelContainer = this.$el.find('.attr-label-container');

                this.$inputField = this.$el.find('.attrField');

                this.addMandatoryEvents();
                this.addReadOnly();
                this.addPopoverHelper();

                var select2Data = [];

                var options = {
                    action: 'getAllRelationValues',
                    data: {
                        attrId: this.model.get('id'),
                        ml_request: true
                    }
                };
                $.when(this.model.callServerAction(options)).then(function (result) {
                    _this.model.set('availableObjects', result.objectList)

                    result.objectList.forEach(function (obj) {
                        select2Data.push({id: obj.id, text: obj.title})
                    })

                    var current_input_values = []
                    _this.model.get('value').objectList.forEach(function (obj) {
                        current_input_values.push(obj.objectId)
                    });

                    _this.$inputField.select2({
                        width: '100%',
                        data: select2Data,
                        allowClear: true,
                        placeholder: "Выберите значение",
                        tags: true,
                        createTag: function () {
                            return undefined
                        }
                    });

                    _this.$inputField.val(current_input_values).trigger('change');

                    _this.$inputField.on("change", function () {
                        _this.changeSelection(_this.$inputField.val());
                    });
                })
            }
        })

        return view
    }
)