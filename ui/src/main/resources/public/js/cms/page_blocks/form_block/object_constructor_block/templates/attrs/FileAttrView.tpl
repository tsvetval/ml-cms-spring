<div class="attr-label-container col-xs-offset-<%=attrModel.get('offset')%> col-xs-<%=attrModel.get('titleLength')%>">
    <b class="attr-label"><%=attrModel.get('description')%>:</b>
</div>

<div class="col-xs-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%> input-group">
    <input type="text" class="form-control attrField_fake" readonly="true"/>

    <span class="btn btn-primary input-group-addon buttonDelete" style="overflow: hidden;position: relative;">
        <label class="glyphicon glyphicon-trash"></label>
    </span>

    <span class="btn btn-primary input-group-addon" style="overflow: hidden;position: relative;">
        <label class="glyphicon glyphicon-file highlight-button"></label>
        <input  type="file"
               class="attrField"/>
    </span>
</div>
