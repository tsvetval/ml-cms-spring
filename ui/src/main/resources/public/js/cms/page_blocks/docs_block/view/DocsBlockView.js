define(
    [
        'cms/view/PageBlockView',
        'autolinker',
        'text!cms/page_blocks/docs_block/template/DocsTemplate.tpl',
        'text!cms/page_blocks/docs_block/template/ClsTemplate.tpl',
        'cms/events/events'
    ],
    function (PageBlockView, linker, DocsTemplate, ClsTemplate, Events) {
        var view = PageBlockView.extend({
            events: {
                "click .cls": "showClass"
            },

            initialize: function () {
                this.listenTo(this.model, Events.RENDER_VIEW, this.render);
                this.listenTo(this.model, 'renderClassInfo', this.renderClassInfo);
                this.listenTo(this.model, 'change:data', this.update);
            },

            render: function () {
                this.$el.html(_.template(DocsTemplate, {model: this.model}));

                this.model.set('blockRendered', true);
                return true;
            },
            renderClassInfo: function(){
                var cont = this.$('#infoContainer');
                cont.html(_.template(ClsTemplate,{classInfo: this.model.get("classInfo"), linker: linker}))
            },

            showClass: function(e){
                e.preventDefault();
                var clsName = $(e.target).parent().data('class');
                this.model.trigger("getClassInfo", {clsName: clsName});
            }

        });
        return view;
    });
