define(
    ['log', 'misc', 'backbone', 'cms/events/events',
        'cms/model/PageBlockModel', 'cms/page_blocks/DialogPageBlock'
    ],
    function (log, misc, backbone, cmsEvents, PageBlockModel, Message) {
        var model = PageBlockModel.extend({

            defaults: {
                pageTitle: 'Просмотр папки Ресурсы',
                controller: 'resourceViewController'
            },

            initialize: function () {
                /* PageBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this)
                this.set('flattenResources', new backbone.Collection())
                console.log("initialize BlockModel");
            },

            /**
             * Добавление событий блока
             * @Override PageBlockModel.bindEvents
             */
            bindEvents: function () {
                /* PageBlockModel.bindEvents() добавляет основные события блока */
                model.__super__.bindEvents.call(this)

                var _this = this;
                this.listenTo(this, cmsEvents.RESTORE_PAGE, function (params) {
                    _this.getPagesInfo();
                });
                this.listenTo(this, cmsEvents.OPEN_FOLDER, function (params) {
                    _this.updatePageTitle(this.get('pageTitle'))
                });
                this.listenTo(this, "getPagesInfo", _this.getPagesInfo);
            },

            /**
             * Остановка обработчиков событий блока
             * @Override PageBlockModel.unbindEvents
             */
            unbindEvents: function () {
                /*  События блока */
                this.stopListening(this, cmsEvents.RESTORE_PAGE);
                this.stopListening(this, cmsEvents.OPEN_FOLDER);
                this.stopListening(this, "getPagesInfo");

                /* PageBlockModel.unbindEvents() удаляет все основные события блока */
                model.__super__.unbindEvents.call(this)
            },

            getPagesInfo: function () {
                var _this = this;
                var data = {
                    action: 'getPagesInfo',
                    pageBlockId: this.get('blockInfo').get('id'),
                    ml_request: true

                };

                _this.callServerAction(data).then(function (result) {
                    if (result && result.pagesInfo) {
                        _this.set("pagesInfo", result.pagesInfo);
                        _this.trigger(cmsEvents.RENDER_VIEW);
                    } else {
                        _this.displayErrorMessage("Данные не получены", "Произошла ошибка при загрузке информации о ресурсах.")
                    }
                })
            }
        });
        return model;
    });
