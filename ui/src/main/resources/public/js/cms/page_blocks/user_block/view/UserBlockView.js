/**
 * @name: UserBlockView
 * @package:
 * @project: ml-cms-all
 * Created by i_tovstyonok on 19.01.2016.
 */

define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'cms/events/events',
    'text!cms/page_blocks/user_block/template/UserBlockTemplate.tpl'],
    function (log, misc, backbone, PageBlockView, cmsEvents, template) {
        var view = PageBlockView.extend({
            events: {
                "click #logoutButton" : "logoutHandler",
                "click #helpPage" : "showHelpHandler"
            },

            initialize: function () {
                log.debug("Initialize UserBlockView");
                this.listenTo(this.model, cmsEvents.RENDER_VIEW, this.render);
            },

            render: function () {
                /* Render view with template engine */
                this.$el.html(_.template(template, {
                    login: this.model.escape('login'),
                    showHelp: this.model.get('helpUrl') && this.model.get('helpUrl') != ''
                }))

                /* After render view set block as rendered */
                this.model.set('blockRendered', true);
                return true
            },

            logoutHandler: function () {
                this.model.logout()
            },

            showHelpHandler : function(){
                if (this.model.get('helpUrl')) {
                    window.open(misc.getContextPath() + "/" + this.model.get('helpUrl'));
                }
            },
        });
        return view
    }
);
