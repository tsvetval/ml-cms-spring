<div class="object-view">
    <div class="ml-main__content-title">
        <div class="ml-main__content-title-text" title="<%=formModel.get('title')%>"><%=formModel.escape('title')%></div>
    </div>
    <% if (canEdit) { %><div class="ml-main__content-edit">Редактировать</div><% } %>
    <div class="ml-main__content-delimiter"></div>
    <div class="ml-main__content-object-columns" id="view_content_container"></div>

    <!--<div class="container control-panel col-xs-24" id="view_buttons_container">
        <h4 class="form-title col-xs-10 callback">
            <%=formModel.get('title')%>
            <small>&nbsp(просмотр объектa класса <%=formModel.get('description')%>)</small>
        </h4>
        <div class="pull-right control-buttons">
            <div>
                <% if (canEdit) { %>
                <button class="btn btn-primary edit-object-button">
                    <span class="ml-main__content-edit"></span> Редактировать
                </button>
                <%}%>
            </div>
        </div>
    </div>
    <div id="view_content_container">

    </div>-->
    <div style="clear: both"></div>
</div>
