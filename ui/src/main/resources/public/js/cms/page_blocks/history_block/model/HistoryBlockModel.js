define(
    ['log', 'misc', 'backbone', 'cms/events/events', 'cms/model/PageBlockModel'],
    function (log, misc, backbone, cmsEvents, PageBlockModel) {
        var model = PageBlockModel.extend({
            defaults: {
                renderTemplate: undefined,
                currentPage: undefined,
                className: undefined,
                objectId : undefined,
                historyUtilId : undefined,
                pageTitle: 'Просмотр истории объекта',
                controller: 'historyController'
            },
            initialize: function () {
                /* PageBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this)

                console.log("initialize ObjectListBlockModel");
            },

            /**
             * Добавление событий блока
             * @Override PageBlockModel.bindEvents
             */
            bindEvents: function () {
                /* PageBlockModel.bindEvents() добавляет основные события блока */
                model.__super__.bindEvents.call(this)

                var _this = this;
                this.listenTo(this, cmsEvents.RESTORE_PAGE, function(params){
                    if(params.objectId && params.className) {
                        _this.loadHistoryList.apply(this, arguments);
                    }
                });
            },

            /**
             * Остановка обработчиков событий блока
             * @Override PageBlockModel.unbindEvents
             */
            unbindEvents: function () {
                /*  События блока */
                this.stopListening(this, cmsEvents.RESTORE_PAGE);

                /* PageBlockModel.unbindEvents() удаляет все основные события блока */
                model.__super__.unbindEvents.call(this)
            },

            loadHistoryList: function (options) {
                this.set('className', options.className);
                this.set('objectId', options.objectId);
                this.set('historyUtilId', options.historyUtilId);
                this._update()
            },

            refreshCurrentObjectList: function(){
                this._update()
            },
            setCurrentPage: function (pageNumber) {
                this.set('currentPage', pageNumber);
                this._update()
            },

            triggerRender: function () {
                this.trigger(cmsEvents.RENDER_VIEW);
            },

            _update: function () {
                var _this = this;
                this.updatePageTitle(this.get('pageTitle'));
                return this.callServerAction({
                    action: 'show',
                    data: {
                        className: this.get('className'),
                        objectId: this.get('objectId'),
                        historyUtilId: this.get('historyUtilId'),
                        currentPage: this.get('currentPage')
                    }
                }).then(function (result) {
                    _this.set('historyData', result);
                    _this.trigger(cmsEvents.RENDER_VIEW);
                });
            }


        });

        return model;
    });
