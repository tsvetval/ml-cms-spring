<div class="ml-main__content-extendend-search-page-icon <% if (!simple) {%>without-simple<% } %>" id="showExtendedSearch" title="Расширенный поиск"></div>
<% if (simple) {%>
<div class="ml-search-simple-search-panel dropdown-toggle"  data-toggle="dropdown">
    <div class="ml-main__content-search-page-icon" title="Найти"></div>
    <input type="text" placeholder="Найти…" id="simpleFilterText" class="simpleFilterText"
    >
</div>
<ul class="dropdown-menu search-choice-container" style="right: 10px;left: auto;cursor: pointer;"></ul>
<% } %>

<div id="extendedSearchModal" class="extended-search-modal modal fade bs-example-modal-lg" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="overflow: auto">
                <!--
                                <div class="row">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                </div>
                -->
                <h2 class="modal-title" id="myModalLabel" style="float: left; font-family: 'roboto_regular'; font-size: 20px;">Расширенный поиск</h2>
                <button type="button" class="btn btn-info" data-dismiss="modal" aria-label="Close" style="float: right">
                    <span aria-hidden="true">Закрыть</span>
                </button>
                <button class="btn btn-warning resetButton" style="float: right; margin-right: 5px">
                    Сброс
                </button>
                <button class="btn btn-success doExtendedSearchButton" style="float: right; margin-right: 5px;">
                    <span class="glyphicon glyphicon-search"></span> Найти
                </button>

            </div>
            <div class="modal-body extendedFiltersContainer">
            </div>
        </div>
    </div>
</div>

<!--
<div style="padding-top: 5px; border-bottom: solid 1px #dedede;">
    <div class="input-group searchbar simpleSearchPanel panel-collapse" id="simpleSearchPanel"
         style="width: 200px; padding-bottom: 5px">
        <div class="input-group-addon">
            <span>Поиск:</span>
        </div>

        <div class="btn-group">
            <input type="text" id="simpleFilterText" class="simpleFilterText dropdown-toggle" data-toggle="dropdown">
            <ul class="dropdown-menu search-choice-container"></ul>
        </div>

        <div class="input-group-btn">
            <button id="simpleFilterButton" class="btn btn-primary doSimpleSearchButton">
                <span class="glyphicon glyphicon-search"></span>
            </button>
        </div>

        <div class="input-group-btn" style="padding-left: 5px;">
            <button data-target="#extendedSearchPanel" id="extendedSearchButton"
                    class="btn btn-primary switchToExtendedSearchButton">
                Расширенный поиск
            </button>
        </div>

        <div class="input-group-btn" style="padding-left: 5px;">
            <button class="btn btn-primary resetButton">
                Сброс
            </button>
        </div>
    </div>

    <div id="extendedSearchPanel" class="extendedSearchPanel hide">
        <div class="input-group-btn" style="padding-left: 5px;">
            <button id="simpleSearchButton" class="btn btn-primary switchToSimpleSearchButton">
                Простой поиск
            </button>
        </div>
        <div class="input-group-btn" style="padding-left: 5px;">
            <button class="btn btn-primary resetButton">
                Сброс
            </button>
        </div>
        <div style="margin-top: 20px; margin-left: 50px;">
            <div class="extendedFiltersContainer"></div>

            <button class="btn btn-primary doExtendedSearchButton">
                <span class="glyphicon glyphicon-search"></span> Найти
            </button>
        </div>
    </div>
</div>
-->
