/**
 * Модель блока импорта файла репликации
 */
define(
    ['log', 'misc', 'backbone', 'cms/events/events', 'cms/model/PageBlockModel'],
    function (log, misc, backbone, Events, PageBlockModel) {
        var model = PageBlockModel.extend({
            defaults: {
                title: 'Импорт репликации из файла',
                fileName: undefined,
                controller: 'importReplicationController'
            },

            /**
             * Инициализация модели
             */
            initialize: function () {
                /* PageBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this)

                console.log("initialize ObjectListBlockModel");
            },

            /**
             * Добавление событий блока
             * @Override PageBlockModel.bindEvents
             */
            bindEvents: function () {
                /* PageBlockModel.bindEvents() добавляет основные события блока */
                model.__super__.bindEvents.call(this)

                this.listenTo(this, Events.RESTORE_PAGE, this.process);
            },

            /**
             * Остановка обработчиков событий блока
             * @Override PageBlockModel.unbindEvents
             */
            unbindEvents: function () {
                /*  События блока */
                this.stopListening(this, Events.RESTORE_PAGE);

                /* PageBlockModel.unbindEvents() удаляет все основные события блока */
                model.__super__.unbindEvents.call(this)
            },

            process: function () {
                var _this = this;
                this.updatePageTitle(this.get('title'));
                this.callServerAction({action: 'getTypes'}).then(function (types) {
                    if (!_.isEmpty(types)) {
                        _this.set('types', types);
                        _this.trigger(Events.RENDER_VIEW)
                    }
                })
            },

            upload: function (file) {
                var _this = this;
                var data = new FormData();
                data.append(file.name, file);

                $.ajax({
                    url: misc.getContextPath() + '/upload',
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();

                        /* Magic progress bar builder */
                        xhr.upload.addEventListener("progress", function(e){
                            if (e.lengthComputable) {
                                var percentComplete = Math.floor((e.loaded / e.total) * 100);
                                _this.trigger('upload:progress', percentComplete)
                            }
                        }, false);

                        return xhr
                    },
                    method: 'post',
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (response) {
                        _this.getFileInfo(file.name)
                    },
                    error: function (xhr) {
                        console.info('Upload fail', xhr)
                    },
                    complete: function () {
                        _this.trigger('upload:complete')
                    }
                })
            },

            /**
             * Получение предварительных данных о репликации
             * @param filename - имя файла предварительно загруженного на сервер
             */
            getFileInfo: function (filename) {
                var _this = this;
                this.callServerAction({
                    action: 'getFileInfo',
                    data: {
                        fileName: filename,
                        importType: this.get('importType')
                    }
                }).then(function (replication) {
                    _this.set('replication', replication);
                    _this.trigger('info:loaded', replication)
                }).fail(function () {
                    _this.trigger(Events.RENDER_VIEW)
                })
            },

            /**
             * Выполнение репликации
             */
            doReplication:function(){
                var _this = this;
                this.callServerAction({
                    action: 'doReplication',
                    data: {
                        fileName: this.get('replication').fileName,
                        importType: this.get('importType')
                    }
                }).then(function (replication) {
                    _this.trigger('import:complete', replication)
                }).fail(function () {
                    _this.trigger(Events.RENDER_VIEW)
                })
            }
        });

        return model;
    });

