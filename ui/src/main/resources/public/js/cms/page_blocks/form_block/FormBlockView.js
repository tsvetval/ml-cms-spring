/**
 * Представление базового блока отображения объекта в форме
 */
define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'cms/page_blocks/DialogPageBlock',
        'cms/page_blocks/form_block/factories/GroupViewFactory',
        'cms/page_blocks/form_block/factories/AttrViewFactory',
        'cms/page_blocks/form_block/collections/AttrGroupCollection'
    ],
    function (log, misc, backbone, PageBlockView, Message,
                GroupViewFactory,
                AttrViewFactory,
                AttrGroupCollection
        ) {
        var view = PageBlockView.extend({

            events : {
                "dblclick .ml-main__content-title" : "openObjectClass",
                "click .show-full-text": "showFullTextClick",
                "click .hide-full-text": "hideFullTextClick"
            },

            /**
             * Инициализация представления
             */
            initialize: function () {
                console.log("initialize FormBlockView");
                this.viewMode = undefined;
            },

            /**
             * Обработчик двойного клика по заголовку (открытие класса)
             */
            openObjectClass: function () {
                /* Только для роли главного администратора */
                if(_.contains(JSON.parse($.cookie().roles), 'MAIN_ADMIN_ROLE')) {
                    this.model.openPage(this.model.getSiteResource('viewObjectPage', 'pages'), 'Просмотр класса...', {objectId : this.model.get('entityId'), className:"MlClass"})
                }
            },

            /**
             * Отрисовка атрибутов без группы
             *
             * @param $content_container
             * @private
             */
            _renderNonGroupAttrs : function($content_container){
                var _this = this;
                // Выводим все атрибуты без групп
                var outAttrContainer = undefined;
                for (var index in this.model.get('nonGroupAttrList').models) {
                    var attrModel =  this.model.get('nonGroupAttrList').models[index];
                    if (attrModel.get('newLine') || !outAttrContainer){
                        // создаем новую строку
                        outAttrContainer = new $("<div/>", {"class": "col-xs-24 newLine"});
                        $content_container.append(outAttrContainer);
                    }
                    // создаем контейнер для атрибута
                    var $attrContainer = $('<div/>', {"class": "attrContainer"});
                    outAttrContainer.append($attrContainer);
                    _this._renderAttr(attrModel, $attrContainer);
                    //TODO refactor
                }
            },

            /**
             * Отрисовка атрибута
             *
             * @param attrModel
             * @param $attrContainer
             * @private
             */
            _renderAttr : function(attrModel, $attrContainer) {
                var _this = this
                AttrViewFactory.getAttrViewClass(attrModel, this.viewMode, $attrContainer)
                    .then(function (attrViewClass, model, $container) {
                        // Получаем представление для конкретного атрибуты
                        var attrView = new attrViewClass({model: model});
                        attrView.model.set('viewMode', _this.viewMode)
                        attrView.setElement($container);
                        attrView.render();
                    });

            },

            /**
             * Отрисовка групп атрибутов класса
             *
             * @param $content_container
             * @private
             */
            _renderRootGroups : function($content_container){
                var _this = this;
                var tabAttrGroupCollection = new AttrGroupCollection(),
                    accordionAttrGroupCollection = new AttrGroupCollection();
                this.model.get('rootGroupList').each(function (groupModel) {

                    // Если есть группы типа таб без родителя
                    if (groupModel.getGroupType() == 'TAB_LIST') {
                        if (tabAttrGroupCollection.length == 0 && !groupModel.getParentGroupId()) {
                            // Пробегаем по всем группам уровня и создаем список табов
                            _this.model.get('rootGroupList').each(function (tmpGroup) {
                                if (tmpGroup.getGroupType() == 'TAB_LIST' && !tmpGroup.getParentGroupId()) {
                                    tabAttrGroupCollection.add(tmpGroup)
                                }
                            });

                            // создание контейнера для группы
                            var $groupContainer = $('<div/>', {"class": "tab-group-container", style: "clear:both;"});
                            $content_container.append($groupContainer);
                            GroupViewFactory.getGroupViewClass(groupModel)
                                .then(function (tabGroupListViewClass) {
                                    var tabGroupListView = new tabGroupListViewClass({ model: tabAttrGroupCollection, viewMode : _this.viewMode, formBlockModel: _this.model});
                                    tabGroupListView.setElement($groupContainer);
                                    tabGroupListView.render();
                                });
                        }

                    } else if (groupModel.getGroupType() == 'ACCORDION') {
                        if (accordionAttrGroupCollection.length == 0 && !groupModel.getParentGroupId()) {

                            _this.model.get('rootGroupList').each(function (tmpGroup) {
                                if (tmpGroup.getGroupType() == 'ACCORDION' && !tmpGroup.getParentGroupId()) {
                                    accordionAttrGroupCollection.add(tmpGroup)
                                }
                            });

                            // создание контейнера для группы
                            var $groupContainer = $('<div/>', {"class": "accordion-group-container", style: "clear:both;"});
                            $content_container.append($groupContainer);
                            GroupViewFactory.getGroupViewClass(groupModel)
                                .then(function (accordionGroupViewClass) {
                                    var accordionGroupListView = new accordionGroupViewClass({ model: accordionAttrGroupCollection, viewMode : _this.viewMode, formBlockModel: _this.model});
                                    accordionGroupListView.setElement($groupContainer);
                                    accordionGroupListView.render();
                                });
                        }

                    } else {
                        if (groupModel.getGroupType()) {
                            //TODO add groupContainer
                            GroupViewFactory.getGroupViewClass(groupModel).then(function (GroupViewClass) {
                                var groupView = new GroupViewClass({model : groupModel, viewMode : _this.viewMode, formBlockModel: _this.model});
                                groupView.setElement($content_container);
                                groupView.render();
                            });
                        }
                    }
                });
            },

            /**
             * развернуть список связанных объектов
             * todo: move & refactor
             */
            showFullTextClick: function (e) {
                $(e.currentTarget).hide();
                $(e.currentTarget).next().show();
                $(e.currentTarget).prevAll(".ellipsis").hide();
                $(e.currentTarget).prevAll(".one-many-text-container").removeClass("collapsed");
            },

            /**
             * свернуть список связанных объектов
             * todo: move & refactor
             */
            hideFullTextClick: function (e) {
                $(e.currentTarget).hide();
                $(e.currentTarget).prev().show();
                $(e.currentTarget).prevAll(".ellipsis").show();
                $(e.currentTarget).prevAll(".one-many-text-container").addClass("collapsed");
            },
        });

        return view;
    });
