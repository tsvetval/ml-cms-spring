/**
 * @name: loadKladrBlockBoot
 * @package:
 * @project: ml-cms-all
 * @javaController:
 * Created by i_tovstyonok on 12.04.2016.
 */

define(
    ['log', 'misc', 'backbone', 'cms/page_blocks/load_kladr_block/model/loadKladrModel', 'cms/page_blocks/load_kladr_block/view/loadKladrView'],
    function (log, misc, backbone, model, view) {
        return {
            model: model,
            view: view
        };
    });
