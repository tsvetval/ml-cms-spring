
<style>
    .tree-color .jstree-anchor {
        color: black !important;
    }
</style>

<div class="panel panel-info col-xs-24">
    <div class="panel-heading">
        <h3 class="panel-title"><%= role.name %> ( <%= role.type %> )</h3>
    </div>
    <div class="panel-body">
        <% if(role.admin){ %>
            <div class="alert alert-success" role="alert">Это же админ! У него с доступом все в порядке! Не волнуйся...</div>
        <%} else {%>
        <% _.each(role.commonError, function(err) { %>
        <div class="alert alert-danger" role="alert"><%= err %></div>
        <% }); %>
        <div class="row">
            <!-- Folder access panel -->
            <div class="panel panel-default col-xs-12">
                <div class="panel-heading row">
                    <h2 class="pull-left">Доступ к папкам</h2>
                    <button type="button" class="btn btn-primary btn-sm addFolder pull-right">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Редактировать
                    </button>
                </div>
                <div class="panel-body">
                    <div id="folders"></div>
                </div>
            </div>

            <!-- Resource access panel -->
            <div class="panel panel-default col-xs-12">
                <div class="panel-heading row">
                    <h2 class="pull-left">Доступ к ресурсам</h2>
                    <button type="button" class="btn btn-primary btn-sm pull-right addPage">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Редактировать
                    </button>
                </div>
                <div class="panel-body">
                    <ul class="list-group">
                        <% _.each(role.resourceAccess, function(resource) { %>
                        <li class="list-group-item"
                        <% if(resource.def.length > 1){%> style="background-color: antiquewhite;"<%}%> data-toggle="collapse"
                        data-target="#collapseExample<%= resource.id %>" aria-expanded="false" aria-controls="collapseExample<%=
                        resource.id %>">
                        <span class="badge"><%= resource.def.length %></span>
                        <%= resource.resourceName %> <%= resource.resourceUrl %>
                        </li>
                        <div class="collapse" id="collapseExample<%= resource.id %>">
                            <div class="well">
                                <% _.each(resource.def, function(def) { %>
                                <span class="label label-warning"><%=def%></span>
                                <% }); %>
                            </div>
                        </div>
                        <% }); %>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- Class access panel -->
            <div class="panel panel-default col-xs-12">
                <div class="panel-heading row">
                    <h2 class="pull-left">Доступ к классам</h2>
                    <button type="button" class="btn btn-primary btn-sm pull-right addclass">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Редактировать
                    </button>
                </div>
                <div class="panel-body">
                    <ul class="list-group">
                        <% _.each(role.classAccess, function(cls) { %>
                        <li class="list-group-item">
                            <span class="badge">
                                <% if(cls.create) {%>C<%}%>
                                <% if(cls.read) {%>R<%}%>
                                <% if(cls.update) {%>U<%}%>
                                <% if(cls.delete) {%>D<%}%>
                            </span>
                            <%= cls.className %>(<%= cls.entityName %>)
                        </li>
                        <% }); %>
                    </ul>
                </div>
            </div>

            <!-- Utils access panel -->
            <div class="panel panel-default col-xs-12">
                <div class="panel-heading row">
                    <h2 class="pull-left">Доступ к утилитам</h2>
                    <button type="button" class="btn btn-primary btn-sm pull-right addUtil">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Редактировать
                    </button>
                </div>
                <div class="panel-body">
                    <ul class="list-group">
                        <% _.each(role.utilAccess, function(util) { %>
                        <li class="list-group-item" <% if(util.classes.length > 0 || util.folders.length > 0){%> style="background-color: antiquewhite;"<%}%> data-toggle="collapse"
                        data-target="#UcollapseExample<%= util.id %>" aria-expanded="false" aria-controls="UcollapseExample<%=
                        util.id %>">
                        <%= util.utilName %>
                        </li>
                        <div class="collapse" id="UcollapseExample<%= util.id %>">
                            <div class="well">
                                <% _.each(util.classes, function(cls) { %>
                                <span class="label label-warning">Утилита отображается на классах <%=cls%>. Нет доступа на чтение.</span>
                                <% }); %>
                                <% _.each(util.folders, function(fld) { %>
                                <span class="label label-warning">Утилита отображается на папке <%=fld%>, к ней нет доступа.</span>
                                <% }); %>
                            </div>
                        </div>
                        <% }); %>
                    </ul>
                </div>
            </div>
        </div>

        <%}%>
    </div>
</div>


