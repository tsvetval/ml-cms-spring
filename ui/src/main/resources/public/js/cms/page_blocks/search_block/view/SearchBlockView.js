define(
    [
        'log',
        'misc',
        'backbone',
        'cms/events/events',
        'cms/view/PageBlockView',
        'cms/events/NotifyPageBlocksEventObject',
        'cms/page_blocks/search_block/extendedSearch/ExtendedSearch',
        'text!cms/page_blocks/search_block/template/searchpanel.tpl'
    ],
    function (log, misc, backbone, cmsEvents, PageBlockView, NotifyEventObject, ExtendedSearch, SearchPanelTemplate) {
        var view = PageBlockView.extend({

            events: {
                "click .resetButton": "resetSearch",

                "click .switchToSimpleSearchButton": "simpleSearchClick",
                "click #showExtendedSearch": "extendedSearchClick",

                "click .ml-main__content-search-page-icon": "doSimpleSearchClick",
                "click .doExtendedSearchButton": "doExtendedSearchClick",

                "keydown .simpleFilterText": "simpleSearchEnter"
            },

            defaults: {
                renderTemplate: undefined
            },

            initialize: function () {
                var _this = this;
                console.log("initialize SimpleSearchBlockView");

                this.listenTo(this.model, cmsEvents.RENDER_VIEW, _this.render);
                this.listenTo(this.model, cmsEvents.HIDE_VIEW, _this.hide);

                this.listenTo(this.model, cmsEvents.OPEN_FOLDER, function (params) {
                    _this.$el.find("#simpleFilter").val("");
                    _this.model.set('search', undefined);
                });

                this.extendedSearch = new ExtendedSearch({blockModel: this.model});
            },

            render: function () {
                var _this = this;
                var simpleAttrs = this.model.get("searchMetaData").attrList.objectList;
                var $html = $(_.template(SearchPanelTemplate, {simple: !_.isEmpty(simpleAttrs)}));

                this.$extendedSearchHtml = this.extendedSearch.render().$el;
                $html.find(".extendedFiltersContainer").append(this.$extendedSearchHtml);


                _this.$el.html($html);

                var $dropdown = _this.$el.find(".dropdown-menu");
                $dropdown.empty();
                $.each(simpleAttrs, function (index, elem) {
                    var $option = $('<li><a><span class="prefix">' + elem.attrValues.description + ' : </span><span class="query"/></a></li>')
                    $option.on('click', function () {
                        var criteria = _this.$el.find("input.simpleFilterText").val();
                        _this.model.set('search', {
                            type: 'simple',
                            criteria: criteria,
                            searchField: elem.attrValues.entityFieldName
                        });
                    });
                    $dropdown.append($option);
                });

                $dropdown.parent().on('shown.bs.dropdown ', function () {
                    if ($dropdown.height() > $('.ml-main__content').height() + 20) {
                        $dropdown.css({'height': $('.ml-main__content').height() + 'px', 'overflow': 'auto'})
                    }
                });

                _this.$el.find("input.simpleFilterText").on('keyup', function () {
                    $dropdown.find(".query").text($(this).val());
                });

                this.model.set('blockRendered', true)
            },

            hide : function(){
                this.$el.html('');
                this.model.set('blockRendered', true)
            },




            simpleSearchEnter: function (e) {
                var code = e.keyCode || e.which;
                if (code == 13) {
                    this.doSimpleSearchClick();
                } else if (code == 27) {
                    this.resetSearch();
                }
            },

            resetSearch: function () {
                this.$el.find("input.simpleFilterText").val("");
                this.doSimpleSearchClick();
                this.model.trigger(cmsEvents.RESET_SEARCH);
            },

            simpleSearchClick: function () {
                this.resetSearch();
                this.$el.find(".simpleSearchPanel").removeClass('hide');
                this.$el.find(".extendedSearchPanel").addClass('hide');
            },

            extendedSearchClick: function () {
                $('#extendedSearchModal').modal();
            },

            doSimpleSearchClick: function () {
                var criteria = this.$el.find("input.simpleFilterText").val();
                if (criteria) {
                    this.model.set('search', {type: 'simple', criteria: criteria});
                } else {
                    this.model.set('search', {type: 'simple', criteria: null});
                }
            },

            doExtendedSearchClick: function () {
                var extFilterJson = this.$extendedSearchHtml.data('extendedFilterView').getJson();
                this.model.set('search', {type: 'extended', criteria: JSON.stringify(extFilterJson)});
            }

        });

        return view;
    });
