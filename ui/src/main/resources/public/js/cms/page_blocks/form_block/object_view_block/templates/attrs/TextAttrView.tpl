<div class="attribute-view-text">
    <div class="col-xs-offset-<%=attrModel.get('offset')%> col-xs-<%=attrModel.get('titleLength')%>">
        <b class="attr-label"><%=attrModel.escape('description')%>:</b>
    </div>
    <div class="col-xs-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%> field-text">
        <%=attrModel.get('value')%>
    </div>
</div>
