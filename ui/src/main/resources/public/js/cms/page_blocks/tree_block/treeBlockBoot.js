/**
 *  дерево
 *  Контроллер: ru.ml.web.block.controller.impl.NavigationPageBlockController
* */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/tree_block/model/TreeBlockModel',
        'cms/page_blocks/tree_block/view/TreeBlockView'],
    function (log, misc, backbone, model, view) {
        return {
            model : model,
            view : view
        };
    });
