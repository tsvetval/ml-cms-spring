<div class="attr-label-container col-xs-offset-<%=attrModel.get('offset')%> col-xs-<%=attrModel.get('titleLength')%>">
    <b class="attr-label"><%=attrModel.escape('description')%>:</b>
</div>

<div class="select2holder col-xs-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%>">
    <select type="hidden" class="attrField"/>
</div>