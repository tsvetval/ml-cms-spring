define(
    ['log', 'misc', 'backbone', 'cms/events/events', 'cms/model/PageBlockModel', 'cms/events/NotifyPageBlocksEventObject'],
    function (log, misc, backbone, cmsEvents, PageBlockModel, NotifyEventObject) {
        var model = PageBlockModel.extend({
            defaults: {
                /*Идентификатор текущего каталога дерева*/
                folderId: undefined,
                controller: 'navigationController'
            },
            initialize: function () {
                /* PageBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this)

                console.log("initialize NavigationBlockModel");
            },

            /**
             * Добавление событий блока
             * @Override PageBlockModel.bindEvents
             */
            bindEvents: function () {
                /* PageBlockModel.bindEvents() добавляет основные события блока */
                model.__super__.bindEvents.call(this)

                var _this = this;
                this.listenTo(this, cmsEvents.RESTORE_PAGE, function (options) {
                    if (options.folderId && options.folderId != this.get('folderId')) {
                        _this.changePageHash({folderId: options.folderId}, 'replaceState', 'extendHash');
                        _this.updatePageTitle('Список объектов ...');
                        _this.set("folderId",options.folderId);
                    }
                    _this.trigger(cmsEvents.RENDER_VIEW);
                });

                this.listenTo(this, cmsEvents.REFRESH_PAGE, function (options) {
                    _this.trigger(cmsEvents.ACTIVATE_FOLDER, options);
                });
            },

            /**
             * Остановка обработчиков событий блока
             * @Override PageBlockModel.unbindEvents
             */
            unbindEvents: function () {
                /*  События блока */
                this.stopListening(this, cmsEvents.RESTORE_PAGE);
                this.stopListening(this, cmsEvents.REFRESH_PAGE);

                /* PageBlockModel.unbindEvents() удаляет все основные события блока */
                model.__super__.unbindEvents.call(this)
            },

            openFolder: function (data) {
                var _this = this;
                var folderInfo;
                if(data.folderId != "#"){
                    folderInfo = JSON.parse(data.folderId);
                }else{
                    folderInfo = data.folderId;
                }
                this.updatePageTitle('Список объектов ...');
                var mode = data.historyMode || 'pushState';
                if(folderInfo.folderId){
                    var folderId = folderInfo.folderId;
                    var classifierId = folderInfo.classifierId;
                    var value = folderInfo.value;

                    this.set('folderId', folderInfo.folderId);
                    this.get('pageModel').once('change:blocksReady', function () {
                        if(_this.get('pageModel').get('blocksReady')) {
                            _this.changePageHash({folderId: folderInfo.folderId}, mode, 'extendHash');
                            _this.notifyPageBlocks(new NotifyEventObject(cmsEvents.OPEN_FOLDER, {folderId: folderInfo.folderId, classifierId: classifierId,value:value}));
                            _this.get('pageModel').set('blocksReady', false)
                        }
                    })
                    this.trigger(cmsEvents.SHOW_PAGE_BLOCK, {folderId:folderInfo.folderId, pageBlocks:data.pageBlocks, data: data.folderId});
                }else{
                    this.set('folderId', data.folderId);
                    this.get('pageModel').once('change:blocksReady', function () {
                        if(_this.get('pageModel').get('blocksReady')) {
                            _this.changePageHash({folderId: data.folderId}, mode, 'extendHash');
                            _this.notifyPageBlocks(new NotifyEventObject(cmsEvents.OPEN_FOLDER, {folderId: data.folderId}));
                            _this.get('pageModel').set('blocksReady', false)
                        }
                    })
                    this.trigger(cmsEvents.SHOW_PAGE_BLOCK, data);
                }
            },

            findFolderPath: function(folderId){
                var def = new $.Deferred();
                this.callServerAction('GET', "/site/navigation/"+ folderId +"/user-data", null,
                    function (result) {
                        def.resolve(result);
                    }
                );
                return def.promise();
            },

            renameFolder: function (folderId, title) {
                alert("Not implemented renameFolder");
                this.callServerAction({
                        action: "modifyNode",
                        data: {
                            objectId: folderId,
                            text: title
                        }
                    }
                );
            },

            deleteFolder: function (folderId) {
                var _this = this;
                alert("Not implemented renameFolder");
                this.callServerAction({
                        action: "removeNode",
                        data: {
                            objectId: folderId
                        }
                    }, function (result) {
                        var options = {
                            parentId: result[0].parentId
                        };
                        _this.trigger(cmsEvents.FOLDER_DELETED, options);
                    }
                );
            },

            createFolder: function (parentId, title, nodeId) {
                var _this = this;
                alert("Not implemented renameFolder");
                this.callServerAction({
                        action: "createNode",
                        data: {
                            parentId: parentId,
                            text: title
                        }
                    }, function (result) {
                        console.log("Node Id: " + nodeId + " Folder Id: " + result[0].objectId);
                        var options = {
                            nodeId: nodeId,
                            folderId: result[0].objectId
                        };
                        _this.trigger(cmsEvents.FOLDER_CREATED, options);
                    }
                );
            },

            editFolder: function (folderId) {
                var _this = this;
                this.replacePage(
                    this.getSiteResource('editObjectPage', 'pages'),
                    'Просмотр объекта ...',
                    {
                        objectId: folderId,
                        className: 'MlFolder'
                    }
                );
            }

        });

        return model;
    });
