<div class="attribute-view-flat_text">
    <div class="col-xs-offset-<%=attrModel.get('offset')%> col-xs-<%=attrModel.get('titleLength')%>">
        <b class="attr-label"><%=attrModel.get('description')%>:</b>
    </div>
    <div class="col-xs-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%> field-text">
        <% if (attrModel.escape('value')) print(attrModel.escape('value').replace(/\n/g, '<br>'))%>
    </div>
</div>
