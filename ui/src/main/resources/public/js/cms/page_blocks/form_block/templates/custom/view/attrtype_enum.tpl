<div class="col-xs-offset-<%=attrModel.get('offset')%> col-xs-<%=attrModel.get('titleLength')%>">
    <b class="attr-title"><%=attrModel.escape('description')%>:</b>
</div>

<div class="col-xs-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%>">
    <%if (attrModel.get('value')){%>
    <%= _.escape(attrModel.get('value').title) %>
    <%}%>
</div>