<div class="attribute-view-file">
    <div class="col-xs-offset-<%=attrModel.get('offset')%> col-xs-<%=attrModel.get('titleLength')%>">
        <b class="attr-label"><%=attrModel.escape('description')%>:</b>
    </div>
    <div class="col-xs-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%>">
        <%if (attrModel.get('value')){%>
        <a href="#" class="file-download-link" objectId="<%= attrModel.get('value').objectId%>"><%=  attrModel.get('value').title ? _.escape(attrModel.get('value').title) : '' %></a>
        <%}%>
    </div>
</div>