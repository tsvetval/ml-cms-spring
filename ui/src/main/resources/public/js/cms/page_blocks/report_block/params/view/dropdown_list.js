/**
 * Наследуем view и шаблон от ManyToOneAttrView
 */

define([
        'select2', 'cms/page_blocks/form_block/object_edit_block/view/attrs/custom/dropdown_list',
        'text!cms/page_blocks/form_block/object_edit_block/templates/attrs/dropdown_list.tpl'],
    function (select2, AttrView, DropdownTemplate) {
        var view = AttrView.extend({
            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.model.set("description", this.model.get("name"));
                this.viewTemplate = DropdownTemplate
            },

            render: function () {
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));

                this.$attrLabelContainer = this.$el.find('.attr-label-container');

                this.$inputField = this.$el.find('.attrField');

                this.addMandatoryEvents();
                this.addReadOnly();
                this.updateList();

            },
            updateList: function (newValue) {
                var _this = this
                var options = {
                    action: 'getAllRelationValues',
                    data: {
                        paramId: _this.model.get('id')
                    }
                };
                if (newValue) {
                    options.data.otherValue = newValue.newValue;
                }
                $.when(this.model.callServerAction(options)).then(function (result) {
                    var select2Data = []
                    result.objectList.forEach(function (obj) {
                        select2Data.push({id: obj.id, text: obj.title})
                    })
                    _this.$inputField.empty();
                    _this.$inputField.select2({
                        width: '100%',
                        data: select2Data,
                        allowClear: true,
                        placeholder: "Выберите значение"
                    });
                    if (_this.model.get("listenParam")) {
                        var otherModel = _this.model.get("pageBlockModel").get("paramsList").findWhere({id: _this.model.get("listenParam")});
                        _this.listenToOnce(otherModel, "reloadList", _this.updateList);
                    }
                    if (_this.model.get('value')) {
                        _this.$inputField.val(_this.model.get('value').objectId).trigger('change');
                    } else {
                        _this.$inputField.val(null).trigger('change');
                    }

                    _this.$inputField.on("change", function () {
                        _this.changeSelection(_this.$inputField.val());
                    });
                })
            },
            /**
             * Обработчик изменения значения HTML-элемента
             */
            changeSelection: function (value) {
                this.model.set('value', {objectId: value}, {silent: true});
                this.model.trigger("reloadList", {newValue: value});
            }
        })

        return view
    }
)