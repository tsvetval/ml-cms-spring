define(
    ['log', 'misc', 'backbone', 'jquery', 'bootstrap', 'underscore',
        'cms/page_blocks/form_block/factories/GroupViewFactory',
        'cms/page_blocks/form_block/view/groups/DefaultGroupView',
        'text!cms/page_blocks/form_block/templates/groups/TabGroupViewTemplate.tpl'
    ],
    function (log, misc, backbone, $, bootstrap, _,
              GroupViewFactory, GroupView, viewTemplate) {

        /**
         * view.model is instance of AttrGroupCollection
         */
        var view = GroupView.extend({
            initialize: function (options) {
                view.__super__.initialize.apply(this, [options])
                log.debug("initialize TabListGroupView");

            },

            render : function() {
                var _this = this;
                this.$el.append( _.template(viewTemplate, {attrGroupCollection : this.model}));
                var $tabsDiv = this.$el.find('.nav-tabs');
                var $contentDiv = this.$el.find('.tab-content');
                this.model.each(function(groupModel, i){
                    // create the tab
                    $tabsDiv.append($('<li>', {
                        'data-groupId': groupModel.getGroupId()
                    }).append($('<a>', {
                        href: '#attr-group-content-' + groupModel.getGroupId(),
                        'data-toggle': 'tab'
                    }).text(groupModel.getTitle())));

                    // create the tab content
                    var $tabGroupContainer = $('<div>', {
                        class: 'tab-pane',
                        id: 'attr-group-content-'+ groupModel.getGroupId()
                    });
                    $contentDiv.append($tabGroupContainer);
                    // make the first tab active
                    if (i == 0) $tabsDiv.find('a:first').tab('show');

                    if(groupModel.get('lazyLoad') && _this.viewMode != 'CREATE' && _this.model.size() > 1 && i != 0 && !groupModel.hasMandatory()) {

                        /* Event on tab activate load object data & render */
                        $(document).one('show.bs.tab', 'a[href="#attr-group-content-' + groupModel.getGroupId() + '"]', function () {
                            _this.formBlockModel._loadLazyGroupData(groupModel.getGroupId(), function () {
                                _this.renderLazyGroup(groupModel.getGroupId(), $tabGroupContainer)
                            })
                        })
                    } else if ((groupModel.get('lazyLoad') && _this.viewMode != 'CREATE' && i == 0) || (groupModel.hasMandatory() && _this.viewMode != 'CREATE') ) {

                        /* if lazy tab is first then load & render immediately */
                        _this.formBlockModel._loadLazyGroupData(groupModel.getGroupId(), function () {
                            _this.renderLazyGroup(groupModel.getGroupId(), $tabGroupContainer)
                        })
                    } else {
                        _this._renderGroupContent(groupModel, $tabGroupContainer)
                    }

                });
                // for right $contentDiv height todo: check & refactor
                $contentDiv.append( $('<div/>', {"class" : "newLine"}));
            }

        });

        return view;
    });
