
<div class="ml-main__content-title">
    <div class="ml-main__content-title-text">Выбор объектов <%=listModel.escape('ClassDescription')%></div>
</div>

<div class="btnClose object-selection-cancel">
    Отмена
</div>
<div class="ml-main__content-class-form-controls-submit object-selection-complete">
    <div class="btnSave">Выбрать <span id="selected-list-counter"><%= listModel.get('selectedList').length %></span> об.</div>
</div>

<div class="ml-main__content-delimiter"></div>

<div class="folder-list-container"></div>
<div class="object-list-container"></div>

<div class="ml-main__content-delimiter"></div>
