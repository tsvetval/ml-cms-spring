<style>
    .list-group-item {
        padding: 5px;
    }
</style>
<div class="row" style="width: 100%">
    <div class="col-xs-5">
    <ul class="nav nav-pills nav-stacked">
        <% _.each(roles, function(role) { %>
            <li class="role" role="presentation" roleId="<%= role.roleId %>"><a href="#"><%= role.name %> (<%= role.type %>)</a></li>
        <% }); %>
        <li role="presentation"><a class="addRole" href="#">Создать роль</a></li>

    </ul>
    </div>
    <div id="roleInfoContainer" class="col-xs-19 row">

    </div>
    <div id="modalDiv"/>
</div>


