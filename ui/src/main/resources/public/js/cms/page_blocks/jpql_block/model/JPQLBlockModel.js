/**
 * @name: jpqlBlockModel
 * @package:
 * @project: ml-cms-all
 * Created by i_tovstyonok on 29.12.2015.
 */

define(
    ['log', 'misc', 'cms/model/PageBlockModel',
        'cms/events/events'],
    function (log, misc, PageBlockModel, cmsEvents) {
        var model = PageBlockModel.extend({
            defaults: {
                controller: 'jpqlController',
                meta: null,
                pageTitle: "JPQL",
                storageKey: 'mlarm.block.jpql.history',
                historyLimit: 20
            },

            initialize: function () {
                /* PageBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this);

                log.debug('Initialize jpqlBlockModel');
            },

            /**
             * Добавление событий блока
             * @Override PageBlockModel.bindEvents
             */

            bindEvents: function () {
                /* PageBlockModel.bindEvents() добавляет основные события блока */
                this.on(cmsEvents.RESTORE_PAGE, this.fetch);
                this.on(cmsEvents.OPEN_FOLDER, this.fetch);
                model.__super__.bindEvents.call(this)
            },

            /**
             * Остановка обработчиков событий блока
             * @Override PageBlockModel.unbindEvents
             */
            unbindEvents: function () {
                this.off(cmsEvents.RESTORE_PAGE);
                this.off(cmsEvents.OPEN_FOLDER);
                /* PageBlockModel.unbindEvents() удаляет все основные события блока */
                model.__super__.unbindEvents.call(this)
            },

            fetch: function (){

                this.updatePageTitle(this.get('pageTitle'))

                var _this = this;
                var data = {
                    action: "getMeta"
                };

                /* Load metadata with available classes */
                this.callServerAction(data).then(function (result) {
                    if (result) {
                        _this.set('meta', result);
                        _this.trigger(cmsEvents.RENDER_VIEW)
                    } else {
                        _this.displayErrorMessage("Данные не получены", "Произошла ошибка при загрузке информации доступных классах.")
                    }
                })
            },

            execQuery: function (query, type) {
                if (query) {
                    this.saveQuery(query, type);
                    var options = {
                        action: 'exec' + type.toUpperCase(),
                        data: {}
                    };
                    options.data[type] = query;
                    return this.callServerAction(options)
                } else {
                    return false
                }

            },

            saveQuery: function (query, type) {
                var history = JSON.parse(localStorage.getItem(this.get('storageKey') + '.' + type));
                if (history){
                    if (query != _.last(history)){
                        history.push(query)
                    } else {
                        return false;
                    }
                } else {
                    history = [];
                    history.push(query)
                }

                if (history.length > this.get('historyLimit')) {
                    history = _.last(history, this.get('historyLimit'))
                }

                localStorage.setItem(this.get('storageKey') + '.' + type, JSON.stringify(history))
            },

            _clearHistory: function (type) {
                localStorage.removeItem(this.get('storageKey') + '.' + type)
            }

        });

        return model
    }
);