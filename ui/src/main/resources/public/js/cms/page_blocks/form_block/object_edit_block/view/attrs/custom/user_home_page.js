/**
 * Наследуем view и шаблон от AttrView
 */

define([
        'select2', 'misc', 'cms/page_blocks/form_block/object_edit_block/view/attrs/ManyToOneAttrView',
        'text!cms/page_blocks/form_block/object_edit_block/templates/attrs/dropdown_list.tpl'],
    function (select2, misc, AttrView, DropdownTemplate) {
        var view = AttrView.extend({
            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DropdownTemplate
            },

            render: function () {
                if (this.isHidden()) {
                    return;
                }
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));

                this.$attrLabelContainer = this.$el.find('.attr-label-container');

                this.$inputField = this.$el.find('.attrField');

                this.addMandatoryEvents();
                this.addReadOnly();
                this.addPopoverHelper();

                var _this = this
                var options = {
                    action: 'getURLs',
                    url: misc.getContextPath() + this.model.get('pageBlockModel').getSiteResource('metaController')
                };
                $.when(this.model.callServerAction(options)).then(function (result) {
                    var select2Data = [];
                    _.pairs(result.pages).forEach(function (page, i) {
                        select2Data.push({id: i, text: page[1]})
                    });

                    if (_this.model.get('value')) {
                        var currentValue = _this.model.get('value');
                        if (!_.contains(_.values(select2Data), currentValue)) {
                            select2Data.push({id: (new Date()).getMilliseconds(), text: '/' + currentValue})
                        }
                    }

                    _this.$inputField.select2({
                        width: '100%',
                        data: select2Data,
                        allowClear: true,
                        placeholder: "Выберите значение",
                        createTag: function (params) {
                            var term = $.trim(params.term);

                            if (term === '') {
                                return null;
                            }

                            return {
                                id: (new Date()).getMilliseconds(),
                                text: term
                            };
                        },
                        selectOnBlur: true,
                        multiple: false,
                        tags: true
                    });

                    if (_this.model.get('value')) {
                        var option = _.findWhere(select2Data, {text: '/' + _this.model.get('value')});
                        _this.$inputField.val(option.id).trigger('change');
                    } else {
                        _this.$inputField.val(null).trigger('change');
                    }

                    _this.$inputField.on("change", function () {
                        _this.changeSelection(_this.$inputField.find('option:selected').text());
                    });
                })
            },

            /**
             * Обработчик изменения значения HTML-элемента
             */
            changeSelection: function (value) {
                if (value.indexOf('/') == 0) value = value.replace('/', '');
                this.model.set('value', value, {silent: true})
            }
        })

        return view
    }
)