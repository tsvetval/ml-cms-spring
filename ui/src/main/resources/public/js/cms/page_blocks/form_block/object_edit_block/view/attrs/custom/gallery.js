/**
 * Представление для атрибута типа gallery
 */
define(['cms/page_blocks/form_block/object_edit_block/view/attrs/OneToManyAttrView'],
    function (OneToManyAttrView) {
        var view = OneToManyAttrView.extend({

            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
            }

        });

        return view;
    });
