<div class="attribute-edit-flat_text">
    <div class="attr-label-container col-xs-offset-<%=attrModel.get('offset')%> col-xs-<%=attrModel.get('titleLength')%>">
        <b class="attr-label"><%=attrModel.escape('description')%>:</b>
    </div>
    <div class="col-xs-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%>">
        <textarea class="attrField form-control" style="height: <%=attrModel.get('tableHeight')%>px"/>
    </div>
</div>

