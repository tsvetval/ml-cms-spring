<div class="attribute-view-manytoone">
    <div class="col-xs-offset-<%=attrModel.get('offset')%> col-xs-<%=attrModel.get('titleLength')%>">
        <b class="attr-label"><%=attrModel.escape('description')%>:</b>
    </div>
    <div class="col-xs-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%>">
        <%if (attrModel.get('value')){%>
        <a class="ml-details"
           objectId="<%=attrModel.get('value').objectId%>"
           mlClass="<%=attrModel.get('value').mlClass%>">
            <%= _.escape(attrModel.get('value').title) %>
        </a>
        <%}%>
    </div>

</div>