/**
 * Модель блока добавления объектов в репликацию
 *
 */
define(
    ['log', 'misc', 'backbone', 'cms/events/events', 'cms/model/PageBlockModel',
        'cms/page_blocks/DialogPageBlock'],
    function (log, misc, backbone, cmsEvents, PageBlockModel, DialogPageBlock) {
        var model = PageBlockModel.extend({
            defaults: {
                renderTemplate: undefined,
                className: undefined,
                objectId : undefined,
                title: undefined,
                openEventIdList: [],
                controller: 'addReplicationController'
            },

            /**
             * Инициализация модели
             */
            initialize: function () {
                /* PageBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this)

                console.log("initialize ObjectListBlockModel");
            },

            /**
             * Добавление событий блока
             * @Override PageBlockModel.bindEvents
             */
            bindEvents: function () {
                /* PageBlockModel.bindEvents() добавляет основные события блока */
                model.__super__.bindEvents.call(this)

                var _this = this;
                this.listenTo(this, cmsEvents.RESTORE_PAGE, function(params){
                    if(params.objectId && params.className)
                        _this.loadSelect.apply(this, arguments);
                });
            },

            /**
             * Остановка обработчиков событий блока
             * @Override PageBlockModel.unbindEvents
             */
            unbindEvents: function () {
                /*  События блока */
                this.stopListening(this, cmsEvents.RESTORE_PAGE);

                /* PageBlockModel.unbindEvents() удаляет все основные события блока */
                model.__super__.unbindEvents.call(this)
            },

            /**
             * Загрузка шагов репликации
             * @param options
             */
            loadSelect: function (options) {
                var _this = this;
                this.set('className', options.className);
                this.set('objectId', options.objectId);
                this.syncRenderTemplate().then(function () {
                    _this.triggerRender()
                });
            },

            /**
             * Обновление списка объектов текущего шага
             */
            refreshCurrentObjectList: function(){
                var _this = this;
                this.syncRenderTemplate().then(function () {
                    _this.triggerRender()
                });
            },

            /**
             * Установка текущей страницы списка объектов             *
             * @param pageNumber
             */
            setCurrentPage: function (pageNumber) {
                var _this = this;
                this.set('currentPage', pageNumber);
                this.syncRenderTemplate().then(function () {
                    _this.triggerRender()
                });
            },

            /**
             * Отрисовка шаблона блока
             */
            syncRenderTemplate: function () {
                var _this = this;
                return $.ajax(misc.getContextPath()+ this.getSiteResource(_this.get("controller")), {
                    type: 'POST',
                    data: {
                        action: 'show',
                        className: this.get('className'),
                        objectId: this.get('objectId'),
                        historyUtilId: this.get('historyUtilId'),
                        pageBlockId: this.get('blockInfo').get('id'),
                        currentPage: this.get('currentPage'),
                        ml_request: true
                    },
                    success: function (result) {
                        _this.set('title', result.title);
                        _this.set('renderTemplate', result.html);

                        // Обновляем текущую крошку
                        _this.navigationEvent({
                            "do":'modify',
                            "title": result.title // todo: нужно получать с сервера только название папки, а не весь заголовок вроде  "Список объектов Авторы"
                        });

                    },
                    error: function (xhr) {
                        var message = 'Код ошибки: ' + xhr.status;
                        if (xhr.responseJSON && xhr.responseJSON.message) {
                            message = xhr.responseJSON.message
                        }

                        var modalMessage = _this.getMessageObject("Ошибка запроса", message);
                        modalMessage.show({
                            okAction: function () {
                                _this.closePage()
                            }
                        });

                    }
                });
            },

            /**
             * Добавление объекта к выбранному шагу репликации
             *
             * @param data
             * @returns {*}
             */
            addToReplication: function (data) {
                var _this = this;

                var param = new Object();
                param.pageBlockId = this.get('blockInfo').get('id');
                param.ml_request = true;
                param.objectId = this.get("objectId");
                param.className = this.get("className");

                if (data) {
                    for (var i in data) {
                        param[i] = data[i];
                    }
                }
                var options = {
                    action: data.action,
                    data: param
                };
                var callback = function (result) {
                    _this.showResult(result);
                };

                return this.callServerAction(options, callback);
            },

            /**
             * Генерация события для отрисовки представления
             */
            triggerRender: function () {
                this.trigger(cmsEvents.RENDER_VIEW);
            },

            /**
             * Отображение сообщения о результате добавления объекта к шагу репликации
             *
             * @param result
             */
            showResult: function (result) {
                if (result.showType) {
                    if (result.showType == 'modal') {
                        this.showModal(result);
                    } else if (result.showType == 'execJs') {
                        this[result.functionName](result);
                    }
                }
            },

            /**
             * Отображение информационного сообщения
             *
             * @param result
             */
            showModal:function(result){
                var dialog = new DialogPageBlock({
                    title: result.title,
                    message: result.content,
                    type: 'infoMessage'
                });

                dialog.show();
            }
        });

        return model;
    });

