/**
 *  Базовый модуль модели блока спика объектов
 *
 */
define(
    ['log', 'misc', 'backbone', 'underscore',
        'cms/events/events',
        'cms/model/PageBlockModel',
        'cms/events/NotifyPageBlocksEventObject',
        'libs/requestloader/jquery.requestloader'],
    function (log, misc, backbone, _,
              cmsEvents,
              PageBlockModel,
              NotifyEventObject) {
        var model = PageBlockModel.extend({
            defaults: {
                // id папки для отображения
                folderId: undefined,
                // имя класса объектов для отображения
                className: undefined,

                // метаданные текущей папки
                currentFolder: undefined,
                // URL для отображения в iFrame
                url: undefined,

                // список вложенных папок
                folders: undefined,
                // список вложенных объектов
                objectList: undefined,

                // номер текущей страницы списка для отображения
                currentPage: undefined,
                // число объектов для отображения на странице
                objectsPerPage: undefined,
                // общее число объектов в списке
                recordsCount: undefined,
                // атрибут для сортировки
                orderAttr: undefined,
                // тип сортировки
                orderType: undefined,
                // id выбраных объектов
                selectedIds: new Array(),
                // Показывать ли кнопку Создать
                showCreate:undefined,
                // Показывать ли кнопку Удалить
                showDelete:undefined,
                // Список классов какие объекты создавать
                createdClasses:undefined,
                //Данные классификатора
                classifierId: undefined,
                classifierValue: undefined,

                pageTitle: 'Просмотр папки <%=folderTitle%>'
            },

            initialize: function () {
                /* PageBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this)

                log.debug("initialize ListBlockModel");
            },

            /**
             * Добавление событий блока
             * @Override PageBlockModel.bindEvents
             */
            bindEvents: function () {
                /* PageBlockModel.bindEvents() добавляет основные события блока */
                model.__super__.bindEvents.call(this)

                var _this = this;
                this.listenTo(this, cmsEvents.RESTORE_PAGE, function (params) {
                    _this.restorePageListener(params);
                });

                this.listenTo(this, cmsEvents.REFRESH_PAGE, function () {
                    _this.refreshPageListener();
                });

                this.listenTo(this, cmsEvents.OPEN_FOLDER, function (params) {
                    _this.openFolderListener(params);
                });

                this.listenTo(this, cmsEvents.SEARCH, function (params) {
                    _this.searchListener(params);
                });

                this.listenTo(this, cmsEvents.GET_SELECTED_IDS,function(param){
                    var data = {
                        className:_this.get("className"),
                        selectedIds: _this.get("selectedIds")
                    };
                    if (this.get('isAllSelected')) {
                        var filter = {};
                        filter = _.extend(filter, this.get('search'));
                        filter = _.extend(filter, {isAllSelected: this.get('isAllSelected')});

                        param.callback(data, filter);
                    } else {
                        param.callback(data);
                    }

                });

                this.on(cmsEvents.UNSET_SELECTED_IDS, function (params) {
                    /* Set selectedIds field as empty array */
                    _this.set('selectedIds', []);
                    if (params && params.update) {
                        _this._update()
                    }
                });

                this.on('list:deleted:complete', function (count) {
                    _this.get('pageModel').get('siteModel').trigger(cmsEvents.SITE_NOTIFY, {type: 'warning', text: 'Удалено объектов: ' + count})
                })
            },

            /**
             * Остановка обработчиков событий блока
             * @Override PageBlockModel.unbindEvents
             */
            unbindEvents: function () {
                /*  События блока */
                this.stopListening(this, cmsEvents.RESTORE_PAGE);
                this.stopListening(this, cmsEvents.REFRESH_PAGE);
                this.stopListening(this, cmsEvents.OPEN_FOLDER);
                this.stopListening(this, cmsEvents.SEARCH);
                this.stopListening(this, cmsEvents.GET_SELECTED_IDS);
                this.off(cmsEvents.UNSET_SELECTED_IDS);
                this.off('list:deleted:complete');

                /* PageBlockModel.unbindEvents() удаляет все основные события блока */
                model.__super__.unbindEvents.call(this)
            },

            searchListener: function(params){
                var _this = this;
                var type = misc.option(params, "type", "Тип фильтрации (простой или расширенный)");
                var criteria = misc.option(params, "criteria", "Критерий поиска");
                var searchField = misc.option(params, "searchField", "Поле для поиска", "");
                var data = {
                    useFilter: type
                };
                if (type == 'simple') {
                    data.simpleFilter = criteria;
                    data.searchField = searchField;
                } else {
                    data.extendedFilterSettings = criteria;
                }
                _this.set('search', data);
                _this.set('currentPage', 1);
                _this._update();
            },

            openFolderListener: function(params){
                var _this = this;
                log.debug("ObjectListBlockModel start openFolder");
                if (params.folderId) {
                    _this.set('folderId', params.folderId);
                    _this.set('classifierId', params.classifierId);
                    _this.set('classifierValue', params.value);
                    _this._reset();
                    _this._update(true);
                }
            },

            restorePageListener: function(params){
                var _this = this;
                log.debug("ObjectListBlockModel start restorePage");
                _this.set('folderId', params.folderId);
                _this.set('classifierData', params.data);
                _this.set('className', params.className);
                _this._reset();
                _this._update();
            },

            refreshPageListener: function(){
                var _this = this;
                log.debug("ObjectListBlockModel start refreshPage");
                _this._update();
            },

            /**
             *  Сброс параметров модели
             *
             * @private
             */
            _reset: function () {
                this.unset("recordsCount");
                this.unset("currentPage");
                this.unset("objectsPerPage");
                this.unset("orderAttr");
                this.unset("orderType");
                this.unset("search");
                this.set('selectedIds', []);
                this.set('isAllSelected', false);
            },

            /**
             * Обновление данных модели
             *
             * @private
             * @params lock boolean
             */
            _update: function (lock) {
                var _this = this;
                $.when(_this._loadListData(lock)).then(
                    function (result) {
                        if (result.CurrentFolder) {
                            var title = _.template(_this.get("pageTitle"), {data : result, folderTitle: result.CurrentFolder.title});
                            _this.updatePageTitle(title);
                            _this.set("currentFolder", result.CurrentFolder);
                        } else {
                            _this.unset("currentFolder");
                        }

                        if (result.CurrentFolder && result.CurrentFolder.url) {
                            _this.set("url", result.CurrentFolder.url);
                        } else {
                            _this.unset("url");
                        }

                        if (result.ObjectClassName) {
                            _this.set("className", result.ObjectClassName);
                        } else {
                            _this.unset("className");
                        }
                        if (result.ObjectListData) {
                            _this.set("objectList", result.ObjectListData);
                        } else {
                            _this.unset("objectList");
                        }
                        if (result.folders) {
                            _this.set("folders", result.folders);
                        } else {
                            _this.unset("folders");
                        }
                        if (result.RecordsCount) {
                            _this.set("recordsCount", result.RecordsCount);
                        } else {
                            _this.unset("recordsCount");
                        }
                        if (result.createdClasses && result.createdClasses.length != 0) {
                            _this.set("createdClasses", result.createdClasses);
                        } else {
                            _this.unset("createdClasses");
                        }
                        _this.set("showCreate",result.canCreate);
                        _this.set("showDelete",result.canDelete);
                        _this.set('ClassDescription', result.ClassDescription);
                        _this._updatePageTitle();
                        _this.set('blockRendered', false)
                        _this.trigger(cmsEvents.RENDER_VIEW);
                    }
                );
            },

            /**
             * Обновление заголовка страницы
             *
             * @private
             */
            _updatePageTitle: function () {
                var currentFolder = this.get("currentFolder");
                var className = this.get("className");
                var title = undefined;
                if (!currentFolder && !className) {
                    title = "Просмотр корневой папки";
                } else if (currentFolder) {
                    title = "Просмотр папки " + currentFolder.text;
                } else if (className) {
                    title = "Просмотр списка объектов " + className;
                }
                this.set("pageTitle", title);
                this.changePageParams({
                    pageTitle: title
                });
            },


            /**
             * Получение списка объектов в соответствии с параметрами модели
             *
             * @private
             */
            _loadListData: function (lock) {
                var options = {
                    action: 'getObjectData',
                    data: {
                        folderId: this.get('folderId'),
                        className: this.get('className'),
                        currentPage: this.get('currentPage'),
                        objectsPerPage: this.get('objectsPerPage'),
                        classifierId: this.get('classifierId'),
                        classifierValue: this.get('classifierValue'),
                        orderAttr: this.get('orderAttr'),
                        orderType: this.get('orderType'),
                        pageBlockId: this.get('blockInfo').get('id'),
                        ml_request: true
                    }
                };

                if (lock) {
                    options.lockElement = $('#CENTER')
                }

                if (this.get("search")) {
                    $.extend(options.data, this.get("search"));
                }
                return this.callServerAction(options);
            },

            /**
             * Открытие папки
             *
             * @param folderOptions  -  {folderId : id папки для открытия, pageBlocks : []}
             */
            openFolder: function (folderOptions) {
                var _this = this;
                this.unset('currentPage');
                this.set('folderId', folderOptions.folderId);
                this.set('className', undefined);
                this.notifyPageBlocks(new NotifyEventObject(cmsEvents.OPEN_SUB_FOLDER, folderOptions));
                /*this.notifyPageBlocks(new NotifyEventObject(cmsEvents.ACTIVATE_FOLDER, folderOptions));*/
                this.set('isAllSelected', false);
                //_this._update(true);
            },

            /**
             * Открытие объекта
             *
             * @param objectId  -   id объекта
             * @param className -   имя (entityName) класса объекта
             */
            openObject: function (objectId, className) {
                this.openPage(this.getSiteResource('viewObjectPage', 'pages'), 'Просмотр объекта ...', {
                    objectId: objectId,
                    className: className
                });
            },

            /**
             * Добавление списка идентификаторов к списку связанных объектов
             * @param ids
             */
            addSelectedIds : function(ids){
                this.set('selectedIds', _.union(this.get('selectedIds'), ids));
            },

            /**
             * Удаление списка идентификаторов из списка связанных объектов
             * @param ids
             */
            removeSelectedIds : function(ids){
                this.set('selectedIds', _.difference(this.get('selectedIds'), ids));
            },
            /**
             * Создание объекта
             *
             * @param className -   имя (entityName) класса объекта
             */
            createObject: function (className) {
                this.openPage(this.getSiteResource('createObjectPage', 'pages'), 'Создание объекта ...', {
                    className: className
                });
            },
            /**
             * Удаление выбранных объектов
             */
            deleteObjects:function(){
                var _this = this;
                var objectIds = this.get("selectedIds");
                var className = this.get('className');
                var options = {
                    action: 'deleteObjects',
                    data: {
                        objectIds: JSON.stringify(objectIds),
                        className: className,
                        pageBlockId: this.get('blockInfo').get('id'),
                        ml_request: true
                    }
                };

                if (this.get('isAllSelected')) {
                    options.data = _.extend(options.data, {
                        isAllSelected: true,
                        folderId: this.get('folderId')
                    })
                    if (this.get("search")) {
                        options.data = _.extend(options.data, this.get("search"));
                    }
                }

                log.debug('Call server to delete Object with id = ' + objectIds + '@' + className);
                return this.callServerAction(options).then(function(result){
                    log.debug('Receive response from server after delete Object with id = ' + objectIds + '@' + className);
                    log.debug('Trigger event to navigate previous BreadCrumb');
                    _this.set("selectedIds", []);
                    var opener = _this.get("pageModel").get("page_opener");
                    // Обновляем страницу открывщую данную

                    /* Unset isAllSelected flag */
                    _this.set('isAllSelected', false);

                    _this.notifyPageBlocks(new NotifyEventObject(
                        cmsEvents.REFRESH_PAGE,
                        {},
                        opener
                    ));
                    if(opener) _this.closePage();
                    _this.trigger('list:deleted:complete', result.count)
                });
            },
            /**
             * Смена текущей страницы/количества объектов на странице
             *
             * @param page              -   номер текущеей страницы
             * @param objectsPerPage    -   число объектов для отображения на странице
             */
            changePage: function (page, objectsPerPage) {
                this.set("currentPage", page);
                this.set("objectsPerPage", objectsPerPage);
                this._update(true);
            },

            /**
             * Смена сортировки списка объектов
             *
             * @param orderAttr     -   атрибут для сортировки
             * @param orderType     -   тип сортировки (asc/desc)
             */
            changeSort: function (orderAttr, orderType) {
                this.set("orderAttr", orderAttr);
                this.set("orderType", orderType);
                this._update(true);
            }

        });

        return model;
    });
