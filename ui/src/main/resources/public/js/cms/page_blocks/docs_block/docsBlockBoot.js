/*
* Создает шаблон лоя pageBlock(JS для Backbone и Java контроллер)
* Контроллер: ru.ml.web.block.controller.impl.PageWizardBlockController
* */


define(
    ['log', 'misc', 'backbone', 'cms/page_blocks/docs_block/model/DocsBlockModel', 'cms/page_blocks/docs_block/view/DocsBlockView'],
    function (log, misc, backbone, model, view) {
        return {
            model: model,
            view: view
        };
    });
