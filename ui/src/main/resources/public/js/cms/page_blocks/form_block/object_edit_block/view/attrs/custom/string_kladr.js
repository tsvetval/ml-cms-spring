/**
 * Представление с поиском по КЛАДР для атрибута типа STRING
 */
define(
    ['log', 'misc', 'backbone', 'inputmask', 'select2',
        'cms/page_blocks/form_block/object_edit_block/view/EditAttrView',
        'text!cms/page_blocks/form_block/object_edit_block/templates/attrs/string_kladr.tpl',
        'cms/events/events'
    ],
    function (log, misc, backbone, inputmask, select2, EditAttrView, DefaultTemplate, Events) {
        var view = EditAttrView.extend({
            $inputField : undefined,

            events: {
                "keyup .attrField": "keyPressed",
                "keydown .attrField": "keyDown"
            },

            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                _.extend(this.events, EditAttrView.prototype.events);

                this.structure = {
                    fields: [
                        {
                            name: 'zip',
                            description: 'Индекс',
                            action: false,
                            min: 3,
                            query: '',
                            key: null,
                            value: null
                        },
                        {
                            name: 'place',
                            description: 'Город или населенный пункт',
                            action: 'getPlace',
                            min: 3,
                            query: '',
                            key: null,
                            value: null
                        },
                        {
                            name: 'street',
                            description: 'Улица/проспект/бульвар и т.д.',
                            action: 'getStreet',
                            min: 3,
                            dep: 'place',
                            query: '',
                            key: null,
                            value: null
                        },
                        {
                            name: 'house',
                            description: 'Номер дома',
                            action: 'getHouse',
                            min: 1,
                            dep: 'street',
                            query: '',
                            key: null,
                            value: null
                        }
                    ],
                    currentState: 0,
                    delimeter: ', '
                }
            },

            /**
             * Отрисовка представления
             */
            render: function () {
                var _this = this;
                if (this.isHidden()) {
                    return;
                }
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));
                this.$inputField = this.$el.find('.attrField');
                this.$inputField.val(this.model.get('value'));
                if (!this.model.get('value') || this.model.get('value') == "") this.$inputField.data('kladr', true);

                /* Update model on change */
                this.$inputField.on('change', function () {
                    _this.model.set('value', $(this).val());
                });
                this.$resultList = $('#kladr-search-results');
                if (this.$resultList.length == 0) {
                    this.$resultList = $('<ul>', {
                        class: 'dropdown-menu',
                        id: 'kladr-search-results',
                        style: 'max-height: 200px; overflow: auto;'
                    }).appendTo($('body'));
                }

                /* Append search result by click */
                this.$resultList.on('click', 'a', function (e) {
                    e.preventDefault();
                    _this.appendSearchResult($(this))
                });

                this.$inputField.on('blur', function (e) {
                    if (e.relatedTarget && !$(e.relatedTarget).is('a.search-result-item')) { // not work in Firefox, relatedTarget is unsupported for blur or focus events
                        _this.$resultList.hide()
                    } else if (e.originalEvent && !$(e.originalEvent.explicitOriginalTarget).is('a.search-result-item')) { // Firefox workaround
                        _this.$resultList.hide()
                    }
                });

                this.$inputField.on('focus', function () {
                    if (!_this.$resultList.is(':empty')) {
                        _this.showResults()
                    }

                });

                this.$('.help-block span').text(_.map(_.filter(this.structure.fields, function (field) {return field.action}), function (field) {
                    return field.description
                }).join(' -> '));

                this.addMandatoryEvents();
                this.addReadOnly();
                _this.listenTo(this.model.get('pageBlockModel'), Events.MODEL_SAVE_COMPLETE, function () {
                    _this.$resultList.remove();
                })
            },

            /**
             * Обработчик изменения значения HTML-элемента
             */
            keyPressed: function (e) {
                this.model.set('value', this.$inputField.val());
                if (this.$inputField.val().length == 0) {
                    /*Enable kladr search*/
                    this.$inputField.data('kladr', true)
                } else {
                    //this.model.set('value', this.$inputField.val());
                    if (!_.contains([13, 38, 40], e.keyCode) && this.$inputField.data('kladr')) {
                        this.structureCheck(this.$inputField.val(), e);
                    }
                }
            },

            keyDown: function (e) {
                if (_.contains([13, 38, 40], e.keyCode)) {
                    e.preventDefault();
                    var $active = this.$resultList.find('li.active');
                    if (e.keyCode == 13) {
                        if ($active.length > 0) {
                            this.appendSearchResult($active.find('a'))
                        }
                    } else {
                        if ($active.length > 0) {
                            if (e.keyCode == 40) {
                                var $next = $active.next('li')
                            } else if (e.keyCode == 38) {
                                var $next = $active.prev('li')
                            }

                            if ($next.length > 0) {
                                $active.removeClass('active');
                                $next.focus().addClass('active');
                                this.$resultList.scrollTop(this.$resultList.scrollTop()  + $next.position().top - this.$resultList.height()/2)
                            }
                        } else {
                            this.$resultList.find('li:first').focus().addClass('active')
                        }
                    }
                }
            },

            extractString: function (string, field) {
                /*var dep = _.findWhere(this.structure.fields, {name: field.dep});
                var zip = _.findWhere(this.structure.fields, {name: 'zip'});
                if (zip.value) {
                    string = string.replace(zip.value, '')
                }
                if (dep) {
                    string = this.extractString(string, dep);
                }
                return string.replace(field.value, '').replace(/,/g, '').trim()*/

                var another = _.reject(this.structure.fields, function (f) {
                    return f.name == field.name
                });

                _.each(another, function (f) {
                    string = string.replace(f.value, '')
                });

                return string.replace(field.value, '').replace(/,/g, '').trim()
            },

            getMinLength: function (field) {
                var _this = this;
                var min = field.min || 0;
                var previousFields = _.first(this.structure.fields, _.indexOf(this.structure.fields, field));
                _.each(previousFields, function (field) {
                    if (field.value) min = min + (field.value + _this.structure.delimeter).length
                });
                return min
            },

            structureCheck: function (string, e) {
                var checkString = string;
                _.each(this.structure.fields, function (field) {
                    if (!~checkString.indexOf(field.value)) {
                        field.value = null;
                        field.key = null;
                    } else {
                        checkString = checkString.replace(field.value, '')
                    }
                });

                /*Reset on backspace */
                if (e.keyCode == 8 && !this.isFilled()) {
                    this.updateInputField()
                } else {
                    this.search(string)
                }

            },

            isFilled: function () {
                return _.every(this.structure.fields, function (field) {
                    return field.value
                })
            },

            search: function (string) {
                var _this = this;
                _this.$resultList.hide();

                var field = _.findWhere(_.filter(this.structure.fields, function (i) {return i.action}), {value: null});
                if (field) {
                    field.query = this.extractString(string, field)
                }
                if (field && string.length >= this.getMinLength(field)) {

                    var model = this.model.get('pageBlockModel');
                    var state = this.model.get('resultState');

                    var options = {
                        action: field.action,
                        data: {
                            find: this.extractString(string, field)
                        },
                        url: misc.getContextPath() + model.getSiteResource('loadKladrController')
                    };

                    if (field.dep) {
                        var dep = _.findWhere(this.structure.fields, {name: field.dep});
                        options.data.code = dep.key;
                    }

                    /* Show animated icon */
                    this.$('.search-run').show();
                    model.callServerAction(options).then(function (result) {
                        _this.$resultList.empty();
                        if (result && !_.isEmpty(result)) {
                            _.each(result, function (item) {
                                var $a = $('<a>', {
                                    class: 'search-result-item',
                                    'data-key': item.code,
                                    'data-for': field.name,
                                    'data-text': item.name,
                                    'data-zip': item.zip,
                                    href: '#',
                                    style: 'cursor: pointer;'
                                }).text( (item.zip ? item.zip + ', ' : '') + item.name);

                                _this.$resultList.append($('<li>').append($a));

                                _this.showResults()
                            })
                        } else {
                            _this.$resultList.append($('<li>').html('<a>Нет результатов</a>'));
                            _this.showResults()
                        }
                    }).always(function () {
                        /*Hide animated icon */
                        _this.$('.search-run').hide();
                    })
                } else {

                }
            },

            appendSearchResult: function ($item) {
                var key = $item.data('key'),
                    zip = $item.data('zip'),
                    value = $item.data('text'),
                    fieldName = $item.data('for');

                if (fieldName) {
                    var zipField = _.findWhere(this.structure.fields, {name: 'zip'});
                    zipField.value = zip;

                    var field = _.findWhere(this.structure.fields, {name: fieldName});
                    field.key = key;
                    field.value = value;
                    this.updateInputField();
                }



                this.$resultList.hide()
            },

            updateInputField: function () {
                /* Update search string from fields values and field queries */
                var string = _.map(_.filter(this.structure.fields, function (field) {
                    return field.value || field.query
                }), function (field) {
                    if (field.value) {
                        return field.value
                    } else {
                        field.query = field.query.length > 1 ? _.initial(field.query).join('') : '';
                        return field.query
                    }
                });
                this.$inputField.val(string.join(this.structure.delimeter)).change();


                this.$inputField.focus()
            },

            showResults: function () {

                var offset = this.$inputField.offset();

                this.$resultList.css({
                    display: 'block',
                    top: offset.top + this.$inputField.height(),
                    left: offset.left,
                    width: this.$inputField.outerWidth()
                })

            }


        });

        return view;
    });
