define([
    'misc',
    'cms/page_blocks/form_block/object_view_block/view/attrs/OneToManyAttrView',
    'text!cms/page_blocks/form_block/object_view_block/templates/attrs/custom/gallery.tpl'
], function (misc, AttrView, GalleryTemplate) {
    var view = AttrView.extend({
        events: {
            "click .object-opener": "linkObjectPreviewClick"
        },

        initialize: function () {
            view.__super__.initialize.call(this);
            this.viewTemplate = GalleryTemplate;
            _.extend(this.events, AttrView.prototype.events);
            this.model.on()
        },

        render: function () {
            if (this.isHidden()) {
                return;
            }

            var _this = this

            if (this.model.get('value')) {
                this.model.callServerAction({
                    action: 'getGallery',
                    data: {
                        objectId: this.model.get('pageBlockModel').get('objectId'),
                        className: this.model.get('className'),
                        attrName: this.model.get('entityFieldName'),
                        refAttrName: 'image'
                    }
                }, function (result) {
                    _this.model.set('gallery', _.each(result.links, function (item) {
                        if (!item.url) item.url = misc.getContextPath() + '/public/images/no-image.jpg'
                    }))

                    _this.$el.html( _.template(_this.viewTemplate, {attrModel : _this.model}));

                    $('#gallery-modal-' + _this.model.get('id')).off('show.bs.modal').on('show.bs.modal', function (event) {
                        var $target = $(event.relatedTarget)
                        var $modal = $(this)

                        if($target.parent().is(':last-child')) {
                            $('.gallery-next').hide()
                        } else {
                            $('.gallery-next').show()
                        }

                        if($target.parent().is(':first-child')) {
                            $('.gallery-prev').hide()
                        } else {
                            $('.gallery-prev').show()
                        }

                        $modal.find('.modal-title').text($target.data('title'))
                        $modal.find('.modal-header .object-opener').attr({
                            'objectid': $target.attr('objectid'),
                            'mlclass': $target.attr('mlclass')
                        })
                        $modal.find('.modal-body img').attr('src', $target.data('image'))

                        $modal.find('.gallery-next').off('click').on('click', function (event) {
                            var currentId = $modal.find('.modal-header .object-opener').attr('objectid')
                            var $next = $('.gallery').find('a[objectid="' + currentId + '"]').parent().next().find('a')
                            var $prev = $('.gallery').find('a[objectid="' + currentId + '"]').parent().prev().find('a')

                            if($next) {
                                $modal.find('.modal-title').text($next.data('title'))
                                $modal.find('.modal-header .object-opener').attr({
                                    'objectid': $next.attr('objectid'),
                                    'mlclass': $next.attr('mlclass')
                                })
                                $modal.find('.modal-body img').attr('src', $next.data('image'))
                            }

                            if($next.parent().is(':last-child')) {
                                $('.gallery-next').hide()
                            }

                            if(!$prev.parent().is(':first-child')) {
                                $('.gallery-prev').show()
                            }

                        })

                        $modal.find('.gallery-prev').off('click').on('click', function (event) {
                            var currentId = $modal.find('.modal-header .object-opener').attr('objectid')
                            var $next = $('.gallery').find('a[objectid="' + currentId + '"]').parent().next().find('a')
                            var $prev = $('.gallery').find('a[objectid="' + currentId + '"]').parent().prev().find('a')

                            console.info($prev)
                            if($prev) {
                                $modal.find('.modal-title').text($prev.data('title'))
                                $modal.find('.modal-header .object-opener').attr({
                                    'objectid': $prev.attr('objectid'),
                                    'mlclass': $prev.attr('mlclass')
                                })
                                $modal.find('.modal-body img').attr('src', $prev.data('image'))

                                if(!$next.parent().is(':last-child')) {
                                    $('.gallery-next').show()
                                }

                                if($prev.parent().is(':first-child')) {
                                    $('.gallery-prev').hide()
                                }
                            }

                        })
                    })
                })
            }
        }
    })

    return view
})