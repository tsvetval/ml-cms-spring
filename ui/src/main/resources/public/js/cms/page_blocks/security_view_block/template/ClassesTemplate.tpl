<div id="clsAccessModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Измениить доступ на классы</h4>
                <button type="button" class="btn btn-small btn-sm btn-primary save-class-access">Сохранить</button>
                <button type="button" class="btn btn-small btn-sm btn-danger" data-dismiss="modal">Отмена</button>
            </div>
            <ul class="list-group">
                <div style="height: 500px; overflow: auto; margin: 20px;">
                <% _.each(classes, function(cls) { %>
                <li class="list-group-item">
                    <input class="class-id" name="<%=cls.id%>" type="checkbox" <%if(cls.hasAccess){%>checked="checked"<%}%>>
                            <span class="badge">
                                C
                                <input name="create" class="class-operation" type="checkbox" <%if(cls.create){%>checked="checked"<%}%>>
                                R
                                <input name="read" class="class-operation" type="checkbox" <%if(cls.read){%>checked="checked"<%}%>>
                                U
                                <input name="update" class="class-operation" type="checkbox" <%if(cls.update){%>checked="checked"<%}%>>
                                D
                                <input name="delete" class="class-operation" type="checkbox" <%if(cls.delete){%>checked="checked"<%}%>>
                            </span>
                    <%= cls.className %>(<%= cls.entityName %>)
                </li>
                <% }); %>
                </div>
            </ul>
        </div>
    </div>
</div>
