<style>
    #class-list {
        max-height: 500px;
        overflow: auto;
    }
    
    .attr-list {
        font-style: italic;
    }
</style>
<div class="jpql-page-block">
    <div class="row">
        <div class="col-xs-4">
            <div class="row">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4>Доступные классы </h4>
                    </div>
                    <div class="list-group class-list" id="class-list">
                        <% _.each(model.get('meta').mlClasses, function(item, i) { %>
                        <li class="list-group-item" data-class="<%= item.entityName %>" data-table="<%= item.entityName %>" data-system="<%= item.system %>">
                            <%= item.entityName %>
                        </li>
                        <% }) %>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="pull-right">Кроме системных <input type="checkbox" id="class-filter-notsystem"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-19 col-xs-offset-1">
            <div class="row">

            </div>
            <div class="row">
                <div class="col-xs-24">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Запрос</h4>
                        </div>
                        <div class="panel-body">
                            <div class="tab-group-container">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#jpql-pane" data-toggle="tab">JPQL</a>
                                    </li>
                                    <li>
                                        <a href="#sql-pane" data-toggle="tab">SQL</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="query-builder">
                                    <div class="tab-pane active" id="jpql-pane">
                                        <div class="form">
                                            <div class="form-group">
                                                <div class="col-xs-4">
                                                    <label>История запросов:</label>
                                                </div>
                                                <div class="col-xs-20">
                                                    <form class="form-horizontal">
                                                        <div class="input-group">
                                                            <select id="jpql-query-history" class="form-control"></select>
                                                            <div class="input-group-addon btn btn-default" id="clear-jpql-history">Очистить</div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>JPQL запрос:</label>
                                                <textarea id="jpql-query-field" class="form-control"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <span id="jpql-attributes" class="attr-list"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="sql-pane">
                                        <div class="form">
                                            <div class="form-group">
                                                <div class="col-xs-4">
                                                    <label>История запросов:</label>
                                                </div>
                                                <div class="col-xs-20">
                                                    <form class="form-horizontal">
                                                        <div class="input-group">
                                                            <select id="sql-query-history" class="form-control"></select>
                                                            <div class="input-group-addon btn btn-default" id="clear-sql-history">Очистить</div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>SQL запрос:</label>
                                                <textarea id="sql-query-field" class="form-control"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <span id="sql-attributes" class="attr-list"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-xs-24">
                                    <button id="query-button" class="btn btn-primary pull-right">Выполнить запрос</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-24">
                    <div class="scrollbar-external_wrapper">
                        <div class="scrollbar-external">
                            <table id="result-table" class="table ml-main__content-table">

                            </table>
                            <div class="external-scroll_x">
                                <div class="scroll-element_outer">
                                    <div class="scroll-element_size"></div>
                                    <div class="scroll-element_track"></div>
                                    <div class="scroll-bar"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>