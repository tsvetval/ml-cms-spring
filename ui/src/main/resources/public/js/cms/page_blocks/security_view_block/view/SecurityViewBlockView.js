define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'markup', 'cms/page_blocks/DialogPageBlock', 'moment',
        'cms/page_blocks/security_view_block/view/tree.sec.customizer',
        'cms/page_blocks/security_view_block/view/tree.checkbox.customizer',
        'text!cms/page_blocks/security_view_block/template/SecurityViewTemplate.tpl',
        'text!cms/page_blocks/security_view_block/template/ClassesTemplate.tpl',
        'text!cms/page_blocks/security_view_block/template/UtilsTemplate.tpl',
        'text!cms/page_blocks/security_view_block/template/ResourcesTemplate.tpl',
        'text!cms/page_blocks/security_view_block/template/FoldersTemplate.tpl',
        'text!cms/page_blocks/security_view_block/template/RoleTemplate.tpl'],
    function (log, misc, backbone, PageBlockView, markup, Message, moment, treeSecCust, treeCheckboxCust,
              SecurityViewTemplate,ClassesTemplate,UtilsTemplate, ResourcesTemplate,FoldersTemplate, RoleTemplate) {

        var view = PageBlockView.extend({

            events: {
                "click .role": "showRole",
                "click .addclass": "showClassesForAdd",
                "click .save-class-access": "saveClassAccess",
                "click .save-util-access": "saveUtilAccess",
                "click .save-page-access": "saveResourcesAccess",
                "click .save-folder-access": "saveFolderAccess",
                "click .addUtil": "showUtilForEdit",
                "click .addPage": "editResourcesAccess",
                "click .addFolder": "showFolderForEdit",
                "click .addRole": "addRole",
                "keydown #newRole":"createRole"
            },

            initialize: function () {
                this.listenTo(this.model, 'render', this.render);
                this.listenTo(this.model, 'change:data', this.update);
                this.listenTo(this.model, 'renderRoleInfo', this.renderRoleInfo);
                this.listenTo(this.model, 'renderClassesPanel', this.renderClassesPanel);
                this.listenTo(this.model, 'renderUtilsPanel', this.renderUtilsPanel);
                this.listenTo(this.model, 'renderResourcePanel', this.renderResourcePanel);
                this.listenTo(this.model, 'renderFoldersPanel', this.renderFoldersPanel);
            },

            render: function () {
                var _this = this;
                this.$el.html(_.template(SecurityViewTemplate, {roles: this.model.get("roles")}));
                this.model.set('blockRendered', true)
                return true;
            },
            showRole: function(e){
                e.preventDefault();
                var _this = this;
                var roleId = e.currentTarget.attributes.getNamedItem("roleId").value;
                _this.model.getRoleInfo(roleId);
            },
            renderRoleInfo: function(){
                var cont = $('#roleInfoContainer');
                var role = this.model.get("role");
                cont.html(_.template(RoleTemplate,{role: role}));
                var treeCont = $('#folders');
                treeCont.jstree({
                    'animation': 1,
                    'plugins': ["json_data","tree_sec_customizer"],
                    'rules': {'multiple': false},
                    'lang': {new_node: "Новый каталог", loading: "Загрузка ..."},
                    'core': {
                        'animation': 200,
                        'check_callback': true,
                        'themes': {
                            'dots': false,
                            'icons': false
                        },
                        'data':role.folderAccess
                    }

                });
                treeCont.on('loaded.jstree', function() {
                    treeCont.jstree('open_all');
                });
            },
            showClassesForAdd: function(){
                this.model.getClassesForShow();
            },
            renderClassesPanel: function(){
                var modalDiv = this.$el.find("#modalDiv");
                modalDiv.html(_.template(ClassesTemplate,{classes: this.model.get("classes")}));
                var clsAccessModal = this.$el.find("#clsAccessModal");
                $(clsAccessModal).modal("show");
            },
            renderUtilsPanel: function(){
                var modalDiv = this.$el.find("#modalDiv");
                var utils = this.model.get("utils");
                modalDiv.html(_.template(UtilsTemplate,{utils: utils}));

                var utilAccessModal = this.$el.find("#utilAccessModal");
                if (!_.isEmpty(utils)) {
                    this.$el.find('#util'+utils[0].id).addClass("active");
                    this.$el.find('#utilTab'+utils[0].id).addClass("active");
                    $(utilAccessModal).modal("show");
                } else {
                    this.model.displayErrorMessage("Доступ к утилитам", "Для роли не создано ни одного доступа.")
                }
            },
            saveClassAccess: function(){
                var _this = this;
                var classes = _this.$el.find('.class-id:checked');
                var data = [];
                for(var i = 0; i<classes.length; i++){
                    var cls = classes[i];
                    var clsAccess = {
                        id: cls.name
                    };
                    var accesses = $(cls.parentElement).find(".class-operation:checked");
                    for(var j = 0; j<accesses.length; j++){
                        clsAccess[accesses[j].name] = true;
                    }
                    data[data.length] = clsAccess;
                }
                var clsAccessModal = this.$el.find("#clsAccessModal");
                $(clsAccessModal).modal("hide");
                _this.model.saveClsAccess(data);
            },
            showUtilForEdit: function(){
                this.model.getUtilsForShow();
            },
            saveUtilAccess:function(){
                var _this = this;
                var utils = this.model.get("utils");
                var data = [];
                for(var i in utils){
                    var utilAccess = utils[i];
                    var newUtilAccess = {id:utilAccess.id, utilList:[]};
                    var utilites = _this.$el.find('#util'+utilAccess.id).find(".util-id:checked");
                    for(var j = 0; j<utilites.length; j++){
                        newUtilAccess.utilList[newUtilAccess.utilList.length] = utilites[j].name;
                    }
                    data[data.length] = newUtilAccess;
                }
                var utilAccessModal = this.$el.find("#utilAccessModal");
                $(utilAccessModal).modal("hide");
                _this.model.saveUtilAccess(data)
            },

            /**
             * Get resources and open modal window for edit
             */
            editResourcesAccess: function(){
                this.model.getResources();
            },

            /**
             * Create resources edit form in modal window
             */
            renderResourcePanel: function(){
                var _this = this;
                var modalDiv = this.$el.find("#modalDiv");
                var resources = this.model.get("resources");
                modalDiv.html(_.template(ResourcesTemplate,{resources: resources}));

                var resourcesAccessModal = this.$el.find("#resourcesAccessModal");

                if (!_.isEmpty(resources)) {
                    this.$el.find('#resource-access-' + resources[0].id).addClass("active");
                    this.$el.find('#resource-access-tab-' + resources[0].id).addClass("active");

                    _.each(resources, function (access) {
                        _.each(access.resources.nodes, function (node) {
                            _this.$('#resource-access-' + access.id).find('> ul').append(_this._renderNode(node))
                        });

                        var $treeContainer = _this.$('#resource-access-' + access.id);

                        $treeContainer.jstree({
                            'rules': {'multiple': false},
                            'plugins': ["tree_checkbox_customizer"],
                            'lang': {loading: "Загрузка ..."},
                            'core': {
                                'animation': 100,
                                'check_callback': true
                            }
                        }).on('ready.jstree', function() {
                            $treeContainer.jstree('open_all');
                        });
                    });
                    $(resourcesAccessModal).modal("show")
                } else {
                    this.model.displayErrorMessage("Доступ к ресурсам", "Для роли не создано ни одного доступа.")
                }
            },

            saveResourcesAccess:function(){
                var _this = this;
                var resources = this.model.get("resources");
                var data = [];
                for(var i in resources){
                    var resourceAccess = resources[i];
                    var newResourceAccess = {id: resourceAccess.id, resources:[]};
                    var resourcesList = _this.$el.find('#resource-access-' + resourceAccess.id).find(".has-id > .folder-access-state:checked");
                    resourcesList.each(function () {
                        newResourceAccess.resources.push($(this).val());
                    });
                    data.push(newResourceAccess);
                }
                var pageAccessModal = this.$el.find("#resourcesAccessModal");
                $(pageAccessModal).modal("hide");
                _this.model.saveResourcesAccess(data)
            },


            /*Render node from resources list with nested*/
            _renderNode: function (node) {
                var _this = this;
                var badge, meta;

                if(node.controllerMeta) {
                    badge = {
                        text: 'C',
                        class: 'label label-success',
                        title: 'Контроллер'
                    };
                    meta = node.controllerMeta

                } else if (node.pageMeta) {
                    badge = {
                        text: 'P',
                        class: 'label label-warning',
                        title: 'Страница'
                    };
                    meta = node.pageMeta
                } else {
                    badge = {
                        text: 'G',
                        class: 'label label-info',
                        title: 'Группа ресурсов'
                    };
                    meta = {};
                    meta.url = node.url;
                }

                var $data = $('<div>', {
                    class: 'clearfix'
                }).append($('<span>', {
                    class: badge.class,
                    title: badge.title
                }).text(badge.text)).append($('<span>').text(meta.url || meta.id));

                var $li =  $('<li>', {
                    'data-jstree': JSON.stringify({
                        leafValue: node.leafValue,
                        checked: !!node.check
                    })
                }).append($data);

                /* Mark <li> as element with real resource with id */
                if (node.id) {
                    $li.prop('id', node.id).addClass('has-id');
                }

                if(node.nodes && node.nodes.length > 0) {
                    $li.append($('<ul>'));
                    node.nodes.forEach(function (subnode){
                        $li.find('> ul').append(_this._renderNode(subnode))
                    })
                }
                return $li
            },

            saveFolderAccess: function () {
                var folders = this.model.get("folders");
                var access = [];

                _.each(folders, function (folderAccess) {
                    var $selected = this.$('#foldersTree' + folderAccess.id).find('.folder-access-state:checked');
                    var folderAccessObject = {id: folderAccess.id, folderList: []};
                    $selected.each(function () {
                        folderAccessObject.folderList.push($(this).val())
                    });
                    access.push(folderAccessObject);
                });



                this.$("#folderAccessModal").modal('hide');
                this.model.saveFolderAccess(access)
            },

            showFolderForEdit: function(){
                this.model.getFoldersForShow();
            },
            renderFoldersPanel: function(){
                var modalDiv = this.$el.find("#modalDiv");
                var folders = this.model.get("folders");
                modalDiv.html(_.template(FoldersTemplate,{folders: folders}));

                folders.forEach(function(folderAccess){
                    var treeCont = $('#foldersTree'+folderAccess.id);
                    treeCont.jstree({
                        'animation': 1,
                        'plugins': ["json_data", "tree_checkbox_customizer"],
                        'rules': {'multiple': false},
                        'lang': {new_node: "Новый каталог", loading: "Загрузка ..."},
                        'core': {
                            'animation': 200,
                            'check_callback': true,
                            'data':folderAccess.folders
                        }

                    });
                    treeCont.on('loaded.jstree', function() {
                        treeCont.jstree('open_all');
                        _.each(folderAccess.folders, function (folder) {
                            //$('#access-folder-'+ folder.id).prop('checked', folder.state.checked)
                        })

                    });
                });

                var folderAccessModal = this.$el.find("#folderAccessModal");
                if (!_.isEmpty(folders)) {
                    this.$el.find('#folder'+folders[0].id).addClass("active");
                    this.$el.find('#folderTab'+folders[0].id).addClass("active");
                    $(folderAccessModal).modal("show");
                } else {
                    this.model.displayErrorMessage("Доступ к папкам", "Для роли не создано ни одного доступа.")
                }
            },
            addRole: function(e){
                e.preventDefault();
                $(e.target.parentElement).html("<input id='newRole' type='text'>");
            },
            createRole:function(e){
                if(e.keyCode == 13){
                    var roleName = e.target.value;
                    this.model.createRole(roleName);
                }
            }
        });
        return view;
    });
