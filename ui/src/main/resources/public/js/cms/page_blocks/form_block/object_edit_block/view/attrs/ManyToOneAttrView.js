/**
 * Представление для атрибута типа MANY_TO_ONE
 */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/form_block/object_edit_block/view/EditAttrView',
        'text!cms/page_blocks/form_block/object_edit_block/templates/attrs/ManyToOneAttrView.tpl'],
    function (log, misc, backbone,
              EditAttrView, DefaultTemplate) {
        var view = EditAttrView.extend({
            /*
             $valueHolderElement: undefined,
             */
            $titleHolderElement: undefined,

            events: {
                "click .editManyToOne": "editManyToOne",
                "click .deleteLinkedObject": "deleteLinkedObject",
                "click .createClick": "createClick",
                "click .selectLinkedObject": "selectLinkedObject"
            },

            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;

                this.listenTo(this.model, 'attribute:render', this.renderAttrValue);
                _.extend(this.events, EditAttrView.prototype.events);

                this.model.set('view', this)
            },

            /**
             * Отрисовка представления
             */
            render: function () {
                if (this.isHidden()) {
                    return;
                }
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));
                this.$toolbar = this.$el.find('.custom-toolbar');

                this.addMandatoryEvents();
                this.addReadOnly();
                this.addPopoverHelper();

                //this.$valueHolderElement = this.$el.find('.attrField');
                this.$titleHolderElement = this.$el.find('.attrFieldTitle');

                if (this.model.get('value') && this.model.get('value').title) {
                    this.$titleHolderElement.text(_.escape(this.model.get('value').title));
                } else {
                    this.$titleHolderElement.text('');
                }

                this.updateToolbar()
            },

            updateToolbar: function () {
                if (!this.model.get('value')) {
                    this.$toolbar.find(".deleteLinkedObject")
                        .removeClass("active-button")
                        .addClass("inactive-button");
                    this.$toolbar.find(".moveUp, .moveDown, .editManyToOne")
                        .removeClass("active-button")
                        .addClass("inactive-button")
                } else {
                    this.$toolbar.find(".deleteLinkedObject")
                        .removeClass("inactive-button")
                        .addClass("active-button");
                    this.$toolbar.find(".editManyToOne")
                        .removeClass("inactive-button")
                        .addClass("active-button");
                }
            },

            /**
             * Отображение значения атрибута
             */
            renderAttrValue: function () {
                var _this = this;
                /*if (this.model.get('value') && !this.model.get('value').title) {
                    this.$titleHolderElement.text('Идет получение заголовка...');
                    this.model.callServerAction({
                        action: "getRefAttrValues",
                        data: {
                            objectId: _this.model.get('pageBlockModel').get('objectId'),
                            className: _this.model.get('className'),
                            attrId: this.model.get('id'),
                            idList: JSON.stringify(_this.model.get('value').idList)
                        }
                    }).then(function (result) {
                        _this.model.set('value', {objectId: result.result.id, title: result.result.title}, {silent: true});
                        _this.$titleHolderElement.text(_this.model.get('value').title);
                        _this.model.get('pageBlockModel').refreshAttr('group')
                        _this.updateToolbar()
                    });
                } else {
                    if (this.model.get('value')) {
                        this.$titleHolderElement.text(this.model.get('value').title);
                    } else {
                        this.$titleHolderElement.text('');
                    }

                    _this.model.get('pageBlockModel').refreshAttr('group')
                    _this.updateToolbar()
                }*/

                if (this.model.get('value') && this.model.get('value').title) {
                    this.$titleHolderElement.text(_.escape(this.model.get('value').title));
                } else {
                    this.$titleHolderElement.text('');
                }

                _this.model.get('pageBlockModel').refreshAttr('group')
                _this.updateToolbar()
            },

            /**
             * Получить id выбранного связанного объекта
             */
            getSelectedObjectId: function () {
                if (this.model.get('value')) {
                    return this.model.get('value').objectId;
                } else {
                    return undefined;
                }
            },

            /**
             * Редактировать связанный объект
             */
            editManyToOne: function () {
                if (this.model.get('value') && this.model.get('value').objectId) {
                    this.model.openObjectEdit(this.model.get('value').objectId, this.model.get('linkClass'));
                }
            },

            /**
             * Удалить связанный объект (удаление связи)
             */
            deleteLinkedObject: function () {
                this.model.set('value', undefined);
                this.model.trigger('attribute:render')
            },

            /**
             * Создание связанного объекта
             */
            createClick: function (e) {
                var params = {
                    refAttrId: this.model.get('id'),
                    className: this.model.get('linkClass')
                };

                if ($(e.currentTarget).prop('className')) {
                    params.className = $(e.currentTarget).attr('className');
                }

                if (this.model.get('pageBlockModel').get('objectId')) {
                    params['refObjectId'] = this.model.get('pageBlockModel').get('objectId');
                }

                this.model.openPage(
                    this.model.get('pageBlockModel').getSiteResource('createObjectPage', 'pages'),
                    'Создание объекта ...',
                    params
                );

            },

            /**
             * Выбрать связанный объект
             */
            selectLinkedObject: function () {
                var objectList = [];
                var selectedObjectId = this.getSelectedObjectId();
                if (selectedObjectId) {
                    objectList.push( String(selectedObjectId));
                }
                var params = {
                    refAttrId: this.model.get('id'),
                    className: this.model.get('linkClass'),
                    selectMode: "select",
                    objectId: this.model.get('id'),
                    selectedList: objectList
                };

                this.model.openPage(
                    this.model.get('pageBlockModel').getSiteResource('selectObjectPage', 'pages'),
                    'Просмотра объекта ...',
                    params
                );
            }

        });

        return view;
    });
