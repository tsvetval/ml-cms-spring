/**
 * @name: UserBlockBoot
 * @package:
 * @project: ml-cms-all
 * @javaController:
 * Created by i_tovstyonok on 19.01.2016.
 */

define(
    ['log', 'misc', 'backbone', 'cms/page_blocks/user_block/model/UserBlockModel', 'cms/page_blocks/user_block/view/UserBlockView'],
    function (log, misc, backbone, model, view) {
        return {
            model: model,
            view: view
        };
    });
