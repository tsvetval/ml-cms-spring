define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'markup', 'cms/page_blocks/DialogPageBlock', 'moment'],
    function (log, misc, backbone, PageBlockView, markup, Message) {
        var view = PageBlockView.extend({

            events: {
                'click .route-home': 'routeToHome'
            },

            initialize:function () {
                console.log("initialize StaticBlockView");
                this.listenTo(this.model, 'render', this.render);
            },

            render:function () {
                var _this = this;
                this.$el.html(this.model.get('renderTemplate'));
                this.model.set('blockRendered', true)
            },

            routeToHome: function (e) {
                e.preventDefault();
                var url = this.model.getSiteResource('authPage', 'pages');
                if ($.cookie('homePage')) {
                    url = '/' + $.cookie('homePage')
                }
                if($(e.currentTarget).hasClass('reload')) {
                    window.location = misc.getContextPath() +  url
                } else {
                    this.model.get('pageModel').openPage({
                        url:  url,
                        params: {}
                    })
                }
            }

        });
        return view;
    });
