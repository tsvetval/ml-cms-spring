/**
 * @name: loadKladrView
 * @package:
 * @project: ml-cms-all
 * Created by i_tovstyonok on 12.04.2016.
 */

define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'cms/events/events', 'text!cms/page_blocks/load_kladr_block/template/loadKladrTemplate.tpl'],
    function (log, misc, backbone, PageBlockView, Events, Template) {
        var view = PageBlockView.extend({
            events: {
                "click #upload-import-kladr-file": "uploadImportFile",
                "click #cancel-import-kladr, #close-page": "closePage",
                "click #reset-import": "resetImport"
            },

            initialize: function () {
                var _this = this;
                log.debug("Initialize loadKladrView");
                this.listenTo(this.model, 'change:import', this.render);
                this.listenTo(this.model, 'upload:progress', this.updateUploadProgress);
                this.listenTo(this.model, 'import:complete', function () {
                    _this.$('#import-result i').hide();
                    _this.$('#result-controls').show()
                })
            },

            render: function () {
                /* Render view with template engine */
                this.$el.html(_.template(Template, {model: this.model}));

                this.$('#progress-block').hide();
                this.$('#result-controls').hide();
                this.inputFileHandler();

                /* After render view set block as rendered */
                this.model.set('blockRendered', true);
                return true
            },

            inputFileHandler: function () {
                var _this = this;
                this.$('#import-kladr-file').on('change', function (e) {
                    var file = e.target.files[0];
                    _this.$('#upload-import-kladr-file').prop('disabled', !file)

                    if (file) {
                        $(e.target).parent().removeClass('has-error')
                    }
                })
            },

            uploadImportFile: function () {
                var file = this.$('#import-kladr-file')[0].files[0];
                if (file) {
                    this.$('#input-block').hide();
                    this.$('#progress-block').show();
                    this.model.uploadImportFile(file)
                } else {
                    this.$('##import-kladr-file').parent().addClass('has-error')
                }
            },

            updateUploadProgress: function (percent) {
                this.$('#progress-block .progress-bar').css('width', percent + '%');
            },

            resetImport: function () {
                this.$('#import-result i').show();
                this.model.process()
            },

            closePage: function () {
                this.model.closePage()
            }
        });
        return view
    }
);
