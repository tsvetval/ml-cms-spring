<div id="photo">
    <div class="container-fluid" style="">
        <div class="col-xs-10" style="display: block !important  border: 1px black solid;">
            <label class="checkbox inline" for="showMask" id="maskControl">
                <input type="checkbox" id="showMask">Показать маску</label>
        </div>
        <div class="col-xs-14">
            <div class="btn-group pull-right">
                <span class="btn btn-primary" id="onCamera">
                    <label class="glyphicon glyphicon-camera"></label>
                </span>
                <span class="btn btn-primary btn-file" id="addFile">
                    <label class="glyphicon glyphicon-file"></label>
                    <input type="file" id="file-crop" name="file"/>
                </span>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="col-xs-24">
            <div class="col-xs-12">
                <div style="height: <%= height*sizeFactor%>px; width: <%= width*sizeFactor%>px; display: block !important; border: 1px black solid;">
                    <div id="imgbox" style="overflow : hidden  !important; height: <%= height*sizeFactor%>px; width: <%= width*sizeFactor%>px;" class="bordered">
                        <div class="photomask" id="photomask"></div>
                        <img id="photo_cut" style="max-width: none !important;"/>
                    </div>

                </div>
            </div>
            <div class="col-xs-12">
                <div class="pull-right " style="border: 1px black solid;">
                    <img id="photo_full"
                         style="display: block ;  overflow : hidden  !important; height:  <%= height*sizeFactor%>px ; width:  <%= width*sizeFactor%>px ;"/>
                </div>
            </div>
        </div>
    </div>
</div>

<div id='camWindow' style="display: none;">
    <div id="webCamDialog" style="width: 640px; height: 480px; margin: 0 auto;"></div>
    <div class="row" style="margin-top: 30px;">
        <div class="col-xs-24 text-center">
            <button class="btn btn-success" id="take-cam-image"><i class="glyphicon glyphicon-camera"></i> Сделать фотографию</button>
        </div>
    </div>
</div>