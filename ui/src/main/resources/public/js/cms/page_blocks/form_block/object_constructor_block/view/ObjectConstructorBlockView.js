/**
 * Представление блока создания объекта
 */
define(
    ['log', 'misc', 'backbone', 'underscore',
        'cms/page_blocks/form_block/FormBlockView',
        'markup',
        'cms/page_blocks/DialogPageBlock',
        'cms/page_blocks/form_block/collections/AttrGroupCollection',
        'cms/page_blocks/form_block/factories/GroupViewFactory',
        'cms/page_blocks/form_block/factories/AttrViewFactory',
        'text!cms/page_blocks/form_block/object_constructor_block/templates/ObjectConstructorTemplate.tpl'],
    function (log, misc, backbone, _, FormBlockView, markup, Message, AttrGroupCollection,
              GroupViewFactory, AttrViewFactory, ObjectEditTemplate) {
        var view = FormBlockView.extend({

            events: {
                "click .save-object-button": "saveObjectClick",
                "click .save-close-object-button": "saveAndCloseObjectClick",
                "click .cancel-object-button": "cancelEditObjectClick"
            },

            /**
             * Инициализация представления
             */
            initialize: function () {
                log.debug("initialize ObjectViewBlockView");
                this.viewMode = 'CREATE';
                _.extend(this.events, FormBlockView.prototype.events);
                this.listenTo(this.model, 'render', this.render);
            },



            /**
             * Обработка клика по кнопке "Сохранить"
             * @returns {boolean}
             */
            saveObjectClick: function () {
                this.model.saveObject();
            },

            /**
             * Обработка клика по кнопке "Сохранить и закрыть"
             * @returns {boolean}
             */
            saveAndCloseObjectClick: function () {
                this.model.saveAndCloseObject();
            },

            /**
             * Обработка клика по кнопке "Отмена"
             * @returns {boolean}
             */
            cancelEditObjectClick: function () {
                this.model.cancelEditObject();
            },

            /**
             * Отрисовка представления
             */
            render: function () {
                var _this = this;
                this.$el.html(_.template(ObjectEditTemplate, {formModel: this.model}));
                // create container
                var $content_container = _this.$el.find('#view_content_container');
                _this._renderNonGroupAttrs($content_container);
                // Выводим группы
                _this._renderRootGroups($content_container);
                return true;
            }
        });

        return view;
    });
