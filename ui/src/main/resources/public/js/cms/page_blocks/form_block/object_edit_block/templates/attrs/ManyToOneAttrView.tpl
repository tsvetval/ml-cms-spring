<div class="attribute-edit-manytoone" >
    <div class="attr-label-container col-xs-offset-<%=attrModel.get('offset')%> col-xs-<%=attrModel.get('titleLength')%>">
        <b class="attr-label"><%=attrModel.escape('description')%>:</b>
    </div>
    <div class="col-xs-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%> input-group">
        <a style="padding-left: 5px; margin-right: 50px; float: left;" class="attrFieldTitle">
        </a>

        <% if (!attrModel.get('readOnly')) { %>
        <div class="ml-main__content-class-form-item-menu custom-toolbar">
            <% if (!((attrModel.get('notShowCreate') && attrModel.get('viewMode') == 'CREATE') || (attrModel.get('notShowCreateInEdit') && attrModel.get('viewMode') == 'EDIT'))) { %>
                <% if (attrModel.get('createdClasses') && !_.isEmpty(attrModel.get('createdClasses'))) { %>
                <div class="btn-group create-dropdown">
                    <div class="ml-main__content-class-form-item-menu-item m-main__content-class-form-item-menu-item_add dropdown-toggle" data-toggle="dropdown" title="Создать"></div>
                    <ul class="dropdown-menu" role="menu">
                        <% _.each(attrModel.get('createdClasses'), function(cls) { %> <li> <a class="create-other-object createClick" className="<%= cls.className %>"><%= cls.title %></a></li> <% });  %>
                    </ul>
                </div>
                <% } else { %>
                <div class="ml-main__content-class-form-item-menu-item m-main__content-class-form-item-menu-item_add createClick" title="Создать"></div>
                <% } %>
            <% } %>
            <% if (!((attrModel.get('notShowChoose') && attrModel.get('viewMode') == 'CREATE') || (attrModel.get('notShowChooseInEdit') && attrModel.get('viewMode') == 'EDIT'))) { %>
            <div class="ml-main__content-class-form-item-menu-item m-main__content-class-form-item-menu-item_search selectLinkedObject" title="Выбрать"></div>
            <% } %>
            <% if (!((attrModel.get('notShowDelete') && attrModel.get('viewMode') == 'CREATE') || (attrModel.get('notShowDeleteInEdit') && attrModel.get('viewMode') == 'EDIT'))) { %>
            <div class="ml-main__content-class-form-item-menu-item m-main__content-class-form-item-menu-item_remove deleteLinkedObject" title="Удалить"></div>
            <% } %>
            <% if (!((attrModel.get('notShowEdit') && attrModel.get('viewMode') == 'CREATE') || (attrModel.get('notShowEditInEdit') && attrModel.get('viewMode') == 'EDIT'))) { %>
            <div class="ml-main__content-class-form-item-menu-item m-main__content-class-form-item-menu-item_right editManyToOne" title="Открыть"></div>
            <% } %>
        </div>
        <% } %>
        
        <!--
            <div>
                <div class="editManyToOne glyphicon glyphicon-pencil">
                          &lt;!&ndash;<span class="glyphicon glyphicon-pencil"></span>&ndash;&gt;
                </div>
                <div class="deleteLinkedObject glyphicon glyphicon-trash">
            &lt;!&ndash;
                    <span class="glyphicon glyphicon-trash"></span>
            &ndash;&gt;
                </div>
                <div class="glyphicon glyphicon-plus-sign createClick active-button">
        &lt;!&ndash;
                    <span class="glyphicon glyphicon-plus-sign"></span>
        &ndash;&gt;
                </div>
                <div class="glyphicon glyphicon-search highlight-button selectLinkedObject">
        &lt;!&ndash;
                    <span class="glyphicon glyphicon-search highlight-button"></span>
        &ndash;&gt;
                </div>
            </div>
        -->

    </div>
</div>