<div class="attribute-view-gallery">
    <div class="col-xs-offset-<%=attrModel.get('offset')%> col-xs-<%=attrModel.get('titleLength')%>">
        <b class="attr-label"><%=attrModel.escape('description')%>:</b>
    </div>
    <div class="col-xs-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%>">
        <%if (attrModel.get('gallery')){%>
        <div class="gallery">
            <% _.each(attrModel.get('gallery'), function(object) { %>
                <% if (object.thumbnailUrl || object.url) {%>
                <div class="col-xs-4">
                    <a href="" data-toggle="modal"
                       data-target="#gallery-modal-<%=attrModel.get('id')%>"
                       data-image="<%=object.url%>"
                       data-title="<%= _.escape(object.title) %>"
                       objectid="<%=object.objectId%>"
                       mlclass="<%=object.className%>"
                       class="thumbnail"
                       title="<%= _.escape(object.title) %>">
                        <!--<img src="<% print(object.thumbnailUrl || object.url) %>" class="img-responsive"> -->
                        <div style="background-image: url('<% print(object.thumbnailUrl || object.url || '') %>');" class="holder"></div>
                    </a>
                </div>
                <% }%>
            <% }) %>
        </div>
        <div id="gallery-modal-<%=attrModel.get('id')%>" class="modal gallery-modal fade">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="col-xs-4 pull-right">
                            <button class="btn btn-sm btn-primary object-opener" data-dismiss="modal">Перейти к объекту</button>
                        </div>
                        <h2 class="modal-title"></h2>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-24">
                                <img src="" class="img-responsive">
                            </div>
                        </div>
                        <div class="row gallery-controls">
                            <div class="col-xs-4">
                                <button class="gallery-prev btn btn-primary">Назад</button>
                            </div>

                            <div class="col-xs-4 pull-right text-right">
                                <button class="gallery-next btn btn-primary">Вперед</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%}%>
    </div>
</div>