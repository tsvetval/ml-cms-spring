/**
 * Модель группы атрибутов
 */
define(
    ['log', 'misc', 'backbone',
        'underscore',
        'cms/events/events',
        'cms/page_blocks/form_block/collections/AttrCollection',
        'cms/page_blocks/form_block/collections/AttrGroupCollection'
        ],
    function (log, misc, backbone, _, cmsEvents, AttrCollection, AttrGroupCollection) {
        var AttrGroupModel = backbone.Model.extend({
            defaults:   {
                /* Заголовок группы*/
                title :  undefined,
                /* Идентификатор группы*/
                groupId :  undefined,
                /**/
                attrCollection :  undefined,
                /*список подгрупп*/
                subGroupCollection : undefined,
                groupType : undefined,
                _tabListGroup : undefined
            },

            /**
             * Инициализация модели
             */
            initialize: function () {
                console.log("initialize PageModel");
                this.set('guid', _.uniqueId('attr'));
                this.set("attrCollection", new AttrCollection());
                this.set("subGroupCollection", new AttrGroupCollection());
            },

            /**
             * Получение заголовка группы
             *
             * @returns {*} -   заголовок группы
             */
            getTitle : function (){
                return this.get('title');
            },

            /**
             * Получение id группы
             *
             * @returns {*} -   id группы
             */
            getGroupId : function (){
                return this.get('id');
            },

            /**
             * Получение id родительской группы
             *
             * @returns {*}
             */
            getParentGroupId : function (){
                return this.get('parent');
            },

            /**
             * Получение типа группы
             *
             * @returns {*} -   тип группы
             */
            getGroupType : function (){
                return this.get('groupType');
            },

            /**
             * Подсветка группы, содержащей незаполненные обязательные атрибуты
             */
            highlightMandatory: function () {
                this.trigger(cmsEvents.HIGHLIGHT_MANDATORY, this.get('id'));
            },

            /**
             * Убрать подсветку группы
             */
            removeHighlightMandatory: function () {
                this.trigger(cmsEvents.REMOVE_HIGHLIGHT_MANDATORY, this.get('id'));
            },

            /**
             * Recursive check group children for mandatory
             * @returns {boolean}
             */
            hasMandatory: function () {
                var nestedMandatory = [];

                this.get('subGroupCollection').each(function (group) {
                    if (group.hasMandatory()) {
                        nestedMandatory.push(group)
                    }
                })

                if (nestedMandatory.length > 0 ) return true

                if (this.get('attrCollection').findWhere({mandatory: true})) return true

                return false
            }

        });

        return AttrGroupModel;
    });
