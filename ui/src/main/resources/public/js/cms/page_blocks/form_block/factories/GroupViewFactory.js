/**
 * Фабрика представлений для групп атрибутов
 */
define(['log'],
    function (log) {

        var GroupModelFactory = new function () {

            /**
             * Получить представление для группы
             *
             * @param data  -   параметры, должны содержать
             *                  groupType (string)  -   тип группы
             *
             * @returns {*}
             */
            this.getGroupViewClass = function (model) {
                var def = new $.Deferred();
                if (model.getGroupType() == 'TAB_LIST'){
                   require(['cms/page_blocks/form_block/view/groups/TabListGroupView'], function (viewClass) {
                       def.resolve(viewClass);
                   });
                } else if (model.getGroupType() == 'FIELDSET'){
                    require(['cms/page_blocks/form_block/view/groups/FieldsetGroupView'], function (viewClass) {
                        def.resolve(viewClass);
                    });
                } else if (model.getGroupType() == 'ACCORDION'){
                    require(['cms/page_blocks/form_block/view/groups/AccordionGroupView'], function (viewClass) {
                        def.resolve(viewClass);
                    });
                } else {
                   require(['cms/page_blocks/form_block/view/groups/DefaultGroupView'], function (viewClass) {
                       def.resolve(viewClass);
                   });
                }
                return def.promise();
            };
        };

        return GroupModelFactory;
    });
