/**
 * Представление для отображение атрибута типа FILE
 */
define(
    ['log', 'misc', 'backbone', 'moment',
        'cms/page_blocks/form_block/view/AttrView',
        'text!cms/page_blocks/form_block/object_view_block/templates/attrs/FileAttrView.tpl',
        'text!cms/page_blocks/form_block/object_view_block/templates/attrs/custom/image_file.tpl'],
    function (log, misc, backbone, moment, AttrView, DefaultTemplate, imageTemplate) {
        var view = AttrView.extend({
            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                _.extend(this.events, AttrView.prototype.events);
            },

            /**
             * Отрисовка представления
             */
            render: function () {
                var _this = this;
                if (this.isHidden()) {
                    return;
                }
                if (this.model.get('value')) {
                    var objectId = this.model.get('value').objectId;
                    this.model.callServerAction(
                        {
                            action: 'downloadFile',
                            data: {
                                objectId: objectId,
                                className: _this.model.get('className'),
                                attrName: _this.model.get('entityFieldName'),
                                thumbnail: true
                            }
                        },
                        function (result) {
                            var thumb = _.findWhere(result.otherLinks, {type: 'thumbnail'}) || {}
                            _this.$el.html(_.template(imageTemplate, {
                                attrModel: _this.model,
                                url: result.url,
                                thumbnail: thumb.url || result.url
                            }));
                        }
                    );

                } else {
                    this.$el.html(_.template(this.viewTemplate, {
                        attrModel: this.model
                    }));
                }
            }

        });

        return view;
    });
