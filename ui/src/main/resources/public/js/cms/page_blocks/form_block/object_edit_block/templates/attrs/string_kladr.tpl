<div class="attribute-edit-string-kladr">
    <div class="attr-label-container col-xs-offset-<%=attrModel.get('offset')%> col-xs-<%=attrModel.get('titleLength')%>">
        <b class="attr-label"><%=attrModel.escape('description')%>:</b>
    </div>
    <div class="col-xs-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%>">

        <div class="col-xs-24">
            <input class="attrField col-xs-24" data-kladr="false">
            <i class="glyphicon glyphicon-refresh search-run pull-right"></i>
            <div class="help-block">Поиск по кладр в следующем порядке: <span>Город, улица, дом</span></div>
        </div>


    </div>
</div>