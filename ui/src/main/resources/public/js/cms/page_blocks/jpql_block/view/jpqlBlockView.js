/**
 * @name: jpqlBlockView
 * @package:
 * @project: ml-cms-all
 * Created by i_tovstyonok on 29.12.2015.
 */

define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'text!cms/page_blocks/jpql_block/template/jpqlBlockTemplate.tpl',
        'boot_table_ru',
        'cms/page_blocks/DialogPageBlock',
        'table_formatter_mixin'
    ],
    function (log, misc, backbone, PageBlockView, template, bootstrapTable, Message, table_formatter_mixin) {
        var view = PageBlockView.extend({
            events: {
                'click #class-list .list-group-item': 'setPredefinedQuery',
                'click #query-button': 'execQuery',
                'change #jpql-query-history, #sql-query-history': 'setQueryFromHistory',
                'keydown #jpql-query-field, #sql-query-field': 'ctrlEnterAction',
                'keyup #jpql-query-field, #sql-query-field': 'resizeField',
                'click #clear-jpql-history, #clear-sql-history': 'clearHistory',
                'change #class-filter-notsystem': 'changeClassFilter'
            },

            initialize: function () {
                log.debug("Initialize jpqlBlockView");
                this.listenTo(this.model, 'render', this.render);
            },

            render: function () {
                /* Render view with template engine */
                this.$el.html(_.template(template, {model: this.model}))

                this.updateHistory()

                /* After render view set block as rendered */
                this.model.set('blockRendered', true);
                return true
            },

            getActiveMode: function (){
                var $activeForm = this.$('#query-builder');
                return $activeForm.find('.tab-pane.active').attr('id').replace('-pane', '')
            },

            setPredefinedQuery: function (e) {
                var $item = $(e.currentTarget);
                var query = _.template('SELECT <%= selection %> FROM <%= entity%> <%= alias %>');
                var mode = this.getActiveMode();

                if (mode == 'jpql') {
                    var where = {entityName: $item.data('class')};
                    var obj = _.findWhere(this.model.get('meta').mlClasses, where);
                    var attributes = _.compact(_.pluck(obj.attrs, 'entityName'))

                } else if (mode == 'sql') {
                    var where = {tableName: $item.data('class')};
                    var obj = _.findWhere(this.model.get('meta').mlClasses, where);
                    var attributes = _.compact(_.pluck(obj.attrs, 'tableName'))
                }


                if (this.$('#'+ mode + '-query-field').val() == '') {
                    this.$('#'+ mode + '-query-field').val(query({
                        selection: mode == 'jpql' ? 'o' : '*',
                        entity: mode == 'jpql' ? $item.data('class') : '"' + $item.data('table') + '"',
                        alias: mode == 'jpql' ? 'o' : ''
                    }));

                    this.resizeField()
                }

                if (attributes) {
                    var $columns = $('<div>').append($('<div>').append($('<label>').text($item.data('class') + ': ')));
                    _.each(attributes, function (attribute) {
                        $columns.append($('<span>', {
                            class: 'col-xs-6'
                        }).text(attribute))
                    });
                    /*this.$('#'+ mode + '-attributes').text($item.data('class') + ': ' + attributes.join(', '))*/
                    this.$('#'+ mode + '-attributes').html($columns)
                }
            },

            setQueryFromHistory: function (e) {
                var $history = $(e.currentTarget);
                var mode = this.getActiveMode();

                if (this.$('#'+ mode + '-query-field').val() == '') {
                    this.$('#'+ mode + '-query-field').val($history.val())
                    this.resizeField()
                } else {
                    $history.val('')
                }
            },
            
            execQuery: function (){
                var _this = this
                var mode = this.getActiveMode();
                var query = this.$('#'+ mode + '-query-field').val();

                this.model.execQuery(query, mode).then(function (result) {
                    var $table = _this.$('#result-table')
                    var columns = []

                    $table.bootstrapTable('destroy')

                    if (result.headers) {
                        columns = _.map(result.headers, function (header) {
                            var pair = _.pairs(header)
                            return {
                                field: pair[0][0],
                                title: pair[0][1],
                                sortable: true,
                                formatter: _this.formatterString
                            }
                        })
                    } else {
                        if (!result.empty && !_.isEmpty(result.table)) {
                            for (var i =0; i < result.table[0].length; i++){
                                columns.push({
                                    field: i,
                                    title: '#'+(i+1),
                                    formatter: _this.formatterString
                                })
                            }
                        }
                    }
                    $table.bootstrapTable({
                        columns: columns,
                        data: result.table
                    });

                    // Подключаем внешний скролл
                    _this.helpers.tableExtendedScrollInit.apply(_this);

                    _this.updateHistory()

                })
            },

            ctrlEnterAction: function (e) {

                if (e.keyCode == 13 && e.ctrlKey) {
                    this.execQuery()
                }
            },

            resizeField: function () {
                var mode = this.getActiveMode();
                var $field = $('#'+ mode +'-query-field').get(0);
                $field.style.overflow = 'hidden';
                $field.style.height = 0;
                $field.style.height = $field.scrollHeight + 'px';
                if (this.$('#'+ mode +'-query-field').val() == '') this.$('#'+ mode +'-attributes').empty()
            },

            updateHistory: function (type) {
                var history = JSON.parse(localStorage.getItem(this.model.get('storageKey') + '.' + type));
                if (!type) {
                    this.updateHistory('jpql');
                    this.updateHistory('sql')
                } else {
                    var $history = this.$('#' + type + '-query-history');
                    $history.empty();
                    if (!_.isEmpty(history)) {
                        $history.append($('<option>'));

                        _.each(history.reverse(), function (query) {
                            $history.append($('<option>', {
                                value: query
                            }).text(query))
                        })
                    }
                }
            },

            clearHistory: function () {
                var type = this.getActiveMode();
                this.model._clearHistory(type);
                this.updateHistory(type)
            },

            changeClassFilter: function (e) {
                var $filter = $(e.currentTarget);
                var $systemClasses = $('#class-list li[data-system="true"]')

                if ($filter.is(':checked')) {
                    $systemClasses.hide()
                } else {
                    $systemClasses.show()
                }

            }
        });

        _.extend(view.prototype, table_formatter_mixin);
        return view
    }
);
