<section id="import-replication-block" role="template">
    <div class="row">
        <div class="col-xs-24 well" id="replication-file-form">
            <div class="row">
                <h3 class="block-header">Выбор файла репликации</h3>
            </div>
            <div class="form-inline">
                <div class="form-group">
                    <input type="file" class="form-control">
                </div>
                <div class="form-group">
                    <select class="form-control" id="import-type">
                        <% _.each(model.get('types'), function (type) { %>
                        <option value="<%= type.code %>"><%= type.label %></option>
                        <% }) %>
                    </select>
                </div>
                <div class="form-group">
                    <button id="replication-file-upload" class="btn btn-primary">Добавить</button>
                    <button id="replication-file-cancel" class="btn btn-default">Отмена</button>
                </div>
            </div>

            <div class="" id="progress-block">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped active"></div>
                </div>
            </div>
        </div>

        <div class="col-xs-24 well" id="replication-info">
            <div class="row">
                <h3 class="block-header">Информация о репликации</h3>
            </div>
            <div id="replication-filename">
                <label>Файл: </label>
                <span></span>
            </div>
            <ul class="list-group" id="replication-steps">

            </ul>
            <div class="row">
                <button class="btn btn-default" id="cancel-replication">Новый импорт</button>
                <button class="btn btn-success" id="do-replication">Применить пакет обновлений</button>
            </div>
        </div>
    </div>
</section>