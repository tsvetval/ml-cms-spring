/**
 * Модель блока просмотра объекта
 */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/form_block/FormBlockModel',
        'cms/events/NotifyPageBlocksEventObject',
        'cms/page_blocks/form_block/model/AttrModel',
        'cms/page_blocks/form_block/model/AttrGroupModel',
        'cms/page_blocks/form_block/collections/AttrCollection',
        'cms/page_blocks/form_block/collections/AttrGroupCollection'
    ],
    function (log, misc, backbone, FormBlockModel, NotifyEventObject, AttrModel, AttrGroupModel, AttrCollection, AttrGroupCollection) {
        var model = FormBlockModel.extend({
            defaults: {
                entityName: undefined,
                description: undefined,
                title: undefined,
                attrList: undefined,
                nonGroupAttrList: undefined,
                groupList: undefined,
                rootGroupList: undefined,
                pageTitle : '',
                viewMode: 'VIEW',
                controller: 'viewObjectController'
            },

            /**
             * Инициализация модели
             */
            initialize: function () {
                /* FormBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this);

                log.debug('initialize ObjectViewBlockModel');
                this.pageTitle = 'Просмотр объекта <%=data.title%>';
            },

            /**
             * Добавление событий блока
             * @Override FormBlockModel.bindEvents
             */
            bindEvents: function () {
                /* FormBlockModel.bindEvents() добавляет основные события блока */
                model.__super__.bindEvents.call(this);
            },

            /**
             * Остановка обработчиков событий блока
             * @Override FormBlockModel.unbindEvents
             */
            unbindEvents: function () {
                /* PageBlockModel.unbindEvents() удаляет все основные события блока */
                model.__super__.unbindEvents.call(this)
            },

            /**
             * Переход в режим редактирования объекта
             */
            startEditObject : function(){
                this.replacePage(
                    this.getSiteResource('editObjectPage', 'pages'),
                    'Просмотр объекта ...',
                    {
                        objectId:  this.get('objectId'),
                        className: this.get('className')
                    }
                );
            }
        });
        return model;
    });
