/**
 * Представление для блока выбора связанных объектов
 *
 */
define(
    ['log', 'misc', 'backbone', 'underscore',
        'boot_table_ru',
        'cms/page_blocks/list_block/object_select_block/view/ObjectSelectBlockView',
        'cms/page_blocks/DialogPageBlock'
    ],
    function (log, misc, backbone, _,
              boot_table,
              ObjectSelectBlockView,
              Message) {
        var view = ObjectSelectBlockView.extend({

            /**
             *  форматтер для отображения выбранных элементов (чекбокс)
             */
            stateFormatter: function (value, row, index) {
                var _objectId = row.objectId + "@" + this.model.get("className");
                if (this.model.get('selectMode') == 'multiselect') {
                    return {
                        /*disabled: true,*/
                        checked: (_.contains(this.model.get('selectedList'), _objectId) && !_.contains(this.model.get('removeIdList'), _objectId)) || _.contains(this.model.get('addIdList'), _objectId)
                    }
                } else {
                    return _.contains(this.model.get('selectedList'), _objectId) && !_.contains(this.model.get('removeIdList'), _objectId);
                }
            }
        });

        return view;
    });
