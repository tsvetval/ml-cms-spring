/*
* Создает шаблон лоя pageBlock(JS для Backbone и Java контроллер)
* Контроллер: ru.ml.web.block.controller.impl.PageWizardBlockController
* */


define(
    ['log', 'misc', 'backbone', 'cms/page_blocks/security_view_block/model/SecurityViewBlockModel', 'cms/page_blocks/security_view_block/view/SecurityViewBlockView'],
    function (log, misc, backbone, model, view) {
        return {
            model: model,
            view: view
        };
    });
