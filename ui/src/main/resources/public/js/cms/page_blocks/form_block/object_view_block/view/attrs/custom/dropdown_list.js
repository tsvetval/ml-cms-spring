/**
 * Наследуем view и шаблон от ManyToOneAttrView
 */

define(['cms/page_blocks/form_block/object_view_block/view/attrs/ManyToOneAttrView'],
    function (AttrView) {
        var view = AttrView.extend({
            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
            }
        })

        return view
    }
)