<div class="">
    <div class="panel panel-default col-xs-24">
        <div class="panel-heading">
            <h3 class="panel-title"><%= classInfo.name %> ( <%= classInfo.entityName %> )</h3>
        </div>
        <div class="panel-body">
            <%= classInfo.definition %>
            <div class="panel panel-default">
                <div class="panel-heading">Атрибуты</div>
                <table class="table panel-body" style="table-layout: fixed; overflow-wrap: break-word;">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th>Тип</th>
                        <th style="width: 50%;">Описание</th>
                    </tr>
                    </thead>
                    <tbody>
                    <% _.each(classInfo.attrs, function(attr) { %>
                    <tr>
                        <td><%= attr.name %> (<%= attr.entityName %>)</td>
                        <td><%= attr.type %></td>
                        <td><%= linker.link(attr.definition, {truncate: 35, urls: { schemeMatches: true, wwwMatches: true, tldMatches: false }}) %></td>
                    </tr>
                    <% }); %>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


</div>


