define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'markup', 'cms/page_blocks/DialogPageBlock', 'moment',
        'text!cms/page_blocks/resource_view_block/template/ResourceViewTemplate.tpl'],
    function (log, misc, backbone, PageBlockView, markup, Message, moment,
              Template) {

        var view = PageBlockView.extend({

            events: {
                "click .cls": "showClass"
            },

            initialize: function () {
                this.listenTo(this.model, 'render', this.render);
            },

            render: function(){
                var _this = this;
                var pagesInfo = this.model.get("pagesInfo");

                this.$el.html(_.template(Template));
                this.$el.find('.details').hide();

                pagesInfo.nodes.forEach(function (node){
                    _this.$el.find('.resource-tree > ul').append(_this._renderNode(node))
                });

                this._renderTree();

                this.model.set('blockRendered', true);
                return true;
            },

            _renderNode: function (node) {
                var _this = this;
                var badge, meta;

                if(node.controllerMeta) {
                    badge = {
                        text: 'C',
                        class: 'label label-success',
                        title: 'Контроллер'
                    };
                    meta = node.controllerMeta

                } else if (node.pageMeta) {
                    badge = {
                        text: 'P',
                        class: 'label label-warning',
                        title: 'Страница'
                    };
                    meta = node.pageMeta
                } else {
                    badge = {
                        text: 'G',
                        class: 'label label-info',
                        title: 'Группа ресурсов'
                    };
                    meta = {};
                    meta.url = node.url;
                }

                var $data = $('<div>', {
                    class: 'clearfix'
                }).append($('<span>', {
                    class: badge.class,
                    title: badge.title
                }).text(badge.text)).append($('<span>').text(meta.url || meta.id));

                var $li =  $('<li>', {
                    'data-jstree': JSON.stringify({
                        leafValue: node.leafValue
                    })
                }).append($data);

                this.model.get('flattenResources').add(node);

                if(node.nodes && node.nodes.length > 0) {
                    $li.append($('<ul>'));
                    node.nodes.forEach(function (subnode){
                        $li.find('> ul').append(_this._renderNode(subnode))
                    })
                }
                return $li
            },

            _renderTree: function () {
                var _this = this;
                this.$treeContainer = this.$el.find('.resource-tree');
                this.$treeContainer.jstree({
                    'rules': {'multiple': false},
                    'lang': {loading: "Загрузка ..."},
                    'core': {
                        'animation': 100,
                        'check_callback': true,
                        'themes': {
                            'dots': false,
                            'icons': false
                        }
                    }
                });

                this.$treeContainer.on('select_node.jstree', function (e, obj) {
                    obj.instance.toggle_node(obj.node);
                    var data = obj.node.data.jstree;
                    var node = _this.model.get('flattenResources').findWhere({leafValue: data.leafValue});
                    if (node) {
                        _this._renderDetails(node)
                    }
                })
            },

            _renderDetails: function (node) {
                this.$detailsContainer = this.$el.find('.resource-details');
                this.$el.find('.details')
                    .width(this.$el.find('.details').parent().width())
                    .css('position', '')
                    .hide();
                this.$el.find('.details-value').empty();

                if(node.get('controllerMeta')) {

                    var $details = this.$el.find('.controller-details');

                    for (var key in node.get('controllerMeta')) {
                        var $field = $details.find('#controller-' + key + '-value');
                        $field.text(node.get('controllerMeta')[key])
                    }

                    $details.show().affix({
                        offset: {
                            top: 90,
                            bottom: 100
                        }
                    })

                } else if (node.get('pageMeta')) {

                    var $details = this.$el.find('.page-details');

                    for (var key in node.get('pageMeta')) {
                        if (key != 'pageBlocks') {
                            var $field = $details.find('#page-' + key + '-value');
                            if (key != 'url') {
                                $field.text(node.get('pageMeta')[key])
                            } else {
                                $field.html($('<a>', {
                                    href: misc.getContextPath() + node.get('pageMeta')[key],
                                    target: '_blank'
                                }).text(node.get('pageMeta')[key]))
                            }

                        } else {
                            var pageBlocks = node.get('pageMeta')[key];
                            var $field = $details.find('#page-blocks-value');

                            pageBlocks.forEach(function (block) {
                                var $template = $('template#page-block-template');
                                var $item = $($template.html())

                                for (var attr in block) {
                                    $item.find('.page-block-' + attr + '-value').text(block[attr]);
                                    if (attr == 'zone') {
                                        /*$item.find('.preview-on-page').on('click', function () {
                                            var $zone = $('#' + block['zone'])
                                            $zone.toggleClass('highlighted-zone')
                                        })*/

                                        $item.find('.preview-in-layout').on('click', function () {
                                            var $preview = $item.find('.layout-preview');
                                            if($preview.is(':empty')) {
                                                $preview.html(node.get('layout'));
                                                var $zone = $preview.find('#' + block['zone']);
                                                $zone.addClass('highlighted-zone')
                                            } else {
                                                $preview.empty()
                                            }

                                            $(this).find('.glyphicon').toggleClass('glyphicon-eye-open glyphicon-eye-close')
                                        })
                                    }
                                }

                                $field.append($item)
                            })
                        }
                    }

                    $details.show().affix({
                        offset: {
                            top: 90,
                            bottom: 100
                        }
                    })

                } else {
                    /*throw new Error("Resource type not available.")*/
                }
            }
        });
        return view;
    });
