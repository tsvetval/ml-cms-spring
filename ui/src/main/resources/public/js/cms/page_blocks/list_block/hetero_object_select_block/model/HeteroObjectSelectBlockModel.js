/**
 *  Модель блока выбора связанных объектов
 *
 */
define(
    ['log', 'misc', 'backbone',
        'cms/events/events',
        'cms/page_blocks/list_block/object_select_block/model/ObjectSelectBlockModel',
        'cms/events/NotifyPageBlocksEventObject'
    ],
    function (log, misc, backbone, cmsEvents, ObjectSelectBlockModel, NotifyEventObject) {
        var model = ObjectSelectBlockModel.extend({
            defaults: {
                folderId: undefined,
                selectedList : undefined,
                controller: 'heteroListController'
            },

            /**
             * инициализация модели
             */
            initialize: function () {
                /* ObjectSelectBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this)

                log.debug("initialize HeteroBlockModel");
            },

            /**
             * Добавление событий блока
             * @Override ObjectSelectBlockModel.bindEvents
             */
            bindEvents: function () {
                /* PageBlockModel.bindEvents() добавляет основные события блока */
                model.__super__.bindEvents.call(this)

                var _this = this;
                this.listenTo(this, cmsEvents.OPEN_FOLDER, function (params) {
                    log.debug("ObjectListBlockModel start cmsEvents.OPEN_FOLDER event");
                    if (params.folderId) {
                        _this.set('folderId', params.folderId);
                        _this._update();
                    }
                });

            },

            /**
             * Остановка обработчиков событий блока
             * @Override ObjectSelectBlockModel.unbindEvents
             */
            unbindEvents: function () {
                /* События блока */
                this.stopListening(this, cmsEvents.OPEN_FOLDER)

                /* PageBlockModel.unbindEvents() удаляет все основные события блока */
                model.__super__.unbindEvents.call(this)
            },

            /**
             * Добавление списка идентификаторов к списку связанных объектов
             * @param ids
             */
            addSelectedIds: function (ids) {
                var _this = this;
                for (var i = 0; i < ids.length; i++) {
                    ids[i] = ids[i] + "@" + _this.get("className");
                }
                var notInSelectedList = _.difference(ids, this.get('selectedList'));

                this.set('addIdList', _.union(this.get('addIdList'), notInSelectedList));
                for (var i = 0; i < ids.length; i++) {
                    this.set('removeIdList', _.difference(this.get('removeIdList'), ids[i]));
                }

                this.set('selectedList', _.union(this.get('selectedList'), ids));

            },

            /**
             * Удаление списка идентификаторов из списка связанных объектов
             * @param ids
             */
            removeSelectedIds: function (ids) {
                var _this = this;
                for (var i = 0; i < ids.length; i++) {
                    ids[i] = ids[i] + "@" + _this.get("className");
                }
                var inSelectedListForRemove = _.intersection(ids, this.get('selectedList'));
                this.set('removeIdList', _.union(this.get('removeIdList'), inSelectedListForRemove));
                var _addIdList = this.get('addIdList');
                for (var i = 0; i < ids.length; i++) {
                    this.set('addIdList', _.difference(this.get('addIdList'), ids[i]));
                }

                this.set('selectedList', _.difference(this.get('selectedList'), ids));

            },

            /*
             * Получаем данные с сервера
             * */
            _loadListData: function () {
                var options = {
                    action: 'getObjectData',
                    data: {
                        folderId: this.get('folderId'),
                        objectId: this.get('objectId'),
                        className: this.get('className'),
                        refAttrId: this.get('refAttrId'),
                        currentPage: this.get('currentPage'),
                        objectsPerPage: this.get('objectsPerPage'),
                        orderAttr: this.get('orderAttr'),
                        orderType: this.get('orderType'),
                        pageBlockId: this.get('blockInfo').get('id'),
                        ml_request: true

                    }
                };
                if (this.get("search")) {
                    $.extend(options.data, this.get("search"));
                }
                return this.callServerAction(options);
            },

            /**
             * Обновление заголовка страницы
             * @private
             */
            _updatePageTitle: function () {
                var title = "Выбор объектов"
                this.updatePageTitle(title);
            }

        });
        return model;
    });
