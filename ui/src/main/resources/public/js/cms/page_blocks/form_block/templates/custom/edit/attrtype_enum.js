define(
    ['log', 'misc', 'backbone', 'select2',
        'cms/page_blocks/form_block/view/AttrView',
        'text!cms/page_blocks/form_block/templates/custom/edit/attrtype_enum.tpl'],
    function (log, misc, backbone, select2,
              AttrView, DefaultTemplate) {
        var view = AttrView.extend({
            $inputField: undefined,

            attrsToHide: {
                STRING: [
                    'linkAttr',
                    'linkClass',
                    'autoIncrement',
                    'linkFilter',
                    'lazy',
                    'enumList',
                    'longLinkValue',
                    'manyToManyTableName',
                    'manyToManyFieldNameM',
                    'manyToManyFieldNameN',
                    'notShowCreate',
                    'notShowChoose',
                    'notShowEdit',
                    'notShowDelete',
                    'notShowCreateInEdit',
                    'notShowChooseInEdit',
                    'notShowEditInEdit',
                    'notShowDeleteInEdit',
                    'tableHeight',
                    'defaultSqlValue',
                    'fileStoreType',
                    'ordered'
                ],
                LONG: [
                    'linkAttr',
                    'linkClass',
                    'linkFilter',
                    'lazy',
                    'longLinkValue',
                    'enumList',
                    'longLinkValue',
                    'manyToManyTableName',
                    'manyToManyFieldNameM',
                    'manyToManyFieldNameN',
                    'notShowCreate',
                    'notShowChoose',
                    'notShowEdit',
                    'notShowDelete',
                    'notShowCreateInEdit',
                    'notShowChooseInEdit',
                    'notShowEditInEdit',
                    'notShowDeleteInEdit',
                    'tableHeight',
                    'defaultSqlValue',
                    'fileStoreType',
                    'ordered'
                ],
                BOOLEAN: [
                    'linkAttr',
                    'linkClass',
                    'autoIncrement',
                    'linkFilter',
                    'lazy',
                    'enumList',
                    'primaryKey',
                    'fieldFormat',
                    'longLinkValue',
                    'inputmask',
                    'manyToManyTableName',
                    'manyToManyFieldNameM',
                    'manyToManyFieldNameN',
                    'notShowCreate',
                    'notShowChoose',
                    'notShowEdit',
                    'notShowDelete',
                    'notShowCreateInEdit',
                    'notShowChooseInEdit',
                    'notShowEditInEdit',
                    'notShowDeleteInEdit',
                    'tableHeight',
                    'defaultSqlValue',
                    'fileStoreType',
                    'ordered'
                ],
                DATE: [
                    'linkAttr',
                    'linkClass',
                    'autoIncrement',
                    'linkFilter',
                    'lazy',
                    'enumList',
                    'primaryKey',
                    'longLinkValue',
                    'inputmask',
                    'manyToManyTableName',
                    'manyToManyFieldNameM',
                    'manyToManyFieldNameN',
                    'notShowCreate',
                    'notShowChoose',
                    'notShowEdit',
                    'notShowDelete',
                    'notShowCreateInEdit',
                    'notShowChooseInEdit',
                    'notShowEditInEdit',
                    'notShowDeleteInEdit',
                    'tableHeight',
                    'defaultSqlValue',
                    'fileStoreType',
                    'ordered'
                ],
                ENUM: [
                    'linkAttr',
                    'linkClass',
                    'autoIncrement',
                    'linkFilter',
                    'lazy',
                    'primaryKey',
                    'fieldFormat',
                    'longLinkValue',
                    'inputmask',
                    'manyToManyTableName',
                    'manyToManyFieldNameM',
                    'manyToManyFieldNameN',
                    'notShowCreate',
                    'notShowChoose',
                    'notShowEdit',
                    'notShowDelete',
                    'notShowCreateInEdit',
                    'notShowChooseInEdit',
                    'notShowEditInEdit',
                    'notShowDeleteInEdit',
                    'tableHeight',
                    'defaultSqlValue',
                    'ordered',
                    'fileStoreType',
                    'virtual'
                ],
                ONE_TO_MANY: [
                    'tableFieldName',
                    'linkClass',
                    'autoIncrement',
                    'enumList',
                    'defaultValue',
                    'primaryKey',
                    'fieldFormat',
                    'longLinkValue',
                    'inputmask',
                    'virtual',
                    'manyToManyTableName',
                    'manyToManyFieldNameM',
                    'manyToManyFieldNameN',
                    'fileStoreType',
                    'defaultSqlValue'
                ],
                MANY_TO_ONE: [
                    'linkAttr',
                    'autoIncrement',
                    'enumList',
                    //'defaultValue',
                    'primaryKey',
                    'fieldFormat',
                    'longLinkValue',
                    'inputmask',
                    'virtual',
                    'manyToManyTableName',
                    'manyToManyFieldNameM',
                    'manyToManyFieldNameN',
                    'defaultSqlValue',
                    'tableHeight',
                    'fileStoreType',
                    'ordered'
                ],
                MANY_TO_MANY: [
                    'tableFieldName',
                    'linkAttr',
                    'autoIncrement',
                    'enumList',
                    'defaultValue',
                    'primaryKey',
                    'fieldFormat',
                    'longLinkValue',
                    'inputmask',
                    'virtual',
                    'fileStoreType',
                    'defaultSqlValue'
                ],
                LONG_LINK: [
                    'linkAttr',
                    'linkClass',
                    'autoIncrement',
                    'enumList',
                    'defaultValue',
                    'primaryKey',
                    'fieldFormat',
                    'inputmask',
                    'mandatory',
                    'manyToManyTableName',
                    'manyToManyFieldNameM',
                    'manyToManyFieldNameN',
                    'tableHeight',
                    'defaultSqlValue',
                    'ordered',
                    'notShowCreate',
                    'notShowChoose',
                    'notShowEdit',
                    'notShowDelete',
                    'notShowCreateInEdit',
                    'notShowChooseInEdit',
                    'notShowEditInEdit',
                    'fileStoreType',
                    'notShowDeleteInEdit'
                ],
                DOUBLE: [
                    'linkAttr',
                    'linkClass',
                    'autoIncrement',
                    'linkFilter',
                    'lazy',
                    'enumList',
                    'longLinkValue',
                    'primaryKey',
                    'manyToManyTableName',
                    'manyToManyFieldNameM',
                    'manyToManyFieldNameN',
                    'notShowCreate',
                    'notShowChoose',
                    'notShowEdit',
                    'notShowDelete',
                    'notShowCreateInEdit',
                    'notShowChooseInEdit',
                    'notShowEditInEdit',
                    'notShowDeleteInEdit',
                    'tableHeight',
                    'defaultSqlValue',
                    'fileStoreType',
                    'ordered'
                ],
                TEXT: [
                    'linkAttr',
                    'linkClass',
                    'autoIncrement',
                    'linkFilter',
                    'lazy',
                    'enumList',
                    'primaryKey',
                    'longLinkValue',
                    'manyToManyTableName',
                    'manyToManyFieldNameM',
                    'manyToManyFieldNameN',
                    'notShowCreate',
                    'notShowChoose',
                    'notShowEdit',
                    'notShowDelete',
                    'notShowCreateInEdit',
                    'notShowChooseInEdit',
                    'notShowEditInEdit',
                    'notShowDeleteInEdit',
                    'defaultSqlValue',
                    'ordered',
                    'inputmask',
                    'fileStoreType',
                    'fieldFormat'
                ],
                FILE: [
                    'linkAttr',
                    'linkClass',
                    'autoIncrement',
                    'linkFilter',
                    'lazy',
                    'enumList',
                    'defaultValue',
                    'primaryKey',
                    'fieldFormat',
                    'longLinkValue',
                    'inputmask',
                    'manyToManyTableName',
                    'manyToManyFieldNameM',
                    'manyToManyFieldNameN',
                    'notShowCreate',
                    'notShowChoose',
                    'notShowEdit',
                    'notShowDelete',
                    'notShowCreateInEdit',
                    'notShowChooseInEdit',
                    'notShowEditInEdit',
                    'notShowDeleteInEdit',
                    'tableHeight',
                    'defaultSqlValue',
                    'ordered'
                ],
                ONE_TO_ONE: [
                    'linkClass',
                    'autoIncrement',
                    'enumList',
                    'defaultValue',
                    'primaryKey',
                    'fieldFormat',
                    'longLinkValue',
                    'inputmask',
                    'fileStoreType',
                    'defaultSqlValue'
                ],
                HETERO_LINK:[
                    'linkAttr',
                    'linkClass',
                    'autoIncrement',
                    'linkFilter',
                    'lazy',
                    'enumList',
                    'defaultValue',
                    'primaryKey',
                    'fieldFormat',
                    'longLinkValue',
                    'inputmask',
                    'manyToManyTableName',
                    'manyToManyFieldNameM',
                    'manyToManyFieldNameN',
                    'notShowCreate',
                    'notShowChoose',
                    'notShowEdit',
                    'notShowDelete',
                    'notShowCreateInEdit',
                    'notShowChooseInEdit',
                    'notShowEditInEdit',
                    'notShowDeleteInEdit',
                    'defaultSqlValue',
                    'useInSimpleSearch',
                    'useInExtendedSearch',
                    'mandatory',
                    'fileStoreType',
                    'virtual'
                ]

            },

            attrsToMandatoty: {
                LONG_LINK: ['longLinkValue'],
                MANY_TO_ONE: ['linkClass'],
                ONE_TO_MANY: ['linkAttr'],
                MANY_TO_MANY: ['linkClass']
            },

            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
            },

            render: function () {
                var _this = this;
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));
                this.$inputField = this.$el.find('.attrField');
                var select2Data = [];
                this.model.get('enumList').forEach(function (enumObj) {
                    select2Data.push({id: enumObj.code, text: _.escape(enumObj.title)});
                });
                this.$inputField.select2({
                    width: '100%',
                    data: select2Data
                });
                this.$inputField.on("change", function () {
                    _this.changeSelection(_this.$inputField.val());
                });
                if (this.model.get('value') && this.model.get('value').code) {
                    this.$inputField.val(this.model.get('value').code).trigger("change");
                } else {
                    this.$inputField.val(null).trigger("change");
                }
                this.addPopoverHelper();
            },

            changeSelection: function (value) {
                this.model.attributes['value'] = {code: value};
                this.model.get('pageBlockModel').refreshAttr('view');
                this.showAttrsForType(value);
            },

            showAttrsForType: function (type) {
                this.model.get('pageBlockModel').hideAttrList(this.attrsToHide[type]);
                this.model.get('pageBlockModel').addMandatory(this.attrsToMandatoty[type]);
            }
        });


        return view;
    });
