<% if (typeof(listModel.get('currentFolder')) != "undefined") { %>
<div class="upper-folder"></div>
<div class="ml-main__content-title">
    <div class="ml-main__content-title-text" title="<%=listModel.get('currentFolder').text%>"><%=listModel.get('currentFolder').text%></div>
    <%if (listModel.get('folders') && listModel.get('folders').length > 0){%>
        <div class="ml-main__content-title-items"><%=listModel.get('folders').length%></div>
    <%}%>
</div>
<% if (listModel.get('className') && listModel.get('showDelete')) { %>
<div class="btnDelete glyphicon glyphicon-trash disabled" title="Удалить">
<% } %>
</div>

<% if (listModel.get('className')) { %>
    <% if (listModel.get('createdClasses')) { %>
    <div class="btn-group btnCreateDropdown" style="float: right;">
        <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            Создать
            <span class="caret"></span>

        </button>
        <ul class="dropdown-menu" role="menu">
            <% _.each(listModel.get('createdClasses'), function(cls) { %> <li> <a class="create-other-object" className="<%= cls.className %>"><%= cls.title %></a></li> <% });  %>
        </ul>
    </div>
    <% } else { %>
    <div class="btnCreate" title="Создать объект">
        Создать
    </div>
    <%}%>
<% } %>


<!--
<h4 class="current-folder-title">Просмотр папки &laquo;<%=listModel.get('currentFolder').text%>&raquo;</h4>
-->
<% } else { %>
    <div class="ml-main__content-title-text">Просмотр корневой папки</div>
<% } %>
<div class="ml-main__content-delimiter"></div>
<div class="folder-list-container">

</div>
<div class="object-list-container">
</div>
<div class="ml-main__content-delimiter"></div>
