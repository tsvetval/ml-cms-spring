/**
 * Представление блока импорта файла репликации
 */
define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'cms/events/events', 'text!cms/page_blocks/replication_block/import_replication/template/ImportReplicationTemplate.tpl'],
    function (log, misc, backbone, PageBlockView, Events, Template) {
        var view = PageBlockView.extend({
            events: {
                "click #replication-file-upload": "upload",
                "click #do-replication": "doReplication",
                "click #cancel-replication": "render",
                "click #replication-file-cancel": function () {
                    this.model.closePage()
                },
            },

            /**
             * Инициализация представления
             */
            initialize:function () {
                console.log("initialize AddToReplicationView");
                this.listenTo(this.model, Events.RENDER_VIEW, this.render);
                this.listenTo(this.model, 'info:loaded', this.showReplicationInfo);
                this.listenTo(this.model, 'upload:progress', this.updateUploadProgress);
                this.listenTo(this.model, 'import:complete', this.importComplete)
            },

            /**
             * Отрисовка представления
             */
            render:function () {
                this.$el.html(_.template(Template, {model: this.model}));

                this.$('#replication-info, #progress-block').hide();

                this.model.set('blockRendered', true)
            },

            upload: function () {
                var $file = this.$('#replication-file-form input[type="file"]').get(0);
                if ($file.files.length > 0) {
                    this.model.set('importType', this.$('#import-type').val());
                    this.model.upload($file.files[0]);
                    this.$('#replication-file-form .form-inline').hide();
                    this.$('#progress-block').show()
                }
            },

            updateUploadProgress: function (percent) {
                this.$('#progress-block .progress-bar').css('width', percent + '%');
            },

            showReplicationInfo: function (replication) {
                var $info = this.$('#replication-info');
                this.$('#replication-file-form').hide();
                $info.show();

                $info.find('#replication-filename span').text(replication.fileName);

                _.each(replication.steps, function (step) {
                    var $step = $('<li>', {
                        class: 'list-group-item'
                    });
                    var $objects = $('<ul>', {class: 'list-group'});

                    _.each(step.objects, function (object) {
                        $objects.append($('<li>').text(object.mluid + ' - ' + object.note))
                    });

                    $step.append($('<label>').text('#' + step.number + ': ' + step.name))
                    $step.append($objects);
                    $step.appendTo($info.find('#replication-steps'))
                })
            },

            doReplication: function () {
                this.$('.btn').prop('disabled', true);
                this.model.doReplication()
            },

            importComplete: function (replication) {
                var _this = this;
                this.$('#do-replication').hide();

                if (replication.steps) {
                    _.each(this.model.get('replication').steps, function(step, index) {
                        console.info(step, replication.steps[index], _.isEqual(step, replication.steps[index]))
                        if (_.isEqual(step, replication.steps[index])) {
                            _this.$('#replication-steps li').eq(index).addClass('list-group-item-success')
                        }
                    });
                }

                this.$('.btn').prop('disabled', false);
            }
        });

        return view;
    });