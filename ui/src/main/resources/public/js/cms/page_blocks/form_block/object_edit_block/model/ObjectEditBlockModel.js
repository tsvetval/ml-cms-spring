/**
 * Модель блока редактирования объекта
 */
define(
    ['log', 'misc', 'backbone',
        'cms/events/events',
        'cms/page_blocks/form_block/FormBlockModel',
        'cms/events/NotifyPageBlocksEventObject',
        'cms/page_blocks/form_block/model/AttrModel',
        'cms/page_blocks/form_block/model/AttrGroupModel',
        'cms/page_blocks/form_block/collections/AttrCollection',
        'cms/page_blocks/form_block/collections/AttrGroupCollection',
        'libs/requestloader/jquery.requestloader'
    ],
    function (log, misc, backbone, cmsEvents, FormBlockModel, NotifyEventObject, AttrModel, AttrGroupModel, AttrCollection, AttrGroupCollection) {
        var model = FormBlockModel.extend({
            defaults: {
                objectId: undefined,
                entityName: undefined,
                description: undefined,
                title: undefined,
                attrList: undefined,
                nonGroupAttrList: undefined,
                groupList: undefined,
                rootGroupList: undefined,
                pageTitle: '',
                guid: undefined,
                viewMode: 'EDIT',
                controller: 'editObjectController'
            },


            /**
             * Инициализация модели
             */
            initialize: function () {
                /* FormBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this);

                log.debug('initialize ObjectEditBlockModel');
                this.pageTitle = 'Редактирование объекта <%=data.title%>';
                this.set('guid', _.uniqueId('attr'));
            },

            /**
             * Добавление событий блока
             * @Override FormBlockModel.bindEvents
             */
            bindEvents: function () {
                model.__super__.bindEvents.call(this);
                /*
                 * Слушаем событие выбора объектов для ссылочных атрибутов
                 * */
                this.listenTo(this, cmsEvents.OBJECTS_SELECTION_DONE, this.objectsSelectionDone);
                this.listenTo(this, cmsEvents.LINK_OBJECT_CREATED, this.linkObjectCreated);
            },

            /**
             * Остановка обработчиков событий блока
             * @Override FormBlockModel.unbindEvents
             */
            unbindEvents: function () {
                /* События блока */
                this.stopListening(this, cmsEvents.OBJECTS_SELECTION_DONE);
                this.stopListening(this, cmsEvents.LINK_OBJECT_CREATED);

                /* PageBlockModel.unbindEvents() удаляет все основные события блока */
                model.__super__.unbindEvents.call(this)
            },

            /** Unlock edited object on deactivate page block
             * @Override
             */
            onDeactivate: function () {
                this.unlockObject();

                /* Call PageBlockModel.onDeactivate method */
                model.__super__.onDeactivate.call(this)
            },

            /**
             * После создания линкованого объекта
             * */
            linkObjectCreated: function (params) {
                var _this = this;
                var refAttrId = misc.option(params, "refAttrId", "Атрибут для обновления");
                var objectId = misc.option(params, "objectId", "ID созданного объекта", []);
                var title = misc.option(params, "title", "ID созданного объекта", []);

                // ищем модель атрибута и проставляем ему новое значение
                var attrModel = this.get('attrList').get(refAttrId);
                attrModel.addLinkObjectId({objectId: objectId, title: title});

                /* Sync */
                this.syncLinkedAttribute(attrModel)
            },

            /**
             * После выбора ссылочных объектов
             * @param params
             */
            objectsSelectionDone: function (params) {
                var _this = this;
                var refAttrId = misc.option(params, "refAttrId", "Атрибут для обновления");
                var addIdList = misc.option(params, "addIdList", "Список ID для добавления", []);
                var removeIdList = misc.option(params, "removeIdList", "Список ID для удаления", []);
                // ищем модель атрибута и проставляем ему новое значение
                var attrModel = this.get('attrList').get(refAttrId);

                if (params.isAllSelected) {
                    var data = {
                        attrId: attrModel.get('id'),
                        className: attrModel.get('className')
                    };
                    if (params.search) {
                        data = _.extend(data, params.search)
                    }
                    attrModel.callServerAction({
                        action: "getAllRelationValues",
                        data: data
                    }).then(function (response) {
                        attrModel.set('value',{idList: _.map(response.objectList, function (obj) {
                            return obj.id
                        })});
                        _this.syncLinkedAttribute(attrModel)
                    }).fail(function (response) {
                        _this.get('pageModel').get('siteModel').trigger(cmsEvents.SITE_NOTIFY, {type:'warning', text: "При выборе объектов произошла ошибка"})
                    });
                } else {
                    var currentValue = attrModel.get('value');
                    var newValueIdList = [];
                    //TODO refactor this вынести логику в модель атрибутов
                    // Удаляем удаленные
                    if (currentValue && currentValue.objectList) {
                        currentValue.objectList.forEach(function (object) {
                            if (!_.contains(removeIdList, object.objectId)) {
                                newValueIdList.push(object.objectId)
                            }
                        })
                    }
                    // Добавляем новые id
                    newValueIdList = _.union(newValueIdList, addIdList);
                    attrModel.set('value', {idList: newValueIdList, title: undefined});
                    this.syncLinkedAttribute(attrModel)
                }
            },

            syncLinkedAttribute: function (model) {
                /* Fetch new data for objects */
                if (model.get('value') && !model.get('value').title) {
                    if (model.get('view').$titleHolderElement) {
                        model.get('view').$titleHolderElement.text('Идет получение заголовка...');
                    }
                    model.callServerAction({
                        action: "getRefAttrValues",
                        data: {
                            objectId: model.get('pageBlockModel').get('objectId'),
                            className: model.get('className'),
                            attrId: model.get('id'),
                            idList: JSON.stringify(model.get('value').idList)
                        },
                        lockElement: model.get('view').$table
                    }).then(function (response) {
                        if (response.result) {
                            if (response.result.id) {
                                model.set('value', {objectId: response.result.id, title: response.result.title}, {silent: true});
                            } else {
                                model.set('value', response.result, {silent: true});
                            }
                        }

                        model.set('selectedIds', []);
                        model.trigger('attribute:render')
                    });
                } else {
                    model.set('selectedIds', []);
                    model.trigger('attribute:render')
                }
            },

            /**
             * Отмена редактирования объекта
             */
            cancelEditObject: function () {
                log.debug("START OPEN OBJECT");
                if (this.hasRefAttr()) {
                    this.get('pageModel').closePage();
                } else {
                    this.replacePage(
                        this.getSiteResource('viewObjectPage', 'pages'),
                        'Просмотр объекта ...',
                        {
                            objectId: this.get('objectId'),
                            className: this.get('className'),
                            refAttrId: this.get('refAttrId')
                        }
                    );
                }

                this.set('active', false);
            },

            hasRefAttr: function () {
                var hashStr = JSON.parse(this.get('pageModel').get('stringHash').substring(1));
                if (hashStr.refAttrId) {
                    return true;
                }
                return false;
            },

            /**
             * Удаление объекта
             * @returns {*}
             */
            deleteObject: function () {
                var _this = this;
                var objectId = this.get('objectId');
                var className = this.get('entityName');
                var options = {
                    action: 'deleteObject',
                    data: {
                        objectId: objectId,
                        className: className,
                        pageBlockId: this.get('blockInfo').get('id'),
                        ml_request: true
                    },
                    lockElement: $('#CENTER')

                };

                log.debug('Call server to delete Object with id = ' + objectId + '@' + className);
                return this.callServerAction(options).then(function () {
                    _this.trigger(cmsEvents.MODEL_DELETE_COMPLETE)
                    log.debug('Receive response from server after delete Object with id = ' + objectId + '@' + className);
                    log.debug('Trigger event to navigate previous BreadCrumb');
                    _this.notifyOpenerObjectChange();
                    _this.closePage();
                });
            },


            /**
             * Сохранение объекта
             * @returns {boolean}
             */
            saveObject: function () {
                var _this = this;
                _this._saveObject(function () {
                    log.debug("END EDIT SAVE");
                    _this.trigger(cmsEvents.MODEL_SAVE_COMPLETE)
                    _this.notifyOpenerObjectChange();
                })
            },

            notifyOpenerObjectChange: function () {
                var _this = this;
                var objectId = this.get('objectId');
                var className = this.get('entityName');
                _this.notifyPageBlocks(new NotifyEventObject(
                    'changeObject',
                    {"className": className, "objectId": objectId},
                    'all'
                ));
            },

            /**
             * Сохранение объекта и последующее закрытие редактирования
             * @returns {boolean}
             */
            saveAndCloseObject: function () {
                var _this = this;
                _this._saveObject(function () {
                    log.debug("END EDIT SAVE");
                    _this.trigger(cmsEvents.MODEL_SAVE_COMPLETE)
                    _this.notifyOpenerObjectChange()
                    _this.cancelEditObject()
                })
            },


            _saveObject: function (callback) {
                var _this = this;
                // Пробегаем по всем атрибутам и собираем значения
                var notFilledMandatoryAttrs = [];
                var fileAttrList = [];
                var dataForSave = {};
                this.get('attrList').forEach(function (attr) {
                    //Для каждого атрибута проверяется обязательность
                    if (attr.isMandatory() && !attr.hasValue()) {
                        notFilledMandatoryAttrs.push(attr);
                        attr.triggerNotFilledMandatory();
                    } else {
                        attr.triggerRemoveHighlightMandatory();
                    }

                    if (attr.get('fieldType') == 'FILE' && attr.hasValue()) {
                        fileAttrList.push(attr);
                    }
                    // Устанавливаем значение атрибута
                    dataForSave[attr.getAttrEntityFiledName()] = attr.serializeValue();
                });

                if (notFilledMandatoryAttrs.length > 0) {
                    log.debug("Not all mandatory attrs filled");
                    return false;
                    /*
                     // Не сохраняем оповещаем модели
                     notFilledMandatoryAttrs.forEach(function (attr) {
                     attr.triggerNotFilledMandatory();
                     });
                     */
                }
                // Отправляем запрос на сохранение
                var options = {
                    action: 'saveObject',
                    data: {
                        objectId: _this.get('objectId'),
                        className: _this.get('entityName'),
                        pageBlockId: this.get('blockInfo').get('id'),
                        data: JSON.stringify(dataForSave),
                        ml_request: true
                    },
                    lockElement: $('#CENTER'),
                    onFail: function (json, code) {
                        /* If error response object contains lock field set current object to locked*/
                        if (json.lock) {
                            /*todo: add status code 400 in controller @see ObjectEditBlockController.java */
                            _this.set('objectLocked', json.lock);
                            _this.trigger('object:locked');
                        } else {
                            /* Show error in modal window */
                            _this.displayErrorMessage("Ошибка приложения", json.message);
                        }

                    }
                };
                this.uploadFiles(fileAttrList, function () {
                    log.debug('uploadFiles done.');
                    _this.callServerAction(options).then(function () {
                        if (callback) {
                            callback();
                        }
                    })
                });
            },

            /**
             * Re-lock object to current user
             * @returns {jQuery.Deferred}
             */
            relockObject: function () {
                var _this = this;
                var options = {
                    action: 'relockObject',
                    data: {
                        objectId: this.get('objectId'),
                        className: this.get('entityName')
                    }
                };
                var def = new $.Deferred();
                this.callServerAction(options).done(function (response) {
                    if (response.result == 'ok') {
                        _this.unset('lock');
                        def.resolve(response);
                    } else {
                        def.reject(response)
                    }
                }).fail(function () {
                    def.reject()
                });

                return def
            },

            /**
             * Unlock current object
             * @returns {jQuery.Deferred}
             */
            unlockObject: function () {
                var _this = this;
                var options = {
                    action: 'unlockObject',
                    data: {
                        objectId: this.get('objectId'),
                        className: this.get('entityName')
                    }
                };
                var def = new $.Deferred();
                this.callServerAction(options).done(function (response) {
                    if (response.result == 'ok') {
                        _this.unset('lock');
                        def.resolve(response);
                    } else {
                        def.reject(response)
                    }
                }).fail(function () {
                    def.reject()
                });

                return def
            }
        });
        return model;
    });
