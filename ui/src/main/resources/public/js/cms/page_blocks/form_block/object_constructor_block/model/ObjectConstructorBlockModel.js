/**
 * Модель блока редактирования объекта
 */
define(
    ['log', 'misc', 'backbone',
        'cms/page_blocks/form_block/FormBlockModel',
        'cms/events/NotifyPageBlocksEventObject',
        'cms/page_blocks/form_block/model/AttrModel',
        'cms/page_blocks/form_block/model/AttrGroupModel',
        'cms/page_blocks/form_block/collections/AttrCollection',
        'cms/page_blocks/form_block/collections/AttrGroupCollection',
        'test_construct/meta_data',
        'test_construct/test_data'
    ],
    function (log, misc, backbone, FormBlockModel, NotifyEventObject, AttrModel, AttrGroupModel, AttrCollection, AttrGroupCollection
        ,resultMetaData, resultObjectData) {
        var model = FormBlockModel.extend({
            defaults: {
                objectId: undefined,
                entityName: undefined,
                description: undefined,
                title: undefined,
                attrList: undefined,
                nonGroupAttrList: undefined,
                groupList: undefined,
                rootGroupList: undefined,
                pageTitle: '',
                guid: undefined
            },


            /**
             * Инициализация модели
             */
            initialize: function () {
                var _this = this;
                log.debug('initialize ObjectConstructorBlockModel');
                model.__super__.initialize.call(this);
                this.pageTitle = 'Конструктор класса <%=data.description%>';
                this.set('guid', _.uniqueId('attr'));
            },

            /**
             * Инициализация обработчиков событий
             */
            initializeListeners: function () {
                var _this = this;

                this.listenTo(this, 'restorePage', function (params) {
                    _this._fillModel(resultMetaData.MetaData);
                    _this._fillModelValues(resultObjectData.ObjectData);
                    _this.set('canCreate', false);
                    _this.set('canDelete', false);
                    _this.set('canEdit', false);
                    //TODO правильно вставить сюда название класса
                    _.template(_this.pageTitle, {data: resultMetaData.MetaData});
                   // _this.updatePageTitle(title);
                    _this.trigger('render');

                });
               // model.__super__.initializeListeners.call(this);
                /*
                 * Слушаем событие выбора объектов для ссылочных атрибутов
                 * */
                //this.listenTo(this, 'objectsSelectionDone', this.objectsSelectionDone);
                //this.listenTo(this, 'linkObjectCreated', this.linkObjectCreated);
            },

            /**
             * Отмена редактирования объекта
             */
            cancelEditObject: function () {
                log.debug("START OPEN OBJECT")

            },

            /**
             * Сохранение объекта
             * @returns {boolean}
             */
            saveObject: function () {
                var _this = this;
                    log.debug("END EDIT SAVE");
            }

        });
        return model;
    });
