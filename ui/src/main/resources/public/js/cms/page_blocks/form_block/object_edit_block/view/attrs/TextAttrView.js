/**
 * Представление для атрибута типа TEXT
 */
define(
    ['log', 'misc', 'backbone', 'inputmask', 'ckeditor-jquery', 'trumbowyg',
        'cms/page_blocks/form_block/object_edit_block/view/EditAttrView',
        'text!cms/page_blocks/form_block/object_edit_block/templates/attrs/TextAttrView.tpl'],
    function (log, misc, backbone, inputmask, ckeditor, trumbowyg,
              EditAttrView, DefaultTemplate) {
        var view = EditAttrView.extend({
            $inputField: undefined,
            $editor: undefined,

            /**
             * Инициализация представления
             */
            initialize: function () {
                view.__super__.initialize.call(this);
                this.viewTemplate = DefaultTemplate;
                _.extend(this.events, EditAttrView.prototype.events);
            },


            /**
             * Отрисовка представления
             */
            render: function () {
                var _this = this;
                if (this.isHidden()) {
                    return;
                }
                this.$el.html(_.template(this.viewTemplate, {attrModel: this.model}));
                this.$inputField = this.$el.find('textarea');
                this.$inputField.trumbowyg({
                    btns: ['btnGrp-design', '|', 'btnGrp-lists', '|', 'link']
                });

                this.$editor = this.$inputField
                this.$editor.on('tbwchange', function () {
                    _this.keyPressed();
                });
                this.$editor.trumbowyg('html', this.model.get('value'));
                this.addMandatoryEvents();
                this.addReadOnly();
                this.addPopoverHelper()
            },

            /**
             * Обработчик изменения значения HTML-элемента
             */
            keyPressed: function (val) {
                this.model.set('value', this.$editor.trumbowyg('html'));
            }

        });

        return view;
    });
