define(
    ['log', 'misc', 'backbone', 'cms/events/events', 'cms/view/PageBlockView', 'jstree'],
    function (log, misc, backbone, cmsEvents, PageBlockView, jstree) {
        var view = PageBlockView.extend({

            initialize: function () {

                console.log("initialize NavigationBlockView");
                var _this = this;
                this.$treeContainer = $('<div class="ml-list">Загрузка...</div>');

                this.listenTo(this.model, cmsEvents.RENDER_VIEW, this.render);
                // При восстановление состояния страницы (при первичной загрузке или при навигацие)
                this.listenTo(this.model, cmsEvents.RESTORE_PAGE, function (options) {
                    if (options.folderId) {
                        // Когда дерево будет загружено, выделить узел согласно текущей папке
                        //todo: узел может быть в подпапке, для успешного выделения необходимо
                        //todo: предварительно загрузить дерево родителей
                        _this.$treeContainer.on("loaded.jstree", function (e, nodeData) {
                            _this.$treeContainer
                                .jstree("deselect_all")
                                .jstree("select_node", "#" + options.folderId, !options.event);
                        });

                        _this.$treeContainer.on("rename_node.jstree", function (e, data) {
                            _this.model.renameFolder(data.node.id, data.text);
                        });

                        _this.$treeContainer.on("delete_node.jstree", function (e, data) {
                            _this.model.deleteFolder(data.node.id);
                        });

                        _this.$treeContainer.on("create_node.jstree", function (e, data) {
                            var node = _this.$treeContainer.jstree("get_node", data.parent);
                            var folderId = node.id;
                            _this.model.createFolder(folderId, data.node.text, data.node.id);
                        });
                    }
                });

                this.listenTo(this.model, cmsEvents.FOLDER_CREATED, this.folderCreated);
                this.listenTo(this.model, cmsEvents.FOLDER_DELETED, this.folderDeleted);

                this.model.on(cmsEvents.ACTIVATE_FOLDER, function (options) {
                    if (options.folderId) {
                        _this.setActiveFolder(options);
                    } else {
                        _this.$treeContainer.jstree("deselect_all");
                    }
                });

                this.model.on(cmsEvents.OPEN_SUB_FOLDER, function (options) {
                    if (options.folderId) {
                        _this.setActiveFolder(options);
                        _this.model.openFolder(options);
                    } else {
                        _this.$treeContainer.jstree("deselect_all");
                        _this.model.openFolder(options);
                    }
                });
            },

            setActiveFolder: function (options) {
                var _this = this;
                var callback = function () {
                    _this.$treeContainer.jstree("deselect_all").jstree('select_node', options.folderId, true);
                    /* Оповещение о открытии папки псле перехода по истории */
                    if(options.history) {
                        var node = _this.$treeContainer.jstree("get_node", options.folderId)
                        _this.model.get('pageModel').showPageBlocksHistory(node.original)
                    }
                };
                var currentFolderId = options.history ? options.folderId : _this.model.get('folderId');
                //раскрываем текущую папку и в коллбэкие помечаем нужную подпапку выделенной
                this.$treeContainer.jstree('open_node', currentFolderId, callback);
            },

            render: function () {
                var _this = this;
                log.debug("Initialize jstree and append it to container");
                this.$treeContainer.appendTo(this.$el);
                this.$treeContainer.jstree({
                    'animation': 1,
                    'plugins': ["json_data", "tree_customizer"],
                    'rules': {'multiple': false},
                    'lang': {new_node: "Новый каталог", loading: "Загрузка ..."},
                    'core': {
                        'animation': 200,
                        'check_callback': true,
                        'themes': {
                            'dots': false,
                            'icons': false
                        },

                        // Настраиваем получение данных с сервера. Настройки аналогичны настройкам функции $.ajax. Все запросы
                        // для обновления данных (динамическая подгрузка, обновление по запросу и т. д) используют эти настройки.
                        // В нашем случае все запросы проходят через экшен "folderChildren"
                        // Информация по плагину: http://www.jstree.com/docs/json/
                        'data': {
                            'type': 'get',
                            // URL может быть либо строкой либо функцией возвращающей строку
                            'beforeSend': function(xhr){xhr.setRequestHeader('XX-ML-REQUEST', 'true');},
                            'url': function (node) {
                                var nodeData = node.id != "#" ? JSON.parse(node.id) : node.id;
                                var folderId;
                                var classifierId;
                                var value;
                                if (nodeData.folderId) {
                                    folderId = nodeData.folderId;
                                } else {
                                    folderId = node.id;
                                }

                                if (folderId == '#') {
                                    folderId = 'root';
                                }
                                return misc.getContextPath()+'/site/navigation/' + folderId  + '/children';
                            },
                            // Формируем данные для отправки на сервер
/*                            'data': function (node) {
                                var nodeData = node.id != "#" ? JSON.parse(node.id) : node.id;
                                var folderId;
                                var classifierId;
                                var value;
                                if (nodeData.folderId) {
                                    folderId = nodeData.folderId;
                                    classifierId = nodeData.classifierId;
                                    value = nodeData.value;
                                } else {
                                    folderId = node.id;
                                }

                                return {
                                    'ml_request': true,
                                    'pageBlockId': _this.model.get('blockInfo').get('id'), // Идентификатор блока
                                    'id': folderId, // Идентификатор ноды (для корневого узла будет '#')
                                    'classifierId': classifierId,
                                    'value': value,
                                    'action': 'getFolderChildren' // Действие для сервера
                                };
                            },*/
                            'success': function(data) {
                                /* Если сессия закончилась то редирект на страницу из ответа */
                                if(data.needAuth) {
                                    window.location = data.url
                                }
                            }
                        }
                    }

                });


                // Выбор ноды
                this.$treeContainer.on("select_node.jstree", function (e, nodeData) {
                    _this.model.openFolder({folderId: nodeData.node.id, pageBlocks: nodeData.node.original.pageBlocks});
                });

                // Скрываем другие листы при открытии листа
                this.$treeContainer.on("before_open.jstree", function (e, nodeData) {
                    if (nodeData.node.parent === '#') {
                        var parentNode = _this.$treeContainer.jstree('get_node', nodeData.node.parent);
                        parentNode.children.forEach(function (subNodeId) {
                                if (subNodeId != nodeData.node.id) {
                                    var tmpNode = _this.$treeContainer.jstree('get_node', subNodeId);
                                    if (tmpNode.state.opened) {
                                        _this.$treeContainer.jstree('close_node', subNodeId)
                                    }
                                }
                            }
                        );
                    }
                });


                if (this.model.get("folderId") && this.model.get("folderId")!="#") {
                    // при загрузке раскрываем дерево на нужную вкладку
                    _this.$treeContainer.on('loaded.jstree', function (e, data) {
                        var deffereds = $.Deferred(function (def) {
                            def.resolve();
                        });
                        var folderId = _this.model.get("folderId");
                        var def = _this.model.findFolderPath(folderId);
                        $.when(def).then(function (nodeValues) {
                            for (var j = 0; j < nodeValues.length - 1; j++) {
                                deffereds = (function (name, deferreds) {
                                    return deferreds.pipe(function () {
                                        return $.Deferred(function (def) {
                                            _this.$treeContainer.jstree("open_node", $("#" + name + "_anchor"), function () {
                                                def.resolve();
                                            });
                                        });
                                    });
                                })(nodeValues [j], deffereds);
                            }
                            $.when(deffereds).then(function () {
                                _this.$treeContainer.jstree("select_node", $("#" + folderId + "_anchor"));
                            });
                        });
                    });
                }
                this.model.set('blockRendered', true)
                log.debug("Finish rendering NavigationBlock");
            },

            refreshTree: function () {
                this.$treeContainer.jstree("refresh");
            },

            folderCreated: function (options) {
                var _this = this;

                if (options.nodeId && options.folderId) {
                    var node = _this.$treeContainer.jstree("get_node", options.nodeId);
                    node.id = options.folderId;
                    _this.$treeContainer.jstree("refresh");
                    //TODO добавить переход в режим редактирования
                    //_this.$treeContainer.jstree("edit", node.id);
                }
            },

            folderDeleted: function (options) {
                if (options.parentId) {
                    this.$treeContainer.jstree("deselect_all").jstree('select_node', options.parentId);
                    this.model.openFolder({folderId: options.parentId});
                }
            }

        });

        return view;
    });
