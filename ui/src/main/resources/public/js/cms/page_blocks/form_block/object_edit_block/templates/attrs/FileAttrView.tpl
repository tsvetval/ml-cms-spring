<div class="attribute-edit-file">
    <div class="attr-label-container col-xs-offset-<%=attrModel.get('offset')%> col-xs-<%=attrModel.get('titleLength')%>">
        <b class="attr-label"><%=attrModel.escape('description')%>:</b>
    </div>

    <div class="col-xs-<%=(attrModel.get('totalLength') - attrModel.get('titleLength') - attrModel.get('offset'))%> input-group field-file">
        <input type="text" class="form-control attrField_fake" readonly="true"/>

        <%if (attrModel.get('value') && !attrModel.get('value').deleted){%>
        <span class="btn btn-success input-group-addon file-download-link"
              title="Скачать файл"
              objectId="<%=attrModel.get('value').objectId%>">
            <i class="glyphicon glyphicon-download"></i>
        </span>
        <%}%>
        <span class="btn btn-primary input-group-addon buttonDelete btn-danger" style="overflow: hidden;position: relative;">
            <i class="glyphicon glyphicon-trash"></i>
        </span>

        <span class="btn btn-primary btn-file input-group-addon" style="overflow: hidden;position: relative;">
            <div class="col-xs-3">
                <label class="glyphicon glyphicon-file highlight-button"></label>
            </div>
            <div class="col-xs-9">
                <input  type="file" class="attrField"/>
            </div>
        </span>
    </div>
</div>
