<div class="row">
    <div class="panel panel-default col-xs-8">
        <div class="panel-heading">
            <h4>Ресурсы
                <div class="pull-right">
                    <span class="label label-success">Контроллеры</span>
                    <span class="label label-warning">Страницы</span>
                    <span class="label label-info">Группы</span>
                </div>
            </h4>
        </div>

        <div class="panel-body">
            <div class="resource-tree">
                <ul class="">

                </ul>
            </div>
        </div>
    </div>
    <div class="resource-details col-xs-16">
        <div class="details controller-details panel panel-default ">
            <div class="panel-heading"><h4>Информация о контроллере</h4></div>
            <div class="panel-body">
                <div class="row" id="controller-id">
                    <div class="col-xs-4">
                        <label>id</label>
                    </div>
                    <div class="col-xs-20 details-value" id="controller-id-value">

                    </div>
                </div>
                <div class="row" id="controller-description">
                    <div class="col-xs-4">
                        <label>Описание</label>
                    </div>
                    <div class="col-xs-20 details-value" id="controller-description-value">

                    </div>
                </div>
                <div class="row" id="controller-javaClass">
                    <div class="col-xs-4">
                        <label>Java Class</label>
                    </div>
                    <div class="col-xs-20 details-value" id="controller-javaClass-value">

                    </div>
                </div>
                <div class="row" id="controller-url">
                    <div class="col-xs-4">
                        <label>URL</label>
                    </div>
                    <div class="col-xs-20 details-value" id="controller-url-value">

                    </div>
                </div>
            </div>
        </div>

        <div class="details page-details panel panel-default ">
            <div class="panel-heading"><h4>Информация о странице</h4></div>
            <div class="panel-body">
                <div class="row" id="page-id">
                    <div class="col-xs-4">
                        <label>id</label>
                    </div>
                    <div class="col-xs-20 details-value" id="page-id-value">

                    </div>
                </div>
                <div class="row" id="page-title">
                    <div class="col-xs-4">
                        <label>Заголовок</label>
                    </div>
                    <div class="col-xs-20 details-value" id="page-title-value">

                    </div>
                </div>
                <div class="row" id="page-description">
                    <div class="col-xs-4">
                        <label>Описание</label>
                    </div>
                    <div class="col-xs-20 details-value" id="page-description-value">

                    </div>
                </div>
                <div class="row" id="page-javaClass">
                    <div class="col-xs-4">
                        <label>Java Class</label>
                    </div>
                    <div class="col-xs-20 details-value" id="page-javaClass-value">

                    </div>
                </div>
                <div class="row" id="page-template">
                    <div class="col-xs-4">
                        <label>Шаблон</label>
                    </div>
                    <div class="col-xs-20 details-value" id="page-template-value">

                    </div>
                </div>
                <div class="row" id="page-projectTemplate">
                    <div class="col-xs-4">
                        <label>Проектный шаблон</label>
                    </div>
                    <div class="col-xs-20 details-value" id="page-projectTemplate-value">

                    </div>
                </div>
                <div class="row" id="page-url">
                    <div class="col-xs-4">
                        <label>URL</label>
                    </div>
                    <div class="col-xs-20 details-value" id="page-url-value">

                    </div>
                </div>
                <div class="row page-blocks">
                    <div class="panel panel-info">
                        <div class="panel-heading"><h4>Страничные блоки</h4></div>
                        <div class="panel-body">
                            <ul class="details-value list-group" id="page-blocks-value">

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<template id="page-block-template">
    <li class="list-group-item">
        <div class="row">
            <div class="col-xs-4">
                <label>id</label>
            </div>
            <div class="col-xs-20 page-block-id-value">

            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                <label>bootJs</label>
            </div>
            <div class="col-xs-20 page-block-bootJs-value">

            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                <label>Контекст</label>
            </div>
            <div class="col-xs-20 page-block-context-value">

            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                <label>Описание</label>
            </div>
            <div class="col-xs-20 page-block-description-value">

            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                <label>Порядок отображения</label>
            </div>
            <div class="col-xs-20 page-block-order-value">

            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                <label>Зона на странице</label>&nbsp;
            </div>
            <div class="col-xs-19 page-block-zone-value">

            </div>
            <div class="col-xs-1">
                <div class="btn-group btn-group-justified">
                    <!--<a class="preview-on-page btn btn-sm btn-info" title="Положение блока на текущей странице.">
                        <i class="glyphicon glyphicon-flash"></i>
                    </a>-->
                    <a class="preview-in-layout btn btn-sm btn-info" data-toggle="button" title="Положение блока на странице шаблона.">
                        <i class="glyphicon glyphicon-eye-open"></i>
                    </a>
                </div>
            </div>
            <div class="col-xs-24 layout-preview"></div>
        </div>
    </li>
</template>


