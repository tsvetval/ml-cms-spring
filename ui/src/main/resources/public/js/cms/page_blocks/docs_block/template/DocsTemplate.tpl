<section id="documentation-block" role="template">
    <div class="row">
        <div class="col-xs-6">
            <ul class="nav nav-pills nav-stacked">
                <% _.each(model.get("classList"), function(cls) { %>
                <li role="presentation" data-class="<%= cls.name %>"><a href="#" class="cls"><%= cls.label %></a></li>
                <% }); %>
            </ul>
        </div>
        <div id="infoContainer" class="col-xs-18">

        </div>
    </div>
</section>


