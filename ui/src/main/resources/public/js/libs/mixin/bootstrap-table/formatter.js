define(
    ['log', 'misc', 'backbone', 'underscore', 'moment', 'jquery',
        'text!libs/mixin/bootstrap-table/templates/OneToManyTemplate.tpl'
    ],
    function (log, misc, backbone, _, moment, $,
              OneToManyTemplate) {

        var Formatter = {

            formatterString: function (value, row, index) {
                return _.escape(value)
            },

            formatterOneToMany: function (value, row, index) {
                var result = [];
                if (value && value.objectList && value.objectList.length > 0) {
                    value.objectList.forEach(function (obj) {
                        result.push(obj.title)
                    })
                }

                if (result.length <= 5) {
                    return result.join('</br>');
                } else {
                    return _.template(OneToManyTemplate, {result: result});
                }
            },

            formatterHeteroLink: function (value, row, index) {
                var result = [];
                if (value && value.objectList && value.objectList.length > 0) {
                    value.objectList.forEach(function (obj) {
                        result.push(obj.title)
                    })
                }

                if (result.length <= 5) {
                    return result.join('</br>');
                } else {
                    return _.template(OneToManyTemplate, {result: result});
                }
            },

            formatterManyToOne: function (value, row, index) {
                var result = '';
                if (value && value.title) {
                    result = value.title;
                }
                return result;
            },

            formatterBoolean: function (value, row, index) {
                var $input = $('<input>', {
                    type: 'checkbox',
                    checked: !!value,
                    disabled: true
                })
                return $input.prop('outerHTML');
            },

            formatterEnum: function (value, row, index) {
                var result = '';
                if (value && value.title) {
                    result = value.title;
                }
                return result;
            },

            formatterSelect: function (value, row, index) {
                var result = '<div class="icon">' +
                    '<span class="glyphicon glyphicon-chevron-right open-object-button clickable"' +
                    'objectId="' + value + '">' +
                    '</span>'+
                    '</div>';
                return result;
            },

            formatterDateWithOptions: function (formatOptions){
                var _formatOptions = $.extend({dateFormat : "DD.MM.YYYY"}, formatOptions);
                return function(value, row, index) {
                    if (!value || value.length == 0) {
                        return "";
                    }
                    //var dateMoment = moment(value)/*.utc()*/;

                    return value;//dateMoment.format(_formatOptions.dateFormat);
                };
            },

            formatterDate: function (value, row, index) {
                if (!value || value.length == 0) {
                    return "";
                }
                var dateMoment = moment(value)/*.utc()*/;

                return dateMoment.format("DD.MM.YYYY");
            },

            formatterFile: function (value, row, index) {
                if (!value || value.length == 0) {
                    return "";
                }

                var link = $('<a>', {
                    text: value.title,
                    "title": value.title,
                    objectId: value.objectId,
                    attrName: value.entityFieldName,
                    href: '#',
                    "class": 'file-download-link'
                });
                return link[0].outerHTML;
            },

            /**
             * @param attr значение атрибута из list block
             * @param field
             * @param value
             * @param row
             * @param $element
             * @param onSave - callback function with args editedValue, previousValue
             */
            formatterEdit: function (attr, field, value, row, $element, onSave) {
                $element.data('edited', true)
                var previousValue = $element.text()
                if (previousValue === '-') previousValue = ""

                var id = 'table-edit-' + field + '-' + row.objectId

                var $input = $('<input>', {
                    class: 'form-control input-sm',
                    id: id + '-input'
                })

                var $group = $('<div>', {
                    class: 'input-group table-inline-edit',
                    id: id
                }).append($input).append($('<span>', {
                    class: 'input-group-btn '
                }).append($('<button>', {
                    class: 'btn btn-sm btn-success'
                }).append($('<i>', {
                    class: 'glyphicon glyphicon-floppy-disk'
                }))))

                /* Add datetimepicker for field with DATE fieldType */
                if(attr.fieldType == 'DATE') {
                    $input.datetimepicker({
                        format: 'DD.MM.YYYY',
                        useCurrent: false,
                        locale: 'ru',
                        language: 'ru'
                    })
                }


                $element.html($group)

                /* Focus on new inline edit element */
                $group.find('input').focus().val(previousValue)

                /* Save event */
                $group.find('.btn').off('click').on('click', function (e) {
                    var editedValue = $group.find('input').val()
                    var groupIndex = $('.table-inline-edit').index($group)

                    if(attr.fieldType == 'DATE') {
                        $input.data("DateTimePicker").destroy()
                    }

                    e.stopPropagation()

                    /* Callback after complete edit in table */
                    if(typeof onSave === 'function') onSave(editedValue, previousValue)

                    /* Focus on next edited cell */
                    if($('.table-inline-edit').find('input').eq(groupIndex).length <= 0) groupIndex = 0
                    $('.table-inline-edit').find('input').eq(groupIndex).focus()
                })

                /* On press enter or escape button */
                $group.find('input').off('keyup').on('keyup', function (e) {
                    if (e.keyCode == 13) {
                        /* On press enter trigger click on save-button*/
                        $group.find('.btn').trigger('click')

                    } else if (e.keyCode == 27) {
                        /* On press escape callback with old values */
                        if(typeof onSave === 'function') onSave(previousValue, previousValue)

                        /* Focus on next edited cell */
                        var groupIndex = $(this).parent().index($group)
                        if($('.table-inline-edit').find('input').eq(groupIndex).length <= 0) groupIndex = 0
                        $('.table-inline-edit').find('input').eq(groupIndex).focus()
                    }

                    return false;
                })
            }
        };

        return Formatter;

    });