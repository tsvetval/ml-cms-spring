define([], function () {
    var ViewHelpers = {
        /**
         * Изменяет max-width заголовка в зависимости от наличия кнопок и блоков в UTILS и SEARCH зонах
         */
        resizeTitle: function () {
            setTimeout(function () {
                var allElementsWidth = 0
                /* Сбрасываем предыдущее значение */
                $('.ml-main__content-title:visible').removeAttr('width')

                /* Считаем ширину элементов */
                $('#UTILS, #SEARCH, .upper-folder, .btnDelete, .btnClose, .btnSave, .btnSaveAndClose, .btnCreate, .btnCreateDropdown, .ml-main__content-edit').each(function () {
                    if ($(this).is(':visible')) {
                        allElementsWidth += $(this).outerWidth(true)
                        /* Js не может получать width для псевдоэлементов, придется добавлять магию для иконок */
                        if ($(this).hasClass('glyphicon')) {
                            allElementsWidth += $(this).outerWidth(true)
                        }
                    }
                })
                /* -10 it's dark voodoo magic */
                var maxTitleWidth = $('.ml-main__content-title:visible').parent().outerWidth() - allElementsWidth
                    - ViewHelpers.getScrollbarWidth() - 10;

                $('.ml-main__content-title:visible')
                    .css({
                        'max-width': maxTitleWidth,
                        'width': maxTitleWidth
                    })
                $('.ml-main__content-title-text:visible')
                    .css({
                        'max-width': $('.ml-main__content-title:visible').outerWidth(true)
                                        - $('.ml-main__content-title-items:visible').outerWidth(true)
                                        - ViewHelpers.getScrollbarWidth() - 10
                    })
            }, 0)
        },

        getScrollbarWidth: function () {
            var outer = document.createElement("div");
            outer.style.visibility = "hidden";
            outer.style.width = "100px";
            document.body.appendChild(outer);

            var widthNoScroll = outer.offsetWidth;
            // force scrollbars
            outer.style.overflow = "scroll";

            // add innerdiv
            var inner = document.createElement("div");
            inner.style.width = "100%";
            outer.appendChild(inner);

            var widthWithScroll = inner.offsetWidth;

            // remove divs
            outer.parentNode.removeChild(outer);

            return widthNoScroll - widthWithScroll;
        },


        notify: {
            /**
             * Создает сообщение нотификации и добавляет на страницу
             * @param options Хэш параметров:
             * type: bootstrap helper class: success, danger, warning etc.
             * text: Текст сообщение
             * stay: Сообщение должно остаться. По умолчанию false
             * el: класс основного элемента куда буду добавляться сообщения
             */
            create: function (options) {
                var defaultOptions = {
                    type: 'success',
                    stay: false,
                    duration: 1000,
                    el: '.notification-messages'
                }
                _.defaults(options, defaultOptions)

                var site = this;
                var $notifyBlock = $(options.el)
                var $message = $('<div>', {
                    'class': 'alert alert-' + options.type
                }).text(options.text)
                $notifyBlock.append($message)
                $message.fadeIn(500)
                if (!options.stay) {
                    setTimeout(function () {
                        site.helpers.notify.delete($message)
                    }, options.duration)
                } else {
                    var $close = $('<button></button>', {
                        'class': 'close',
                        'type': 'button',
                        'data-dismiss': 'alert'
                    }).append('<span>&times;</span>')
                    $message.addClass('alert-dismissible').append($close)
                }
            },

            /**
             * Удаляет сообщение нотификации
             * @param message
             */
            delete: function (message) {
                message.fadeOut(500, function () {
                    $(this).remove()
                })
            }
        },

        /**
         * Добаляет горизонтальный скролл под шапкой таблиц.
         */
        tableExtendedScrollInit: function () {
            this.$el.find('.scrollbar-external').scrollbar({
                "autoScrollSize": true,
                "scrollx": this.$el.find('.external-scroll_x'),
                disableBodyScroll: true
            });
            this.$el.find('.external-scroll_x').css('top', this.$el.find('thead').first().height() - 10);
        },

        /**
         * Применяется для фильтрации атрибутов 1-N, M-N, N-1
         * @param query
         * @param key
         * @returns {boolean} | array
         */
        dataFilter: function (query, key) {
            var data = this.model.get('value').objectList;
            if (data && query) {
                if (typeof query == 'string') {
                    return _.filter(data, function (item) {
                        if (key == 'title') {
                            if (item.title && item.title.toLowerCase().indexOf(query.toLowerCase()) != -1) return true
                        } else {
                            if (item.attrValues && _.has(item.attrValues, key)) {
                                if (typeof item.attrValues[key] == 'string') {
                                    if (item.attrValues[key].toLowerCase().indexOf(query.toLowerCase()) != -1) return true
                                }
                            }
                        }
                    })
                } else if (typeof query == 'object') {
                    /* Filter with object as filter parameters */
                    return _.filter(data, function (item) {
                        for (var key in query) {
                            if (item.attrValues && _.has(item.attrValues, key)) {
                                if (typeof item.attrValues[key] == 'string') {
                                    /* Search in string value */
                                    if (item.attrValues[key].toLowerCase().indexOf(query[key].toLowerCase()) == -1) return false
                                } else if (typeof item.attrValues[key] == 'boolean') {
                                    /* Compare boolean values */
                                    if (item.attrValues[key] != JSON.parse(query[key])) return false
                                } else if (typeof item.attrValues[key] == 'object') {
                                    /* Search in all object values */
                                    var values = [];
                                    if (_.has(item.attrValues[key], 'objectList')) {
                                        _.each(item.attrValues[key].objectList, function (object) {
                                            values.push(object.title)
                                        })
                                    } else {
                                        values = _.values(item.attrValues[key]);
                                    }

                                    var searchResult = _.filter(values, function (value) {
                                        if (value.toLowerCase().indexOf(query[key].toLowerCase()) != -1) return true
                                    });


                                    if (_.isEmpty(searchResult)) return false
                                } else {
                                    return false;
                                }
                            } else {
                                return false;
                            }
                        }
                        return true;
                    })
                }

            } else {
                return false;
            }
        }
    };

    return ViewHelpers
});