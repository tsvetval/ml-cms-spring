define(['jquery'], function ($) {

    var methods = {
        lock: function () {
            var $modal = $('<div>', {
                class: 'requestholder-modal',
                style: 'position: absolute; top: 0; bottom: 0; left: 0; right: 0; background: rgba(0, 0, 0, 0.6); z-index: 100;'
            })
            $modal.append($('<div>', {
                class: 'progress col-xs-12 col-xs-offset-6',
                style: 'position: absolute; top: 50%; margin-top: -10px;'
            }).append(($('<div>', {
                class: 'progress-bar progress-bar-striped active',
                width: '100%'
            }))))

            this.css('position', 'relative')
            this.append($modal)

        },
        unlock: function () {
            var _this = this;
            this.find('.requestholder-modal').fadeOut(500, function () {
                $(this).remove();
                _this.css('position', '')
            })
        }
    }

    $.fn.requestloader = function (method) {

        return this.each(function () {
            if (typeof method == 'string' && typeof methods[method] == 'function') {
                methods[method].apply($(this))
            }
        })
    }

    /* todo: remove form plugin */
    $(function () {
        $.ajaxSetup({
            beforeSend: function () {
                if (this.lockElement && (typeof this.lockElement == 'object' || typeof this.lockElement == 'string')) {
                    if (typeof this.lockElement == 'string') {
                        this.lockElement = $(this.lockElement)
                    }

                    this.lockElement.requestloader('lock')
                    this.lockElement.on('unlock.requestloader', function () {
                        $(this).requestloader('unlock')
                    })
                }
            }
        })
    })
})