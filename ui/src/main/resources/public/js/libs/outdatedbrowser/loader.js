/* Запуск проверки совместимости браузера */
function addLoadEvent(func) {
    var oldonload = window.onload;
    if (typeof window.onload != 'function') {
        window.onload = func;
    } else {
        window.onload = function() {
            if (oldonload) {
                oldonload();
            }
            func();
        }
    }
}
//call function after DOM ready
addLoadEvent(function(){
    var el = document.getElementById('outdated')
    outdatedBrowser({
        bgColor: '#f25648',
        color: '#ffffff',
        lowerThan: 'transform',
        languagePath: el.getAttribute('context-path') + "/public/js/libs/outdatedbrowser/lang/ru.html"
    })
});