/**
 * Основной класс описывающий зависимости
 */

require.config({
    urlArgs: "bust=v1",
    waitSeconds : 100,
    baseUrl: document.getElementsByTagName("body")[0].attributes.getNamedItem("cms_context_path").value + '/public/js',
    paths: {
        text: 'libs/text',
        jquery: 'libs/jquery-2.1.3.min',
        jqueryUi: 'libs/jquery-ui-1.11.3.custom/jquery-ui',
        jstree: 'libs/jstree/dist/jstree',
        jstreeCustomizer: 'libs/jstree/dist/tree.customizer',
        bootstrap: 'libs/bootstrap.min',
        datetimepicker: 'libs/bootstrap-datetimepicker.min',
        moment: 'libs/moment-with-locales.min',
        spin: 'libs/spin.min',
        misc: 'common/misc',
        markup: 'common/markup',
        log: 'common/log',
        stacktrace: 'libs/stacktrace',
        select2: 'libs/select2/select2',
        backbone: 'libs/backbone',
        backbone_queryparams: 'libs/backbone.queryparams',
        inputmask: 'libs/jquery.inputmask.bundle',
        underscore: 'libs/underscore',
        breadcrumbs: 'libs/breadcrumb/breadcrumbs',
        channel: 'libs/channel',
        iconselect: 'libs/iconselect/iconselect',
        "ckeditor-core": 'libs/ckeditor/ckeditor',
        "ckeditor-jquery": 'libs/ckeditor/adapters/jquery',
        boot_table: 'libs/boot-table/bootstrap-table',
        scroll: 'libs/scroll/jquery.scrollbar',
        boot_table_ru: 'libs/boot-table/locale/bootstrap-table-ru-RU',
        table_formatter_mixin: 'libs/mixin/bootstrap-table/formatter',
        jquery_cookie: 'libs/jquery-cookie-master/src/jquery.cookie',
        jszip: 'libs/jszip.min',
        filesaver: 'libs/FileSaver.min',
        big: 'libs/big',
        helpers: 'libs/mixin/helpers/',
        trumbowyg: 'libs/trumbowyg/trumbowyg',
        jcrop: 'libs/jquery.Jcrop.min',
        ymap: 'https://api-maps.yandex.ru/2.1/?lang=ru_RU',
        autolinker: 'libs/autolinker/Autolinker',
        webcamjs: 'libs/webcamjs/webcam.min'
    },
    shim: {
        /* Set bootstrap dependencies (just jQuery) */
        'bootstrap': ['jquery'],
        'datetimepicker': ['jquery', 'bootstrap', 'moment'],
        'select2': ['jquery', 'bootstrap'],
        'jquery_cookie': ['jquery'],
        'jstreeCustomizer' : ['jquery', 'jstree'],
        'jqueryUi': ['jquery'],
        'ckeditor-jquery': {
            deps: ['jquery', 'ckeditor-core']
        },
        'boot_table': ['jquery', 'bootstrap'],
        'scroll' : ['jquery'],
        'boot_table_ru': ['jquery', 'bootstrap', 'boot_table'],
        'stacktrace': {
            exports: 'printStackTrace'
        },

        underscore: {
            exports: '_'
        },
        backbone: {
            deps: ["underscore", "jquery"],
            exports: "backbone"
        },
        backbone_queryparams: ['backbone'],
        'inputmask': ['jquery'],
        'breadcrumbs': ['backbone'],
        'trumbowyg' : ['jquery']
    }
});


require(["jquery", "bootstrap", "boot", "jquery_cookie", "jstreeCustomizer", "scroll"], function ($, bootstrap, boot, jquery_cookie) {

    $(window).on('load reload resize', function () {
        // Высота фона
        $('body').css('min-height', $(window).height());
    });

    /*
     Run boot function
     */
    boot();
});