package ru.ml.core.filter.strategy;

import ru.ml.core.filter.result.SimpleFilterResult;
import ru.ml.platform.core.model.MlAttr;
import ru.ml.platform.core.model.MlClass;
import ru.ml.platform.core.model.context.holders.MetaDataHolder;

/**
 * Стратегия для простого поиска
 */
public class SimpleFilterStrategy extends BaseFilterStrategy<SimpleFilterResult> {


    public SimpleFilterStrategy(MetaDataHolder metaDataHolder) {
        super(metaDataHolder);
    }

    @Override
    public SimpleFilterResult createResult() {
        String whereCause = whereCause(filterResult);
        filterResult.setRowQuery(createSelectStatement(whereCause));
        filterResult.setCountRecordQuery(createCountSelectStatement(filterResult,whereCause));
        filterResult.addParameter("simple", filterResult.getSimpleSearchValue());
        filterResult.setDeleteQuery(createDeleteStatement(whereCause));
        return filterResult;
    }

    private String createDeleteStatement(String whereCause) {
        StringBuilder builder = new StringBuilder("delete from " + filterResult.getQueryMlClass().getEntityName() + " o ");
        builder.append(whereCause);

        addAdditional(builder);

        return builder.toString();
    }

    @Override
    public void setFilterResult(SimpleFilterResult filterResult) {
        this.filterResult = filterResult;
    }

    private String createSelectStatement( String whereCause) {
        StringBuilder builder = new StringBuilder("select o from " + filterResult.getQueryMlClass().getEntityName() + " o ");
        builder.append(whereCause);

        addAdditional(builder);

        builder.append(addOrder());
        return builder.toString();
    }

    private void addAdditional(StringBuilder builder) {
        if(filterResult.getAdditionalConditions()!=null && !filterResult.getAdditionalConditions().isEmpty()){
            for(String condition: filterResult.getAdditionalConditions()){
                builder.append(" and ").append("(").append(condition).append(") ");
            }
        }
    }


    private String createCountSelectStatement(SimpleFilterResult filterService, String whereCause) {
        StringBuilder builder = new StringBuilder("select count(o) from " + filterService.getQueryMlClass().getEntityName() + " o ");
        builder.append(whereCause);

        addAdditional(builder);
        return builder.toString();
    }


    private String whereCause(SimpleFilterResult filterResult) {
        StringBuilder builder = new StringBuilder("where ( 1 > 1 ");
        if (filterResult.getSimpleSearchAttrName() != null && !filterResult.getSimpleSearchAttrName().isEmpty()) {
            MlAttr attr = filterResult.getQueryMlClass().getAttr(filterResult.getSimpleSearchAttrName());
            whereCauseFromAttr(builder, attr);
        } else {
            for (MlAttr attr : filterResult.getQueryMlClass().getAttrSet()) {
                if (attr.isUseInSimpleSearch()) {
                    whereCauseFromAttr(builder, attr);
                }
            }
        }
        builder.append(" ) ");
        return builder.toString();
    }

    private void whereCauseFromAttr(StringBuilder builder, MlAttr attr) {
        switch (attr.getFieldType()) {
            case TEXT:
            case STRING:
                builder.append(" or UPPER(o.").append(attr.getEntityFieldName()).append(") like UPPER(concat('%',:simple,'%'))");
                break;
            case DOUBLE:
            case LONG:
                builder.append(" or cast( o." + attr.getEntityFieldName() + " as text) like concat('%',:simple,'%')");
                break;
            case DATE:
                builder.append(" or FUNC('to_char',o." + attr.getEntityFieldName() + ",'dd.mm.yyyy hh24:mi:ss' ) like concat('%',:simple,'%')");
                break;
            case ENUM:
                builder.append(" or (select upper(e.title) from MlEnum e where e.mlAttr.id = " + attr.getId() + " and e.code = o." + attr.getEntityFieldName() + ") like UPPER(concat('%',:simple,'%'))");
                break;
            case LONG_LINK:
                MlAttr llAttr = null;
                MlClass mlClass = filterResult.getQueryMlClass();
                for(String path: attr.getLongLinkValue().split("->")){
                    llAttr = mlClass.getAttr(path);
                    if(llAttr.getLinkClass()!=null){
                        mlClass = llAttr.getLinkClass();
                    }
                }
                String attrName = attr.getLongLinkValue().replaceAll("->",".");
                switch (llAttr.getFieldType()) {
                    case TEXT:
                    case STRING:
                        builder.append(" or UPPER(o.").append(attrName).append(") like UPPER(concat('%',:simple,'%'))");
                        break;
                    case DOUBLE:
                    case LONG:
                        builder.append(" or cast( o." + attrName + " as text) like concat('%',:simple,'%')");
                        break;
                    case DATE:
                        builder.append(" or FUNC('to_char',o." + attrName + ",'dd.mm.yyyy hh24:mi:ss' ) like concat('%',:simple,'%')");
                        break;
                    case ENUM:
                        builder.append(" or (select upper(e.title) from MlEnum e where e.mlAttr.id = " + llAttr.getId() + " and e.code = o." + attrName + ") like UPPER(concat('%',:simple,'%'))");
                        break;
                }
                break;
        }
    }

}
