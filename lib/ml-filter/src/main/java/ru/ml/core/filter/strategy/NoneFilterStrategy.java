package ru.ml.core.filter.strategy;

import ru.ml.core.filter.result.FilterResult;
import ru.ml.platform.core.model.context.holders.MetaDataHolder;

/**
 *
 */
public class NoneFilterStrategy extends BaseFilterStrategy<FilterResult> {

    public NoneFilterStrategy(MetaDataHolder metaDataHolder) {
        super(metaDataHolder);
    }

    @Override
    public FilterResult createResult() {
        filterResult.setRowQuery(createSelectStatement());
        filterResult.setDeleteQuery(createDeleteStatement());
        filterResult.setCountRecordQuery(createCountSelectStatement(filterResult));
        return filterResult;
    }

    private String createDeleteStatement() {
        StringBuilder builder = new StringBuilder("delete from "+filterResult.getQueryMlClass().getEntityName()+" o ");
        if(filterResult.getAdditionalConditions()!=null && !filterResult.getAdditionalConditions().isEmpty()){
            builder.append(" where 1 = 1 ");
            for(String condition: filterResult.getAdditionalConditions()){
                builder.append(" and ").append(condition);
            }
        }
        return builder.toString();
    }

    @Override
    public void setFilterResult(FilterResult filterResult) {
        this.filterResult = filterResult;
    }

    private String createSelectStatement() {
        StringBuilder builder = new StringBuilder("select o from "+filterResult.getQueryMlClass().getEntityName()+" o ");
        if(filterResult.getAdditionalConditions()!=null && !filterResult.getAdditionalConditions().isEmpty()){
            builder.append(" where 1 = 1 ");
            for(String condition: filterResult.getAdditionalConditions()){
                builder.append(" and ").append(condition);
            }
        }
        builder.append(addOrder());
        return builder.toString();
    }

    private String createCountSelectStatement(FilterResult filterResult) {
        StringBuilder builder = new StringBuilder("select count(o) from "+filterResult.getQueryMlClass().getEntityName()+" o ");
        if(filterResult.getAdditionalConditions()!=null && !filterResult.getAdditionalConditions().isEmpty()){
            builder.append(" where 1 = 1 ");
            for(String condition: filterResult.getAdditionalConditions()){
                builder.append(" and ").append("(").append(condition).append(") ");
            }
        }
        return builder.toString();
    }



}
