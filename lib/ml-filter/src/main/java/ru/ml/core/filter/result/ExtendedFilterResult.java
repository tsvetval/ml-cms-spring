package ru.ml.core.filter.result;

import ru.ml.core.filter.ExtendedFilterParameter;
import ru.ml.platform.context.CommonDao;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class ExtendedFilterResult extends FilterResult {

    private List<ExtendedFilterParameter> extendedFilterParameters = new ArrayList<>();

    public ExtendedFilterResult(CommonDao commonDao) {
        super(commonDao);
    }

    public List<ExtendedFilterParameter> getExtendedFilterParameters() {
        return extendedFilterParameters;
    }

    public void setExtendedFilterParameters(List<ExtendedFilterParameter> extendedFilterParameters) {
        this.extendedFilterParameters = extendedFilterParameters;
    }
}
