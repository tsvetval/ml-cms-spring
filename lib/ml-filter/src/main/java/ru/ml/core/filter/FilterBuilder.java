package ru.ml.core.filter;

import ru.ml.core.filter.result.ExtendedFilterResult;
import ru.ml.core.filter.result.FilterResult;
import ru.ml.core.filter.result.SimpleFilterResult;
import ru.ml.core.filter.strategy.ExtendedFilterStrategy;
import ru.ml.core.filter.strategy.FilterStrategy;
import ru.ml.core.filter.strategy.NoneFilterStrategy;
import ru.ml.core.filter.strategy.SimpleFilterStrategy;
import ru.ml.platform.context.CommonDao;
import ru.ml.platform.core.model.MlClass;
import ru.ml.platform.core.model.context.holders.MetaDataHolder;
import ru.ml.platform.core.model.exceptions.MlServerException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Билдер FilterResult
 */
public class FilterBuilder {
    private final MetaDataHolder metaDataHolder;
    private final CommonDao commonDao;


    private Strategy strategy = Strategy.none;
    private String simpleSearchValue;
    private String simpleSearchAttrName;
    private Long rowPerPage = 20L;
    private Long pageNumber = 1L;
    private List<ExtendedFilterParameter> extendedFilterParameters = new ArrayList<>();
    private List<String> additionalCondition = new ArrayList<>();
    private Map<String, String> orderMap = new HashMap<>();
    private Map<String, Object> additionalParameters = new HashMap<>();

    public FilterBuilder(MetaDataHolder metaDataHolder, CommonDao commonDao) {
        this.metaDataHolder = metaDataHolder;
        this.commonDao = commonDao;
    }

    public enum Strategy {simple, extended, none}

    private MlClass queryMlClass;

    public MlClass getQueryMlClass() {
        return queryMlClass;
    }

    public FilterBuilder setQueryMlClass(MlClass queryMlClass) {
        this.queryMlClass = queryMlClass;
        return this;
    }

    public FilterBuilder setQueryMlClass(String queryMlClassName) {
        this.queryMlClass = metaDataHolder.getMlClassByName(queryMlClassName);
        return this;
    }

    public FilterResult buildFilterResult() {
        if (strategy == null) {
            throw new MlServerException("Тип фильтра не выбран");
        }
        FilterStrategy filterStrategy = null;
        FilterResult filterResult;
        if (getQueryMlClass() == null) {
            throw new MlServerException("Не указан MlClass для выборки");
        }
        switch (strategy) {
            case extended:
                filterResult = new ExtendedFilterResult(commonDao);
                ((ExtendedFilterResult) filterResult).setExtendedFilterParameters(getExtendedFilterParameters());
                filterStrategy = new ExtendedFilterStrategy(metaDataHolder);
                break;
            case simple:
                filterResult = new SimpleFilterResult(commonDao);
                ((SimpleFilterResult) filterResult).setSimpleSearchValue(getSimpleSearchValue());
                ((SimpleFilterResult) filterResult).setSimpleSearchAttrName(getSimpleSearchAttrName());
                filterStrategy = new SimpleFilterStrategy(metaDataHolder);
                break;
            case none:
                filterResult = new FilterResult(commonDao);
                filterStrategy = new NoneFilterStrategy(metaDataHolder);
                break;
            default:
                throw new MlServerException(String.format("Filter strategy %s not implemented", strategy.name()));
        }

        filterResult.setStrategy(strategy);
        filterResult.setQueryMlClass(getQueryMlClass());
        filterResult.setRowPerPage(getRowPerPage());
        filterResult.setPageCurrent(getPageNumber());
        filterResult.setAdditionalParamsMap(getAdditionalParameters());
        filterResult.setAdditionalConditions(getAdditionalCondition());

        filterResult.getOrderMap().putAll(getOrderMap());
        filterStrategy.setFilterResult(filterResult);
        filterResult = filterStrategy.createResult();

        return filterResult;
    }

    public Map<String, Object> getAdditionalParameters() {
        return additionalParameters;
    }

    public FilterBuilder setRowPerPage(Long rowPerPage) {
        this.rowPerPage = rowPerPage;
        return this;
    }

    public FilterBuilder setPageNumber(Long pageNumber) {
        this.pageNumber = pageNumber;
        return this;
    }

    public FilterBuilder setAdditionalParameters(Map<String, Object> additionalParameters) {
        this.additionalParameters = additionalParameters;
        return this;
    }

    public FilterBuilder addAdditionalParameters(Map<String, Object> additionalParameters) {
        this.additionalParameters.putAll(additionalParameters);
        return this;
    }


    /* simple filter*/
    public FilterBuilder useSimpleSearch() {
        this.strategy = Strategy.simple;
        return this;
    }

    public FilterBuilder setSimpleSearchValue(String simpleSearchValue) {
        this.simpleSearchValue = simpleSearchValue;
        return this;
    }

    public FilterBuilder setSimpleSearchAttr(String attrName) {
        this.simpleSearchAttrName = attrName;
        return this;
    }

    /* Extended filter*/
    public FilterBuilder useExtendedSearch() {
        this.strategy = Strategy.extended;
        return this;
    }

    public FilterBuilder addExtendedParameter(String attrName, String operation, Object value) {
        extendedFilterParameters.add(new ExtendedFilterParameter(attrName, operation, value));
        return this;
    }

    public String getSimpleSearchValue() {
        return simpleSearchValue;
    }

    public String getSimpleSearchAttrName() {
        return simpleSearchAttrName;
    }

    public void setSimpleSearchAttrName(String simpleSearchAttrName) {
        this.simpleSearchAttrName = simpleSearchAttrName;
    }

    public Long getRowPerPage() {
        return rowPerPage;
    }

    public Long getPageNumber() {
        return pageNumber;
    }

    public List<ExtendedFilterParameter> getExtendedFilterParameters() {
        return extendedFilterParameters;
    }

    public List<String> getAdditionalCondition() {
        return additionalCondition;
    }

    public FilterBuilder addAdditionalCondition(String additionalCondition) {
        if (additionalCondition != null && !additionalCondition.trim().isEmpty()) {
            this.additionalCondition.add(additionalCondition);
        }
        return this;
    }

    public FilterBuilder addAdditionalCondition(List<String> additionalCondition) {
        this.additionalCondition.addAll(additionalCondition);
        return this;
    }

    public Map<String, String> getOrderMap() {
        return orderMap;
    }

    public void addOrder(String attrName, String orderType) {
        if (attrName != null) {
            if (metaDataHolder.getAttr(queryMlClass, attrName) == null) {
                throw new MlServerException(String.format("Can't add attr with name [%s] to order for class [%s]", attrName, queryMlClass.getEntityName()));
            }
            if (orderType != null && !orderType.isEmpty() && !(orderType.toUpperCase().equals("ASC") || orderType.toUpperCase().equals("DESC"))) {
                throw new MlServerException(String.format("Can't add order type [%s] to order query", orderType));
            }
            this.orderMap.put(attrName, orderType);
        }
    }
}

