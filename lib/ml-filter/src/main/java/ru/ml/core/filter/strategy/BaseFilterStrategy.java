package ru.ml.core.filter.strategy;

import ru.ml.core.filter.result.FilterResult;
import ru.ml.platform.core.model.MlAttr;
import ru.ml.platform.core.model.context.holders.MetaDataHolder;
import ru.ml.platform.core.model.exceptions.MlServerException;

import java.util.Map;

/**
 *  Базовый класс стратегии
 */
public abstract class BaseFilterStrategy<T extends FilterResult> implements FilterStrategy<T>{
    private final MetaDataHolder metaDataHolder;
    protected T filterResult;

    protected BaseFilterStrategy(MetaDataHolder metaDataHolder) {
        this.metaDataHolder = metaDataHolder;
    }


    protected String addOrder() {
        StringBuilder builder = new StringBuilder();
        if(filterResult.getOrderMap() != null && !filterResult.getOrderMap().isEmpty()){
            boolean firstOrder = true;
            builder.append(" order by");
            for (Map.Entry<String, String> entry : filterResult.getOrderMap().entrySet()) {
                if (!firstOrder){
                    builder.append(",");
                }
                firstOrder = false;
                MlAttr attr = metaDataHolder.getAttr(filterResult.getQueryMlClass().getEntityName(),entry.getKey());
                if (attr == null) {
                    throw new MlServerException(String.format("Can't find order attr with name [%s]", entry.getKey()));
                }
                builder.append(" o.").append(attr.getEntityFieldName());
                if (entry.getValue() != null) {
                    builder.append(" ").append(entry.getValue());
                } else {
                    builder.append(" asc");
                }
            }
        }
        return builder.toString();
    }

}
