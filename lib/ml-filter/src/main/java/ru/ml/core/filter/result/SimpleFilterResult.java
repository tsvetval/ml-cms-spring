package ru.ml.core.filter.result;

import ru.ml.platform.context.CommonDao;

/**
 *
 */
public class SimpleFilterResult extends FilterResult {

    private String simpleSearchValue;
    private String simpleSearchAttrName;

    public SimpleFilterResult(CommonDao commonDao) {
        super(commonDao);
    }

    public String getSimpleSearchValue() {
        return simpleSearchValue;
    }

    public void setSimpleSearchValue(String simpleSearchValue) {
        this.simpleSearchValue = simpleSearchValue;
    }

    public String getSimpleSearchAttrName() {
        return simpleSearchAttrName;
    }

    public void setSimpleSearchAttrName(String simpleSearchAttrName) {
        this.simpleSearchAttrName = simpleSearchAttrName;
    }
}
