package ru.ml.core.filter.result;

import lombok.extern.slf4j.Slf4j;
import ru.ml.core.filter.FilterBuilder;
import ru.ml.platform.context.CommonDao;
import ru.ml.platform.core.model.MlClass;
import ru.ml.platform.core.model.MlDynamicEntityImpl;

import javax.persistence.Parameter;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Фильтр
 */
@Slf4j
public class FilterResult {
    private List<MlDynamicEntityImpl> resultList;
    private Long recordsCount;
    private String rowQuery;
    private String deleteQuery;
    private Long pagesCount;
    private String countRecordQuery;

    private Long pageCurrent;
    private Long rowPerPage;
    private MlClass queryMlClass;
    private Map<String, Object> paramsMap = new HashMap<>();
    private Map<String, Object> additionalParamsMap;
    private FilterBuilder.Strategy strategy;
    private List<String> additionalConditions;
    private Map<String, String> orderMap = new HashMap<>();

    private final CommonDao commonDao;

    public FilterResult(CommonDao commonDao) {
        this.commonDao = commonDao;
    }

    public List<MlDynamicEntityImpl> getResultList() {
        return getResultList(true);
    }

    public <T> List<T> getAllRows() {
        return getAllRows(true);
    }

    public <T> List<T> getAllRows(boolean checkSecurity) {
        Query query;
        if (checkSecurity) {
            query = commonDao.getQueryWithSecurityCheck(rowQuery, queryMlClass.getEntityName());
        } else {
            query = commonDao.getQueryWithoutSecurityCheck(rowQuery);
        }
        for (String paramName : paramsMap.keySet()) {
            query.setParameter(paramName, paramsMap.get(paramName));
        }
        putAdditionalParameters(query);
        return query.getResultList();
    }

    public List<MlDynamicEntityImpl> getResultList(boolean checkSecurity) {
        if (resultList == null) {
            Query query;
            if (checkSecurity) {
                query = commonDao.getQueryWithSecurityCheck(rowQuery, queryMlClass.getEntityName());
            } else {
                query = commonDao.getQueryWithoutSecurityCheck(rowQuery);
            }

            query.setFirstResult((int) ((pageCurrent - 1) * rowPerPage)).setMaxResults(rowPerPage.intValue());
            for (String paramName : paramsMap.keySet()) {
                query.setParameter(paramName, paramsMap.get(paramName));
            }
            putAdditionalParameters(query);
            resultList = query.getResultList();
        }
        return resultList;
    }


    public Integer delete() {
        Query query = commonDao.getQueryWithSecurityCheck(deleteQuery, queryMlClass.getEntityName());
        for (String paramName : paramsMap.keySet()) {
            query.setParameter(paramName, paramsMap.get(paramName));
        }
        putAdditionalParameters(query);
        return query.executeUpdate();
    }

    private void putAdditionalParameters(Query query) {
        for (String paramName : additionalParamsMap.keySet()) {
            for (Parameter parameter : query.getParameters()) {
                if (parameter.getName().equals(paramName)) {
                    if (paramName.contains(".")) {
                        // Если параметр представлен как строка obj.field1.field2
                        String paramObjectName = paramName.substring(0, paramName.indexOf("."));
                        if (additionalParamsMap.get(paramObjectName) != null) {
                            Object value = ((MlDynamicEntityImpl) additionalParamsMap.get(paramObjectName))
                                    .getValueByPath(paramName.substring(paramName.indexOf("."), paramName.length()));
                            query.setParameter(paramName, value);
                        }
                    } else {
                        query.setParameter(paramName, additionalParamsMap.get(paramName));
                    }
                }
            }
        }
    }

    public Long getRecordsCount() {
        return getRecordsCount(true);
    }

    public Long getRecordsCount(boolean checkSecurity) {
        if (recordsCount == null) {
            Query query;
            if (checkSecurity) {
                query = commonDao.getQueryWithSecurityCheck(countRecordQuery, queryMlClass.getEntityName());
            } else {
                query = commonDao.getQueryWithoutSecurityCheck(countRecordQuery);
            }

            for (String paramName : paramsMap.keySet()) {
                query.setParameter(paramName, paramsMap.get(paramName));
            }
            putAdditionalParameters(query);
            recordsCount = (Long) query.getSingleResult();
            pagesCount = recordsCount / rowPerPage;
            long addOnePage = recordsCount % rowPerPage;
            if (addOnePage > 0) {
                pagesCount++;
            }
        }
        return recordsCount;
    }

    public void setRecordsCount(Long recordsCount) {
        this.recordsCount = recordsCount;
    }


    public Long getPagesCount() {
        return getPagesCount(true);
    }

    public Long getPagesCount(boolean checkSecurity) {
        if (pagesCount == null) {
            getRecordsCount(checkSecurity);
        }
        return pagesCount;
    }

    public void setPagesCount(Long pagesCount) {
        this.pagesCount = pagesCount;
    }

    public Long getPageCurrent() {
        return pageCurrent;
    }

    public void setPageCurrent(Long pageCurrent) {
        this.pageCurrent = pageCurrent;
    }

    public void addParameter(String name, Object value) {
        paramsMap.put(name, value);
    }

    public Long getRowPerPage() {
        return rowPerPage;
    }

    public void setRowPerPage(Long rowPerPage) {
        this.rowPerPage = rowPerPage;
    }

    public String getRowQuery() {
        return rowQuery;
    }

    public void setRowQuery(String rowQuery) {
        this.rowQuery = rowQuery;
    }

    public String getCountRecordQuery() {
        return countRecordQuery;
    }

    public void setCountRecordQuery(String countRecordQuery) {
        this.countRecordQuery = countRecordQuery;
    }

    public MlClass getQueryMlClass() {
        return queryMlClass;
    }

    public void setQueryMlClass(MlClass queryMlClass) {
        this.queryMlClass = queryMlClass;
    }

    public FilterBuilder.Strategy getStrategy() {
        return strategy;
    }

    public void setStrategy(FilterBuilder.Strategy strategy) {
        this.strategy = strategy;
    }

    public List<String> getAdditionalConditions() {
        return additionalConditions;
    }

    public void setAdditionalConditions(List<String> additionalConditions) {
        this.additionalConditions = additionalConditions;
    }

    public Map<String, String> getOrderMap() {
        return orderMap;
    }

    public void setOrderMap(Map<String, String> orderMap) {
        this.orderMap = orderMap;
    }


    public void setAdditionalParamsMap(Map<String, Object> additionalParamsMap) {
        this.additionalParamsMap = additionalParamsMap;
    }

    public String getDeleteQuery() {
        return deleteQuery;
    }

    public void setDeleteQuery(String deleteQuery) {
        this.deleteQuery = deleteQuery;
    }


}
