package ru.ml.core.filter.strategy;

import ru.ml.core.filter.result.FilterResult;

/**
 *
 */
public interface FilterStrategy<T extends FilterResult > {

    public T createResult();

    public void setFilterResult(T filterResult);

}
