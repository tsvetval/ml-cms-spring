package ru.ml.platform.context.holders.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.comparator.Comparators;
import ru.ml.platform.core.model.MlAttr;
import ru.ml.platform.core.model.MlEnum;
import ru.ml.platform.core.model.context.holders.EnumHolder;
import ru.ml.platform.core.model.context.holders.MetaDataHolder;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Хранилище проинициализированных энумов
 */
public class EnumHolderImpl implements EnumHolder {
    private static final Logger log = LoggerFactory.getLogger(EnumHolderImpl.class);


    private final MetaDataHolder metaDataHolder;
    /**
     * key -attr id
     * value - map key = enum Code
     * value - enum
     */
    private final Map<Long, Map<String, MlEnum>> mlAttr_mlEnumListMap = new ConcurrentHashMap<>();


    public EnumHolderImpl(MetaDataHolder metaDataHolder) {
        this.metaDataHolder = metaDataHolder;
    }


    @Override
    public void addEnum(MlEnum newEnum) {
        log.debug(String.format("Try Add enum [%s] with id [%d] ", newEnum.getCode(), newEnum.getId()));
        if (newEnum.getAttr() != null) {
            Map<String, MlEnum> enumMap = mlAttr_mlEnumListMap.get(newEnum.getAttr().getId());
            if (enumMap == null) {
                enumMap = new HashMap<>();
                mlAttr_mlEnumListMap.put(newEnum.getAttr().getId(), enumMap);
            } else {
                removeEnumById(enumMap, newEnum.getId());
            }
            enumMap.put(newEnum.getCode(), newEnum);
        } else {
            log.warn(String.format("Enum [%s] with id [%d] has empty attr filed. Skip it", newEnum.getCode(), newEnum.getId()));
        }
    }

    private void removeEnumById(Map<String, MlEnum> enumMap, Long id) {
        for (Iterator<Map.Entry<String, MlEnum>> it = enumMap.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, MlEnum> entry = it.next();
            if (entry.getValue().getId().equals(id)) {
                it.remove();
            }
        }
    }

    @Override
    public Collection<MlEnum> getEnumList(MlAttr IMlAttr) {
        Map<String, MlEnum> enumMap = mlAttr_mlEnumListMap.get(IMlAttr.getId());
        if (enumMap != null) {
            List<MlEnum> enumList = new ArrayList<MlEnum>(enumMap.values());
            enumList.sort((o1, o2) -> Comparators.nullsHigh().compare(o1.getOrder(), o2.getOrder()));
            return enumList;
        }
        return new ArrayList<>();
    }

    @Override
    public MlEnum getEnum(MlAttr IMlAttr, String enumCode) {
        Map<String, MlEnum> enumMap = mlAttr_mlEnumListMap.get(IMlAttr.getId());
        if (enumMap != null) {
            return enumMap.get(enumCode);
        }
        return null;
    }

    @Override
    public MlEnum getEnum(String className, String attrName, String enumCode) {
        MlAttr attr = metaDataHolder.getAttr(className, attrName);
        return getEnum(attr, enumCode);
    }
}
