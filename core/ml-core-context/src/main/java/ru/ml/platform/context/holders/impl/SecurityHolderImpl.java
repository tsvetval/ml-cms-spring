package ru.ml.platform.context.holders.impl;

import ru.ml.platform.core.model.*;
import ru.ml.platform.core.model.context.holders.SecurityHolder;
import ru.ml.platform.core.model.security.*;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class SecurityHolderImpl implements SecurityHolder {

    private final Map<Long, Set<MlFolder>> roles_Folders = new ConcurrentHashMap<>();
    private final Map<Long, Set<MlFolder>> roles_RootFolders = new ConcurrentHashMap<>();

    private final Map<MlFolder, Set<Long>> folders_PageBlocksId = new ConcurrentHashMap<>();

    private final Map<Long, Set<MlUtil>> roles_Utils = new ConcurrentHashMap<>();

    private final Map<Long, Set<MlClass>> roles_ClassesCreate = new ConcurrentHashMap<>();
    private final Map<Long, Set<MlClass>> roles_ClassesRead = new ConcurrentHashMap<>();
    private final Map<Long, Set<MlClass>> roles_ClassesUpdate = new ConcurrentHashMap<>();
    private final Map<Long, Set<MlClass>> roles_ClassesDelete = new ConcurrentHashMap<>();

    private final Map<Long, Set<MlAttr>> roles_AttrNotShow = new ConcurrentHashMap<>();
    private final Map<Long, Set<MlAttr>> roles_AttrNotEdit = new ConcurrentHashMap<>();

    private final Map<Long, Map<MlClass, String>> roles_ClassesQuery = new ConcurrentHashMap<>();

    private final Map<Long, Set<String>> roles_resources = new ConcurrentHashMap<>();

    public SecurityHolderImpl() {
    }


    @Override
    public void init(List<MlRole> mlRoles) {
        roles_Folders.clear();
        roles_Utils.clear();

        roles_ClassesCreate.clear();
        roles_ClassesRead.clear();
        roles_ClassesUpdate.clear();
        roles_ClassesDelete.clear();
        roles_AttrNotShow.clear();
        roles_AttrNotEdit.clear();
        roles_ClassesQuery.clear();
        folders_PageBlocksId.clear();
        roles_resources.clear();

        for (MlRole role : mlRoles) {
            addRole(role);
        }

    }


    @Override
    public void addRole(MlRole role) {

        // Инициализируем доступные папки
        roles_Folders.remove(role.getId());
        roles_RootFolders.remove(role.getId());

        Set<MlFolder> folders = new HashSet<>();
        for (MlFolderAccess folderAccess : role.getFolderAccess()) {
            folders.addAll(folderAccess.getFolders());
//            for(MlFolder folder: folderAccess.getFolders()){
//                while (folder.getParent()!=null){
//                    folders.add(folder.getParent());
//                    folder = folder.getParent();
//                }
//            }
        }
        roles_Folders.put(role.getId(), folders);

        // инициализируем рутовые папки
        Set<MlFolder> rootFolders = new HashSet<>();
        for (MlFolder folder : folders) {
            if (folder.getParent() == null ||
                    // Есть родитель но нет доступа к нему
                    !folders.contains(folder.getParent())) {
                rootFolders.add(folder);
            }
        }
        roles_RootFolders.put(role.getId(), rootFolders);


        roles_Utils.remove(role.getId());
        Set<MlUtil> utils = new HashSet<>();
        for (MlUtilAccess utilAccess : role.getUtilAccess()) {
            utils.addAll(utilAccess.getUtils());
        }
        roles_Utils.put(role.getId(), utils);

        roles_ClassesCreate.remove(role.getId());
        roles_ClassesRead.remove(role.getId());
        roles_ClassesUpdate.remove(role.getId());
        roles_ClassesDelete.remove(role.getId());
        roles_ClassesQuery.remove(role.getId());
        Set<MlClass> classesCreate = new HashSet<>();
        Set<MlClass> classesRead = new HashSet<>();
        Set<MlClass> classesUpdate = new HashSet<>();
        Set<MlClass> classesDelete = new HashSet<>();
        Map<MlClass, String> additionalQuery = new ConcurrentHashMap<>();
        for (MlClassAccess classAccess : role.getClassAccess()) {
            if (classAccess.isCreate()) {
                classesCreate.add(classAccess.getMlClass());
            }
            if (classAccess.isRead()) {
                classesRead.add(classAccess.getMlClass());
            }
            if (classAccess.isUpdate()) {
                classesUpdate.add(classAccess.getMlClass());
            }
            if (classAccess.isDelete()) {
                classesDelete.add(classAccess.getMlClass());
            }
            if (classAccess.getAdditionalQuery() != null && !classAccess.getAdditionalQuery().isEmpty()) {
                additionalQuery.put(classAccess.getMlClass(), classAccess.getAdditionalQuery());
            }
        }
        roles_ClassesCreate.put(role.getId(), classesCreate);
        roles_ClassesRead.put(role.getId(), classesRead);
        roles_ClassesUpdate.put(role.getId(), classesUpdate);
        roles_ClassesDelete.put(role.getId(), classesDelete);
        roles_ClassesQuery.put(role.getId(), additionalQuery);

        roles_AttrNotShow.remove(role.getId());
        roles_AttrNotEdit.remove(role.getId());
        Set<MlAttr> attrNotShow = new HashSet<>();
        Set<MlAttr> attrNotEdit = new HashSet<>();
        for (MlAttrAccess attrAccess : role.getAttrAccess()) {
            if (attrAccess.isNotShow()) {
                attrNotShow.add(attrAccess.getMlAttr());
            }
            if (attrAccess.isNotEdit()) {
                attrNotEdit.add(attrAccess.getMlAttr());
            }
        }
        roles_AttrNotShow.put(role.getId(), attrNotShow);
        roles_AttrNotEdit.put(role.getId(), attrNotEdit);
        Set<String> resources = new HashSet<>();
        for (MlResourceAccess resourceAccess : role.getResourceAccess()) {
            for (MlResources resource : resourceAccess.getResources()) {
                resources.add(resource.getId());
            }
            roles_resources.put(role.getId(), resources);
        }
    }


    @Override
    public Set<MlFolder> getRootFolders(List<MlRole> roles) {
        Set<MlFolder> result = new HashSet();

        for (MlRole role : roles) {
            if (RoleType.MAIN_ADMIN_ROLE.equals(role.getRoleType())) {
                throw new RuntimeException("Check for MAIN_ADMIN_ROLE before ");
            }
            result.addAll(roles_RootFolders.get(role.getId()));
        }
        for (MlFolder folder : result) {
            folder.setParent(null);
        }

        return result;
    }

    @Override
    public boolean checkAccessFolder(List<MlRole> roles, MlFolder folder) {
        for (MlRole role : roles) {
            if (RoleType.MAIN_ADMIN_ROLE.equals(role.getRoleType())) {
                return true;
            }
            if (roles_Folders.get(role.getId()).contains(folder)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessUtil(List<MlRole> roles, MlUtil util) {
        for (MlRole role : roles) {
            if (RoleType.MAIN_ADMIN_ROLE.equals(role.getRoleType())) {
                return true;
            }
            if (roles_Utils.get(role.getId()).contains(util)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessClassCreate(List<MlRole> roles, MlClass mlClass) {
        for (MlRole role : roles) {
            if (RoleType.MAIN_ADMIN_ROLE.equals(role.getRoleType())) {
                return true;
            }
            if (roles_ClassesCreate.get(role.getId()).contains(mlClass)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessClassRead(List<MlRole> roles, MlClass mlClass) {
        for (MlRole role : roles) {
            if (RoleType.MAIN_ADMIN_ROLE.equals(role.getRoleType())) {
                return true;
            }
            if (roles_ClassesRead.get(role.getId()).contains(mlClass)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessClassUpdate(List<MlRole> roles, MlClass mlClass) {
        for (MlRole role : roles) {
            if (RoleType.MAIN_ADMIN_ROLE.equals(role.getRoleType())) {
                return true;
            }
            if (roles_ClassesUpdate.get(role.getId()).contains(mlClass)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessClassDelete(List<MlRole> roles, MlClass mlClass) {
        for (MlRole role : roles) {
            if (RoleType.MAIN_ADMIN_ROLE.equals(role.getRoleType())) {
                return true;
            }
            if (roles_ClassesDelete.get(role.getId()).contains(mlClass)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessAttrEdit(List<MlRole> roles, MlAttr mlAttr) {
        for (MlRole role : roles) {
            if (RoleType.MAIN_ADMIN_ROLE.equals(role.getRoleType())) {
                return true;
            }
            if (roles_AttrNotEdit.get(role.getId()).contains(mlAttr)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean checkAccessAttrShow(List<MlRole> roles, MlAttr mlAttr) {
        for (MlRole role : roles) {
            if (RoleType.MAIN_ADMIN_ROLE.equals(role.getRoleType())) {
                return true;
            }
            if (roles_AttrNotShow.get(role.getId()).contains(mlAttr)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public List<String> getAdditionalQueryForClass(List<MlRole> roles, MlClass mlClass) {
        List<String> result = new ArrayList<>();
        for (MlRole role : roles) {
            if (roles_ClassesQuery.get(role.getId()).containsKey(mlClass)) {
                result.add(roles_ClassesQuery.get(role.getId()).get(mlClass));
            }
        }
        return result;
    }


    @Override
    public boolean checkAccessResource(List<MlRole> roles, String id) {
        for (MlRole role : roles) {
            if (RoleType.MAIN_ADMIN_ROLE.equals(role.getRoleType())) {
                return true;
            }
            if (roles_resources.containsKey(role.getId())) {
                for (String resourceUrl : roles_resources.get(role.getId())) {
                    if (resourceUrl.equals(id)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

}
