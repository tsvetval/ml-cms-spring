package ru.ml.platform.context.holders.impl;

import org.springframework.stereotype.Component;
import org.springframework.util.comparator.Comparators;
import ru.ml.platform.core.model.MlAttr;
import ru.ml.platform.core.model.MlAttrGroup;
import ru.ml.platform.core.model.MlClass;
import ru.ml.platform.core.model.context.holders.MetaDataHolder;
import ru.ml.platform.core.model.exceptions.MlServerException;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Хранилище проинициализированных мета данных
 */
public class MetaDataHolderImpl implements MetaDataHolder {

    public MetaDataHolderImpl() {
    }


    private final Map<Long, MlClass> mlClassId_MlClass_Map = new ConcurrentHashMap<Long, MlClass>();
    private final Map<String, MlClass> entityName_mlClass_Map = new ConcurrentHashMap<String, MlClass>();

    private final Map<String, Class> entityName_entityDynamicClass_Map = new ConcurrentHashMap<String, Class>();
    private final Map<String, MlClass> entityDynamicClass_MlClass_Map = new ConcurrentHashMap<String, MlClass>();

    /*Все атрибуты класса*/
    private final Map<String, Map<String, MlAttr>> classAttrMap = new ConcurrentHashMap<String, Map<String, MlAttr>>();
    private final Map<Long, MlAttr> idAttrMap = new ConcurrentHashMap<Long, MlAttr>();
    /*Атрибуты являющиеся первичным ключем класса*/
    private final Map<String, List<MlAttr>> mlClass_ListPkAttrMap = new ConcurrentHashMap<>();
    /* Атрибуты класса отображаемые в списке*/
    private final Map<String, List<MlAttr>> mlClass_inListAttrListMap = new ConcurrentHashMap<>();
    /* Атрибуты класса отображаемые в форме*/
    private final Map<String, List<MlAttr>> mlClass_inFormAttrListMap = new ConcurrentHashMap<>();
    /*Все группы атрибутов класса*/
    private final Map<String, List<MlAttrGroup>> mlClass_AttrGroupList_Map = new ConcurrentHashMap<String, List<MlAttrGroup>>();
    /*Классы и их потомки(только листья)*/
    private Map<String, List<MlClass>> mlClass_children_Map = new ConcurrentHashMap<String, List<MlClass>>();
    /* Атрибуты класса для простого поиска*/
    private final Map<String, List<MlAttr>> mlClass_simpleSearchAttrListMap = new ConcurrentHashMap<>();
    /* Атрибуты класса для расширенного поиска*/
    private final Map<String, List<MlAttr>> mlClass_extendedSearchAttrListMap = new ConcurrentHashMap<>();

    /**
     * Обновляет метоописание для заданного класса в хранилище
     *
     * @param mlClass
     * @throws ClassNotFoundException
     */
    @Override
    public void updateMlClass(MlClass mlClass) throws ClassNotFoundException {
        Class clazz;
        if (mlClass.getJavaClass() != null && !mlClass.getJavaClass().isEmpty()) {
            clazz = Class.forName(mlClass.getJavaClass());
        } else {
            //TODO fix this
            clazz = getEntityClassByName(mlClass.getEntityName());
            if (clazz == null) {
                throw new MlServerException("Невозможно обновить метоописание класса %s, данный класс не был проинициализорован ");
            }
        }
        addMlClass(mlClass, clazz);
    }

    /**
     * Добавляет класс в хранилище
     * !!!! Все предки данного класса уже должны быть в хранилище
     *
     * @param clazz       MlClass
     * @param entityClass класс Java
     * @throws ClassNotFoundException
     */
    @Override
    public void addMlClass(MlClass clazz, Class entityClass) throws ClassNotFoundException {
        // очищаем мыпы
        entityName_mlClass_Map.put(clazz.getEntityName(), clazz);
        mlClassId_MlClass_Map.put(clazz.getId(), clazz);
        entityName_entityDynamicClass_Map.put(clazz.getEntityName(), entityClass);
        entityDynamicClass_MlClass_Map.put(entityClass.getCanonicalName(), clazz);


        classAttrMap.put(clazz.getEntityName(), new HashMap<String, MlAttr>());
        mlClass_ListPkAttrMap.put(clazz.getEntityName(), new ArrayList<MlAttr>());
        mlClass_inListAttrListMap.put(clazz.getEntityName(), new ArrayList<MlAttr>());
        mlClass_inFormAttrListMap.put(clazz.getEntityName(), new ArrayList<MlAttr>());
        mlClass_simpleSearchAttrListMap.put(clazz.getEntityName(), new ArrayList<MlAttr>());
        mlClass_extendedSearchAttrListMap.put(clazz.getEntityName(), new ArrayList<MlAttr>());
        mlClass_AttrGroupList_Map.put(clazz.getEntityName(), new ArrayList<MlAttrGroup>());
        mlClass_children_Map.put(clazz.getEntityName(), new ArrayList<MlClass>());


        //=================Добавляем в мапу атрибуты парентов==============
        MlClass parent = clazz.getParent();
        List<MlClass> parentList = new ArrayList<>();
        Map<String, MlAttr> parentAttrMap = new HashMap<>();
        // Формируем список родителей
        while (parent != null) {
            parentList.add(parent);
            parent = parent.getParent();
        }
        // Заполняем parentAttrMap списком атрибутов родителей начиная с самого дальнего родителя
        if (!parentList.isEmpty()) {
            for (int i = parentList.size() - 1; i >= 0; i--) {
                parent = parentList.get(i);
                Map<String, MlAttr> tmpAttrMap = classAttrMap.get(parent.getEntityName());
                if (tmpAttrMap != null) {
                    parentAttrMap.putAll(tmpAttrMap);
                }
            }
        }

        //====================================================================

        // Заполняем мапу attrMap текущими атрибутами класса
        for (MlAttr attr : clazz.getAttrSet()) {
            parentAttrMap.put(attr.getEntityFieldName(), attr);
        }
        classAttrMap.put(clazz.getEntityName(), parentAttrMap);


        // Раскидываем по коллекциям все атрибуты класса
        for (MlAttr attr : classAttrMap.get(clazz.getEntityName()).values()) {
            idAttrMap.put(attr.getId(), attr);
            if (attr.getPrimaryKey()) {
                List<MlAttr> pkAttrList = mlClass_ListPkAttrMap.get(clazz.getEntityName());
                pkAttrList.add(attr);
            }
            if (attr.getInList()) {
                List<MlAttr> inListAttrList = mlClass_inListAttrListMap.get(clazz.getEntityName());
                inListAttrList.add(attr);
            }
            if (attr.getInForm()) {
                List<MlAttr> inFormAttrList = mlClass_inFormAttrListMap.get(clazz.getEntityName());
                inFormAttrList.add(attr);
            }
            if (attr.isUseInSimpleSearch()) {
                List<MlAttr> inSimpleSearchAttrList = mlClass_simpleSearchAttrListMap.get(clazz.getEntityName());
                inSimpleSearchAttrList.add(attr);
            }
            if (attr.isUseInExtendedSearch()) {
                List<MlAttr> inExtendedSearchAttrList = mlClass_extendedSearchAttrListMap.get(clazz.getEntityName());
                inExtendedSearchAttrList.add(attr);
            }
        }

        //======================добавляем в мапу группы парентов============================
        if (!parentList.isEmpty()) {
            for (int i = parentList.size() - 1; i >= 0; i--) {
                parent = parentList.get(i);
                List<MlAttrGroup> parentGroupList = mlClass_AttrGroupList_Map.get(parent.getEntityName());
                List<MlAttrGroup> attrGroupList = mlClass_AttrGroupList_Map.get(clazz.getEntityName());
                if (parentGroupList != null) {
                    attrGroupList.addAll(parentGroupList);
                }
            }
        }
        List<MlAttrGroup> attrGroupList = mlClass_AttrGroupList_Map.get(clazz.getEntityName());
        attrGroupList.addAll((List<MlAttrGroup>) clazz.getPropertiesMap().get("groupList").getValue());
        //================================================================================

        // Сортируем коллекции
        mlClass_inListAttrListMap.get(clazz.getEntityName()).sort((o1, o2) ->
                Comparators.nullsHigh().compare(o1.getViewPos(), o2.getViewPos())
        );
        mlClass_inFormAttrListMap.get(clazz.getEntityName()).sort((o1, o2) ->
                Comparators.nullsHigh().compare(o1.getViewPos(), o2.getViewPos())
        );

        /* Group sort disabled for resolve [I-9185] issue */
        //Collections.sort(mlClass_AttrGroupList_Map.get(clazz.getEntityName()), new MlClassGroupOrderComparator());

        fillInheritanceMap();
    }

    private void fillInheritanceMap() {
        mlClass_children_Map = new ConcurrentHashMap<>();
        for (MlClass mlClass : mlClassId_MlClass_Map.values()) {
            mlClass_children_Map.put(mlClass.getEntityName(), new ArrayList<MlClass>());
        }
        for (MlClass mlClass : mlClassId_MlClass_Map.values()) {
            if (mlClass.getParent() != null) {
                if (mlClass_children_Map.get(mlClass.getParent().getEntityName()) == null) {
                    mlClass_children_Map.put(mlClass.getParent().getEntityName(), new ArrayList<MlClass>());
                }
                mlClass_children_Map.get(mlClass.getParent().getEntityName()).add(mlClass);
            }
        }
    }


    @Override
    public void addMlAttr(MlAttr attr) {
        Map<String, MlAttr> attrMap = classAttrMap.get(attr.getMlClass().getEntityName());
        if (attrMap == null) {
            attrMap = new HashMap<>();
            classAttrMap.put(attr.getMlClass().getEntityName(), attrMap);
        }
        attrMap.put(attr.getEntityFieldName(), attr);
    }

    @Override
    public MlAttr getAttrById(Long id) {
        return idAttrMap.get(id);
    }

    /**
     * Возвращает объект MLClass по имени энтити
     *
     * @param mlClassName имени энтити
     * @return MLClass
     */
    @Override
    public MlClass getMlClassByName(String mlClassName) {
        return entityName_mlClass_Map.get(mlClassName);
    }

    @Override
    public MlClass getMlClassById(Long id) {
        return mlClassId_MlClass_Map.get(id);
    }

    @Override
    public Class getEntityClassByName(String entityClassName) {
        return entityName_entityDynamicClass_Map.get(entityClassName);
    }

    @Override
    public MlAttr getAttr(MlClass mlClass, String attrName) {
        Map<String, MlAttr> attrMap = classAttrMap.get(mlClass.getEntityName());
        if (attrMap != null) {
            return attrMap.get(attrName);
        }
        return null;
    }

    @Override
    public MlAttr getAttr(String mlClass, String attrName) {
        return getAttr(getMlClassByName(mlClass), attrName);
    }

    @Override
    public List<MlAttr> getPrimaryKeyAttrList(MlClass mlClass) {
        return mlClass_ListPkAttrMap.get(mlClass.getEntityName());
    }


    /*
        private final static Comparator<MlAttr> comparator = new Comparator<MlAttr>() {
            @Override
            public int compare(MlAttr o1, MlAttr o2) {
                if (o1.getViewPos() == null && o2.getViewPos() == null) return 0;
                else if (o1.getViewPos() == null && o2.getViewPos() != null) return -1;
                else if (o1.getViewPos() != null && o2.getViewPos() == null) return 1;
                else return o1.getViewPos().compareTo(o2.getViewPos());
            }
        };
    */
    public List<MlAttr> getInFormAttrList(MlClass mlClass) {
        List<MlAttr> result = mlClass_inFormAttrListMap.get(mlClass.getEntityName());
        return result;
    }


    public List<MlAttr> getInListAttrList(MlClass mlClass) {
        List<MlAttr> result = mlClass_inListAttrListMap.get(mlClass.getEntityName());
        return result;
    }

    @Override
    public List<MlAttr> getPrimaryKey(MlClass mlClass) {
        return mlClass_ListPkAttrMap.get(mlClass.getEntityName());
    }

    @Override
    public List<MlAttr> getSimpleSearchAttrList(MlClass mlClass) {
        return mlClass_simpleSearchAttrListMap.get(mlClass.getEntityName());
    }

    @Override
    public List<MlAttr> getExtendedSearchAttrList(MlClass mlClass) {
        return mlClass_extendedSearchAttrListMap.get(mlClass.getEntityName());
    }

    @Override
    public MlClass getMlClassByEntityDynamicClass(Class clazz) {
        return entityDynamicClass_MlClass_Map.get(clazz.getCanonicalName());
    }

    @Override
    public List<MlAttrGroup> getGroupList(MlClass mlClass) {
        return mlClass_AttrGroupList_Map.get(mlClass.getEntityName());
    }

    @Override
    public List<MlClass> getChildrenByClassName(String parentName) {
        return mlClass_children_Map.get(parentName);
    }

    @Override
    public MlAttr findAttrByPath(MlClass mlClass, String path) {
        String[] attrs = path.split("\\.");
        MlClass cls = mlClass;
        MlAttr result = null;
        for (String attrName : attrs) {
            MlAttr attr = cls.getAttr(attrName);
            result = attr;
            if (attr.isRelation()) {
                cls = attr.getRelationClass();
            }
        }
        return result;
    }

    @Override
    public Collection<MlClass> getAllMlClasses() {
        return mlClassId_MlClass_Map.values();
    }
}
