package ru.ml.platform.context.holders.impl;

import ru.ml.platform.core.model.context.handler.MlClassHandler;
import ru.ml.platform.core.model.context.holders.HandlerHolder;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class HandlerHolderImpl implements HandlerHolder<MlClassHandler> {

    private final Map<Class, MlClassHandler> handlers = new ConcurrentHashMap<>();

    @Override
    public void init(Collection<MlClassHandler> handlers) {
        handlers.forEach(handler -> {
            this.handlers.putIfAbsent(handler.getClass(), handler);
        });
    }

    @Override
    public MlClassHandler getHolder(Class cls) {
        return handlers.get(cls);
    }
}
