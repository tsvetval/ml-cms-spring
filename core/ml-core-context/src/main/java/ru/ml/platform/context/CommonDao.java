package ru.ml.platform.context;

import org.eclipse.persistence.dynamic.DynamicEntity;
import ru.ml.platform.core.model.MLUID;
import ru.ml.platform.core.model.MlDynamicEntityImpl;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

public interface CommonDao<T extends MlDynamicEntityImpl> {

    <M extends T> TypedQuery<M> getTypedQueryWithoutSecurityCheck(String query, Class<M> clazz);
    <M extends T> TypedQuery<M> getTypedQueryWithSecurityCheck(String query, Class<M> clazz);

    TypedQuery<T> getTypedQueryWithoutSecurityCheck(String query, String mlClassName);

    TypedQuery<T> getTypedQueryWithSecurityCheck(String query, String mlClassName);

    Query getQueryWithoutSecurityCheck(String query);

    Query getQueryWithSecurityCheck(String query, String mlClassName);

    <M extends T> M persistTransactionalWithSecurityCheck(M object);

    <M extends T> M persistTransactionalWithSecurityCheck(M object, boolean runBeforeHandlers, boolean runAfterHandlers);

    <M extends T> M persistTransactionalWithoutSecurityCheck(M object);

    <M extends T> M persistTransactionalWithoutSecurityCheck(M object, boolean runBeforeHandlers, boolean runAfterHandlers);

    <M extends T> M mergeTransactional(M object);

    <M extends T> void removeTransactional(M object);

    //TODO добавить проверку прав доступа на чтение к объектам данного класса
    <M extends T> M findById(Object id, Class<M> clazz);

    //TODO добавить проверку прав доступа на чтение к объектам данного класса
    T findById(Object id, String mlClassName);

    List<T> getAll(Class<T> clazz);

    <M extends T> M persistWithSecurityCheck(M object);

    <M extends T> M persistWithoutSecurityCheck(M object);

    //TODO refactor соединить с сеюрной проверкой
    <M extends T> M persistWithoutSecurityCheck(M object, boolean runBeforeHandlers, boolean runAfterHandlers);

    <M extends T> M persistWithSecurityCheck(M object, boolean runBeforeHandlers, boolean runAfterHandlers);

    <M extends T> M mergeWithSecurityCheck(M object);

    <M extends T> M mergeWithSecurityCheck(M object, boolean runBeforeHandlers, boolean runAfterHandlers);

    <M extends T> M mergeWithoutSecurityCheck(M object);

    <M extends T> M mergeTransactionalWithoutSecurityCheck(M object);

    <M extends T> M mergeWithoutSecurityCheck(M object, boolean runBeforeHandlers, boolean runAfterHandlers);

    <M extends T> void removeWithoutSecurityCheck(M object);

    <M extends T> void removeWithoutSecurityCheck(M object, boolean runBeforeHandlers, boolean runAfterHandlers);

    <M extends T> void removeWithSecurityCheck(M object);

    <M extends T> void removeWithSecurityCheck(M object, boolean runBeforeHandlers, boolean runAfterHandlers);

    <M extends T> void refresh(M object);

    MLUID getMlUID(Object objectId, String mlClass);

    T getByMlUID(MLUID mlUID);

    MLUID getMlUID(DynamicEntity entity);

    <M extends T> M createNewEntity(Class<M> clazz);

    <M extends T> M createNewEntity(Class<M> clazz, boolean fillDefaults);

    void fillObjectDefaultValues(DynamicEntity object);

    MlDynamicEntityImpl cloneObject(DynamicEntity forClone);

    void evictL2Cache(DynamicEntity entity);

    void detach(DynamicEntity entity);

    Query createNativeQuery(String sql);

    void evictAllL2Cache();
}
