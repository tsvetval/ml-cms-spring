package ru.ml.platform.context;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.ml.platform.context.holders.impl.*;
import ru.ml.platform.core.model.context.MlContext;
import ru.ml.platform.core.model.context.holders.EnumHolder;
import ru.ml.platform.core.model.context.holders.HandlerHolder;
import ru.ml.platform.core.model.context.holders.MetaDataHolder;
import ru.ml.platform.core.model.context.holders.SecurityHolder;

@Configuration
@ComponentScan
public class MlContextConfiguration {

    @Bean
    public UserContext userContext() {
        return new UserContext();
    }

    public static void initEmptyContext() {
        MetaDataHolder metaDataHolder = new MetaDataHolderImpl();
        EnumHolder enumHolder = new EnumHolderImpl(metaDataHolder);
        HandlerHolder handlerHolder = new HandlerHolderImpl();
        SecurityHolder securityHolder = new SecurityHolderImpl();
        MlContext.initInstance(enumHolder, metaDataHolder, handlerHolder, securityHolder);
    }
}
