package ru.ml.platform.context.holders.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import ru.ml.platform.core.model.security.MlUser;

public class UserContext {
    private final Logger log = LoggerFactory.getLogger(UserContext.class);
    private static final String ABSENT_USER_LOGIN = "System";
    public static final String ML_USER_HEADER = "ML_USER";


    private ThreadLocal<MlUser> userContext = new ThreadLocal<>();

    public MlUser getCurrentUser() {
        //FIXME fix this
        if (userContext.get() == null){
            MlUser anonim = new MlUser();
            anonim.setLogin("anonymous");
            anonim.setAnonymous(true);
            return anonim;
        }
        return userContext.get();
    }

    public void setCurrentUser(MlUser mlUser) {
        log.trace("Set current user. [user = {}]", mlUser != null ? mlUser.getLogin() : null);
        userContext.set(mlUser);

        // add user info to logs
        String login = mlUser != null ? mlUser.getLogin() : ABSENT_USER_LOGIN;
        MDC.put(ML_USER_HEADER, login);
    }

    public void clearCurrentUser() {
        if (log.isTraceEnabled()) {
            MlUser ngpUser = getCurrentUser();
            log.trace("Clear current user. [user = {}]", ngpUser != null ? ngpUser.getLogin() : null);
        }
        userContext.remove();
        MDC.remove(ML_USER_HEADER);
    }

}
