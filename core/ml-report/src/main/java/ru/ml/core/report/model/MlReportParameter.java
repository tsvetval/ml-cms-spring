package ru.ml.core.report.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.ml.platform.core.model.MlClass;
import ru.ml.platform.core.model.MlDynamicEntityImpl;

/**
 * Created by d_litovchenko on 31.03.15.
 */
public class MlReportParameter extends MlDynamicEntityImpl {

    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;
    }

    protected static class Properties {
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String CODE = "code";
        public static final String TYPE = "type";
        public static final String DEFAULT_VALUE = "defaultValue";
        public static final String VIEW_HANDLER = "viewHandler";
        public static final String MANDATORY = "mandatory";
        public static final String ML_CLASS = "mlClass";
        public static final String REPORT = "report";
        public static final String FILTER = "filter";
        public static final String SINGLE_CHOICE = "singleChoice";
        public static final String LISTEN_PARAM = "listenParam";
        public static final String LISTEN_PARAM_ADDITIONAL_QUERY = "listenParamAdditionalQuery";
    }

    public Long getId() {
        return get(Properties.ID);
    }

    public Boolean getMandatory() {
        return get(Properties.MANDATORY);
    }

    public String getCode() {
        return get(Properties.CODE);
    }

    public String getName() {
        return get(Properties.NAME);
    }

    public String getViewHandler() {
        return get(Properties.VIEW_HANDLER);
    }

    public String getFilter() {
        return get(Properties.FILTER);
    }

    public ReportParameterType getParameterType() {
        return ReportParameterType.valueOf((String) get(Properties.TYPE));
    }

    public Boolean getSingleChoice() {
        return get(Properties.SINGLE_CHOICE);
    }

    public MlClass getLinkMlClass() {
        return get(Properties.ML_CLASS);
    }

    public MlReportParameter getListenParam() {
        return get(Properties.LISTEN_PARAM);
    }

    public MlReport getReport() {
        return get(Properties.REPORT);
    }

    public String getListenParamAdditionalQuery() {
        return get(Properties.LISTEN_PARAM_ADDITIONAL_QUERY);
    }
}
