package ru.ml.core.report.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.ml.core.report.service.ReportGenerator;
import ru.ml.platform.core.model.MlDynamicEntityImpl;
import ru.ml.platform.core.model.exceptions.MlServerException;

import java.util.List;

/**
 * Created by d_litovchenko on 31.03.15.
 */
public class MlReport extends MlDynamicEntityImpl {

    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;
    }

    protected static class Properties {
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String TEMPLATE = "template";
        public static final String TEMPLATE_FILENAME = "template_filename";
        public static final String CONTROLLER = "controller";
        public static final String PARAMETERS = "parameters";
        public static final String REPORT_FILE_NAME = "reportFileName";
    }

    public Long getId() {
        return get(Properties.ID);
    }

    public String getName() {
        return get(Properties.NAME);
    }

    public String getReportFileName() {
        return get(Properties.REPORT_FILE_NAME);
    }

    public byte[] getTemplate() {
        return get(Properties.TEMPLATE);
    }

    public List<MlReportParameter> getParameters() {
        return get(Properties.PARAMETERS);
    }

    public MlReportParameter findParameterByCode(String code) {
        for (MlReportParameter parameter : getParameters()) {
            if (parameter.getCode().equals(code)) {
                return parameter;
            }
        }
        throw new MlServerException(String.format("Параметр с кодом %s не найден", code));
    }

    public boolean hasController() {
        return get(Properties.CONTROLLER) != null;
    }

    public <T extends ReportGenerator> Class<T> getController() throws ClassNotFoundException {
        return (Class<T>) Class.forName(get(Properties.CONTROLLER));
    }
}
