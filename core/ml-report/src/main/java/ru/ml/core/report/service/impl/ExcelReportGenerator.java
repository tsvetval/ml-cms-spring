package ru.ml.core.report.service.impl;

import net.sf.jxls.transformer.XLSTransformer;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ml.core.report.model.MlExcelReport;
import ru.ml.core.report.model.MlExcelReportQuery;
import ru.ml.core.report.model.MlReport;
import ru.ml.core.report.model.MlReportParameter;
import ru.ml.core.report.service.ReportGenerator;
import ru.ml.platform.context.CommonDao;
import ru.ml.platform.core.model.MlDynamicEntityImpl;
import ru.ml.platform.core.model.exceptions.MlServerException;

import javax.persistence.Parameter;
import javax.persistence.Query;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by d_litovchenko on 01.04.15.
 */
@Service
public class ExcelReportGenerator implements ReportGenerator {
    @Autowired
    CommonDao commonDao;

    @Override
    public byte[] generateReport(MlReport mlReport, Map<String, Object> reportData) throws InvalidFormatException {
        MlExcelReport mlExcelReport = (MlExcelReport) mlReport;
        for (MlExcelReportQuery reportQuery : mlExcelReport.getQueries()) {
            List resultList = executeQuery(reportQuery, reportData, mlReport);
            reportData.put(reportQuery.getCode(), resultList);
        }
        ByteArrayOutputStream baOs = new ByteArrayOutputStream();
        ByteArrayInputStream baIs = new ByteArrayInputStream(mlExcelReport.getTemplate());
        try {
            XLSTransformer xlsTransformer = new XLSTransformer();
            Workbook workbook = xlsTransformer.transformXLS(baIs, reportData);
            workbook.write(baOs);
        } catch (IOException e) {
            throw new MlServerException("Ошибка при генерации отчета ", e);
        }
        return baOs.toByteArray();
    }

    private List executeQuery(MlExcelReportQuery reportQuery, Map<String, Object> reportData, MlReport mlReport) {
        Query query = commonDao.getQueryWithoutSecurityCheck(reportQuery.getQuery());
        for (Parameter parameter : query.getParameters()) {
            MlReportParameter reportParameter = mlReport.findParameterByCode(parameter.getName());
            switch (reportParameter.getParameterType()) {
                case LINK:
                    if (reportParameter.getSingleChoice()) {
                        //проставляем в параметр запроса только primaryKey
                        MlDynamicEntityImpl entity = (MlDynamicEntityImpl) reportData.get(parameter.getName());
                        query.setParameter(parameter.getName(), entity.get(reportParameter.getLinkMlClass().getPrimaryKeyAttr().getEntityFieldName()));
                    } else {
                        //собираем лист primaryKey и подставляем в запрос
                        List values = new ArrayList();
                        List<MlDynamicEntityImpl> entities = (List<MlDynamicEntityImpl>) reportData.get(parameter.getName());
                        for (MlDynamicEntityImpl entity : entities) {
                            values.add(entity.get(reportParameter.getLinkMlClass().getPrimaryKeyAttr().getEntityFieldName()));
                        }
                        query.setParameter(parameter.getName(), values);
                    }
                    break;
                default:
                    query.setParameter(parameter.getName(), reportData.get(parameter.getName()));
            }
        }
        return query.getResultList();
    }
}
