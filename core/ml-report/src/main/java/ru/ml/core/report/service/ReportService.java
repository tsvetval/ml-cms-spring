package ru.ml.core.report.service;

import ru.ml.core.report.model.MlReport;
import ru.ml.core.report.service.dto.ReportResult;

import java.util.Map;

/**
 * Created by d_litovchenko on 31.03.15.
 */
public interface ReportService {
    public ReportResult generateReport(MlReport mlReport, Map<String, Object> reportData);
}
