package ru.ml.core.report.service.impl;

import net.sf.jxls.transformer.XLSTransformer;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ml.core.report.model.MlExcelReport;
import ru.ml.core.report.model.MlExcelReportQuery;
import ru.ml.core.report.model.MlReport;
import ru.ml.core.report.model.MlReportParameter;
import ru.ml.core.report.service.ReportGenerator;
import ru.ml.platform.context.CommonDao;
import ru.ml.platform.core.model.MlDynamicEntityImpl;
import ru.ml.platform.core.model.exceptions.MlServerException;

import javax.persistence.Query;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ExcelReportNativeSQLGenerator implements ReportGenerator {
    @Autowired
    CommonDao commonDao;

    @Override
    public byte[] generateReport(MlReport mlReport, Map<String, Object> reportData) throws InvalidFormatException {
        MlExcelReport mlExcelReport = (MlExcelReport) mlReport;
        for (MlExcelReportQuery reportQuery : mlExcelReport.getQueries()) {
            List resultList = executeQuery(reportQuery, reportData, mlReport);
            reportData.put(reportQuery.getCode(), resultList);
        }
        ByteArrayOutputStream baOs = new ByteArrayOutputStream();
        ByteArrayInputStream baIs = new ByteArrayInputStream(mlExcelReport.getTemplate());
        try {
            XLSTransformer xlsTransformer = new XLSTransformer();
            Workbook workbook = xlsTransformer.transformXLS(baIs, reportData);
            workbook.write(baOs);
        } catch (IOException e) {
            throw new MlServerException("Ошибка при генерации отчета ", e);
        }
        return baOs.toByteArray();
    }

    private List executeQuery(MlExcelReportQuery reportQuery, Map<String, Object> reportData, MlReport mlReport) {
        String sql = reportQuery.getQuery();
        Map<Integer, Object> paramMap = new HashMap<>();
        sql = prepareQuery(sql, reportData, paramMap, mlReport);
        Query query = commonDao.createNativeQuery(sql);

        for (Integer index : paramMap.keySet()) {
            query.setParameter(index, paramMap.get(index));
        }
        query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
        /*for (Parameter parameter : query.getParameters()) {

        }*/
        return query.getResultList();
    }

    private String prepareQuery(String sql, Map<String, Object> reportData, Map<Integer, Object> paramMap, MlReport mlReport) {
        String resulSql = "";
        Integer index = 1;
        while (sql.indexOf(":") > 0) {
            resulSql += sql.substring(0, sql.indexOf(":")) + "?";
            sql = sql.substring(sql.indexOf(":"));
            String param = sql.indexOf(" ") > 0 ? sql.substring(sql.indexOf(":") + 1, sql.indexOf(" ")) : sql.substring(sql.indexOf(":") + 1);
            Object paramVal = reportData.get(param);
            MlReportParameter reportParameter = mlReport.findParameterByCode(param);
            switch (reportParameter.getParameterType()) {
                case LINK:
                    if (reportParameter.getSingleChoice()) {
                        //проставляем в параметр запроса только primaryKey
                        MlDynamicEntityImpl entity = (MlDynamicEntityImpl) reportData.get(param);
                        paramMap.put(index, entity.get(reportParameter.getLinkMlClass().getPrimaryKeyAttr().getEntityFieldName()));
                    } else {
                        //собираем лист primaryKey и подставляем в запрос
                        List values = new ArrayList();
                        List<MlDynamicEntityImpl> entities = (List<MlDynamicEntityImpl>) reportData.get(param);
                        for (MlDynamicEntityImpl entity : entities) {
                            values.add(entity.get(reportParameter.getLinkMlClass().getPrimaryKeyAttr().getEntityFieldName()));
                        }
                        paramMap.put(index, values);
                    }
                    break;
                default:
                    paramMap.put(index, paramVal);
            }
            index++;
            sql = sql.substring(sql.indexOf(param) + param.length());

        }
        if (sql.length() > 0) {
            resulSql += sql;
        }
        return resulSql;
    }
}
