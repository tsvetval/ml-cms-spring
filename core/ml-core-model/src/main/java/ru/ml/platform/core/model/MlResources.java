package ru.ml.platform.core.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;

/**
 * Created by d_litovchenko on 28.01.15.
 */
public class MlResources extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }

    protected static class Properties{
        public static final String id = "id";
        public static final String url = "url";
        public static final String description = "description";
        public static final String javaClass = "javaClass";
        public static final String type = "type";
        public static final String guid = "guid";
    }

    public String getId(){return get(Properties.id);}
    public String getUrl(){return get(Properties.url);}
    public String getDescription(){return get(Properties.description);}
    public String getJavaClass(){return get(Properties.javaClass);}
    public String getGuid(){return get(Properties.guid);}
    public String getResourceType(){return get(Properties.type);}


    public void setId(String value){set(Properties.id,value);}
    public void setUrl(String value){set(Properties.url,value);}
    public void setDescription(String value){set(Properties.description,value);}
    public void setJavaClass(String value){set(Properties.javaClass,value);}
    public void setType(String value){set(Properties.type, value);}
    public void setGuid(String value){set(Properties.guid, value);}

    public String toString() {
        return "("+getUrl()+")"+getId()+"="+getDescription();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MlResources) {
            return this.getId().equals(((MlResources) obj).getId());
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        if (this.getId() == null) {
            return super.hashCode();
        } else {
            return this.getId().hashCode();
        }
    }
}
