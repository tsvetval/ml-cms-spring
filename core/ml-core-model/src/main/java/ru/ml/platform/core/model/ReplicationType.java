package ru.ml.platform.core.model;

/**
 */
public enum ReplicationType {
    NOT_REPLICATE,
    REPLICATE,
    CASCADE_REPLICATE
}
