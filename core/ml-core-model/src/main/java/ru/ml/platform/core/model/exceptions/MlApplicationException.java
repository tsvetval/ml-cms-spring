package ru.ml.platform.core.model.exceptions;

public class MlApplicationException extends RuntimeException {
    private Integer responceStartus = 400;//todo изменнить
    public MlApplicationException(String message) {
        super(message);
    }

    public MlApplicationException(String message, Throwable cause) {
        super(message, cause);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public MlApplicationException(String message, Throwable cause, Integer responceStartus) {
        super(message, cause);
        this.responceStartus = responceStartus;
    }

    public MlApplicationException(String message, Integer responceStartus) {
        super(message);
        this.responceStartus = responceStartus;
    }

    public Integer getResponceStartus() {
        return responceStartus;
    }
}
