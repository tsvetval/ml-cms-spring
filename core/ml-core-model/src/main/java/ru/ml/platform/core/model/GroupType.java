package ru.ml.platform.core.model;

/**
 * Created by i_tovstyonok on 12.10.2015.
 */
public enum GroupType {
    DEFAULT_GROUP, TAB_LIST, FIELDSET, ACCORDION
}
