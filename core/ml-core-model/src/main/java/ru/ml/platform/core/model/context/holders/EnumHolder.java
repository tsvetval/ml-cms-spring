package ru.ml.platform.core.model.context.holders;

import ru.ml.platform.core.model.MlAttr;
import ru.ml.platform.core.model.MlEnum;

import java.util.Collection;

/**
 *
 */
public interface EnumHolder {
    void addEnum(MlEnum newEnum);

    Collection<MlEnum> getEnumList(MlAttr IMlAttr);

    MlEnum getEnum(MlAttr IMlAttr, String enumCode);

    MlEnum getEnum(String className, String attrName, String enumCode);
}
