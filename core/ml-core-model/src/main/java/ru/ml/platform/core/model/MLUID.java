package ru.ml.platform.core.model;

import org.eclipse.persistence.dynamic.DynamicEntity;

/**
 *
 */
public class MLUID {
    private String className;
    private String guid;

    public MLUID(String className, String guid) {
        this.className = className;
        this.guid = guid;
    }

    public MLUID(String mlUID) {
        className = mlUID.split(";")[0];
        guid = mlUID.split(";")[1];
    }

    public MLUID(DynamicEntity mlDynamicEntity) {
        guid = mlDynamicEntity.get(MlAttr.GUID);
        /*className = GuiceConfigSingleton.inject(MetaDataHolder.class)
                .getMlClassByEntityDynamicClass(mlDynamicEntity.getClass()).getEntityName();*/
    }

    @Override
    public String toString() {
        return className + ";" + guid;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public static boolean isMLUID(String value) {
        if (value.split(";").length == 3) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int result = className.hashCode();
        result = 31 * result + guid.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MLUID mluid = (MLUID) o;

        if (!className.equals(mluid.className)) return false;
        return guid.equals(mluid.guid);

    }
}
