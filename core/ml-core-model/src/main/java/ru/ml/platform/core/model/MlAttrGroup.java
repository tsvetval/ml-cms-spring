package ru.ml.platform.core.model;


import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import org.springframework.util.comparator.Comparators;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class MlAttrGroup extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    private List<MlAttrGroup> subGroups;
    private List<MlAttr> attrList;

    public static final String ID = "id";
    public static final String TITLE = "title";
    public static final String PARENT = "parent";
    public static final String LINKED_CLASS = "linkedClass";
    public static final String LAZY_LOAD = "lazyLoad";
    public static final String GROUP_TYPE = "groupType";

    public Long getId() {
        return get(ID);
    }

    @Override
    public String getTitle() {
        return get(TITLE);
    }

    public MlAttrGroup getParent() {
        return get(PARENT);
    }

    public Boolean isLazyLoad() {
        return get(LAZY_LOAD) == null ? false : get(LAZY_LOAD);
    }

    public MlClass getMlClass() {
        return get(LINKED_CLASS);
    }

    //todo; implement?
    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public List<MlAttrGroup> getSubGroups() {
        if (subGroups == null) {
            subGroups = new ArrayList<>();
            if (this.getMlClass().getGroupList() != null) {
                for (MlAttrGroup group : this.getMlClass().getGroupList()) {
                    if (group.getParent() != null && group.getParent().equals(this)) {
                        subGroups.add(group);
                    }
                }
            }
        }
        return subGroups;
    }

    public List<MlAttr> getAttrList() {
        if (attrList == null) {
            attrList = new ArrayList<>();
            if (this.getMlClass().getAttrSet() != null) {
                for (MlAttr attr : this.getMlClass().getAttrSet()) {
                    if (attr.getGroup() != null && attr.getGroup().equals(this)) {
                        attrList.add(attr);
                    }
                }
            }
            attrList.sort((o1, o2) ->
                    Comparators.nullsHigh().compare(o1.getViewPos(), o2.getViewPos())
            );
        }
        return attrList == null ? new ArrayList<MlAttr>() : attrList;
    }

    private List<MlAttr> inFormAttrList;

    public List<MlAttr> getInFormAttrList() {
        if (inFormAttrList == null) {
            inFormAttrList = new ArrayList<>();
            for (MlAttr attr : this.getAttrList()) {
                if (attr.getInForm()) {
                    inFormAttrList.add(attr);
                }
            }
        }
        return inFormAttrList == null ? new ArrayList<MlAttr>() : inFormAttrList;
    }

    public GroupType getGroupType() {
        return GroupType.valueOf(get(GROUP_TYPE));
    }

    public void setGroupType(GroupType value) {
        set(GROUP_TYPE, value.name());
    }

}
