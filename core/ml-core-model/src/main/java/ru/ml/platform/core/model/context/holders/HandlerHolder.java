package ru.ml.platform.core.model.context.holders;

import ru.ml.platform.core.model.context.handler.MlClassHandler;

import java.util.Collection;

public interface HandlerHolder<T extends MlClassHandler>{

     void init(Collection<T> handlers);

     T getHolder(Class<T> cls);
}
