package ru.ml.platform.core.model.security;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.ml.platform.core.model.MlDynamicEntityImpl;
import ru.ml.platform.core.model.MlResources;

import java.util.List;

/**
 *
 */
public class MlResourceAccess extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    public Long getId() {
        return (Long) this.getPropertiesMap().get("id").getValue();
    }

    public void setId(Long id) {
        set("id", id);
    }

    public List<MlResources> getResources() {
        return get("resources");
    }

    public void setResources(List<MlResources> resources) {
        set("resources", resources);
    }

    public String getName() {
        return get("name");
    }

    public void setName(String name) {
        set("name", name);
    }

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
