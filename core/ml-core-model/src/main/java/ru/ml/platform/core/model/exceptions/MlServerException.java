package ru.ml.platform.core.model.exceptions;

public class MlServerException extends RuntimeException {
    private Integer responceStartus = 555;//todo изменнить

    public MlServerException(String message, Throwable cause) {
        super(message, cause);
    }

    public MlServerException(String message) {
        super(message);
    }

    public MlServerException(String message, Throwable cause, Integer responceStartus) {
        super(message, cause);
        this.responceStartus = responceStartus;
    }

    public MlServerException(String message, Integer responceStartus) {
        super(message);
        this.responceStartus = responceStartus;
    }

    public Integer getResponceStartus() {
        return responceStartus;
    }
}
