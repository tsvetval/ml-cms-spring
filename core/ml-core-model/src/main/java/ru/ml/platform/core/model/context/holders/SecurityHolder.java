package ru.ml.platform.core.model.context.holders;

import ru.ml.platform.core.model.MlAttr;
import ru.ml.platform.core.model.MlClass;
import ru.ml.platform.core.model.MlFolder;
import ru.ml.platform.core.model.MlUtil;
import ru.ml.platform.core.model.security.MlRole;

import java.util.List;
import java.util.Set;

public interface SecurityHolder {
    void init(List<MlRole> mlRoles);

    void addRole(MlRole role);

    Set<MlFolder> getRootFolders(List<MlRole> roles);

    boolean checkAccessFolder(List<MlRole> roles, MlFolder folder);

    boolean checkAccessUtil(List<MlRole> roles, MlUtil util);

    boolean checkAccessClassCreate(List<MlRole> roles, MlClass mlClass);
    boolean checkAccessClassRead(List<MlRole> roles, MlClass mlClass);
    boolean checkAccessClassUpdate(List<MlRole> roles, MlClass mlClass);
    boolean checkAccessClassDelete(List<MlRole> roles, MlClass mlClass);

    boolean checkAccessAttrShow(List<MlRole> roles, MlAttr mlAttr);
    boolean checkAccessAttrEdit(List<MlRole> roles, MlAttr mlAttr);
    List<String> getAdditionalQueryForClass(List<MlRole> roles, MlClass mlClass);

    boolean checkAccessResource(List<MlRole> roles, String id);

}
