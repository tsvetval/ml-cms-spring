package ru.ml.platform.core.model.context;

import lombok.Getter;
import ru.ml.platform.core.model.context.holders.EnumHolder;
import ru.ml.platform.core.model.context.holders.HandlerHolder;
import ru.ml.platform.core.model.context.holders.MetaDataHolder;
import ru.ml.platform.core.model.context.holders.SecurityHolder;
import ru.ml.platform.core.model.exceptions.MlApplicationException;

@Getter
public class MlContext {
    private static final MlContext instance = new MlContext();

    private EnumHolder enumHolder;
    private MetaDataHolder metaDataHolder;
    private HandlerHolder handlerHolder;
    private SecurityHolder securityHolder;

    private MlContext(EnumHolder enumHolder, MetaDataHolder metaDataHolder, HandlerHolder handlerHolder, SecurityHolder securityHolder) {
        this.enumHolder = enumHolder;
        this.metaDataHolder = metaDataHolder;
        this.handlerHolder = handlerHolder;
        this.securityHolder = securityHolder;
    }

    public MlContext() {
    }

    public static void initInstance(EnumHolder enumHolder, MetaDataHolder metaDataHolder, HandlerHolder handlerHolder, SecurityHolder securityHolder) {
            synchronized (MlContext.class) {
                instance.enumHolder = enumHolder;
                instance.metaDataHolder = metaDataHolder;
                instance.handlerHolder = handlerHolder;
                instance.securityHolder = securityHolder;
            }
    }

    public static MlContext getInstance(){
        return instance;
    }
}
