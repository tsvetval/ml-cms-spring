package ru.ml.platform.core.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;

import java.util.List;

/**
 *
 */
public class MlUtil extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Long getId() {
        return get("id");
    }

    public Long getOrderNumber() {
        return get("orderNumber");
    }

    public String getButtonLabel() {
        return get("buttonLabel");
    }

    public String getButtonImgUrl() {
        return get("buttonImgUrl");
    }

    public String getButtonIcon() {
        return get("buttonIcon");
    }


    public String getUrl() {
        return get("url");
    }

    public String getMethod() {
        return get("method");
    }

    public String getOpenType() {
        return get("openType");
    }

    public String getDesription() {
        return get("description");
    }
    public List<MlClass> getMlClasses() {
        return get("mlClasses");
    }
    public List<MlFolder> getMlFolders() {
        return get("folders");
    }

    public Boolean getConfirmExec() {
        return get("confirmExec");
    }

    public String getConfirmExecMsg() {
        return get("confirmExecMsg");
    }

    public String getUtilBootJs(){
        return get("utilBootJs");
    }

    public Boolean getShowInAllObjects(){
        return get("showInAllObjects");
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MlUtil) {
            return this.getId().equals(((MlUtil) obj).getId());
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        if (this.getId() == null) {
            return super.hashCode();
        } else {
            return this.getId().intValue();
        }
    }
}
