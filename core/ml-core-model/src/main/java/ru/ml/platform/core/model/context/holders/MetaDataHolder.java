package ru.ml.platform.core.model.context.holders;

import ru.ml.platform.core.model.MlAttr;
import ru.ml.platform.core.model.MlAttrGroup;
import ru.ml.platform.core.model.MlClass;

import java.util.Collection;
import java.util.List;

/**
 *
 */
public interface MetaDataHolder {

    void updateMlClass(MlClass mlClass) throws ClassNotFoundException;

    void addMlClass(MlClass clazz, Class entityClass) throws ClassNotFoundException;

    void addMlAttr(MlAttr attr);

    MlAttr getAttrById(Long id);

    MlClass getMlClassByName(String mlClassName);

    MlClass getMlClassById(Long id);

    Class getEntityClassByName(String entityClassName);

    MlAttr getAttr(MlClass mlClass, String attrName);

    MlAttr getAttr(String mlClass, String attrName);

    List<MlAttr> getPrimaryKeyAttrList(MlClass mlClass);

    MlClass getMlClassByEntityDynamicClass(Class clazz);

    List<MlAttrGroup> getGroupList(MlClass mlClass);

    List<MlAttr> getInListAttrList(MlClass mlClass);
    List<MlAttr> getInFormAttrList(MlClass mlClass);
    List<MlAttr> getPrimaryKey(MlClass mlClass);

    List<MlAttr> getSimpleSearchAttrList(MlClass mlClass);

    List<MlAttr> getExtendedSearchAttrList(MlClass mlClass);

    List<MlClass> getChildrenByClassName(String parentName);

    MlAttr findAttrByPath(MlClass mlClass, String path);
    Collection<MlClass> getAllMlClasses();
}
