package ru.ml.platform.core.model;

public enum MlClassAccessType {
    CREATE ("создание"),
    READ ("чтение"),
    UPDATE ("изменение"),
    DELETE ("удаление");

    private final String operationName;

    public String getOperationName() {
        return operationName;
    }

    MlClassAccessType(String operationName) {
        this.operationName = operationName;
    }
}
