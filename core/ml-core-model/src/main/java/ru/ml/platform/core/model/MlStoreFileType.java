package ru.ml.platform.core.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;

/**
 *
 */
public class MlStoreFileType extends MlDynamicEntityImpl {
    public static final String MlStoreFileType_ID = "id";
    public static final String MlStoreFileType_NAME = "name";
    public static final String MlStoreFileType_TYPE = "type";
    public static final String MlStoreFileType_PATH = "path";

    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    public Long getId() {
        return get(MlStoreFileType_ID);
    }

    public String getName() {
        return get(MlStoreFileType_NAME);
    }

    public StoreFileType getStoreType() {
        return StoreFileType.valueOf(get(MlStoreFileType_TYPE));
    }

    public String getPath() {
        return get(MlStoreFileType_PATH);
    }

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
