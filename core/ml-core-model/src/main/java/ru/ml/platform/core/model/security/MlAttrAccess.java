package ru.ml.platform.core.model.security;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.ml.platform.core.model.MlAttr;
import ru.ml.platform.core.model.MlDynamicEntityImpl;

/**
 *
 */
public class MlAttrAccess extends MlDynamicEntityImpl {

    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    public Long getId() {
        return (Long) this.getPropertiesMap().get("id").getValue();
    }

    public void setId(Long id) {
        set("id", id);
    }

    public MlAttr getMlAttr() {
        return get("mlAttr");
    }

    public Boolean isNotShow() {
        return get("notShow");
    }

    public Boolean isNotEdit() {
        return get("notEdit");
    }

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;
    }
}
