package ru.ml.platform.core.model.security;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.ml.platform.core.model.MlDynamicEntityImpl;
import ru.ml.platform.core.model.MlFolder;

import java.util.List;

/**
 *
 */
public class MlFolderAccess extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    public Long getId() {
        return (Long) this.getPropertiesMap().get("id").getValue();
    }

    public void setId(Long id) {
        set("id", id);
    }

    public String getName() {
        return get("name");
    }

    public void setName(String name) {
        set("name", name);
    }

    public List<MlFolder> getFolders() {
        return get("folders");
    }

    public void setFolders(List<MlFolder> folders) {
        set("folders", folders);
    }

    /*public Boolean isShowChildren(){
        return get("showChildren");
    }
*/
    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
