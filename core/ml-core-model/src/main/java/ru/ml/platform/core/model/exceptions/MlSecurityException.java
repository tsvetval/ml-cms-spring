package ru.ml.platform.core.model.exceptions;

/**
 *
 */
public class MlSecurityException extends RuntimeException {
    private Integer responceStartus = 403;//todo изменнить

    public MlSecurityException(String message) {
        super(message);
    }

    public MlSecurityException(String message, Throwable cause) {
        super(message, cause);
    }

    public MlSecurityException(String message, Integer responceStartus) {
        super(message);
        this.responceStartus = responceStartus;
    }

    public MlSecurityException(String message, Throwable cause, Integer responceStartus) {
        super(message, cause);
        this.responceStartus = responceStartus;
    }

    public Integer getResponceStartus() {
        return responceStartus;
    }
}
