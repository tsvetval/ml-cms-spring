package ru.ml.platform.core.handler.impl.mlclass.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ml.platform.context.CommonDao;
import ru.ml.platform.core.handler.validator.Validator;
import ru.ml.platform.core.model.MlClass;
import ru.ml.platform.core.model.exceptions.MlApplicationException;

import javax.persistence.Query;
import java.util.List;
import java.util.regex.Pattern;

@Component
public class MlClassNameValidator implements Validator<MlClass> {
    private static final Logger log = LoggerFactory.getLogger(MlClassNameValidator.class);
    private static String emptyEntityName = "Кодовое имя не может быть пустым.";
    private static String emptyTableName = "Табличное имя не может быть пустым.";
    private static String notUniqueEntityName = "Кодовое имя должно быть уникальным.";
    private static String notUniqueTableName = "Табличное имя должно быть уникальным.";
    private static String badEntityName = "Кодовое имя может содержать буквы латинского алфавита, цифры и символ нижнего подчеркивания. Должно начинаться с буквы и содержать не менее двух символов.";
    private static String badTableName = "Табличное имя может содержать буквы латинского алфавита, цифры и символ нижнего подчеркивания. Должно начинаться с буквы и содержать не менее двух символов.";

    @Autowired
    CommonDao commonDao;

    @Override
    public void validate(MlClass mlClass) {
        String regexp = "^[a-zA-Z][a-zA-Z0-9_]+$";

        if (mlClass.getEntityName() == null || mlClass.getEntityName().equals("")) {
            log.error("Entity name is empty");
            throw new MlApplicationException(emptyEntityName);
        }
        if (mlClass.getTableName() == null || mlClass.getTableName().equals("")) {
            log.error("Table name is empty");
            throw new MlApplicationException(emptyTableName);
        }

        if (!Pattern.matches(regexp, mlClass.getEntityName())) {
            log.error("Bad entity name");
            throw new MlApplicationException(badEntityName);
        }
        if (!Pattern.matches(regexp, mlClass.getTableName())) {
            log.error("Bad table name");
            throw new MlApplicationException(badTableName);
        }
        Query query = commonDao.getQueryWithoutSecurityCheck("select o from MlClass o where o.entityName = :entityName").setParameter("entityName", mlClass.getEntityName());
        List<MlClass> mlClasses = query.getResultList();
        if (mlClasses.size() > 0) {
            for (MlClass mlClazz : mlClasses) {
                if (!mlClazz.getId().equals(mlClass.getId())) {
                    log.error("Entity name not unique");
                    throw new MlApplicationException(notUniqueEntityName);
                }
            }
        }
        query = commonDao.getQueryWithoutSecurityCheck("select o from MlClass o where o.tableName = :tableName").setParameter("tableName", mlClass.getTableName());
        mlClasses = query.getResultList();
        if (mlClasses.size() > 0) {
            for (MlClass mlClazz : mlClasses) {
                if (!mlClazz.getId().equals(mlClass.getId())) {
                    log.error("Table name not unique");
                    throw new MlApplicationException(notUniqueTableName);
                }
            }
        }
    }
}
