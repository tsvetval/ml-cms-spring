package ru.ml.platform.core.handler.impl.mlattr.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ml.platform.core.model.MlAttr;
import ru.ml.platform.core.model.MlClass;
import ru.ml.platform.core.model.context.MlContext;
import ru.ml.platform.core.model.exceptions.MlApplicationException;

@Service
public class MlAttrValueValidator {
    private static final Logger log = LoggerFactory.getLogger(MlAttrValueValidator.class);
    private static final String wrongLongLink = "Дальняя ссылка %s имеет неверный формат. Атрибут с кодовым именем %s отсутствует в классе %s";
    private static final String wrongLongLinkNotEnd = "Дальняя ссылка %s имеет неверный формат. Ссылка должна оканчиваться именем атрибута";

    MlContext mlContext = MlContext.getInstance();

    public void validate(MlAttr mlAttr) {
        switch (mlAttr.getFieldType()) {
            case LONG_LINK:
                longLinkCheck(mlAttr);
                break;
        }
    }

    private void longLinkCheck(MlAttr mlAttr) {
        String link = mlAttr.getLongLinkValue();
        if (link.endsWith("->")) {
            log.error("Wrong longLink " + link);
            throw new MlApplicationException(String.format(wrongLongLinkNotEnd, link));
        }
        String[] fields = link.split("->");
        MlClass mlClass = mlAttr.getMlClass();
        for (int i = 0; i < fields.length; i++) {
            String field = fields[i];


            MlAttr attr = mlContext.getMetaDataHolder().getAttr(mlClass, field);
            if (attr == null) {
                log.error("Wrong longLink " + link + " attribute " + field + " not exist in class " + mlClass.getDescription());
                throw new MlApplicationException(String.format(wrongLongLink, link, field, mlClass.getDescription()));
            } else {
                if ((i + 1) != fields.length) {
                    mlClass = attr.getLinkClass();
                }
            }
        }
    }
}
