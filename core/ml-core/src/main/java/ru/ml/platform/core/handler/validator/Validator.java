package ru.ml.platform.core.handler.validator;

import org.eclipse.persistence.internal.dynamic.DynamicEntityImpl;

/**
 */
public interface Validator<T extends DynamicEntityImpl> {
    void validate(T entity);
}
