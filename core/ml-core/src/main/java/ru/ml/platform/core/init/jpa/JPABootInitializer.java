package ru.ml.platform.core.init.jpa;

import org.eclipse.persistence.jpa.dynamic.JPADynamicTypeBuilder;
import ru.ml.platform.core.model.system.MlClassSystemModel;

import java.util.Collection;
import java.util.Map;

/**
 *
 */
public interface JPABootInitializer {
    Map<String, JPADynamicTypeBuilder> initializeClasses(Collection<MlClassSystemModel> detachedMlClassSystemModelList, Boolean fullReload) throws ClassNotFoundException;
}
