package ru.ml.platform.core.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ml.platform.context.CommonDao;
import ru.ml.platform.core.model.exceptions.MlApplicationException;

import java.util.List;


/**
 * Created by d_litovchenko on 04.03.15.
 */
@Component
public class JPQLSingleResultValueGenerator extends DefaultValueGenerator {
    public static final String COMMAND_NAME = "#JPQL_SINGLE_RESULT=";
    @Autowired
    CommonDao commonDao;

    @Override
    public Object generateValue() {
        String defaultValue = mlAttr.getDefaultValue().toString();
        String jpql = defaultValue.replaceFirst(COMMAND_NAME, "");
        List data = commonDao.getTypedQueryWithoutSecurityCheck(jpql, mlAttr.getFieldType().getaClass()).getResultList();
        if (data.size() != 1) {
            throw new MlApplicationException(String.format("Ошибка генерации значения по умолчанию для поля %s класса %s! Запрос вернул больше одной записи", mlAttr.getDescription(), mlAttr.getMlClass().getDescription()));
        }
        return data.get(0);
    }

}
