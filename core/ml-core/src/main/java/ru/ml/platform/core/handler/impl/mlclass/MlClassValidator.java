package ru.ml.platform.core.handler.impl.mlclass;

import org.eclipse.persistence.internal.sessions.ObjectChangeSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ml.platform.core.handler.impl.mlclass.validator.*;
import ru.ml.platform.core.handler.validator.AbstractValidator;
import ru.ml.platform.core.model.MlClass;

@Component
public class MlClassValidator extends AbstractValidator<MlClass> {
    @Autowired
    MlClassClassesValidator mlClassClassesValidator;
    @Autowired
    MlClassInheritanceValidator mlClassInheritanceValidator;
    @Autowired
    MlClassAttrValidator mlClassAttrValidator;
    @Autowired
    MlClassNameValidator mlClassNameValidator;
    @Autowired
    MlClassDeleteValidator mlClassDeleteValidator;

    @Override
    public void validateBeforeCreate(MlClass entity) {
        mlClassClassesValidator.validate(entity);
        mlClassInheritanceValidator.validate(entity);
        mlClassAttrValidator.validate(entity);
        mlClassNameValidator.validate(entity);
    }

    @Override
    public void validateBeforeUpdate(MlClass entity, ObjectChangeSet objectChangeSet) {
        mlClassClassesValidator.validate(entity);
        mlClassInheritanceValidator.validate(entity);
        mlClassInheritanceValidator.validateOnUpdate(entity, objectChangeSet);
        mlClassAttrValidator.validate(entity);
        mlClassNameValidator.validate(entity);
        mlClassAttrValidator.validateUpdate(entity, objectChangeSet);
    }

    @Override
    public void validateBeforeDelete(MlClass entity) {
        mlClassDeleteValidator.validate(entity);
        mlClassInheritanceValidator.validateOnDelete(entity);
    }
}
