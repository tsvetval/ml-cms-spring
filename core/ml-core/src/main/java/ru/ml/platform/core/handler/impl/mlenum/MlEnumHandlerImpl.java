package ru.ml.platform.core.handler.impl.mlenum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ml.platform.core.handler.DefaultMlClassHandler;
import ru.ml.platform.core.init.MlMetaDataInitializeService;
import ru.ml.platform.core.model.MlEnum;

@Component("MlEnumHandlerImpl")
public class MlEnumHandlerImpl extends DefaultMlClassHandler<MlEnum> {

    @Autowired
    MlMetaDataInitializeService mlMetaDataInitializeService;

    @Override
    public void beforeUpdate(MlEnum entity) {
        if (entity.get("title") == null) {
            entity.set("title", "");
        }
    }

    @Override
    public void beforeCreate(MlEnum entity) {
        if (entity.get("title") == null) {
            entity.set("title", "");
        }
    }

    @Override
    public void afterUpdate(MlEnum entity, boolean runBeforeHandlers) {
        mlMetaDataInitializeService.addEnum(entity);
    }

    @Override
    public void afterCreate(MlEnum entity) {
        mlMetaDataInitializeService.addEnum(entity);
    }
}
