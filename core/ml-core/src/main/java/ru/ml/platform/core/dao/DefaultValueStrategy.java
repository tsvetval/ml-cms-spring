package ru.ml.platform.core.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ml.platform.core.model.MlAttr;

@Component
public class DefaultValueStrategy {
    @Autowired
    JPQLSingleResultValueGenerator jpqlSingleResultValueGenerator;

    public DefaultValueGenerator getGenerator(MlAttr mlAttr) {
        DefaultValueGenerator defaultValueGenerator = new DoNothingValueGenerator();
        if (mlAttr.getDefaultValue().toString().startsWith(JPQLSingleResultValueGenerator.COMMAND_NAME)) {
            defaultValueGenerator = jpqlSingleResultValueGenerator;
        }
        defaultValueGenerator.setMlAttr(mlAttr);
        return defaultValueGenerator;
    }
}
