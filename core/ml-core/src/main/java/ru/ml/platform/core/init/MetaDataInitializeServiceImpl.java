package ru.ml.platform.core.init;

import org.eclipse.persistence.jpa.dynamic.JPADynamicTypeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ml.platform.core.model.context.MlContext;
import ru.ml.platform.core.model.context.holders.EnumHolder;
import ru.ml.platform.core.model.context.holders.MetaDataHolder;
import ru.ml.platform.core.init.jpa.JPABootInitializer;
import ru.ml.platform.core.init.jpa.JPADynamicClassInitializerService;
import ru.ml.platform.core.model.MlClass;
import ru.ml.platform.core.model.MlEnum;
import ru.ml.platform.core.model.system.MlClassSystemModel;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Service
public class MetaDataInitializeServiceImpl implements MlMetaDataInitializeService {
    private static final Logger log = LoggerFactory.getLogger(MetaDataInitializeServiceImpl.class);
    MlContext mlContext = MlContext.getInstance();

    @Autowired
    EntityManagerFactory emf;
    @Autowired
    JPADynamicClassInitializerService jpaClassInitializer;
    @Autowired
    JPABootInitializer jpaBootInitializer;

    @Override
    public void initializeAllMetaData() {
        // Bootstrap
        initializeBootSystemClasses();
    }

    @Override
    public void initializeClasses(Collection<MlClass> detachedMlClassList, Boolean fullReload) throws ClassNotFoundException {
        Map<String, JPADynamicTypeBuilder> result = jpaClassInitializer.initializeClasses(detachedMlClassList, false);
        //Для всех вновь проинициализированных сохраняем метоописание
        for (MlClass mlClass : detachedMlClassList) {
            emf.getCache().evict(result.get(mlClass.getEntityName()).getType().getJavaClass());
        }
        for (MlClass mlClass : detachedMlClassList) {
            mlContext.getMetaDataHolder().addMlClass(mlClass, result.get(mlClass.getEntityName()).getType().getJavaClass());
        }

    }

    //TODO remove
    public void addMlClass(MlClass clazz, Class entityClass) throws ClassNotFoundException {
        mlContext.getMetaDataHolder().addMlClass(clazz, entityClass);
    }

    //TODO remove
    public void updateMlClass(MlClass entity) throws ClassNotFoundException {
        mlContext.getMetaDataHolder().updateMlClass(entity);
    }

    public void addEnum(MlEnum newEnum) {
        mlContext.getEnumHolder().addEnum(newEnum);
    }

    private void initializeBootSystemClasses() {
        // Инициализируем основное классы сситемы (Класс и атрибут)
        log.debug("Select Cms Boot MetaData (select o from MlClassSystemModel o  where o.entityName = 'MlClass' or o.entityName = 'MlAttr' or o.entityName = 'MlAttrGroup' or o.entityName = 'MlEnum')");
        TypedQuery<MlClassSystemModel> query = emf.createEntityManager().createQuery(
                "select o from MlClassSystemModel o  " +
                        "where o.entityName = 'MlClass' " +
                        "or o.entityName = 'MlAttr' " +
                        "or o.entityName = 'MlAttrGroup' " +
                        "or o.entityName = 'MlAttrView' " +
                        "or o.entityName = 'MlStoreFileType' " +
                        "or o.entityName = 'MlEnum'", MlClassSystemModel.class);
        List<MlClassSystemModel> classList = query.getResultList();
        try {
            jpaBootInitializer.initializeClasses(classList, true);
            //jpaClassInitializer.initializeClasses(systemMlClassConverter.convertToMlClassList(classList), true);
        } catch (Throwable e) {
            log.error("!!!!!!!!!!!!! Error while initializing Meta Classes", e);
            //TODO
        }

        // Проводим инициализацию всех классов системы
        //em.clear();
        log.debug("Select All Cms MetaData (SELECT o FROM MlClass o)");
        emf.getCache().evictAll();
        EntityManager em = emf.createEntityManager();

        List<MlClass> allMlClassList = (List<MlClass>) em.createQuery("SELECT o FROM MlClass o ORDER BY o.id").getResultList();
        for (MlClass mlClass : allMlClassList) {
            mlClass.setMlContext(mlContext);
            em.detach(mlClass);
        }

        try {
            Map<String, JPADynamicTypeBuilder> result = jpaClassInitializer.initializeClasses(allMlClassList, true);
            //Для всех вновь проинициализированных сохраняем метоописание
            //TODO  сортируем классы в порядке иерархии

            for (MlClass mlClass : allMlClassList) {
                mlContext.getMetaDataHolder().addMlClass(mlClass, result.get(mlClass.getEntityName()).getType().getJavaClass());
            }
        } catch (Throwable e) {
            //TODO
            log.error("Error adding Meta data to MetaDataSingleton", e);
        }
        //em.clear();
        emf.getCache().evictAll();
        // Initialize MlEnum
        log.debug("Select All Cms MlEnum (SELECT o FROM MlEnum o)");
        em = emf.createEntityManager();
        try {
            List<MlEnum> enumList = (List<MlEnum>) em.createQuery("SELECT o FROM MlEnum o").getResultList();
            for (MlEnum mlEnum : enumList) {
                em.detach(mlEnum);
                mlContext.getEnumHolder().addEnum(mlEnum);
            }
        } catch (Throwable e) {
            log.error("Error initialize enums ", e);
        }

    }


    private List<MlClass> getAllParent(MlClass entity) {
        List<MlClass> result = new ArrayList<>();
        result.add(entity);
        MlClass parent = entity;
        do {
            parent = parent.getParent();
            if (parent != null) {
                result.add(parent);
            }
        } while (parent != null);
        return result;
    }

}
