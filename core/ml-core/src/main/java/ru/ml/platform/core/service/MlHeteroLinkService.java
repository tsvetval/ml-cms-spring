package ru.ml.platform.core.service;

import ru.ml.platform.core.model.MlDynamicEntityImpl;
import ru.ml.platform.core.model.MlHeteroLink;

/**
 * Created by d_litovchenko on 23.03.15.
 */
public interface MlHeteroLinkService {
    MlDynamicEntityImpl getObjectHeteroLink(MlHeteroLink link);
}
