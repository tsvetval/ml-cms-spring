package ru.ml.platform.core.init.jpa;

import org.eclipse.persistence.jpa.dynamic.JPADynamicTypeBuilder;
import ru.ml.platform.core.model.MlClass;

import java.util.Collection;
import java.util.Map;

/**
 *
 */
public interface JPADynamicClassInitializerService {

    Map<String, JPADynamicTypeBuilder> initializeClasses(Collection<MlClass> detachedMlClassList, Boolean fullReload) throws ClassNotFoundException;

    void deleteClassWriter(MlClass entity);
}
