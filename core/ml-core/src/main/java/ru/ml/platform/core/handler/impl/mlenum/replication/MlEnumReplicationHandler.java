package ru.ml.platform.core.handler.impl.mlenum.replication;

import org.eclipse.persistence.dynamic.DynamicEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ml.platform.core.handler.MlReplicationHandler;
import ru.ml.platform.core.init.MlMetaDataInitializeService;
import ru.ml.platform.core.model.MLUID;
import ru.ml.platform.core.model.MlEnum;

/**
 *
 */
@Service
public class MlEnumReplicationHandler implements MlReplicationHandler {
    @Autowired
    MlMetaDataInitializeService mlMetaDataInitializeService;

    @Override
    public void beforeReplication(MLUID mluid) {

    }

    @Override
    public void afterReplication(DynamicEntity dynamicEntity) {
        mlMetaDataInitializeService.addEnum((MlEnum) dynamicEntity);
    }
}
