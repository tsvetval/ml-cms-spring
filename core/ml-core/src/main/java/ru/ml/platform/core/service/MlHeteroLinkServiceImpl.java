package ru.ml.platform.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ml.platform.context.CommonDao;
import ru.ml.platform.core.model.MlDynamicEntityImpl;
import ru.ml.platform.core.model.MlHeteroLink;
import ru.ml.platform.core.model.context.MlContext;
import ru.ml.platform.core.model.exceptions.MlServerException;

/**
 * Created by d_litovchenko on 23.03.15.
 */
@Service
public class MlHeteroLinkServiceImpl implements MlHeteroLinkService {
    private static final Logger log = LoggerFactory.getLogger(MlHeteroLinkServiceImpl.class);
    MlContext mlContext = MlContext.getInstance();

    @Autowired
    CommonDao commonDao;


    @Override
    public MlDynamicEntityImpl getObjectHeteroLink(MlHeteroLink link) {
        MlDynamicEntityImpl entity = commonDao.findById(link.getObjectId(), mlContext.getMetaDataHolder().getEntityClassByName(link.getClassName()));
        if (entity == null) {
            log.error("MlHeteroLink not found " + link.getStrId());
            throw new MlServerException("Невозможно найти гетерогенную ссылку " + link.getStrId());
        }
        return entity;
    }
}
