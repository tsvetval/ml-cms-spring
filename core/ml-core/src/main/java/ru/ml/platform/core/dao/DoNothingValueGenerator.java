package ru.ml.platform.core.dao;

/**
 * Created by d_litovchenko on 04.03.15.
 */
public class DoNothingValueGenerator extends DefaultValueGenerator {

    @Override
    public Object generateValue() {
        return mlAttr.getDefaultValue();
    }

}
