package ru.ml.platform.core.handler;

import org.eclipse.persistence.dynamic.DynamicEntity;
import ru.ml.platform.core.model.MLUID;

/**
 *
 */
public interface MlReplicationHandler {
    public void beforeReplication(MLUID mluid);

    public void afterReplication(DynamicEntity dynamicEntity);
}
