package ru.ml.platform.core;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ru.ml.platform.context.MlContextConfiguration;
import ru.ml.platform.context.holders.impl.EnumHolderImpl;
import ru.ml.platform.context.holders.impl.HandlerHolderImpl;
import ru.ml.platform.context.holders.impl.MetaDataHolderImpl;
import ru.ml.platform.core.init.MlDynamicClassLoader;
import ru.ml.platform.core.model.context.MlContext;
import ru.ml.platform.core.model.context.handler.MlClassHandler;
import ru.ml.platform.core.model.context.holders.EnumHolder;
import ru.ml.platform.core.model.context.holders.HandlerHolder;
import ru.ml.platform.core.model.context.holders.MetaDataHolder;

@Configuration
@ComponentScan
public class MlCoreConfiguration {

    @Bean
    public MlDynamicClassLoader mlDynamicClassLoader() {
        return new MlDynamicClassLoader(Thread.currentThread().getContextClassLoader()/*currentLoader*/);
    }

}
