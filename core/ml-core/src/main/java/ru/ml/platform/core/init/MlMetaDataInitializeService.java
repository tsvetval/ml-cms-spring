package ru.ml.platform.core.init;


import ru.ml.platform.core.model.MlClass;
import ru.ml.platform.core.model.MlEnum;

import java.util.Collection;

/**
 *
 */
public interface MlMetaDataInitializeService {
    /**
     * Проводит инициализацию мета данных
     */
    public void initializeAllMetaData();

    public void initializeClasses(Collection<MlClass> detachedMlClassList, Boolean fullReload) throws ClassNotFoundException;


    /**
     * добавляет заданный класс в хранилище мета данных
     * @param entity объект  класса MlBootClass
     * @param entityClass  java класс для данного объекта
     * @throws ClassNotFoundException если для заданного класса не найден  entityClass
     */
    public void addMlClass(MlClass entity, Class entityClass) throws ClassNotFoundException;

    /**
     * добавляет заданный класс в хранилище мета данных
     * @param entity  entity объект  класса MlClass
     * @throws ClassNotFoundException
     */
    public void updateMlClass(MlClass entity) throws ClassNotFoundException;

    /**добавляет заданное перечислимое в хранилище мета данных
     *
     * @param newEnum перечислимое
     */
    public void addEnum(MlEnum newEnum);
}
