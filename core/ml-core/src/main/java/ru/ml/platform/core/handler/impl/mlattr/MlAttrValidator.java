package ru.ml.platform.core.handler.impl.mlattr;

import org.eclipse.persistence.internal.sessions.ObjectChangeSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ml.platform.core.handler.impl.mlattr.validator.MlAttrInheritanceValidator;
import ru.ml.platform.core.handler.impl.mlattr.validator.MlAttrNameValidator;
import ru.ml.platform.core.handler.impl.mlattr.validator.MlAttrTypeValidator;
import ru.ml.platform.core.handler.impl.mlattr.validator.MlAttrValueValidator;
import ru.ml.platform.core.handler.validator.AbstractValidator;
import ru.ml.platform.core.model.MlAttr;

/**
 *
 */
@Component
public class MlAttrValidator extends AbstractValidator<MlAttr> {
    @Autowired
    MlAttrNameValidator mlAttrNameValidator;
    @Autowired
    MlAttrValueValidator mlAttrValueValidator;
    @Autowired
    MlAttrTypeValidator mlAttrTypeValidator;
    @Autowired
    MlAttrInheritanceValidator mlAttrInheritanceValidator;

    @Override
    public void validateBeforeCreate(MlAttr entity) {
        mlAttrNameValidator.validate(entity);
        mlAttrValueValidator.validate(entity);
    }

    @Override
    public void validateBeforeUpdate(MlAttr entity, ObjectChangeSet objectChangeSet) {
        mlAttrNameValidator.validate(entity);
        mlAttrTypeValidator.validate(entity, objectChangeSet);
        mlAttrInheritanceValidator.validateOnChange(entity, objectChangeSet);
        mlAttrValueValidator.validate(entity);
    }

    @Override
    public void validateBeforeDelete(MlAttr entity) {
        mlAttrInheritanceValidator.validateOnDelete(entity);
    }
}
