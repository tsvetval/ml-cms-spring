package ru.ml.platform.core.handler.impl.mlfolder;

import org.eclipse.persistence.internal.descriptors.changetracking.AttributeChangeListener;
import org.eclipse.persistence.internal.sessions.ObjectChangeSet;
import org.eclipse.persistence.internal.sessions.ObjectReferenceChangeRecord;
import org.eclipse.persistence.sessions.changesets.ChangeRecord;
import org.springframework.stereotype.Component;
import ru.ml.platform.core.handler.DefaultMlClassHandler;
import ru.ml.platform.core.model.MlClass;
import ru.ml.platform.core.model.MlFolder;
import ru.ml.platform.core.model.exceptions.MlApplicationException;

@Component("MlFolderHandler")
public class MlFolderHandler extends DefaultMlClassHandler<MlFolder> {
    @Override
    public void beforeUpdate(MlFolder entity) {
        AttributeChangeListener changeListener = ((AttributeChangeListener) entity._persistence_getPropertyChangeListener());
        ObjectChangeSet objectChangeSet = changeListener.getObjectChangeSet();
        if (objectChangeSet == null) {
            return;
        }
        for (ChangeRecord changeRecord : objectChangeSet.getChanges()) {
            switch (changeRecord.getAttribute()) {
                case "childClass":
                    if (classChanged((ObjectReferenceChangeRecord) changeRecord)) {
                        if (entity.getAttrListToShow().size() > 0) {
                            throw new MlApplicationException("Для изменения Класса объектов очистите список Атрибутов для отображения!");
                        }
                        if (entity.getChildClassCondition() != null && !"".equals(entity.getChildClassCondition())) {
                            throw new MlApplicationException("Для изменения Класса объектов удалите Условие выбора объектов!");
                        }
                    }
                    break;
            }
        }
    }

    private boolean classChanged(ObjectReferenceChangeRecord changeRecord) {
        ObjectChangeSet newVal = (ObjectChangeSet) changeRecord.getNewValue();
        MlClass oldVal = changeRecord.getOldValue() == null ? null : (MlClass) changeRecord.getOldValue();
        if (newVal == null && oldVal != null) {
            return true;
        } else if (newVal != null && oldVal != null && !newVal.getId().equals(oldVal.getId())) {
            return true;
        }
        return false;
    }
}
