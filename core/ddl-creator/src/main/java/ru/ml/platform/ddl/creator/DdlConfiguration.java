package ru.ml.platform.ddl.creator;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManager;

@Configuration
public class DdlConfiguration {

    @Bean
    public DDLGenerator ddlGenerator(EntityManager entityManager){
        return DDLGeneratorFactory.createDDLGenerator(entityManager);
    }
}
