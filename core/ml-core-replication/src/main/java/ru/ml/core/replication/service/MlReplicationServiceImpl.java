package ru.ml.core.replication.service;

import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.eclipse.persistence.dynamic.DynamicEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import ru.ml.core.replication.dto.Replication;
import ru.ml.core.replication.enums.ImportReplicationOption;
import ru.ml.core.replication.enums.ReplicationType;
import ru.ml.core.replication.jaxb.stub.ReplicationBlock;
import ru.ml.core.replication.jaxb.stub.SchemeConverter;
import ru.ml.core.replication.model.MlReplication;
import ru.ml.core.replication.model.MlReplicationStep;
import ru.ml.core.replication.model.MlReplicationStepAbstract;
import ru.ml.core.replication.model.MlReplicationStepSql;
import ru.ml.platform.context.CommonDao;
import ru.ml.platform.core.model.*;
import ru.ml.platform.core.model.context.MlContext;
import ru.ml.platform.core.model.exceptions.MlApplicationException;
import ru.ml.platform.core.model.exceptions.MlServerException;
import ru.ml.platform.core.service.MlHeteroLinkService;
import sun.misc.BASE64Encoder;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

//import ru.ml.web.replication.ReplicationBlock;

public class MlReplicationServiceImpl implements MlReplicationService {
    private static final Logger log = LoggerFactory.getLogger(MlReplicationServiceImpl.class);
    MlContext mlContext = MlContext.getInstance();

    @Autowired
    CommonDao commonDao;
    @Autowired
    SchemeConverter schemeConverter;
    @Autowired
    MlImportReplicationImpl mlImportReplication;
    @Autowired
    MlHeteroLinkService heteroService;

    private Map<String, Boolean> mluids = new HashMap<String, Boolean>();

    @Override
    public byte[] doReplication(MlReplication mlReplication) throws JAXBException, DatatypeConfigurationException {
        log.debug(String.format("Start replication process. MlReplication.id = %s", mlReplication.getId()));
        validateReplication(mlReplication);
        ReplicationBlock replicationBlock = new ReplicationBlock();
        replicationBlock.setName(mlReplication.getName());
        List<MlReplicationStepAbstract> mlReplicationSteps = commonDao.getQueryWithoutSecurityCheck("select o from MlReplicationStepAbstract o where o.replication.id =" + mlReplication.getId() + "  order by o.stepNumber").getResultList();
        for (MlReplicationStepAbstract mlReplicationStep : mlReplicationSteps) {
            log.debug(String.format("   Replication step. %s", mlReplicationStep.toString()));
            ReplicationBlock.ReplicationStep replicationStep = new ReplicationBlock.ReplicationStep();
            replicationStep.setStepNumber(mlReplicationStep.getId());
            replicationStep.setName(mlReplicationStep.getName());
            replicationStep.setOffHandler(mlReplicationStep.getOffHandlers());
            replicationBlock.getReplicationStep().add(replicationStep);
            switch (mlReplicationStep.getReplicationType()) {
                case REPLICATION_HETERO_OBJECTS:
                    MlReplicationStep mlReplicationStepObject = (MlReplicationStep) mlReplicationStep;
                    for (MlHeteroLink heteroLink : mlReplicationStepObject.getObjects()) {
                        MlDynamicEntityImpl heteroEntity = heteroService.getObjectHeteroLink(heteroLink);
                        MlClass mlClass = heteroEntity.getInstanceMlClass();
                        if (mlClass.getAttr(MlAttr.GUID) != null && mlClass.getAttr(MlAttr.LAST_CHANGE) != null) {
                            checkMlClassOrMlAttr(mlClass, mlReplicationStepObject);
                            addEntity(heteroEntity, mlClass, replicationStep);
                        } else {
                            throw new MlApplicationException(String.format("В классе %s отсутсвтвует необходимые для репликации поля!", mlClass.getDescription()));
                        }
                    }
                    break;
                case REPLICATION_SQL:
                    MlReplicationStepSql mlReplicationStepSql = (MlReplicationStepSql) mlReplicationStep;
                    replicationStep.setReplicationSql(mlReplicationStepSql.getSql());
                    break;
            }
        }
        if (mlReplication.getAddInPackage()) {
            replicationBlock.setReplicationMeta(generateReplicationMeta(mlReplication));
        }
        System.out.println(schemeConverter.ToXml(replicationBlock));
        return schemeConverter.ToXml(replicationBlock).getBytes();
    }

    private ReplicationBlock.ReplicationMeta generateReplicationMeta(MlReplication mlReplication) throws DatatypeConfigurationException {
        ReplicationBlock.ReplicationMeta replicationMeta = new ReplicationBlock.ReplicationMeta();
        replicationMeta.setAddInPackage(mlReplication.getAddInPackage());
        replicationMeta.setGuid(mlReplication.getGuid());
        replicationMeta.setLastChange(toXMLCalendar(mlReplication.getLastChange()));
        replicationMeta.setName(mlReplication.getName());
        for (MlReplicationStepAbstract mlStep : mlReplication.getReplicationSteps()) {
            ReplicationBlock.ReplicationMeta.Step step = new ReplicationBlock.ReplicationMeta.Step();
            step.setName(mlStep.getName());
            step.setLastChange(toXMLCalendar(mlStep.getLastChange()));
            step.setGuid(mlStep.getGuid());
            step.setEntityName(mlStep.getInstanceMlClass().getEntityName());
            step.setOffHandlers(mlStep.getOffHandlers());
            step.setStepNumber(mlStep.getStepNumber());
            switch (mlStep.getReplicationType()) {
                case REPLICATION_HETERO_OBJECTS:
                    step.setValue(replicateHeteroLink(mlStep, mlContext.getMetaDataHolder().getAttr("MlReplicationStep", "objects")));
                    break;
                case REPLICATION_SQL:
                    step.setValue(((MlReplicationStepSql) mlStep).getSql());
                    break;
            }
            replicationMeta.getStep().add(step);
        }
        return replicationMeta;
    }

    private XMLGregorianCalendar toXMLCalendar(Date date) throws DatatypeConfigurationException {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(date);
        XMLGregorianCalendar lastChangeDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
        return lastChangeDate;
    }

    private void validateReplication(MlReplication mlReplication) {
        List<MlClass> meta = getAllMetaFromReplication(mlReplication);
        List<MlClass> data = getMlClassesFromData(mlReplication);
        meta.retainAll(data);
        meta.forEach(mlClass -> {
            throw new MlApplicationException(String.format("Ошибка! В одном пакете обновлений содериться мета информация и данные класса %s", mlClass.getEntityName()));
        });
    }

    private List<MlClass> getMlClassesFromData(MlReplication mlReplication) {
        List<MlClass> result = new ArrayList<>();
        for (MlReplicationStepAbstract step : mlReplication.getReplicationSteps()) {
            if (step.getReplicationType() == ReplicationType.REPLICATION_HETERO_OBJECTS) {
                MlReplicationStep replicationStep = (MlReplicationStep) step;
                for (MlHeteroLink heteroLink : replicationStep.getObjects()) {
                    MlDynamicEntityImpl entity = heteroService.getObjectHeteroLink(heteroLink);
                    result.add(entity.getInstanceMlClass());
                }
            }
        }
        return result;

    }

    private List<MlClass> getAllMetaFromReplication(MlReplication mlReplication) {
        List<MlClass> result = new ArrayList<>();
        for (MlReplicationStepAbstract step : mlReplication.getReplicationSteps()) {
            if (step.getReplicationType() == ReplicationType.REPLICATION_HETERO_OBJECTS) {
                MlReplicationStep replicationStep = (MlReplicationStep) step;
                for (MlHeteroLink heteroLink : replicationStep.getObjects()) {
                    MlDynamicEntityImpl entity = heteroService.getObjectHeteroLink(heteroLink);
                    if (entity instanceof MlClass) {
                        result.add((MlClass) entity);
                    } else if (entity instanceof MlAttr) {
                        result.add(((MlAttr) entity).getMlClass());
                    }
                }
            }
        }
        return result;
    }

    private void checkMlClassOrMlAttr(MlClass mlClass, MlReplicationStep mlReplicationStep) {
        if (mlClass.getEntityName().equals("MlClass") || mlClass.getEntityName().equals("MlAttr")) {
            if (!mlReplicationStep.getOffHandlers()) {
                throw new MlServerException("В пакете репликации не отключены обработчики но присутствует " + mlClass.getEntityName());
            }
        }
    }

    private void addEntity(DynamicEntity entity, MlClass mlClass, ReplicationBlock.ReplicationStep replicationStep) throws DatatypeConfigurationException {
        MLUID mluid = commonDao.getMlUID((Long) entity.get(MlAttr.ID), mlClass.getEntityName());
        if (mluids.containsKey(mluid.toString())) {
            return;
        }
        mluids.put(mluid.toString(), true);
        addManyToOneRelations(entity, mlClass, replicationStep);
        ReplicationBlock.ReplicationStep.ReplicationObject replicationObject = new ReplicationBlock.ReplicationStep.ReplicationObject();

        replicationObject.setMluid(mluid.toString());
        replicationObject.setLasChangeDate(toXMLCalendar(entity.get(MlAttr.LAST_CHANGE)));

        for (MlAttr attr : mlClass.getAttrSet()) {
            switch (attr.getReplicationType()) {
                case NOT_REPLICATE:
                    break;
                case REPLICATE:
                case CASCADE_REPLICATE:
                    ReplicationBlock.ReplicationStep.ReplicationObject.Property property =
                            new ReplicationBlock.ReplicationStep.ReplicationObject.Property();

                    if (entity.get(attr.getEntityFieldName()) != null) {
                        if (!attr.getPrimaryKey()) {
                            switch (attr.getFieldType()) {
                                case MANY_TO_ONE:
                                    switch (attr.getReplicationType()) {
                                        case CASCADE_REPLICATE:
                                        case REPLICATE:
                                            property.setEntityFielName(attr.getEntityFieldName());
                                            DynamicEntity dynamicEntity = entity.get(attr.getEntityFieldName());
                                            property.setValue(new MLUID(dynamicEntity).toString());
                                            replicationObject.getProperty().add(property);
                                    }
                                    break;
                                case BOOLEAN:
                                case LONG_LINK:
                                case TEXT:
                                case DOUBLE:
                                case ENUM:
                                case STRING:
                                case LONG:
                                    property.setEntityFielName(attr.getEntityFieldName());
                                    property.setValue(entity.get(attr.getEntityFieldName()).toString());
                                    replicationObject.getProperty().add(property);
                                    break;
                                case DATE:
                                    property.setEntityFielName(attr.getEntityFieldName());
                                    property.setValue(Long.toString(((Date) entity.get(attr.getEntityFieldName())).getTime()));
                                    replicationObject.getProperty().add(property);
                                    break;
                                case FILE:
                                    property.setEntityFielName(attr.getEntityFieldName());
                                    BASE64Encoder base64Encoder = new BASE64Encoder();
                                    String value = entity.get(attr.getEntityFieldName() + "_filename") + "//" + base64Encoder.encode((byte[]) entity.get(attr.getEntityFieldName()));
                                    property.setValue(value);
                                    replicationObject.getProperty().add(property);
                                    break;
                                default:
                                    System.out.println(attr.getEntityFieldName());

                            }
                        }
                    }
            }
        }
        replicationStep.getReplicationObject().add(replicationObject);
        for (MlAttr attr : mlClass.getAttrSet()) {
            switch (attr.getFieldType()) {
                case HETERO_LINK:
                    switch (attr.getReplicationType()) {
                        case CASCADE_REPLICATE:
                            List<MlHeteroLink> links = entity.get(attr.getEntityFieldName());
                            for (MlHeteroLink link : links) {
                                MlDynamicEntityImpl childEntity = heteroService.getObjectHeteroLink(link);
                                addEntity(childEntity, childEntity.getInstanceMlClass(), replicationStep);
                            }
                        case REPLICATE:
                            ReplicationBlock.ReplicationStep.ReplicationObject.Property property =
                                    new ReplicationBlock.ReplicationStep.ReplicationObject.Property();
                            property.setEntityFielName(attr.getEntityFieldName());
                            property.setValue(replicateHeteroLink(entity, attr));
                            replicationObject.getProperty().add(property);
                            break;
                    }
                    break;
                case ONE_TO_MANY:
                    switch (attr.getReplicationType()) {
                        case CASCADE_REPLICATE:
                            List<MlDynamicEntityImpl> entities = entity.get(attr.getEntityFieldName());
                            for (MlDynamicEntityImpl childEntity : entities) {
                                addEntity(childEntity, childEntity.getInstanceMlClass(), replicationStep);
                            }
                        case REPLICATE:
                            ReplicationBlock.ReplicationStep.ReplicationObject.Property property =
                                    new ReplicationBlock.ReplicationStep.ReplicationObject.Property();
                            String oneToMany = "";
                            List<DynamicEntity> oneToManyEnityes = entity.get(attr.getEntityFieldName());
                            for (DynamicEntity childEntity : oneToManyEnityes) {
                                oneToMany += commonDao.getMlUID(childEntity) + "#";
                            }
                            property.setEntityFielName(attr.getEntityFieldName());
                            property.setValue(oneToMany);
                            replicationObject.getProperty().add(property);
                            break;
                    }
                    break;
                case MANY_TO_MANY:
                    switch (attr.getReplicationType()) {
                        case CASCADE_REPLICATE:
                            List<DynamicEntity> entities = entity.get(attr.getEntityFieldName());
                            if (entities != null) {
                                for (DynamicEntity childEntity : entities) {
                                    addEntity(childEntity, attr.getLinkClass(), replicationStep);
                                }
                            }
                        case REPLICATE:
                            ReplicationBlock.ReplicationStep.ReplicationObject.Property property =
                                    new ReplicationBlock.ReplicationStep.ReplicationObject.Property();
                            String manyToMany = "";
                            List<DynamicEntity> MNentities = entity.get(attr.getEntityFieldName());
                            for (DynamicEntity childEntity : MNentities) {
                                manyToMany += commonDao.getMlUID(childEntity) + "#";
                            }
                            property.setEntityFielName(attr.getEntityFieldName());
                            property.setValue(manyToMany);
                            replicationObject.getProperty().add(property);
                    }
            }
        }
    }

    private String replicateHeteroLink(DynamicEntity entity, MlAttr attr) {
        String oneToMany = "";
        List<MlHeteroLink> mlHeteroLinks = entity.get(attr.getEntityFieldName());
        for (MlHeteroLink heteroLink : mlHeteroLinks) {
            MlDynamicEntityImpl childEntity = heteroService.getObjectHeteroLink(heteroLink);
            try {
                oneToMany += commonDao.getMlUID(childEntity) + "#";
            } catch (Exception e) {
                throw new MlApplicationException(String.format("Невозможно получить MLUID у обьекта %s", childEntity.getTitle()));
            }
        }
        return oneToMany;
    }

    private void addManyToOneRelations(DynamicEntity entity, MlClass mlClass, ReplicationBlock.ReplicationStep replicationStep) throws DatatypeConfigurationException {
        for (MlAttr attr : mlClass.getAttrSet()) {
            switch (attr.getFieldType()) {
                case MANY_TO_ONE:
                    switch (attr.getReplicationType()) {
                        case CASCADE_REPLICATE:
                            if (entity.get(attr.getEntityFieldName()) != null) {
                                addEntity((DynamicEntity) entity.get(attr.getEntityFieldName()), attr.getLinkClass(), replicationStep);
                            }
                            break;
                    }
                    break;
            }
        }
    }

    @Override
    public ReplicationBlock getReplicationInfo(byte[] file) throws JAXBException {
        return schemeConverter.fromXml(file);
    }

    @Override
    public Replication importReplication(byte[] zipFile, Boolean onlyGenerateResultFile, ImportReplicationOption option) throws IOException, JAXBException, MlApplicationException, ArchiveException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(zipFile);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        ArchiveInputStream input = new ArchiveStreamFactory().createArchiveInputStream(byteArrayInputStream);
        input.getNextEntry();

        byte[] buffer = new byte[1024];
        int len;
        while ((len = input.read(buffer)) > 0) {
            byteArrayOutputStream.write(buffer, 0, len);
        }
        input.close();

        ReplicationBlock block = getReplicationInfo(byteArrayOutputStream.toByteArray());
        byteArrayOutputStream.close();
        return mlImportReplication.doImport(block, onlyGenerateResultFile, option);
    }
}
