package ru.ml.core.replication.enums;

/**
 * Created by d_litovchenko on 21.01.2016.
 */
public enum ReplicationType {
    REPLICATION_HETERO_OBJECTS,
    REPLICATION_SQL
}
