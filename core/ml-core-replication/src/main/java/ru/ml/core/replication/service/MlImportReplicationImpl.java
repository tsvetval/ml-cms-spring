package ru.ml.core.replication.service;

import org.eclipse.persistence.dynamic.DynamicEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.ml.core.replication.dto.Replication;
import ru.ml.core.replication.dto.ReplicationObject;
import ru.ml.core.replication.dto.ReplicationStep;
import ru.ml.core.replication.enums.ImportReplicationOption;
import ru.ml.core.replication.jaxb.stub.ReplicationBlock;
import ru.ml.core.replication.model.MlReplication;
import ru.ml.core.replication.model.MlReplicationStep;
import ru.ml.core.replication.model.MlReplicationStepAbstract;
import ru.ml.core.replication.model.MlReplicationStepSql;
import ru.ml.platform.context.CommonDao;
import ru.ml.platform.core.handler.MlReplicationHandler;
import ru.ml.platform.core.init.MlMetaDataInitializeService;
import ru.ml.platform.core.model.*;
import ru.ml.platform.core.model.context.MlContext;
import ru.ml.platform.core.model.exceptions.MlApplicationException;
import ru.ml.platform.core.model.exceptions.MlServerException;
import ru.ml.platform.core.service.MlHeteroLinkService;
import sun.misc.BASE64Decoder;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import java.io.IOException;
import java.util.*;

/**
 *
 */
public class MlImportReplicationImpl {
    private static final Logger log = LoggerFactory.getLogger(MlImportReplicationImpl.class);
    private MlContext mlContext = MlContext.getInstance();

    @Autowired
    private CommonDao commonDao;
    @Autowired
    private EntityManagerFactory emf;
    @Autowired
    MlMetaDataInitializeService jpaClassInitializer;
    @Autowired
    ApplicationContext applicationContext;
    @Autowired
    MlHeteroLinkService mlHeteroLinkService;

    Map<MLUID, Map<String, List<MLUID>>> manyToMany = new HashMap<>();
    Set<MlClass> mlClassForInitialization = new HashSet<>();
    Map<MLUID, MlDynamicEntityImpl> editEntityes = new HashMap<>();

    //@Transactional
    public Replication doImport(ReplicationBlock block, Boolean onlyGenerateResultFile, ImportReplicationOption option) throws MlApplicationException {
        Replication replication = new Replication();
        replication.setName(block.getName());
        replication.setMlReplication(setReplicationSteps(replication, block, option, onlyGenerateResultFile));
        log.debug(String.format("Import Replication %s", replication.toString()));
        return replication;
    }

    private MlReplication setReplicationSteps(Replication replication, ReplicationBlock block, ImportReplicationOption option, Boolean onlyGenerateResultFile) throws MlApplicationException {
        MlReplication mlReplication = null;
        for (ReplicationBlock.ReplicationStep step : block.getReplicationStep()) {
            ReplicationStep replicationStep = new ReplicationStep();
            replicationStep.setNumber(step.getStepNumber());
            replicationStep.setName(step.getName());
            replicationStep.setReinitiaslize(step.isReinitialization());
            replicationStep.setOffHandler(step.isOffHandler());
            log.debug(String.format("Import step %s", replicationStep.toString()));
            setReplicationObjects(replicationStep, step, option, onlyGenerateResultFile);
            replicationStep.setSql(step.getReplicationSql());
            replication.getReplicationSteps().add(replicationStep);
            if (!onlyGenerateResultFile) {
                execSql(replicationStep.getSql());
                insertManyToMany();
                insertManyToOne(step, replicationStep);

                persist(step);
                if (!onlyGenerateResultFile && block.getReplicationMeta() != null) {
                    mlReplication = insertReplicationData(block.getReplicationMeta());
                }
                initHandlers(step);
                initializeMlClasses();
            }
        }
        return mlReplication;
    }

    private MlReplication insertReplicationData(ReplicationBlock.ReplicationMeta replicationMeta) {
        MlReplication mlReplication = (MlReplication) commonDao.getByMlUID(new MLUID("MlReplication", replicationMeta.getGuid()));
        if (mlReplication == null) {
            mlReplication = (MlReplication) commonDao.createNewEntity(MlReplication.class);
            mlReplication.setGuid(replicationMeta.getGuid());
        }
        if (mlReplication.getLastChange() == null || mlReplication.getLastChange().after(replicationMeta.getLastChange().toGregorianCalendar().getTime())) {
            mlReplication.setLastChange(replicationMeta.getLastChange().toGregorianCalendar().getTime());
            mlReplication.setName(replicationMeta.getName());
            mlReplication.setAddInPackage(replicationMeta.isAddInPackage());
        }
        commonDao.persistWithoutSecurityCheck(mlReplication);
        for (ReplicationBlock.ReplicationMeta.Step step : replicationMeta.getStep()) {
            MlReplicationStepAbstract stepAbstract = (MlReplicationStepAbstract) commonDao.getByMlUID(new MLUID("MlReplicationStepAbstract", step.getGuid()));
            if (stepAbstract == null) {
                stepAbstract = (MlReplicationStepAbstract) commonDao.createNewEntity(mlContext.getMetaDataHolder().getEntityClassByName(step.getEntityName()));
                stepAbstract.setGuid(step.getGuid());
            }
            if (stepAbstract.getLastChange() == null || stepAbstract.getLastChange().after(step.getLastChange().toGregorianCalendar().getTime())) {
                stepAbstract.setName(step.getName());
                stepAbstract.setLastChange(step.getLastChange().toGregorianCalendar().getTime());
                stepAbstract.setOffHandlers(step.isOffHandlers());
                stepAbstract.setStepNumber(step.getStepNumber());
                stepAbstract.setReplication(mlReplication);
                switch (stepAbstract.getReplicationType()) {
                    case REPLICATION_SQL:
                        MlReplicationStepSql mlReplicationStepSql = (MlReplicationStepSql) stepAbstract;
                        mlReplicationStepSql.setSql(step.getValue());
                        break;
                    case REPLICATION_HETERO_OBJECTS:
                        MlReplicationStep replicationStep = (MlReplicationStep) stepAbstract;
                        List<MlHeteroLink> mlHeteroLinks = new ArrayList<>();
                        for (String mluidStr : step.getValue().split("#")) {
                            MLUID mluid = new MLUID(mluidStr);
                            MlDynamicEntityImpl dynamicEntityRelation = commonDao.getByMlUID(mluid);
                            if (dynamicEntityRelation == null) {
                                throw new MlApplicationException(String.format("Не найден объект по MLUID :%s", mluid.toString()));
                            }
                            MlHeteroLink mlHeteroLink = (MlHeteroLink) commonDao.createNewEntity(MlHeteroLink.class);
                            mlHeteroLink.setObject(dynamicEntityRelation);
                            commonDao.persistWithoutSecurityCheck(mlHeteroLink);
                            mlHeteroLinks.add(mlHeteroLink);
                        }
                        replicationStep.setObjects(mlHeteroLinks);
                        break;
                }
                commonDao.persistWithoutSecurityCheck(stepAbstract);
                log.debug("Step id = " + stepAbstract.getId());
            }

        }
        return mlReplication;
    }

    private void execSql(String sql) {
        if (sql != null && !"".equals(sql)) {
            EntityManager entityManager = emf.createEntityManager();
            entityManager.getTransaction().begin();
            Query query = entityManager.createNativeQuery(sql);
            query.executeUpdate();
            entityManager.getTransaction().commit();
            entityManager.close();
        }
    }

    private void persist(ReplicationBlock.ReplicationStep step) {
        for (MlDynamicEntityImpl entity : editEntityes.values()) {
            if (step.isOffHandler()) {
                commonDao.persistWithoutSecurityCheck(entity, false, false);
            } else {
                commonDao.persistWithoutSecurityCheck(entity);
            }
        }
    }

    private void initializeMlClasses() {
        if (!mlClassForInitialization.isEmpty()) {
            try {
                for (MlClass mlClass : mlClassForInitialization) {
                    commonDao.refresh(mlClass);
                }
                /**
                 * Тут надо добавить в переинициализацию все классы, т.к. eclipse хранит в дескрипторах связей дескрипторы связаных классов,
                 * и в случае если мы имеем связь : A->B->C и меняем класс А, то связь С будет хранить старый дескриптор А.
                 * */
                Collection<MlClass> allClasses = mlContext.getMetaDataHolder().getAllMlClasses();
                allClasses.removeAll(mlClassForInitialization);
                mlClassForInitialization.addAll(allClasses);
                jpaClassInitializer.initializeClasses(mlClassForInitialization, false);
            } catch (ClassNotFoundException e) {
                log.error("", e);
            }
        }
    }

    private void initHandlers(ReplicationBlock.ReplicationStep step) {

        for (ReplicationBlock.ReplicationStep.ReplicationObject object : step.getReplicationObject()) {
            MlClass mlClass = mlContext.getMetaDataHolder().getMlClassByName(new MLUID(object.getMluid()).getClassName());
            DynamicEntity dynamicEntity = commonDao.getByMlUID(new MLUID(object.getMluid()));
            if (dynamicEntity instanceof MlClass) {
                mlClassForInitialization.add((MlClass) dynamicEntity);
            }
            if (dynamicEntity instanceof MlAttr) {
                mlClassForInitialization.add(((MlAttr) dynamicEntity).getMlClass());
            }
            invokeHandler(mlClass, dynamicEntity);
        }

    }

    private void insertManyToOne(ReplicationBlock.ReplicationStep step, ReplicationStep replicationStep) {

        for (ReplicationBlock.ReplicationStep.ReplicationObject object : step.getReplicationObject()) {
            MlClass mlClass = mlContext.getMetaDataHolder().getMlClassByName(new MLUID(object.getMluid()).getClassName());
            MlDynamicEntityImpl dynamicEntity = editEntityes.get(new MLUID(object.getMluid()));//commonDao.getByMlUID(new MLUID(object.getMluid()));
            ReplicationObject replicationObject = replicationStep.getReplicationObjectByMLUID(new MLUID(object.getMluid()));
            switch (replicationObject.getType()) {
                case NO_REFRESH:
                    break;
                case REFRESH:
                    for (ReplicationBlock.ReplicationStep.ReplicationObject.Property property : object.getProperty()) {
                        MlAttr attr = mlContext.getMetaDataHolder().getAttr(mlClass, property.getEntityFielName());
                        switch (attr.getFieldType()) {
                            case MANY_TO_ONE:
                                DynamicEntity dynamic = commonDao.getByMlUID(new MLUID(property.getValue()));
                                if (dynamic == null) {
                                    dynamic = editEntityes.get(new MLUID(property.getValue()));
                                    if (dynamic == null) {
                                        throw new MlApplicationException(String.format("Не найден объект по MLUID :%s", property.getValue()));
                                    }
                                }
                                dynamicEntity.set(property.getEntityFielName(), dynamic);

                                break;
                        }
                    }
                    /*if (step.isOffHandler()) {
                        commonDao.persistWithoutSecurityCheck(dynamicEntity, false, false);
                    } else {
                        commonDao.persistWithoutSecurityCheck(dynamicEntity);
                    }*/
            }

        }
    }


    private void setReplicationObjects(ReplicationStep replicationStep, ReplicationBlock.ReplicationStep step, ImportReplicationOption option, Boolean onlyGenerateResultFile) throws MlApplicationException {

        for (ReplicationBlock.ReplicationStep.ReplicationObject object : step.getReplicationObject()) {
            ReplicationObject replicationObject = new ReplicationObject();
            replicationObject.setMluid(object.getMluid());
            replicationObject.setLastChangeDate(object.getLasChangeDate().toGregorianCalendar().getTime());
            log.debug(String.format("Import object %s", replicationObject.toString()));
            MlClass mlClass = mlContext.getMetaDataHolder().getMlClassByName(new MLUID(object.getMluid()).getClassName());
            if (!onlyGenerateResultFile) {
                MlAttr mlattr = mlContext.getMetaDataHolder().getAttr(mlClass, MlAttr.GUID);
                if (mlattr == null) {
                    throw new MlApplicationException(String.format("У класса %s отсутствует атрибут guid! Репликация невозможна!", mlClass.getEntityName()));
                }
            }
            if (onlyGenerateResultFile && mlClass == null) {
                replicationObject.setNote("Создан");
                replicationObject.setType(ReplicationObject.Type.REFRESH);
            } else {
                if (mlClass.getAttr("guid") == null) {
                    replicationObject.setNote("Создан");
                    replicationObject.setType(ReplicationObject.Type.REFRESH);
                } else {
                    if (commonDao.getByMlUID(new MLUID(object.getMluid())) != null) {
                        switch (option.getImportType()) {
                            case ONLY_CREATE:
                                replicationObject.setType(ReplicationObject.Type.NO_REFRESH);
                                replicationObject.setNote("Не обновлен");
                                break;
                            case REWRITE:
                                replicationObject.setType(ReplicationObject.Type.REFRESH);
                                replicationObject.setNote("Перезаписан");
                                break;
                            case CREATE_UPDATE:
                                replicationObject.setType(ReplicationObject.Type.REFRESH);
                                replicationObject.setNote("Обновлен");
                                break;
                        }
                    } else {
                        replicationObject.setNote("Создан");
                        replicationObject.setType(ReplicationObject.Type.REFRESH);
                    }
                }
            }
            if (!onlyGenerateResultFile) {
                replicateObject(object, option, step.isOffHandler());
            }
            replicationStep.getReplicationObjects().add(replicationObject);
        }
    }

    private void replicateObject(ReplicationBlock.ReplicationStep.ReplicationObject replicationObject, ImportReplicationOption option, Boolean offHandlers) throws MlApplicationException {

        MLUID mluid = new MLUID(replicationObject.getMluid());
        MlClass MlClass = mlContext.getMetaDataHolder().getMlClassByName(mluid.getClassName());
        MlDynamicEntityImpl dynamicEntity = commonDao.getByMlUID(mluid);
        if (dynamicEntity == null) {
            dynamicEntity = commonDao.createNewEntity(mlContext.getMetaDataHolder().getEntityClassByName(mluid.getClassName()), true);
            dynamicEntity.set(MlAttr.GUID, mluid.getGuid());
            commonDao.persistWithoutSecurityCheck(dynamicEntity, false, false);
            Map<String, List<MLUID>> listMany = fillDynamicEntityNewData(dynamicEntity, MlClass, replicationObject.getProperty());
            manyToMany.put(mluid, listMany);
        } else {
            switch (option.getImportType()) {
                case ONLY_CREATE:
                    break;
                case REWRITE:
                    deleteRelationsInObject(dynamicEntity, MlClass);
                case CREATE_UPDATE:
                    Map<String, List<MLUID>> listMany = fillDynamicEntityNewData(dynamicEntity, MlClass, replicationObject.getProperty());
                    manyToMany.put(mluid, listMany);
            }
        }
        editEntityes.put(mluid, dynamicEntity);
        //commonDao.persistWithoutSecurityCheck(dynamicEntity, false, false);
    }

    private void invokeHandler(MlClass MlClass, DynamicEntity dynamicEntity) {
        String handlerClassName = MlClass.getReplicationHandler();
        MlReplicationHandler handler = null;
        if (handlerClassName != null && !"".equals(handlerClassName)) {
            Class<MlReplicationHandler> handlerClass = null;
            try {
                handlerClass = (Class<MlReplicationHandler>) Class.forName(handlerClassName);
            } catch (ClassNotFoundException e) {
                log.error("", e);
            }
            handler = applicationContext.getBean(handlerClass);
        }


        if (handler != null) {
            handler.afterReplication(dynamicEntity);
        }
    }

    private void deleteRelationsInObject(DynamicEntity dynamicEntity, MlClass MlClass) {
        for (MlAttr attr : MlClass.getAttrSet()) {
            switch (attr.getFieldType()) {
                case MANY_TO_MANY:
                case ONE_TO_MANY:
                    dynamicEntity.set(attr.getEntityFieldName(), new ArrayList<DynamicEntity>());
            }
        }
    }

    private Map<String, List<MLUID>> fillDynamicEntityNewData(DynamicEntity dynamicEntity, MlClass mlClass, List<ReplicationBlock.ReplicationStep.ReplicationObject.Property> properties) throws MlApplicationException {
        Map<String, List<MLUID>> mapMany = new HashMap<>();
        for (ReplicationBlock.ReplicationStep.ReplicationObject.Property property : properties) {
            log.debug(String.format("Import property %s = %s", property.getEntityFielName(), property.getValue()));
            MlAttr attr = mlContext.getMetaDataHolder().getAttr(mlClass, property.getEntityFielName());
            switch (attr.getFieldType()) {
                case DATE:
                    dynamicEntity.set(property.getEntityFielName(), new Date(Long.parseLong(property.getValue())));
                    break;
                case BOOLEAN:
                    dynamicEntity.set(property.getEntityFielName(), Boolean.parseBoolean(property.getValue()));
                    break;
                case LONG:
                    dynamicEntity.set(property.getEntityFielName(), Long.parseLong(property.getValue()));
                    break;
                case DOUBLE:
                    dynamicEntity.set(property.getEntityFielName(), Double.parseDouble(property.getValue()));
                    break;
                case FILE:
                    try {
                        String file = property.getValue();
                        BASE64Decoder base64Decoder = new BASE64Decoder();
                        String fileName = file.substring(0, file.indexOf("//"));
                        byte[] fileValue = base64Decoder.decodeBuffer(file.substring(file.indexOf("//") + 2));
                        dynamicEntity.set(property.getEntityFielName(), fileValue);
                        dynamicEntity.set(property.getEntityFielName() + "_filename", fileName);
                    } catch (IOException e) {
                        throw new MlServerException(String.format("Ощибка репликации типа Файл. [%s,%s]", mlClass.getEntityName(), property.getEntityFielName()), e);
                    }
                    break;
                case MANY_TO_ONE:
                    dynamicEntity.set(property.getEntityFielName(), commonDao.getByMlUID(new MLUID(property.getValue())));
                    break;
                case MANY_TO_MANY:
                case ONE_TO_MANY:
                case HETERO_LINK:
                    if (!property.getValue().equals("")) {
                        List<MLUID> mluids = new ArrayList<>();
                        for (String mluidStr : property.getValue().split("#")) {
                            mluids.add(new MLUID(mluidStr));
                        }
                        mapMany.put(property.getEntityFielName(), mluids);
                        System.out.println(property.getValue());
                    }
                    break;
                default:
                    dynamicEntity.set(property.getEntityFielName(), property.getValue());
            }
        }
        return mapMany;
    }

    private void insertManyToMany() throws MlApplicationException {
        for (MLUID mluid : manyToMany.keySet()) {
            if (manyToMany.get(mluid) != null && !manyToMany.get(mluid).isEmpty()) {
                MlDynamicEntityImpl dynamicEntity = editEntityes.get(mluid);
                Map<String, List<MLUID>> mapMany = manyToMany.get(mluid);
                Boolean refreshed = false;
                for (String attrName : mapMany.keySet()) {
                    MlAttr mlAttr = dynamicEntity.getInstanceMlClass().getAttr(attrName);
                    List<MLUID> relationMluidList = mapMany.get(attrName);
                    switch (mlAttr.getFieldType()) {
                        case HETERO_LINK:
                            Set<MlHeteroLink> mlHeteroLinks = new HashSet<>();
                            for (MLUID relationMluid : relationMluidList) {
                                MlDynamicEntityImpl dynamicEntityRelation = commonDao.getByMlUID(relationMluid);
                                if (dynamicEntityRelation == null) {
                                    throw new MlApplicationException(String.format("Не найден объект по MLUID :%s", relationMluid.toString()));
                                }

                                boolean needInsert = true;

                                for (MlHeteroLink heteroLink : (List<MlHeteroLink>) dynamicEntity.get(attrName)) {
                                    MlDynamicEntityImpl heteroEntity = mlHeteroLinkService.getObjectHeteroLink(heteroLink);
                                    if (heteroLink.getClassName().equals(dynamicEntityRelation.getInstanceMlClass().getEntityName())
                                            && heteroEntity.get(MlAttr.GUID).equals(dynamicEntityRelation.get(MlAttr.GUID))) {
                                        needInsert = false;
                                        break;
                                    }
                                }
                                if (needInsert) {
                                    MlHeteroLink heteroLink = (MlHeteroLink) commonDao.createNewEntity(MlHeteroLink.class);
                                    heteroLink.setObject(dynamicEntityRelation);
                                    commonDao.persistWithoutSecurityCheck(heteroLink);
                                    mlHeteroLinks.add(heteroLink);
                                }
                            }
                            if (!refreshed) {
                                //  commonDao.refresh(dynamicEntity);
                                refreshed = true;
                            }
                            mlHeteroLinks.addAll((List<MlHeteroLink>) dynamicEntity.get(attrName));
                            List<MlHeteroLink> links = new ArrayList<>();
                            links.addAll(mlHeteroLinks);
                            dynamicEntity.set(attrName, links);
                            //commonDao.persistWithoutSecurityCheck(dynamicEntity, false, false);

                            break;
                        case MANY_TO_MANY:
                        case ONE_TO_MANY:
                            List<MlDynamicEntityImpl> entityList = new ArrayList<>();
                            for (MLUID relationMluid : relationMluidList) {
                                MlDynamicEntityImpl dynamicEntityRelation = commonDao.getByMlUID(relationMluid);
                                if (dynamicEntityRelation == null) {
                                    dynamicEntityRelation = editEntityes.get(relationMluid);
                                    if (dynamicEntityRelation == null) {
                                        throw new MlApplicationException(String.format("Не найден объект по MLUID :%s", relationMluid.toString()));
                                    }
                                }
                                entityList.add(dynamicEntityRelation);
                            }
                            if (!refreshed) {
                                //commonDao.refresh(dynamicEntity);
                                refreshed = true;
                            }

                            Collection<MlDynamicEntityImpl> relations = (Collection<MlDynamicEntityImpl>) dynamicEntity.getPropertiesMap().get(attrName).getValue();
                            if (relations == null) {
                                relations = new ArrayList<>();
                                dynamicEntity.set(attrName, relations);
                            } else if (!mlAttr.isOrdered()) {
                                relations = new ArrayList<>();
                                dynamicEntity.set(attrName, relations);
                            }
                            relations.clear();
                            relations.addAll(entityList);

                            //dynamicEntity.set(attrName, relations);
                            //commonDao.persistWithoutSecurityCheck(dynamicEntity, false, false);
                            break;
                    }
                }
            }
        }
        manyToMany = new HashMap<>();
    }
}
