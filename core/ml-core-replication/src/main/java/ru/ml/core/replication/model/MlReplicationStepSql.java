package ru.ml.core.replication.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.ml.core.replication.enums.ReplicationType;

/**
 *
 */
public class MlReplicationStepSql extends MlReplicationStepAbstract {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    @Override
    public ReplicationType getReplicationType() {
        return ReplicationType.REPLICATION_SQL;
    }

    public String getSql(){
        return get("sql");
    }
    public void setSql(String sql){
        set("sql",sql);
    }

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String toString() {
        return (new StringBuffer()).append("MlReplicationStep(Id = ").append(getId()).append(", name = ").append(getName()).
                append(", offHandlers = ").append(getOffHandlers()).
                append(", stepNumber = ").append(getStepNumber()).toString();
    }

    public void setReplication(MlReplication replication) {
        set("replication", replication);
    }

    public void setStepNumber(Long stepNumber) {
        set("stepNumber", stepNumber);
    }

    public void setReinitialization(boolean reinitialization) {
        set("reinitialization", reinitialization);
    }
}
