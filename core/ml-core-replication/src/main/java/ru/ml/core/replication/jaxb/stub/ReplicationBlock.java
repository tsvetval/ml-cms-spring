
package ru.ml.core.replication.jaxb.stub;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="replicationStep" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="replicationObject" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="property" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;simpleContent>
 *                                   &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                                     &lt;attribute name="entityFielName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/extension>
 *                                 &lt;/simpleContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                           &lt;attribute name="mluid" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="lasChangeDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="replicationSql" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="stepNumber" type="{http://www.w3.org/2001/XMLSchema}long" />
 *                 &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="reinitialization" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *                 &lt;attribute name="offHandler" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="replicationMeta" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="step" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                           &lt;attribute name="entityName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="stepNumber" type="{http://www.w3.org/2001/XMLSchema}long" />
 *                           &lt;attribute name="offHandlers" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *                           &lt;attribute name="guid" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="lastChange" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *                 &lt;attribute name="addInPackage" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *                 &lt;attribute name="guid" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="lastChange" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *                 &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "replicationStep",
    "replicationMeta"
})
@XmlRootElement(name = "replicationBlock")
public class ReplicationBlock {

    protected List<ReplicationBlock.ReplicationStep> replicationStep;
    protected ReplicationBlock.ReplicationMeta replicationMeta;
    @XmlAttribute(name = "name")
    protected String name;

    /**
     * Gets the value of the replicationStep property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the replicationStep property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReplicationStep().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReplicationBlock.ReplicationStep }
     * 
     * 
     */
    public List<ReplicationBlock.ReplicationStep> getReplicationStep() {
        if (replicationStep == null) {
            replicationStep = new ArrayList<ReplicationBlock.ReplicationStep>();
        }
        return this.replicationStep;
    }

    /**
     * Gets the value of the replicationMeta property.
     * 
     * @return
     *     possible object is
     *     {@link ReplicationBlock.ReplicationMeta }
     *     
     */
    public ReplicationBlock.ReplicationMeta getReplicationMeta() {
        return replicationMeta;
    }

    /**
     * Sets the value of the replicationMeta property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplicationBlock.ReplicationMeta }
     *     
     */
    public void setReplicationMeta(ReplicationBlock.ReplicationMeta value) {
        this.replicationMeta = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="step" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *                 &lt;attribute name="entityName" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="stepNumber" type="{http://www.w3.org/2001/XMLSchema}long" />
     *                 &lt;attribute name="offHandlers" type="{http://www.w3.org/2001/XMLSchema}boolean" />
     *                 &lt;attribute name="guid" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="lastChange" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *       &lt;attribute name="addInPackage" type="{http://www.w3.org/2001/XMLSchema}boolean" />
     *       &lt;attribute name="guid" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="lastChange" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
     *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "step"
    })
    public static class ReplicationMeta {

        protected List<ReplicationBlock.ReplicationMeta.Step> step;
        @XmlAttribute(name = "addInPackage")
        protected Boolean addInPackage;
        @XmlAttribute(name = "guid")
        protected String guid;
        @XmlAttribute(name = "lastChange")
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar lastChange;
        @XmlAttribute(name = "name")
        protected String name;

        /**
         * Gets the value of the step property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the step property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getStep().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ReplicationBlock.ReplicationMeta.Step }
         * 
         * 
         */
        public List<ReplicationBlock.ReplicationMeta.Step> getStep() {
            if (step == null) {
                step = new ArrayList<ReplicationBlock.ReplicationMeta.Step>();
            }
            return this.step;
        }

        /**
         * Gets the value of the addInPackage property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isAddInPackage() {
            return addInPackage;
        }

        /**
         * Sets the value of the addInPackage property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setAddInPackage(Boolean value) {
            this.addInPackage = value;
        }

        /**
         * Gets the value of the guid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGuid() {
            return guid;
        }

        /**
         * Sets the value of the guid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGuid(String value) {
            this.guid = value;
        }

        /**
         * Gets the value of the lastChange property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getLastChange() {
            return lastChange;
        }

        /**
         * Sets the value of the lastChange property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setLastChange(XMLGregorianCalendar value) {
            this.lastChange = value;
        }

        /**
         * Gets the value of the name property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getName() {
            return name;
        }

        /**
         * Sets the value of the name property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setName(String value) {
            this.name = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *       &lt;attribute name="entityName" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="stepNumber" type="{http://www.w3.org/2001/XMLSchema}long" />
         *       &lt;attribute name="offHandlers" type="{http://www.w3.org/2001/XMLSchema}boolean" />
         *       &lt;attribute name="guid" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="lastChange" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class Step {

            @XmlElement(required = true)
            protected String value;
            @XmlAttribute(name = "entityName")
            protected String entityName;
            @XmlAttribute(name = "name")
            protected String name;
            @XmlAttribute(name = "stepNumber")
            protected Long stepNumber;
            @XmlAttribute(name = "offHandlers")
            protected Boolean offHandlers;
            @XmlAttribute(name = "guid")
            protected String guid;
            @XmlAttribute(name = "lastChange")
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar lastChange;

            /**
             * Gets the value of the value property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * Sets the value of the value property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * Gets the value of the entityName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEntityName() {
                return entityName;
            }

            /**
             * Sets the value of the entityName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEntityName(String value) {
                this.entityName = value;
            }

            /**
             * Gets the value of the name property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Sets the value of the name property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

            /**
             * Gets the value of the stepNumber property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getStepNumber() {
                return stepNumber;
            }

            /**
             * Sets the value of the stepNumber property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setStepNumber(Long value) {
                this.stepNumber = value;
            }

            /**
             * Gets the value of the offHandlers property.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isOffHandlers() {
                return offHandlers;
            }

            /**
             * Sets the value of the offHandlers property.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setOffHandlers(Boolean value) {
                this.offHandlers = value;
            }

            /**
             * Gets the value of the guid property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGuid() {
                return guid;
            }

            /**
             * Sets the value of the guid property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGuid(String value) {
                this.guid = value;
            }

            /**
             * Gets the value of the lastChange property.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getLastChange() {
                return lastChange;
            }

            /**
             * Sets the value of the lastChange property.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setLastChange(XMLGregorianCalendar value) {
                this.lastChange = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="replicationObject" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="property" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;simpleContent>
     *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *                           &lt;attribute name="entityFielName" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/extension>
     *                       &lt;/simpleContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *                 &lt;attribute name="mluid" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="lasChangeDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="replicationSql" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *       &lt;attribute name="stepNumber" type="{http://www.w3.org/2001/XMLSchema}long" />
     *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="reinitialization" type="{http://www.w3.org/2001/XMLSchema}boolean" />
     *       &lt;attribute name="offHandler" type="{http://www.w3.org/2001/XMLSchema}boolean" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "replicationObject",
        "replicationSql"
    })
    public static class ReplicationStep {

        protected List<ReplicationBlock.ReplicationStep.ReplicationObject> replicationObject;
        protected String replicationSql;
        @XmlAttribute(name = "stepNumber")
        protected Long stepNumber;
        @XmlAttribute(name = "name")
        protected String name;
        @XmlAttribute(name = "reinitialization")
        protected Boolean reinitialization;
        @XmlAttribute(name = "offHandler")
        protected Boolean offHandler;

        /**
         * Gets the value of the replicationObject property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the replicationObject property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getReplicationObject().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ReplicationBlock.ReplicationStep.ReplicationObject }
         * 
         * 
         */
        public List<ReplicationBlock.ReplicationStep.ReplicationObject> getReplicationObject() {
            if (replicationObject == null) {
                replicationObject = new ArrayList<ReplicationBlock.ReplicationStep.ReplicationObject>();
            }
            return this.replicationObject;
        }

        /**
         * Gets the value of the replicationSql property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReplicationSql() {
            return replicationSql;
        }

        /**
         * Sets the value of the replicationSql property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReplicationSql(String value) {
            this.replicationSql = value;
        }

        /**
         * Gets the value of the stepNumber property.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getStepNumber() {
            return stepNumber;
        }

        /**
         * Sets the value of the stepNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setStepNumber(Long value) {
            this.stepNumber = value;
        }

        /**
         * Gets the value of the name property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getName() {
            return name;
        }

        /**
         * Sets the value of the name property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setName(String value) {
            this.name = value;
        }

        /**
         * Gets the value of the reinitialization property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isReinitialization() {
            return reinitialization;
        }

        /**
         * Sets the value of the reinitialization property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setReinitialization(Boolean value) {
            this.reinitialization = value;
        }

        /**
         * Gets the value of the offHandler property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isOffHandler() {
            return offHandler;
        }

        /**
         * Sets the value of the offHandler property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setOffHandler(Boolean value) {
            this.offHandler = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="property" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;simpleContent>
         *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
         *                 &lt;attribute name="entityFielName" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/extension>
         *             &lt;/simpleContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *       &lt;attribute name="mluid" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="lasChangeDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "property"
        })
        public static class ReplicationObject {

            protected List<ReplicationBlock.ReplicationStep.ReplicationObject.Property> property;
            @XmlAttribute(name = "mluid")
            protected String mluid;
            @XmlAttribute(name = "lasChangeDate")
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar lasChangeDate;

            /**
             * Gets the value of the property property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the property property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getProperty().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ReplicationBlock.ReplicationStep.ReplicationObject.Property }
             * 
             * 
             */
            public List<ReplicationBlock.ReplicationStep.ReplicationObject.Property> getProperty() {
                if (property == null) {
                    property = new ArrayList<ReplicationBlock.ReplicationStep.ReplicationObject.Property>();
                }
                return this.property;
            }

            /**
             * Gets the value of the mluid property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMluid() {
                return mluid;
            }

            /**
             * Sets the value of the mluid property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMluid(String value) {
                this.mluid = value;
            }

            /**
             * Gets the value of the lasChangeDate property.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getLasChangeDate() {
                return lasChangeDate;
            }

            /**
             * Sets the value of the lasChangeDate property.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setLasChangeDate(XMLGregorianCalendar value) {
                this.lasChangeDate = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;simpleContent>
             *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
             *       &lt;attribute name="entityFielName" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/extension>
             *   &lt;/simpleContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "value"
            })
            public static class Property {

                @XmlValue
                protected String value;
                @XmlAttribute(name = "entityFielName")
                protected String entityFielName;

                /**
                 * Gets the value of the value property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValue() {
                    return value;
                }

                /**
                 * Sets the value of the value property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValue(String value) {
                    this.value = value;
                }

                /**
                 * Gets the value of the entityFielName property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEntityFielName() {
                    return entityFielName;
                }

                /**
                 * Sets the value of the entityFielName property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEntityFielName(String value) {
                    this.entityFielName = value;
                }

            }

        }

    }

}
