package ru.ml.core.replication.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.ml.platform.core.model.MlAttr;
import ru.ml.platform.core.model.MlDynamicEntityImpl;

import java.util.Date;
import java.util.List;

/**
 *
 */
public class MlReplication extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    public Long getId() {
        return get("id");
    }

    public String getName() {
        return get("name");
    }

    public void setName(String name) {
        set("name", name);
    }

    public MlReplication getMlReplication() {
        return get("replication");
    }

    public boolean getAddInPackage() {
        return get("addInPackage");
    }

    public void setAddInPackage(boolean val) {
        set("addInPackage", val);
    }

    public List<MlReplicationStepAbstract> getReplicationSteps() {
        return get("replicationSteps");
    }

    public void setFile(byte[] file) {
        set("replicationFile", file);
    }

    public void setFileName(String fileName) {
        set("replicationFile" + MlAttr.FILE_NAME_POSTFIX, fileName);
    }

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getGuid() {
        return get("guid");
    }

    public void setGuid(String value) {
        set("guid", value);
    }

    public Date getLastChange() {
        return get("lastChange");
    }

    public void setLastChange(Date date) {
        set("lastChange", date);
    }
}
