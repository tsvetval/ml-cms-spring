package ru.ml.core.replication.service;

import org.apache.commons.compress.archivers.ArchiveException;
import ru.ml.core.replication.dto.Replication;
import ru.ml.core.replication.enums.ImportReplicationOption;
import ru.ml.core.replication.jaxb.stub.ReplicationBlock;
import ru.ml.core.replication.model.MlReplication;
import ru.ml.platform.core.model.exceptions.MlApplicationException;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: d_litovchenko
 * Date: 04.07.14
 * Time: 17:24
 * To change this template use File | Settings | File Templates.
 */
public interface MlReplicationService {

    byte[] doReplication(MlReplication mlReplication) throws JAXBException, DatatypeConfigurationException;

    ReplicationBlock getReplicationInfo(byte[] file) throws JAXBException;

    Replication importReplication(byte[] zipFile, Boolean onlyGenerateResultFile, ImportReplicationOption option) throws IOException, JAXBException, MlApplicationException, ArchiveException;
}
