package ru.ml.core.replication.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.ml.core.replication.enums.ReplicationType;
import ru.ml.platform.core.model.MlDynamicEntityImpl;

import java.util.Date;

/**
 *
 */
public abstract class MlReplicationStepAbstract extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    public Long getId() {
        return get("id");
    }

    public abstract ReplicationType getReplicationType();

    public Long getStepNumber() {
        return get("stepNumber");
    }

    public String getName() {
        return get("name");
    }

    public void setName(String name) {
        set("name", name);
    }

    public MlReplication getMlReplication() {
        return get("replication");
    }

    public Boolean getOffHandlers() {
        return get("offHandlers");
    }

    public void setOffHandlers(Boolean offHandlers) {
        set("offHandlers", offHandlers);
    }

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String toString() {
        return (new StringBuffer()).append("MlReplicationStep(Id = ").append(getId()).append(", name = ").append(getName()).
                append(", offHandlers = ").append(getOffHandlers()).
                append(", stepNumber = ").append(getStepNumber()).toString();
    }

    public void setReplication(MlReplication replication) {
        set("replication", replication);
    }

    public void setStepNumber(Long stepNumber) {
        set("stepNumber", stepNumber);
    }

    public void setReinitialization(boolean reinitialization) {
        set("reinitialization", reinitialization);
    }

    public String getGuid() {
        return get("guid");
    }

    public void setGuid(String value) {
        set("guid", value);
    }

    public Date getLastChange() {
        return get("lastChange");
    }

    public void setLastChange(Date date) {
        set("lastChange", date);
    }
}
