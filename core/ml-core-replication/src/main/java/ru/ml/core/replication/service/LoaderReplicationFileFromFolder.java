package ru.ml.core.replication.service;

import org.apache.commons.compress.archivers.ArchiveException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import ru.ml.core.replication.dto.Replication;
import ru.ml.core.replication.enums.ImportReplicationOption;
import ru.ml.core.replication.model.MlImportReplicationFile;
import ru.ml.platform.context.CommonDao;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by admin on 02.03.2016.
 */
public class LoaderReplicationFileFromFolder {
    @Autowired
    EntityManager entityManager;
    @Autowired
    MlReplicationService replicationService;
    @Autowired
    CommonDao commonDao;


    private static final Logger log = LoggerFactory.getLogger(LoaderReplicationFileFromFolder.class);

    private static final String SHOW_RESULT = "blocks/utils/replication/import/showResultImport.hml";

    public void findReplicationFile(File directory) throws IOException, JAXBException, ArchiveException {
        File[] files = directory.listFiles();
        List<File> orderedFiles = orderFiles(files);
        for (File zipFile : orderedFiles) {
            if (zipFile.isDirectory()) {
                findReplicationFile(zipFile);
            } else {
                if (zipFile.getName().endsWith("zip")) {

                    /*UnitOfWork unitOfWork = GuiceConfigSingleton.inject(UnitOfWork.class);
                    try {
                        unitOfWork.end();
                    } catch (Exception e) {
                        log.warn("", e);
                    }
                    unitOfWork.begin();*/
                    //TemplateEngine templateEngine = GuiceConfigSingleton.inject(TemplateEngine.class);

                    List<MlImportReplicationFile> replicationFile = commonDao.getQueryWithoutSecurityCheck("select o from MlImportReplicationFile o where o.replicationFile_filename = :fname")
                            .setParameter("fname", zipFile.getName())
                            .getResultList();
                    if (replicationFile.isEmpty()) {
                        entityManager.getTransaction().begin();
                        byte[] data = Files.readAllBytes(Paths.get(zipFile.toURI()));

                        MlImportReplicationFile mlImportReplication = (MlImportReplicationFile) commonDao.createNewEntity(MlImportReplicationFile.class);
                        mlImportReplication.setDateLoad(new Date());
                        mlImportReplication.setReplicationFile(data);
                        mlImportReplication.setReplicationFileName(zipFile.getName());
                        commonDao.persistWithoutSecurityCheck(mlImportReplication);
                        log.debug(String.format("Start replicate file %s", zipFile.getName()));
                        ImportReplicationOption.ImportType importType = ImportReplicationOption.ImportType.CREATE_UPDATE;
                        Replication replication = replicationService.importReplication(data, false, new ImportReplicationOption(importType));
                        HashMap<String, Object> dataMap = new HashMap<>();
                        dataMap.put("block", replication);
                        String html = "FIX IT";//templateEngine.renderTemplate(SHOW_RESULT, dataMap);

                        mlImportReplication.setName(replication.getName());
                        mlImportReplication.setReplication(replication.getMlReplication());
                        mlImportReplication.setReportFile(html.getBytes());
                        mlImportReplication.setReportFileName("report_" + zipFile.getName().replace(".zip", ".html"));
                        commonDao.mergeWithoutSecurityCheck(mlImportReplication);
                        log.debug("Replication success. Created row in MlImportReplicationFile id = " + mlImportReplication.getId());

                        entityManager.flush();
                        entityManager.getTransaction().commit();
                    }
                    //unitOfWork.end();
                }
            }
        }
    }

    private List<File> orderFiles(File[] files) {
        List<File> result = Arrays.asList(files);
        result.sort(new Comparator<File>() {
            @Override
            public int compare(File o1, File o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        return result;
    }
}
