package ru.ml.platform.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;
import ru.ml.platform.context.MlContextConfiguration;
import ru.ml.platform.core.MlCoreConfiguration;
import ru.ml.platform.ddl.creator.DdlConfiguration;
import ru.ml.ui.UIConfiguration;

@SpringBootApplication
@Import({
        MlCoreConfiguration.class,
        DdlConfiguration.class,
        UIConfiguration.class,
        MlContextConfiguration.class
})
public class MLApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(MLApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(MLApplication.class, args);
    }

}
