package ru.ml.platform.server;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.ml.platform.context.CommonDao;
import ru.ml.platform.context.MlContextConfiguration;
import ru.ml.platform.context.holders.impl.EnumHolderImpl;
import ru.ml.platform.context.holders.impl.HandlerHolderImpl;
import ru.ml.platform.context.holders.impl.MetaDataHolderImpl;
import ru.ml.platform.core.init.MlMetaDataInitializeService;
import ru.ml.platform.core.model.context.MlContext;
import ru.ml.platform.core.model.context.handler.MlClassHandler;
import ru.ml.platform.core.model.context.holders.EnumHolder;
import ru.ml.platform.core.model.context.holders.HandlerHolder;
import ru.ml.platform.core.model.context.holders.MetaDataHolder;
import ru.ml.platform.core.model.security.MlRole;

import java.util.List;

@Component
@Slf4j
@AllArgsConstructor
public class MlBootstrapInitalizer implements InitializingBean {

    private final ApplicationContext applicationContext;
    private final CommonDao commonDao;


    @Override
    public void afterPropertiesSet() throws Exception {
        MlContextConfiguration.initEmptyContext();
        MlMetaDataInitializeService mlMetaDataInitializeService = applicationContext.getBean(MlMetaDataInitializeService.class);

        log.debug("Start MlBootstrapListener, Try to initialize MetaData");
        mlMetaDataInitializeService.initializeAllMetaData();
        log.debug("Start MlBootstrapListener started. Meta Data ready to work.");

        MlContext.getInstance().getHandlerHolder().init(applicationContext.getBeansOfType(MlClassHandler.class).values());

        log.debug("Start Security init.");
        List<MlRole> mlRoles = commonDao.getTypedQueryWithoutSecurityCheck("select o from MlRole o", MlRole.class).getResultList();
        MlContext.getInstance().getSecurityHolder().init(mlRoles);
    }




}
