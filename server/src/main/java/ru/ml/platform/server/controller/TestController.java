package ru.ml.platform.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.ml.platform.context.CommonDao;
import ru.ml.platform.core.model.MlFolder;

import java.util.List;

@RequestMapping("/test")
@RestController
public class TestController {
    @Autowired
    private CommonDao commonDao;

    @GetMapping
    public List<MlFolder> getFolders(){
        return commonDao.getTypedQueryWithoutSecurityCheck("select o from MlFolder o", MlFolder.class).getResultList();
    }

}
